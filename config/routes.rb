Rails.application.routes.draw do
  mount PgHero::Engine, at: "pghero"
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: "management_module/settings/employee_registrations" }
  devise_for :member_accounts, controllers: { sessions: 'member_accounts/sessions', registrations: "member_accounts/registrations" }

  authenticated :user do
    root :to => 'accounting_module/accounts#index', :constraints => lambda { |request| request.env['warden'].user.role == 'accountant' if request.env['warden'].user }, as: :accounting_module_root
    root :to => 'accounting_module/reports/ticket_entries#index', :constraints => lambda { |request| request.env['warden'].user.role == 'bookkeeper' if request.env['warden'].user }, as: :bookkeeper_root

    root :to => 'loans#index', :constraints => lambda { |request| request.env['warden'].user.role == 'loan_officer' if request.env['warden'].user }, as: :loan_officer_root
    root :to => 'treasury_module/cash_accounts#index', :constraints => lambda { |request| request.env['warden'].user.role == 'teller' if request.env['warden'].user }, as: :teller_root
    root :to => 'members#index', :constraints => lambda { |request| request.env['warden'].user.role == 'general_manager' if request.env['warden'].user }, as: :general_manager_root
    root :to => 'members#index', :constraints => lambda { |request| request.env['warden'].user.role == 'branch_manager' if request.env['warden'].user }, as: :branch_manager_root
    root :to => 'members#index', :constraints => lambda { |request| request.env['warden'].user.role == 'treasurer' if request.env['warden'].user }, as: :treasurer_root

    root :to => 'store_front_module/line_items/sales_line_items#new', :constraints => lambda { |request| request.env['warden'].user.role == 'sales_clerk' if request.env['warden'].user }, as: :sales_clerk_root
  end
  resources :home, only: [:index]

  resources :accounting_module, only: [:index]
  namespace :accounting_module do
    resources :income_statement_dashboards, only: [:index],module: :dashboards
    resources :assets_dashboards, only: [:index],module: :dashboards

    resources :grand_parent_account_categories, only: [:index, :new, :create]
    resources :general_ledgers,    only: [:index]
    resources :subsidiary_ledgers, only: [:index]

    resources :account_categories, only: [:index, :new, :create, :edit, :update], module: :settings do
      resources :account_sub_categories, only: [:new, :create]
    end

    resources :account_categories, only: [:show] do
      resources :entries,        only: [:index], module: :account_categories
      resources :sub_categories, only: [:index], module: :account_categories
      resources :settings,       only: [:index], module: :account_categories
      resources :accounts,       only: [:index], module: :account_categories
    end

    resources :settings, only: [:index]
    resources :amounts, only: [:destroy]
    resources :commercial_document_adjusting_entry_line_items, only: [:new, :create, :destroy], module: :entries
    resources :per_employee_entries, only: [:index], module: :entries
    resources :per_office_entries, only: [:index], module: :entries
    resources :scheduled_entries, only: [:index]
    resources :interest_expense_vouchers, only: [:show]
    namespace :scheduled_entries do
      resources :savings_accounts_interest_postings, only: [:index, :new, :create]
      resources :share_capital_dividend_postings, only: [:new, :create]
    end

    namespace :cash_books do
      resources :cash_receipts,                       only: [:index]
      resources :cash_disbursements,                  only: [:index]
    end

    resources :cash_receipts,           module: :entries,     only: [:index]
    resources :cash_disbursements,      module: :entries, only: [:index]
    resources :journal_entry_vouchers,  module: :entries, only: [:index]

    resources :entry_line_item_vouchers, only: [:create, :show, :destroy], module: :entries do
      resources :confirmations, only: [:create], module: :entry_line_item_vouchers
    end

    resources :interests_earned_postings, only: [:create]
    resources :entry_line_items, only: [:new, :create, :destroy]
    resources :entry_processings, only: [:create]
    resources :financial_condition_comparisons, only: [:new, :create, :show]
    resources :reports, only: [:index]

    namespace :reports do
      resources :equity_changes_statements, only: [:index]
      resources :cash_receipts, only: [:index]
      resources :cash_disbursements, only: [:index]
      resources :journal_entries, only: [:index]
      resources :entries, only: [:index]


      namespace :cashbooks do
        resources :cash_receipts,      only: [:index]
        resources :cash_disbursements, only: [:index]
      end
      resources :entries, only: [:index]
      resources :net_income_distributions, only: [:index]
      resources :trial_balances,           only: [:index]
      resources :ticket_entries,           only: [:index]

      resources :proofsheets,              only: [:index]
      resources :income_statements,        only: [:index]
      resources :balance_sheets,           only: [:index]
      resources :financial_conditions,     only: [:index]
      resources :cash_books,               only: [:index]
    end

    resources :settings, only: [:index]

    resources :accounts, except: [:destroy] do
      resources :account_categories,  only: [:new, :create], module: :accounts

      resources :reports,             only: [:index], module: :accounts
      resources :settings,            only: [:index], module: :accounts
      resources :entries,             only: [:index], module: :accounts
    end

    resources :assets, controller: 'accounts', type: 'AccountingModule::Asset'

    resources :entries do
      resources :cancellations, only: [:new, :create], module: :entries
    end

    resources :debit_amounts, only: [:edit, :update]
    resources :credit_amounts, only: [:edit, :update]

  end

  resources :loans_module, only: [:index]

  namespace :loans_module do
    resources :loan_groups, only: [:new, :create], module: :settings
    resources :loan_application_product_selections, only: [:new]
    resources :interest_calculation_selections, only: [:new]
    resources :amortization_schedules, only: [:index, :show]
    resources :amortization_schedule_date_filters, only: [:index]

    resources :organizations, only: [:index, :show] do
      resources :loans, only: [:index], module: :organizations
      resources :reports, only: [:index], module: :organizations
    end

    resources :archived_loans, only: [:index]
    resources :disbursement_vouchers, only: [:create]

    namespace :monitoring do
      resources :metrics, only: [:index]
    end

    resources :search_results, only: [:index]

    namespace :reports do
      resources :loan_agings,                    only: [:index]
      resources :loan_agings_summaries,          only: [:index]
      resources :loan_agings_by_loan_products,   only: [:index]
      resources :loan_agings_by_barangays,       only: [:index]
      resources :loan_maturities,                only: [:index]
      resources :loan_releases,                  only: [:index]
      resources :loan_releases_by_loan_products, only: [:index]
      resources :loan_releases_by_barangays,     only: [:index]
      resources :loan_releases_summaries,        only: [:index]



      resources :loan_collections, only: [:index]
      resources :loan_collections_summaries, only: [:index]

      resources :aging_loans, only: [:index]
    end

    resources :member_borrowers, only: [:index, :show]
    resources :reports, only: [:index]
    resources :aging_loans, only: [:index, :show]

    resources :loan_applications, only: [:index, :new, :create, :show, :destroy] do
      resources :guarantors,             only: [:new, :create], module: :loan_applications
      resources :share_capital_openings, only: [:new, :create], module: :loan_applications
      resources :savings_account_openings, only: [:new, :create], module: :loan_applications

      resources :disbursements,                       only: [:new, :create],                           module: :loan_applications
      resources :capital_build_up_processings,        only: [:new, :create],                           module: :loan_applications
      resources :savings_account_deposit_processings, only: [:new, :create],                           module: :loan_applications
      resources :previous_loan_payment_processings,   only: [:new, :create],                           module: :loan_applications
      resources :vouchers,                            only: [:new, :create, :show, :destroy],          module: :loan_applications do
        resources :disbursements, only: [:new, :create]
      end
      resources :amortization_schedules,              only: [:index],                                  module: :loan_applications
      resources :voucher_confirmations,               only: [:create],                                 module: :loan_applications
      resources :loan_amounts,                        only: [:new, :create],                           module: :loan_applications
      resources :voucher_amounts,                     only: [:new, :create, :edit, :update, :destroy], module: :loan_applications
      resources :voucher_credit_amounts,                     only: [:new, :create, :edit, :update, :destroy], module: :loan_applications

      resources :program_subscription_payments,       only: [:new, :create], module: :loan_applications
      resources :program_subscriptions,       only: [:new, :create], module: :loan_applications
    end

    resources :dashboard, only: [:index]

    resources :loan_products, only:[:index, :show] do
      resources :insights, only: [:index], module: :loan_products
      resources :loans, only: [:index], module: :loan_products
      resources :reports, only: [:index], module: :loan_products
    end

    resources :loans, only: [:index, :show] do
      resources :auto_pay_setups, only: [:new, :create], module: :loans
      resources :restructure_vouchers, only: [:new, :create], module: :loans
      resources :loss_vouchers, only: [:show], module: :loans
      resources :interests, only: [:index], module: :loans
      resources :penalties, only: [:index], module: :loans

      resources :co_makers,                 only: [:new, :create], module: :loans
      resources :past_due_vouchers,              only: [:new, :create, :show, :destroy], module: :loans
      resources :past_due_voucher_confirmations, only: [:create],                        module: :loans
      resources :organizations,                  only: [:edit, :update],                 module: :loans
      resources :loan_penalty_discounts,         only: [:new, :create],                  module: :loans
      resources :loan_interest_discounts,        only: [:new, :create],                  module: :loans
      resources :archivings,                     only: [:create],                        module: :loans

      resources :terms,                         only: [:new, :create], module: :loans
      resources :interest_postings,             only: [:new, :create], module: :loans
      resources :penalty_postings,              only: [:new, :create], module: :loans
      resources :interest_rebate_postings,      only: [:new, :create], module: :loans
      resources :amortization_schedules,        only: [:index],        module: :loans

      resources :payments,                      only: [:new, :create], module: :loans
      resources :losses,                        only: [:new, :create], module: :loans
      resources :tracking_numbers,              only: [:edit, :update], module: :loans
    end
  end
  resources :share_capitals, only: [:index, :show] do
    resources :adjusting_entries,             only: [:index], module: :share_capitals
    resources :loan_payments,                 only: [:new, :create], module: [:share_capitals, :adjusting_entries]
    resources :adjusting_entry_voucher_processings,           only: [:new, :create], module: [:share_capitals, :adjusting_entries]
    resources :adjusting_entry_voucher_confirmations, only: [:show, :create, :destroy], module: [:share_capitals, :adjusting_entries]
    resources :program_subscription_payments, only: [:new, :create], module: [:share_capitals, :adjusting_entries]
    resources :balance_transfer_destination_accounts, only: [:new, :create], module: :share_capitals

    resources :vouchers, only: [:show, :destroy], module: :share_capitals do
      resources :confirmations, only: [:create]
    end

    resources :balance_transfer_vouchers, only: [:show, :destroy], module: :share_capitals do
      resources :confirmations, only: [:create]
    end

    resources :barangays, only: [:edit, :update], module: :share_capitals
    resources :settings, only: [:index],          module: :share_capitals
    resources :mergings, only: [:create],         module: :share_capitals
    resources :merging_line_items, only: [:new, :create,  :destroy], module: :share_capitals
    resources :capital_build_ups, only: [:new, :create],  module: :share_capitals
    resources :account_closings, only: [:new, :create],   module: :share_capitals
    resources :offices, only: [:edit, :update],           module: :share_capitals
    resources :balance_transfers, only: [:new, :create],  module: :share_capitals
  end

  resources :members, only: [:index, :show, :edit, :update, :destroy] do
    resources :activities,            only: [:index], module: :members
    resources :infos,                 only: [:new, :create], module: :members
    resources :credit_scores,         only: [:index], module: :members
    resources :bills_payments,        only: [:index], module: :members
    resources :organizations,         only: [:new, :create],                 module: :members
    resources :beneficiaries,         only: [:new, :create, :destroy],       module: :members
    resources :merging_line_items,    only: [:new, :create, :show],                 module: :members
    resources :mergings,              only: [:create],                       module: :members
    resources :contacts,              only: [:new, :create],                 module: :members
    resources :tins,                  only: [:new, :create],                 module: :members
    resources :time_deposits,         only: [:index, :new, :create],         module: :members
    resources :tins,                  only: [:new, :create],                 module: :members
    resources :offices,               only: [:edit, :update],                module: :members
    resources :addresses,             only: [:new, :create],                 module: :members
    resources :memberships,           only: [:edit, :update, :new, :create], module: :members
    resources :info,                  only: [:index],                        module: :members
    resources :settings,              only: [:index],                        module: :members
    resources :loans,                 only: [:index],                        module: :members
    resources :share_capitals,        only: [:index],                        module: :members
    resources :occupations,           only: [:new, :create],                 module: :members
    resources :savings_accounts,      only: [:index],                        module: :members
    resources :subscriptions,         only: [:index],                        module: :members
    resources :program_subscriptions, only: [:create],                       module: :members
    resources :sales,                 only: [:index, :show],                 module: :members
    resources :credit_sales_line_items, only: [:new, :create, :destroy],     module: :members
    resources :account_mergings,      only: [:new, :create],                 module: :members
    resources :signature_specimens,   only: [:create],                       module: :members
    resources :avatars,               only: [:update],                       module: :members
    resources :retirements,           only: [:edit, :update],                module: :members
  end

  resources :member_registrations, only: [:new, :create]
  resources :organization_registries, only: [:new, :create], module: [:registries]
  resources :savings_account_registries, only: [:create],    module: [:registries]
  resources :share_capital_registries, only: [:create],      module: [:registries]
  resources :loan_registries, only: [:create],               module: [:registries]
  resources :time_deposit_registries, only: [:create],       module: [:registries]
  resources :member_registries, only: [:create],             module: [:registries]
  resources :program_subscription_registries, only: [:create],       module: [:registries]


  namespace :management_module do
    resources :account_budgets, only: [:index, :new, :create]
    resources :loan_protection_plan_providers, only: [:new, :create], module: :configurations

    namespace :settings do
      resources :general, only: [:index]
      resources :users, only: [:index]
      resources :employee_registrations, only: [:new, :create]
      resources :offices, only: [:index, :new, :create]
      resources :time_deposit_products, only: [:index]
      resources :store_fronts, only: [:index]
      resources :cooperative_services, only: [:index]
      resources :account_budgets, only: [:index]
      resources :cooperative_products, only: [:index]
      resources :configurations, only: [:index]
      resources :data_migrations, only: [:index]
      resources :barangays,         only: [:index, :new, :create]
      resources :loan_products,     only: [:index, :new, :create, :show, :edit, :update] do
        resources :charges, only: [:new, :create], module: :loan_products
      end
      resources :share_capital_products,     only: [:index, :new, :create]
      resources :time_deposit_products, only: [:index, :new, :create]
      resources :saving_products, only: [:index, :new, :create]
      resources :programs, only: [:index, :new, :create]



      resources :store_fronts, only: [:new, :create]
      resources :net_income_distributions, only: [:new, :create]

      resources :cooperatives, only: [:edit, :update, :show] do
      end

      resources :time_deposit_products, only: [:new, :create, :show, :edit, :update]
    end

    resources :employees, only: [:index, :show]
    resources :employees, only: [:new, :create], module: :settings
    resources :settings, only: [:index]
  end

  resources :teller_module, only: [:index]
  resources :users, only: [:show]

  namespace :store_front_module do
    namespace :line_item_processings do
      resources :sales_line_items, only: [:create]
    end
    namespace :order_processings do
      resources :sales_orders, only: [:create]
    end
    resources :store, only: [:index]
    resources :stock_registry_processings, only: [:create]
    resources :stock_registries, only: [:create, :show], module: :settings
    resources :suppliers, only: [:index, :show, :new, :create] do
      resources :vouchers, only: [:index, :show, :create, :destroy], module: :suppliers
      resources :voucher_confirmations, only: [:create], module: :suppliers
      resources :purchase_deliveries, only: [:index, :new, :create], module: :suppliers
      resources :purchase_returns, only: [:index, :new, :create], module: :suppliers
      resources :voucher_amounts, only: [:new, :create, :destroy], module: :suppliers
      resources :purchase_line_items, only: [:new, :create], module: :suppliers
      resources :product_selections, only: [:new, :create], module: :suppliers
      resources :purchase_order_processings, only: [:create], module: :suppliers

    end

    resources :inventories, only: [:index, :show] do
      resources :sales,            only: [:index], module: :inventories
      resources :sales_returns,    only: [:index], module: :inventories
      resources :purchase_returns, only: [:index], module: :inventories
      resources :spoilages,        only: [:index], module: :inventories
      resources :settings,         only: [:index], module: :inventories
      resources :internal_uses,    only: [:index], module: :inventories
      resources :stock_transfers,  only: [:index], module: :inventories
    end

    resources :purchase_orders,          only: [:index, :show, :create], module: :orders
    resources :sales,              only: [:index, :show, :create], module: :orders
    resources :credit_sales,       only: [:create],                module: :orders
    resources :sales_returns,      only: [:index, :show, :create], module: :orders
    resources :purchase_returns,   only: [:index, :create],        module: :orders
    resources :spoilages,          only: [:index, :create],        module: :orders
    resources :stock_transfers,    only: [:index],                 module: :orders

    resources :purchase_line_items,                only: [:new, :create, :destroy], module: :line_items
    resources :sales_line_items,                   only: [:new, :create, :destroy], module: :line_items
    resources :sales_return_line_items,            only: [:new, :create, :destroy], module: :line_items
    resources :purchase_return_line_items,         only: [:new, :create, :destroy], module: :line_items
    resources :credit_sales_line_items,            only: [:new, :create, :destroy], module: :line_items
    resources :spoilage_line_items,                only: [:new, :create, :destroy], module: :line_items
    resources :stock_transfer_line_items,          only: [:new, :create, :destroy], module: :line_items
    resources :internal_use_line_items,            only: [:new, :create, :destroy], module: :line_items
    resources :received_stock_transfer_line_items, only: [:new, :create, :destroy], module: :line_items

    resources :sales_reports,             only: [:index], module: :reports
    resources :sales_clerk_reports,       only: [:index], module: :reports
    resources :sales_clerk_sales_reports, only: [:index], module: :reports
    resources :purchases_reports,         only: [:index], module: :reports
    resources :spoilages_reports,         only: [:index], module: :reports

    resources :employees,      only: [:show]
    resources :settings,       only: [:index]
    resources :reports,         only: [:index]
    resources :search_results, only: [:index]

    resources :products, only: [:index, :show, :new, :create] do
      resources :purchases,                only: [:index, :new, :create], module: :products
      resources :sales,                    only: [:index],                module: :products
      resources :purchase_returns,         only: [:index],                module: :products
      resources :sales_returns,            only: [:index],                module: :products
      resources :settings,                 only: [:index],                module: :products
      resources :spoilages,                only: [:index],                module: :products
      resources :internal_uses,            only: [:index],                module: :products
      resources :stock_transfers,          only: [:index],                module: :products
      resources :received_stock_transfers, only: [:index],                module: :products
      resources :unit_of_measurements, only: [:new, :create]
    end

    resources :unit_of_measurements, only: [:show] do
      resources :mark_up_prices, only: [:new, :create]
    end

    resources :customers, only: [:index, :show] do
      resources :sales_orders, only: [:index], module: :customers
    end
  end

  resources :schedules, only: [:index, :show]
  resources :treasury_module, only: [:index]

  namespace :treasury_module do
    resources :daily_transactions, only: [:index]
    resources :loan_payments, only: [:index]
    namespace :summaries do
      resources :cash_books, only: [:index]
      resources :loans, only: [:index]
    end
    resources :bank_accounts, only: [:index, :show]
    resources :cash_receipts, only: [:index], module: :cash_receipts_reports
    resources :cash_receipts_by_cash_accounts, only: [:index, :show], module: :cash_receipts_reports
    resources :cash_receipts_by_dates, only: [:index, :show], module: :cash_receipts_reports
    resources :cash_receipts_by_employees, only: [:index, :show], module: :cash_receipts_reports


    resources :cash_disbursements, only: [:index]

    resources :cash_accounts, only: [:index, :show] do
      resources :receipts,      only: [:index], module: :cash_accounts
      resources :disbursements, only: [:index], module: :cash_accounts
      resources :cash_receipt_line_items, only: [:new, :create, :destroy],      module: :cash_accounts
      resources :cash_disbursement_line_items, only: [:new, :create, :destroy], module: :cash_accounts
      resources :cash_disbursement_vouchers, only: [:show],                     module: :cash_accounts
    end

    resources :disbursements, only: [:index]
    resources :cash_receipts, only: [:index]
    resources :cash_disbursement_voucher_processings, only: [:create]
    resources :cash_receipt_voucher_processings, only: [:create]

    resources :cash_disbursement_vouchers, only: [:show, :destroy] do
      resources :confirmations, only: [:create], module: :cash_disbursement_vouchers
    end

    resources :cash_receipt_vouchers, only: [:show, :destroy] do
      resources :confirmations, only: [:create], module: :cash_receipt_vouchers
    end
  end

  resources :savings_accounts_dashboards, only: [:index]
  resources :share_capitals_dashboards, only: [:index]
  resources :loans_dashboards, only: [:index]
  resources :members_dashboards, only: [:index]



  resources :savings_accounts_below_minimum_balances, only: [:index]
  resources :share_capitals_below_minimum_balances, only: [:index]

  resources :savings_accounts, only: [:index, :show] do
    resources :lock_in_periods, only: [:new, :create],              module: :savings_accounts
    resources :depositors,   only: [:index], module: :savings_accounts
    resources :balance_transfer_destination_accounts, only: [:new, :create], module: :savings_accounts
    resources :balance_transfer_vouchers, only: [:show],            module: :savings_accounts
    resources :deposit_vouchers, only: [:show, :destroy],           module: :savings_accounts
    resources :withdrawal_vouchers, only: [:show, :destroy],        module: :savings_accounts
    resources :account_closing_vouchers, only: [:show, :destroy],   module: :savings_accounts
    resources :balance_transfers, only: [:new, :create],            module: :savings_accounts
    resources :settings,          only: [:index],                   module: :savings_accounts
    resources :deposits,          only: [:new, :create],            module: :savings_accounts
    resources :withdrawals,       only: [:new, :create],            module: :savings_accounts
    resources :account_closings,  only: [:new, :create],            module: :savings_accounts
    resources :barangay_settings,  only: [:edit, :update],          module: :savings_accounts
    resources :voucher_confirmations, only: [:create],              module: :savings_accounts
    resources :closing_voucher_confirmations, only: [:create],      module: :savings_accounts
    resources :adjusting_entries, only: [:index],                   module: :savings_accounts
    resources :loan_payments, only: [:new, :create],                module: [:savings_accounts, :adjusting_entries]
    resources :program_subscription_payments, only: [:new, :create],module: [:savings_accounts, :adjusting_entries]
    resources :share_capital_transfers, only: [:new, :create],      module: [:savings_accounts, :adjusting_entries]
    resources :voucher_processings, only: [:create],                module: [:savings_accounts, :adjusting_entries]
    resources :adjusting_entry_voucher_confirmations, only: [:show, :create, :destroy], module: [:savings_accounts, :adjusting_entries]
    resources :loan_payment_line_items, only: [:new, :create],                module: [:savings_accounts, :adjusting_entries]
    resources :adjusting_entry_voucher_amounts, only: [:destroy], module: [:savings_accounts, :adjusting_entries]
    resources :interest_expenses, only: [:new, :create], module: [:savings_accounts, :settings]
    resources :interest_expense_vouchers, only: [:show], module: [:savings_accounts, :settings]

  end

  resources :search_results, only: [:index, :show]

  resources :time_deposits, only: [:index, :show] do
    resources :transfers, only: [:new, :create],           module: :time_deposits
    resources :transfer_vouchers, only: [:show, :destroy], module: :time_deposits do
      resources :confirmations, only: [:create],           module: :transfer_vouchers
    end

    resources :withdrawal_vouchers, only: [:show, :destroy], module: :time_deposits do
      resources :confirmations, only: [:create],             module: :withdrawal_vouchers
    end
    resources :withdrawals, only: [:new, :create],       module: :time_deposits
    resources :term_extensions, only: [:new, :create],   module: :time_deposits
    resources :break_contracts, only: [:new, :create],   module: :time_deposits
    resources :settings, only: [:index],                 module: :time_deposits
    resources :beneficiaries, only: [:edit, :update],    module: :time_deposits
    resources :transactions, only: [:index],             module: :time_deposits
    resources :interest_expenses,                      only: [:new, :create], module: :time_deposits
    resources :interest_expense_vouchers,              only: [:show], module: :time_deposits
    resources :interest_expense_voucher_confirmations, only: [:create], module: :time_deposits
  end

  resources :employees, only: [:index, :show, :edit, :update] do
    resources :cash_count_line_items,only: [:new, :create],                module: :employees
    resources :settings,             only: [:index],                        module: :employees
    resources :cash_accounts,        only: [:new, :create, :destroy],       module: :employees
    resources :store_fronts,         only: [:edit, :update],                module: [:employees, :settings]
    resources :info,                 only: [:index],                        module: :employees
    resources :blotters,             only: [:index],                        module: :employees
    resources :cash_disbursements,   only: [:index],                        module: [:employees, :reports]
    resources :cash_receipts,        only: [:index],                        module: [:employees, :reports]
    resources :cash_book_transactions, only: [:index],                      module: [:employees, :reports]
    resources :memberships,          only: [:new, :create, :edit, :update], module: :employees
    resources :time_deposits,        only: [:index],                        module: :employees
    resources :savings_accounts,     only: [:index],                        module: :employees
    resources :share_capitals,       only: [:index],                        module: :employees
    resources :entries,              only: [:index, :show],                 module: :employees
    resources :remittances,          only: [:new, :create],                 module: :employees
    resources :cash_transfers,       only: [:new, :create],                 module: :employees
    resources :vault_fund_transfers, only: [:new, :create],                 module: :employees
    resources :reports,              only: [:index],                        module: :employees
    resources :vouchers,             only: [:index, :new, :create],         module: :employees
    resources :amounts,              only: [:new, :create, :destroy],       module: :employees
    resources :orders,               only: [:index],                        module: :employees
    resources :loans,                only: [:index],                        module: :employees
    resources :avatars,              only: [:update],                       module: :employees
    resources :accounts,            only: [:edit, :update],                 module: :employees
  end

  resources :loans, only: [:index, :show] do
    resources :payment_vouchers,     only: [:new, :create, :show],                  module: :loans do
      resources :confirmations,      only: [:create],                module: :payment_vouchers
    end
    resources :notes,                 only: [:index, :new, :create], module: :loans
    resources :barangays,             only: [:edit, :update],        module: :loans
    resources :settings,              only: [:index],                module: :loans
    resources :payments,              only: [:index, :new, :create], module: :loans

  end

  namespace :loans_module do
    resources :disbursements, only: [:index]
    resources :collections,   only: [:index]
    resources :settings,     only: [:index]
    namespace :settings do
      resources :archives,           only: [:new, :create]
    end
  end

  resources :vouchers, only: [:index, :show, :destroy] do
    resources :cancellations, only: [:create], module: :vouchers
    resources :disbursements, only: [:create],            module: :vouchers
    resources :loan_disbursements, only: [:new, :create], module: :vouchers
  end

  resources :bank_accounts, only: [:index, :show, :new, :create] do
    resources :vouchers, only: [:show, :destroy],            module: :bank_accounts
    resources :voucher_confirmations, only: [:create],       module: :bank_accounts
    resources :deposits, only: [:new, :create],              module: :bank_accounts
    resources :withdrawals, only: [:new, :create],           module: :bank_accounts
    resources :settings, only: [:index],                     module: :bank_accounts
  end

  resources :calendars, only: [:index, :show]

  resources :organizations, only: [:index, :show, :new, :create, :edit, :update] do
    resources :members, only: [:index, :new, :create],   module: :organizations
    resources :loans, only: [:index],            module: :organizations
    resources :reports, only: [:index],          module: :organizations
    resources :share_capitals, only: [:index],   module: :organizations
    resources :settings, only: [:index],         module: :organizations
    resources :savings_accounts, only: [:index], module: :organizations
    resources :loan_payment_line_items, only: [:index, :new, :create], module: :organizations
    resources :voucher_amounts, only: [:destroy], module: :organizations
  end

  resources :membership_applications, only: [:new, :create]

  resources :cooperatives, only: [:show] do
    resources :barangays, only: [:new, :create, :edit, :update], module: :cooperatives
    resources :logos, only: [:create],           module: :cooperatives
  end

  resources :monitoring, only: [:index]
  namespace :monitoring do
    resources :share_capitals, only: [:index]
    resources :savings_products, only: [:index]
    resources :loan_products, only: [:index]
    resources :store_fronts, only: [:index]
  end

  resources :filtered_loans, only: [:index], module: :loans_module
  resources :matured_loans, only: [:index],  module: :loans_module

  resources :barangays, only: [:index, :show, :new, :create, :edit, :update] do
    resources :loans, only: [:index],                  module: :barangays
    resources :savings, only: [:index],                module: :barangays
    resources :members, only: [:index, :new, :create], module: :barangays
    resources :settings, only: [:index],               module: :barangays
  end

  namespace :barangays do
    resources :imports, only: [:create]
  end

  resources :memberships, only: [:index, :show] do
    resources :share_capital_subscriptions, only: [:new, :create], module: :memberships
    resources :program_subscriptions, only: [:create],             module: :memberships do
      resources :payments, only: [:new, :create],                  module: :program_subscriptions
    end
  end

  namespace :cooperators do
    resources :accounts, only: [:show]
    resources :sign_ups, only: [:new]
  end

  resources :program_subscriptions, only: [:show] do
    resources :payments, only: [:new, :create], module: :program_subscriptions
    resources :vouchers, only: [:show], module: :program_subscriptions
    resources :voucher_confirmations, only: [:create], module: :program_subscriptions

  end

  resources :metrics, only: [:index]
  namespace :metrics do
    resources :savings_accounts, only: [:index, :create]
  end

  namespace :reports do
    resources :audit_reports, only: [:index]
  end

  resources :cooperative_services, only: [:index, :show] do
    resources :entries, only: [:index], module: :cooperative_services
    resources :balance_sheets,  only: [:index],        module: :cooperative_services
    resources :income_statements,  only: [:index],     module: :cooperative_services
    resources :settings,        only: [:index],        module: :cooperative_services
    resources :accounts,        only: [:index, :new, :create]
  end

  namespace :coop_module do
    resources :search_results, only: [:index]
  end

  resources :time_deposit_applications, only: [:new, :create] do
    resources :vouchers, only: [:show, :destroy],      module: :time_deposit_applications
    resources :voucher_confirmations, only: [:create], module: :time_deposit_applications
  end

  resources :savings_account_applications, only: [:new, :create] do
    resources :vouchers, only: [:show, :destroy],      module: :savings_account_applications
    resources :voucher_confirmations, only: [:create], module: :savings_account_applications
  end

  resources :share_capital_applications, only: [:new, :create] do
    resources :vouchers, only: [:show, :destroy],      module: :share_capital_applications
    resources :voucher_disbursements, only: [:create], module: :share_capital_applications
  end

  resources :portfolios, only: [:index]

  namespace :portfolios do
    resources :savings,        only: [:index]
    resources :share_capitals, only: [:index]
    resources :time_deposits,  only: [:index]
    resources :loans,          only: [:index]
  end

  authenticated :member_account do
    root to: 'member_accounts#show'
  end

  unauthenticated :user do
    root :to => 'home#index', :constraints => lambda { |request| request.env['warden'].user.nil? }, as: :unauthenticated_root
  end

  resources :bank_account_applications, only: [:new, :create]
  mount ActionCable.server => '/cable'
  resources :leads, only: [:new, :create]
  namespace :portfolios do
    resources :loans, only: [:index]
  end
  resources :merchants, only: [:index, :show, :new, :create] do
    resources :payment_line_items, only: [:new, :create], module: :merchants
  end

  resources :offices, only: [:index, :show] do

    resources :parent_account_categories,      only: [:index, :new, :create, :show], module: [:offices, :accounting_module] do
      resources :parent_account_sub_categories, only: [:index, :new, :create]
    end
    resources :parent_account_sub_categories, only: [:show], module: [:offices, :accounting_module] do
      resources :account_categories, only: [:index, :new, :create]
    end
    resources :saving_products,                only: [:new, :create, :index, :show, :edit, :update],         module: [:offices, :settings]
    resources :amortization_types,             only: [:new, :create],         module: [:offices, :settings]
    resources :programs,                       only: [:index, :show, :new, :create, :edit, :update],         module: [:offices, :settings]
    resources :loan_products,                  only: [:index, :new, :create, :show, :edit, :update],         module: [:offices, :settings] do
      resources :loan_product_charges,                      only: [:new, :create]
    end
    resources :loan_protection_plan_providers, only: [:new, :create],         module: [:offices, :settings]
    resources :share_capital_products,         only: [:index, :show, :new, :create, :edit, :update],         module: [:offices, :settings]
    resources :time_deposit_products,          only: [:new, :create],         module: [:offices, :settings]
    resources :loans,                          only: [:index],                module: :offices
    resources :savings_accounts,               only: [:index],                module: :offices
    resources :share_capitals,                 only: [:index],                module: :offices
    resources :time_deposits,                  only: [:index],                module: :offices
    resources :entries,                        only: [:index, :show],         module: :offices
    resources :reports,                        only: [:index],                module: :offices
    resources :accounts,                       only: [:index, :new, :create, :show], module: :offices
    resources :settings,                       only: [:index],                module: :offices
    resources :cooperative_services,           only: [:index, :new, :create], module: [:offices, :settings]
    resources :saving_groups,                  only: [:index, :new, :create], module: [:offices, :settings]
    resources :savings_migrations,             only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :share_capitals_migrations,      only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :members_migrations,             only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :organizations_migrations,       only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :bank_accounts_migrations,       only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :loans_migrations,               only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :time_deposits_migrations,               only: [:index, :new, :create], module: [:offices, :data_migrations]
    resources :asset_inventories,               only: [:index], module: :offices
    resources :account_categories,             only: [:index, :show, :edit, :update], module: :offices do
      resources :accounts, only: [:index, :new, :create], module: :account_categories
      resources :entries,  only: [:index], module: :account_categories
      resources :settings, only: [:index], module: :account_categories
    end
  end


  resources :credit_scores, only: [:index]
  resources :program_subscriptions, only: [:show]
  resources :identifications, except: [:destroy], module: :identification_module
  resources :insights, only: [:index]
  namespace :insights do
    resources :savings, only: [:index]
    namespace :members do
      resources :by_locations, only: [:index]
      resources :by_ages, only: [:index]
      resources :by_occupations, only: [:index]
      resources :by_civil_statuses, only: [:index]
      resources :by_sexes, only: [:index]
      resources :summaries, only: [:index]
    end
    namespace :programs do
      resources :summaries, only: [:index]
    end
  end

  resources :loan_products, only: [:show] do
    resources :loan_protection_plan_providers, only: [:edit, :update], module: :loan_products
  end

  resources :multiple_transactions, only: [:new, :create]
  resources :loan_multiple_payment_line_items, only: [:new, :create]
  resources :multiple_loan_payment_processings, only: [:create]
  resources :multiple_loan_payment_vouchers, only: [:show]
  resources :loans, only: [:show] do
    resources :multiple_payments, only: [:new, :create], module: :loans
  end
  resources :carts ,only: [:destroy]
  namespace :credit_records do
    resources :members, only: [:index, :show]
  end
  resources :below_minimum_balance_share_capitals, only: [:index]
  namespace :share_capitals_module do
    resources :insights, only: [:index]
  end
  resources :store_fronts, only: [:show] do
    resources :suppliers, only: [:index, :show, :new, :create], module: :store_fronts do
      resources :product_selections, only: [:new], module: :suppliers
      resources :purchases, only: [:new, :create], module: :suppliers
      resources :purchase_processings, only: [:create], module: :suppliers
    end
  end

  resources :reports, only: [:index]
  namespace :reports do
    resources :members_masterlists, only: [:index]
    resources :identifications,     only: [:index]
    namespace :members_masterlists do
      resources :by_genders, only: [:index]
      resources :by_civil_statuses, only: [:index]
      resources :by_age_ranges, only: [:index]
    end
  end
  resources :programs, only: [:index, :show] do
    resources :reports, only: [:index], module: :programs
    resources :unpaid_subscriptions, only: [:index], module: [:programs, :reports]

  end
  resources :saving_groups, only: [:show]
  resources :loan_groups, only: [:show]

  resources :savings_accounts_below_minimum_balances, only: [:index]
  resources :savings_accounts_with_minimum_balances, only: [:index]
  resources :share_capitals_below_minimum_balances, only: [:index]
  resources :share_capitals_with_minimum_balances, only: [:index]
  namespace :members_module do
    resources :reports, only: [:index]
    resources :members_listings,               only: [:index], module: :reports
    resources :members_by_sexes,               only: [:index], module: :reports
    resources :members_by_barangays,           only: [:index], module: :reports
    resources :members_by_date_of_births,      only: [:index], module: :reports
    resources :members_by_ages,                only: [:index], module: :reports
    resources :members_by_date_of_memberships, only: [:index], module: :reports
    resources :members_by_occupations,         only: [:index], module: :reports
    resources :members_by_organizations,       only: [:index], module: :reports
  end
  resources :occupation_registrations, only: [:new, :create]
  namespace :savings_module do
    resources :filter_by_saving_products, only: [:index]
  end
end
