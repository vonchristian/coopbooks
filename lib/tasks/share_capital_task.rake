desc 'Create savings liability account'
namespace :share_capitals do
  task :create_equity_account => :environment do
    MembershipsModule::ShareCapital.all.each do |share_capital|
      if share_capital.equity_account.blank?
        account = AccountingModule::Equity.create!(
          name: "Share Capital - Common #{share_capital.account_number}",
          code: share_capital.account_number
        )
        share_capital.update_attributes!(equity_account: account)
        puts 'Success'
      end
    end
  end
  task :transfer_entries => :environment do
    MembershipsModule::ShareCapital.all.each do |share_capital|
      share_capital.credit_amounts.each do |amount|
        amount.update_attributes!(account_id: share_capital.equity_account_id)
      end
      puts 'Success'
    end
  end
end
