desc 'Create savings liability account'
namespace :savings do
  task :create_liability_account => :environment do
    MembershipsModule::Saving.all.each do |saving|
      AccountCreators::Saving.new(saving: saving).create_account!
    end
  end

  task :transfer_entries => :environment do
    MembershipsModule::Saving.all.each do |saving|
      saving.debit_amounts.each do |amount|
        amount.update_attributes!(account_id: saving.liability_account_id)
      end
      puts 'Success'
    end
  end
end
