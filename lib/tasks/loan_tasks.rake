desc 'Create loan receivable_account'
namespace :loans do
  task :create_accounts => :environment do
    LoansModule::Loan.all.each do |loan|
      AccountCreators::Loan.new(loan: loan).create_accounts!
    end
  end

  task :transfer_entries => :environment do
    LoansModule::Loan.all.each do |loan|
      loan.principal_account.amounts.where(commercial_document: loan).each do |amount|
        amount.update_attributes!(account: loan.receivable_account)
      end
      puts 'Success'
    end
  end
end
