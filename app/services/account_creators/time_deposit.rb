module AccountCreators
  class TimeDeposit
    attr_reader :time_deposit, :time_deposit_product, :liability_account_category, :interest_expense_account_category, :office

    def initialize(args)
      @time_deposit = args.fetch(:time_deposit)
      @time_deposit_product = @time_deposit.time_deposit_product
      @office = @time_deposit.office
      @liability_account_category = @office.office_time_deposit_products.where(time_deposit_product: @time_deposit_product).last.liability_account_category
      @interest_expense_account_category = @office.office_time_deposit_products.where(time_deposit_product: @time_deposit_product).last.interest_expense_account_category

    end
    def create_accounts!
      create_liability_account
      create_interest_expense_account
    end

    private
    def create_liability_account
      if time_deposit.liability_account.blank?
        account = office.accounts.liabilities.create!(
          name:           "Time Deposits - (#{time_deposit.depositor_name} - #{time_deposit.account_number}",
          code:           SecureRandom.uuid,
          account_number: SecureRandom.uuid,
          level_one_account_category: liability_account_category
        )
        if time_deposit.respond_to?(:liability_account)
          time_deposit.update(liability_account: account)
        end
      end
    end

    def create_interest_expense_account
      if time_deposit.interest_expense_account.blank?
        account = office.accounts.expenses.create!(
          name:           "Interest Expense on Time Deposits - (#{time_deposit.depositor_name} - #{time_deposit.account_number}",
          code:           SecureRandom.uuid,
          account_number: SecureRandom.uuid,
          account_category: interest_expense_account_category
        )
        if time_deposit.respond_to?(:interest_expense_account)
          time_deposit.update(interest_expense_account: account)
        end
      end
    end

  end
end
