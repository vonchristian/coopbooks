module AccountCreators
  module StoreFronts
    module Orders
      class SalesOrder
        attr_reader :sales_order, :store_front, :office
        def initialize(sales_order:)
          @sales_order = sales_order
          @store_front = @sales_order.store_front
          @employee    = @sales_order.employee
          @office = @employee.office
        end
        def create_accounts!
          create_sales_account
          create_sales_discount_account
          create_cost_of_goods_sold_account
          create_receivable_account
        end

        private

        def create_sales_account
          if sales_order.sales_account.blank?
            account = AccountingModule::Accounts::Revenue.create!(
              code: SecureRandom.uuid,
              name: "Sales - #{sales_order.account_number}",
              account_number: SecureRandom.uuid,
              level_one_account_category: store_front.sales_category,
              office: office,

            )
            sales_order.update(sales_account: account)
          end
        end

        def create_sales_discount_account
          if sales_order.sales_discount_account.blank?
            account = AccountingModule::Accounts::Revenue.create!(
              code: SecureRandom.uuid,
              name: "Sales Discount - #{sales_order.account_number}",
              contra: true,
              account_number: SecureRandom.uuid,
              level_one_account_category: store_front.sales_discount_category,
              office: office
            )
            sales_order.update(sales_discount_account: account)
          end
        end

        def create_cost_of_goods_sold_account
          if sales_order.cost_of_goods_sold_account.blank?
            account = AccountingModule::Accounts::Expense.create!(
              code: SecureRandom.uuid,
              name: "Cost of Goods Sold - #{sales_order.account_number}",
              account_number: SecureRandom.uuid,
              level_one_account_category: store_front.cost_of_goods_sold_category,
              office: office
            )
            sales_order.update(cost_of_goods_sold_account: account)
          end
        end

        def create_receivable_account
          if sales_order.receivable_account.blank?
            account = AccountingModule::Accounts::Asset.create!(
              code: SecureRandom.uuid,
              name: "Sales Receivable - #{sales_order.account_number}",
              account_number: SecureRandom.uuid,
              level_one_account_category: store_front.receivable_category,
              office: office
            )
            sales_order.update(receivable_account: account)
          end
        end
      end
    end
  end
end
