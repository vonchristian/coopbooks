module AccountCreators
  class ProgramSubscription
    attr_reader :subscription, :program, :office

    def initialize(args)
      @subscription = args.fetch(:subscription)
      @office       = @subscription.office
      @program      = @subscription.program
    end

    def create_accounts!
      if subscription.account.blank?
        account_category = Offices::OfficeProgram.where(program: program, office: office).last.level_one_account_category
        account = account_category.accounts.create!(
        type:             "AccountingModule::Accounts::#{account_category.normalized_type}",
        name:             "#{program.name} (#{subscription.subscriber_name} - #{subscription.account_number}",
        code:             subscription.account_number,
        account_number:   subscription.account_number,
        office:           subscription.office)
        subscription.update!(account: account)
        office.accounts << account
      end
    end
  end
end
