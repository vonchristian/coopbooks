module AccountCreators
  class ShareCapitalProduct
    attr_reader :office, :share_capital_product
    def initialize(office:, share_capital_product:)
      @share_capital_product = share_capital_product
      @office                = office
    end

    def create_accounts!
      create_temporary_account
    end

    private

    def create_temporary_account
     account = AccountingModule::Accounts::Equity.create!(
      office:         office,
      name:           "Temporary #{share_capital_product.name} - #{office.name}",
      code:           SecureRandom.uuid,
      level_one_account_category: share_capital_product.equity_account_category,
      account_number: SecureRandom.uuid)
      share_capital_product.update(temporary_account: account)
    end
  end
end
