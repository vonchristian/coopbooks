module AccountFinders
  class PurchaseOrder
    attr_reader :purchase_order, :entry

    def initialize(args)
      @purchase_order = args.fetch(:purchase_order)
      @voucher        = @purchase_order.voucher
      @entry          = @voucher.entry
    end

    def find_payable_account
      entry.accounts.where.not(id: Employees::EmployeeCashAccount.cash_accounts.ids).first
    end
  end
end
