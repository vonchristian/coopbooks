module TimeDeposits
  class InterestCalculator
    attr_reader :time_deposit, :time_deposit_product

    def initialize(args)
      @time_deposit         = args.fetch(:time_deposit)
      @time_deposit_product = @time_deposit.time_deposit_product
    end

    def compute
      if time_deposit.matured?
        computation_for_matured
      else
        computation_for_not_matured
      end
    end

    private
    def computation_for_matured
      BigDecimal(time_deposit.balance * time_deposit_product.interest_rate)
    end

    def computation_for_not_matured
      BigDecimal(time_deposit.balance *
      time_deposit_product.rate_for_not_matured *
      time_deposit.days_elapsed)
    end
  end
end
