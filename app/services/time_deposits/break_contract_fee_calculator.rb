module TimeDeposits
  class BreakContractFeeCalculator
    attr_reader :time_deposit

    def initialize(args)
      @time_deposit         = args.fetch(:time_deposit)
      @time_deposit_product = @time_deposit.product
    end
    def compute
      time_deposit_product.break_contract_rate * time_deposit.balance
    end
  end
end
