module LoansModule
  class LoanProtectionCalculator
    attr_reader :loan_application, :plan_provider

    def initialize(loan_application:, plan_provider:)
      @loan_application = loan_application
      @plan_provider    = plan_provider
    end
    def calculate
      plan_provider.rate *
      loan_application.number_of_thousands *
      loan_application.term
    end
  end
end 
