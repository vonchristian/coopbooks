module LoansModule
  module PenaltyCalculators
    class Amortization
      attr_reader :amortization

      def compute
        balance * rate
      end
    end
  end
end
