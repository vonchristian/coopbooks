module LoansModule
  module PenaltyCalculators
    class Loan
      attr_reader :loan

      def compute
        balance * rate
      end
    end
  end
end
