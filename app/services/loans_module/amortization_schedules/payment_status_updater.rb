module LoansModule
  module AmortizationSchedules
    class PaymentStatusUpdater
      attr_reader :amortization_schedule, :entries
      def initialize(amortization_schedule:)
        @amortization_schedule = amortization_schedule
        @entries               = @amortization_schedule.entries
      end


      def update_payment_status!
        if entries.blank?
          amortization_schedule.missed_payment!
        else
          update_status_with_entries
        end
      end

      def update_status_with_entries
        if entries.total_cash_amount >= amortization_schedule.total_repayment
          amortization_schedule.fully_paid!
        else
          amortization_schedule.partially_paid!
        end
      end
    end
  end
end
