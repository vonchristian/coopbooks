module LoansModule
  module Loans
    class Archiving
      attr_reader :loan, :date
      def initialize(loan:, date:)
        @loan = loan
        @date = date
      end

      def archive!
        if loan.balance.zero?
          loan.update!(archived_at: date)
        end
      end
    end
  end
end
