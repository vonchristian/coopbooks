module LoansModule
  module Loans
    class TermCreation
      attr_reader :loan, :effectivity_date, :term

      def initialize(args={})
        @loan             = args.fetch(:loan)
        @effectivity_date = args.fetch(:effectivity_date)
        @term             = args.fetch(:term)
      end

      def update_term
        loan.terms.create!(
          effectivity_date: effectivity_date,
          maturity_date:    effectivity_date + add_terms
        )
      end
      
      def add_terms
        term.months
      end
    end
  end
end
