module LoansModule
  module Loans
    class LoanGroupUpdate
      attr_reader :loan, :office, :loan_groups, :date

      def initialize(loan:, date:)
        @loan        = loan
        @office      = @loan.office
        @loan_groups = @office.loan_groups
        @date        = date
      end

      def set_loan_group
        loan_groups.each do |loan_group|
          if loan_group.num_range.include?(loan.number_of_days_past_due)
            loan.loan_agings.find_or_create_by(loan_group: loan_group, date: date)
          end
        end
      end
    end
  end
end
