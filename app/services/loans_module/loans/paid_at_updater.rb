module LoansModule
  module Loans
    class PaidAtUpdater
      attr_reader :loan, :date

      def initialize(loan:, date:)
        @loan = loan
        @date = date
      end

      def update_paid_at!
        if loan.balance.zero?
          loan.update!(paid_at: date)
          archived_loan
        end
      end

      def archived_loan
        LoansModule::Loans::Archiving.new(loan: loan, date: date).archive!
      end
    end
  end
end
