module LoansModule
  module Loans
    class StatusUpdate
      attr_reader :loan
      def initialize(loan:)
        @loan = loan
      end

      def set_status
        if loan.maturity_date >= Date.current
          loan.current_loan!
        else
          loan.past_due!
        end
      end
    end
  end
end
