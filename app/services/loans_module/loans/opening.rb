module LoansModule
  module Loans
    class Opening

      attr_reader :loan_application, :cooperative, :employee, :voucher, :office, :loan_product

      def initialize(args)
        @loan_application = args.fetch(:loan_application)
        @loan_product     = @loan_application.loan_product
        @employee         = args.fetch(:employee)
        @office           = @employee.office
        @voucher          = @loan_application.voucher
        @cooperative      = @loan_application.cooperative
      end

      def find_loan
        office.loan_applications.find_by!(account_number: loan_application.account_number)
      end

      def create_loan!
        ActiveRecord::Base.transaction do
          create_loan
        end
      end

      private
      def create_loan
        loan = office.loans.build(
          voucher:                  loan_application.voucher,
          loan_application:         loan_application,
          mode_of_payment:          loan_application.mode_of_payment,
          cooperative:              loan_application.cooperative,
          loan_amount:              loan_application.loan_amount.amount,
          borrower:                 loan_application.borrower,
          borrower_full_name:       loan_application.borrower.name,
          loan_product:             loan_application.loan_product,
          purpose:                  loan_application.purpose,
          disbursement_date:        voucher.disbursement_date,
          tracking_number:          loan_application.voucher.reference_number,
          account_number:           loan_application.account_number,
          receivable_account:       loan_application.receivable_account,
          interest_revenue_account: loan_application.interest_revenue_account)
          AccountCreators::Loan.new(loan: loan, loan_product: loan_application.loan_product).create_accounts!
          loan.save!
          loan.loan_groups << office.loan_groups.where(start_num: 0).first
        create_amortization_schedules(loan)
        create_voucher_amounts(loan)
        create_term(loan)
        create_loan_interests
      end

      def create_amortization_schedules(loan)
        loan.amortization_schedules << loan_application.amortization_schedules
      end

      def create_term(loan) #move to entry processing
        loan.terms.create!(
          term:             loan_application.term,
          account_number:   SecureRandom.uuid,
          effectivity_date: loan_application.application_date,
          maturity_date:    loan_application.application_date + loan_application.term.to_i.months)
      end

      def create_voucher_amounts(loan)
        loan.amortization_schedules << loan_application.amortization_schedules
      end
      #move to a new class
      def create_loan_interests
        if loan_application.prededucted?
          loan.        loan_interests.create!(
          date:        loan_application.application_date,
          amount:      loan_application.total_interest,
          description: "Computed loan interests on #{loan_application.application_date.strftime("%B %e, %Y")}",
          employee: employee)
        end
      end

    end
  end
end
