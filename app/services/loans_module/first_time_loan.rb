module LoansModule
  class FirstTimeLoan
    attr_reader :from_date, :to_date, :loans
    def initialize(from_date:, to_date:, loans:)
      @loans     = loans
      @from_date = from_date
      @to_date   = to_date
    end

    def first_time_loans
      member_ids = loans.disbursed_on(from_date: from_date, to_date: to_date).pluck(:borrower_id)
      loan_ids   = Member.where(id: member_ids).select{ |member| member.loans.count == 1 }.collect{ |member| member.loans.ids }
      LoansModule::Loan.where(id: loan_ids.uniq.compact.flatten)
    end
  end
end
