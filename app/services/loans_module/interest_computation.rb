module LoansModule
  class InterestComputation
    attr_reader :loan_amount, :term, :rate
    def initialize(args)
      @loan_amount = args.fetch(:loan_amount)
      @term        = args.fetch(:term)
      @rate        = args.fetch(:rate)
    end

    def compute
      loan_amount * rate * applicable_term
    end


    def applicable_term
      if term >= 12
        applicable_term = 12
      else
        applicable_term = term
      end
    end
  end
end
