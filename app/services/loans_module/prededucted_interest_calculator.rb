module LoansModule
  class PredeductedInterestCalculator
    attr_reader :loan_application, :interest_prededuction

    def initialize(args)
      @loan_application      = args.fetch(:loan_application)
      @interest_prededuction = @loan_application.loan_product.try(:current_interest_prededuction)
    end

    def calculate
      if interest_prededuction && interest_prededuction.on_first_year?
        loan_application.first_year_interest * loan_application.interest_prededuction_rate
      else
        loan_application.total_interest * loan_application.interest_prededuction_rate
      end
    end
  end
end
