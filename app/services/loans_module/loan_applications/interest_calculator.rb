module LoansModule
  module LoanApplications
    class InterestCalculator
      attr_reader :loan_application
      def initialize(loan_application:)
        @loan_application = loan_application
      end
      def calculate
        Money.new(loan_application.loan_amount * loan_application.interest_rate).amount
      end
    end
  end
end
