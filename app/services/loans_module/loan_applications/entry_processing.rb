module LoansModule
  module LoanApplications
    class EntryProcessing
      include ActiveModel::Model
      attr_reader :voucher, :employee, :loan, :loan_application, :cooperative, :office
      def initialize(args)
        @employee         = args.fetch(:employee)
        @loan_application = args.fetch(:loan_application)
        @loan             = @loan_application.loan
        @voucher          = @loan_application.voucher
        @cooperative      = @employee.cooperative
        @office           = @employee.office
      end

      def find_entry
        office.entries.find_by!(reference_number: voucher.reference_number)
      end

      def process!
        ActiveRecord::Base.transaction do
          create_entry
          update_voucher_entry_reference
        end
      end

      private
      def create_entry
        entry = office.entries.build(
          cooperative:         cooperative,
          commercial_document: voucher.payee,
          description:         voucher.description,
          recorder:            employee,
          reference_number:    voucher.reference_number,
          entry_date:          voucher.disbursement_date)

          voucher.voucher_amounts.debit.excluding_account(account: loan.receivable_account).each do |amount|
            entry.debit_amounts.build(account: amount.account, amount: amount.amount)
          end

          voucher.voucher_amounts.credit.each do |amount|
            entry.credit_amounts.build(account: amount.account, amount:  amount.amount)
          end

          voucher.voucher_amounts.debit.for_account(account: loan.receivable_account).each do |amount|
            entry.debit_amounts.build(account: amount.account, amount: amount.amount)
          end
        entry.save!
      end
      def update_voucher_entry_reference
        voucher.update!(entry: find_entry, disburser: employee)
      end
    end
  end
end
