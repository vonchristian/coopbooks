module LoansModule
  module LoanApplications
    class Cancellation

      attr_reader :loan_application, :cart
      def initialize(loan_application:)
        @loan_application = loan_application
        @cart             = @loan_application.cart
      end

      def cancel!
        ActiveRecord::Base.transaction do
          delete_loan_application
          delete_share_capital_application
          delete_voucher_amounts
          delete_accounts
          delete_amortization_schedules
        end
      end

      private
      def delete_share_capital_application
        loan_application.cart.share_capital_applications.each do |share_capital_application|
          share_capital_application.equity_account.destroy
        end
        loan_application.cart.share_capital_applications.destroy_all
      end
      def delete_accounts
        loan_application.receivable_account.destroy
        loan_application.interest_revenue_account.destroy
      end

      def delete_voucher_amounts
        cart.voucher_amounts.destroy_all
        cart.destroy
      end

      def delete_loan_application
        loan_application.destroy
      end

      def delete_amortization_schedules
        loan_application.amortization_schedules.destroy_all
      end
    end
  end
end
