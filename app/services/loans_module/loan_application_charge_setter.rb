module LoansModule
  class LoanApplicationChargeSetter
    attr_reader :loan_application, :loan_product, :borrower, :cart, :office_loan_product

    def initialize(args)
      @cart             = args.fetch(:cart)
      @loan_application = args.fetch(:loan_application)
      @loan_product     = @loan_application.loan_product
      @borrower         = @loan_application.borrower
      @office           = @loan_application.office
      @office_loan_product = @office.office_loan_products.find_by!(loan_product: @loan_product)
    end

    def create_charges!
      create_interest_on_loan_charge
      create_charges_based_on_loan_product
      create_loan_protection_fund
    end

    private

    def create_interest_on_loan_charge
      cart.voucher_amounts.credit.create!(
        cooperative: loan_application.cooperative,
        recorder:    loan_application.preparer,
        description: "Interest on Loan",
        amount:      loan_application.prededucted_interest,
        account:     loan_application.interest_revenue_account)
    end

    def create_charges_based_on_loan_product
      office_loan_product.loan_product_charges.except_capital_build_up.each do |charge|
        cart.voucher_amounts.credit.create!(
          recorder:    loan_application.preparer,
          cooperative: loan_application.cooperative,
          description: charge.name,
          amount:      computed_charge(charge, loan_application.loan_amount),
          account:     charge.account
        )
      end
    end

    def create_loan_protection_fund
      if loan_product.loan_protection_plan_provider.present?
        cart.voucher_amounts.credit.create!(
        recorder:    loan_application.preparer,
        cooperative: loan_application.cooperative,
        amount:      loan_product.loan_protection_plan_provider.amount_for(loan_application),
        account:     loan_product.loan_protection_plan_provider.payable_account,
        description: 'Loan Protection Fund'
        )
      end
    end


    def computed_charge(charge, amount)
      charge.charge_amount(chargeable_amount: loan_application.loan_amount.amount)
    end
  end
end
