module LoansModule
  module Payments
    class Classifier
      attr_reader :entry, :loan, :office, :debit_amounts, :credit_amounts

      def initialize(args)
        @loan           = args.fetch(:loan)
        @entry          = args.fetch(:entry)
        @office         = @entry.office
        @debit_amounts  = @entry.debit_amounts
        @credit_amounts = @entry.credit_amounts
      end


      def principal
        credit_amounts.where(account: loan.receivable_account).total
      end

      def interest
        entry.amounts.where(account: loan.interest_revenue_account).total
      end

      def penalty
        credit_amounts.where(account: loan.penalty_revenue_account).total
      end

      def total_cash_payment
        principal.to_f +
        interest.to_f +
        penalty.to_f
      end
    end
  end
end
