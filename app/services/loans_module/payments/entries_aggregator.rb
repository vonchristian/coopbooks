module LoansModule
  module Payments
    class EntriesAggregator
      attr_reader :loans, :office, :from_date, :to_date

      def initialize(args)
        @from_date = args.fetch(:from_date)
        @to_date   = args.fetch(:to_date)
        @office    = args.fetch(:office)
        @loans     = @office.loans
      end

      def entries
        entry_ids = AccountingModule::Amount.where(account_id: account_ids).pluck(:entry_id)
        office.entries.entered_on(from_date: from_date, to_date: to_date).
        where(id: entry_ids).distinct
      end

      def total_principals
        loans.receivable_accounts.balance(from_date: from_date, to_date: to_date)
      end

      def total_interests
        loans.interest_revenue_accounts.balance(from_date: from_date, to_date: to_date)
      end

      def total_penalties
        loans.penalty_revenue_accounts.balance(from_date: from_date, to_date: to_date)
      end

      def total_cash_payments
        total_principals + total_interests + total_penalties
      end

      def account_ids
        ids = []
        ids << loans.receivable_accounts.ids
        ids << loans.interest_revenue_accounts.ids
        ids << loans.penalty_revenue_accounts.ids
        ids.compact.flatten
      end
    end
  end
end
