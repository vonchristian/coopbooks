module AccountingModule
  module CashAccounts
    class EntryAmountCalculator
      attr_reader :account_category, :cash_account, :from_date, :to_date, :accounts
      def initialize(date:, account_category:, cash_account:)
        @account_category = account_category
        @cash_account     = cash_account
        @date             = date
        @from_date        = @date.beginning_of_day
        @to_date          = @date.end_of_day
        @accounts         = @account_category.accounts
      end


      def payment
        if Offices::OfficeLoanProduct.receivable_account_categories.include?(account_category)
          payment_for_loan
        else
          cash_account.
          debit_entries.
          except_loan_disbursements.
          entered_on(from_date: from_date, to_date: to_date).
          map{ |entry| entry.credit_amounts.where(account: accounts).total }.sum
        end
      end

      def payment_for_loan
        account_category.credit_entries.
        entered_on(from_date: from_date, to_date: to_date).
        map{ |entry| entry.total_cash_amount }.sum
      end

      def disbursement
        if Offices::OfficeLoanProduct.receivable_account_categories.include?(account_category)
          disbursement_for_loan
        else
          cash_account.
          credit_entries.
          except_loan_disbursements.
          entered_on(from_date: from_date, to_date: to_date).
          map{ |entry| entry.debit_amounts.where(account: accounts).total }.sum
        end
      end

      def disbursement_for_loan
        account_category.debit_entries.
        entered_on(from_date: from_date, to_date: to_date).
        map{ |entry| entry.total_cash_amount }.sum
      end
    end
  end
end
