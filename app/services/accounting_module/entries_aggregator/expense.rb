module AccountingModule
  module EntriesAggregator
    class Expense
      attr_reader :entries, :office, :expenses
      def initialize(entries:, office:)
        @entries  = entries
        @office   = office
        @expenses = @office.accounts.expenses
      end

      def debit_entries
        ids = entries.
        where.not(id: AccountingModule::EntriesAggregator::BankAccount.new(entries: entries, office: office).debit_entries).
        where.not(id: AccountingModule::EntriesAggregator::Loan.new(entries: entries, office: office).debit_entries).
        where.not(id: AccountingModule::EntriesAggregator::ProgramSubscription.new(entries: entries, office: office, program_subscriptions: office.program_subscriptions).debit_entries).
        where.not(id: AccountingModule::EntriesAggregator::Saving.new(entries: entries, office: office).debit_entries).
        where.not(id: AccountingModule::EntriesAggregator::ShareCapital.new(entries: entries, office: office).debit_entries).
        amounts.debits.where(account: expenses).pluck(:entry_id)

        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
