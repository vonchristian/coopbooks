module AccountingModule
  module EntriesAggregator
    class Loan
      attr_reader :entries, :office, :loans
      def initialize(entries:, office:)
        @entries = entries
        @office  = office
        @loans   = @office.loans
      end

      def debit_entries
        ids = entries.amounts.debits.where(account: loans.accounts).pluck(:entry_id)
        office.entries.where(id: ids.uniq.compact.flatten)
      end

      def credit_entries
        ids = entries.amounts.credits.where(account: loans.accounts).pluck(:entry_id)
        office.entries.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
