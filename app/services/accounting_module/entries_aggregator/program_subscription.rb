module AccountingModule
  module EntriesAggregator
    class ProgramSubscription
      attr_reader :entries, :office, :program_subscriptions
      def initialize(entries:, office:, program_subscriptions:)
        @entries = entries
        @office  = office
        @program_subscriptions = program_subscriptions
      end
      #releases
      def debit_entries
        ids = entries.amounts.debits.where(account: program_subscriptions.accounts).pluck(:entry_id)
        office.entries.where(id: ids.uniq.compact.flatten)
      end
      #payments
      def credit_entries
        ids = entries.amounts.credits.where(account: program_subscriptions.accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
