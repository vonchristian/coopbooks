module AccountingModule
  module EntriesAggregator
    class ShareCapital
      attr_reader :entries, :office, :share_capitals
      def initialize(entries:, office:)
        @entries = entries
        @office  = office
        @share_capitals = @office.share_capitals
      end
      #releases
      def debit_entries
        ids = entries.amounts.debits.where(account: share_capitals.equity_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
      #payments
      def credit_entries
        ids = entries.amounts.credits.where(account: share_capitals.equity_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
