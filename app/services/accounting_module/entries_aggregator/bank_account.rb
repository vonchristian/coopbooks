module AccountingModule
  module EntriesAggregator
    class BankAccount
      attr_reader :entries, :office, :bank_accounts
      def initialize(entries:, office:)
        @entries         = entries
        @office          = office
        @bank_accounts   = @office.bank_accounts
      end

      def debit_entries
        ids = entries.amounts.debits.where(account: bank_accounts.cash_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end

      def credit_entries
        ids = entries.amounts.credits.where(account: bank_accounts.cash_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
