module AccountingModule
  module EntriesAggregator
    class Saving
      attr_reader :entries, :office, :savings
      def initialize(entries:, office:)
        @entries = entries
        @office  = office
        @savings   = @office.savings
      end

      def debit_entries
        ids = entries.amounts.debits.where(account: savings.liability_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end

      def credit_entries
        ids = entries.amounts.credits.where(account: savings.liability_accounts).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
