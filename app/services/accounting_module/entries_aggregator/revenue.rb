module AccountingModule
  module EntriesAggregator
    class Revenue
      attr_reader :entries, :office, :revenues
      def initialize(entries:, office:)
        @entries  = entries
        @office   = office
        @revenues = @office.accounts.revenues
      end

      def credit_entries
        ids = entries.
        where.not(id: AccountingModule::EntriesAggregator::BankAccount.new(entries: entries, office: office).credit_entries).
        where.not(id: AccountingModule::EntriesAggregator::Loan.new(entries: entries, office: office).credit_entries).
        where.not(id: AccountingModule::EntriesAggregator::ProgramSubscription.new(entries: entries, office: office, program_subscriptions: office.program_subscriptions).credit_entries).
        where.not(id: AccountingModule::EntriesAggregator::Saving.new(entries: entries, office: office).credit_entries).
        where.not(id: AccountingModule::EntriesAggregator::ShareCapital.new(entries: entries, office: office).credit_entries).
        amounts.debits.where(account: revenues).pluck(:entry_id)
        office.entries.except_loan_disbursements.where(id: ids.uniq.compact.flatten)
      end
    end
  end
end
