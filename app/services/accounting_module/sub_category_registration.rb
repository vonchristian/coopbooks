module AccountingModule
  class SubCategoryRegistration
    attr_reader :main_category, :sub_category
    def initialize(main_category:, sub_category:)
      @main_category = main_category
      @sub_category  = sub_category
    end
    def create_sub_category!
      if main_category.cooperative == sub_category.cooperative
        main_category.sub_categories << sub_category
      end
    end
  end
end
