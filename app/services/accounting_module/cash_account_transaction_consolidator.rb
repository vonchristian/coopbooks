module AccountingModule
  class CashAccountTransactionConsolidator
    attr_reader :cash_account, :office, :from_date, :to_date

    def initialize(cash_account:, date:, office:)
      @cash_account = cash_account
      @office       = office
      @date         = date
      @from_date    = @date.beginning_of_day
      @to_date      = @date.end_of_day
    end

    def credit_account_categories
      cash_account.
      credit_entries.
      entered_on(from_date: from_date, to_date: to_date).
      accounts.
      except_cash_accounts.
      account_categories.
      where(office: office).
      updated_at(from_date: from_date, to_date: to_date).
      distinct
    end

    def debit_account_categories
      cash_account.
      debit_entries.
      entered_on(from_date: from_date, to_date: to_date).
      accounts.
      except_cash_accounts.
      account_categories.
      where(office: office).
      updated_at(from_date: from_date, to_date: to_date).
      distinct

    end
    def updated_account_categories
      cash_account.
      entries.
      entered_on(from_date: from_date, to_date: to_date).
      accounts.
      account_categories.
      where(office: office).
      updated_at(from_date: from_date, to_date: to_date).
      distinct
    end
  end
end
