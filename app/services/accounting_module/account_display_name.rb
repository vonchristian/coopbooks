module AccountingModule
  class AccountDisplayName
    attr_reader :account, :office, :cash_accounts, :saving_products

    def initialize(args)
      @account                 = args.fetch(:account)
      @office                  = @account.office
      @cash_accounts           = @office.cash_accounts
      @saving_products         = @office.saving_products
    end

    def display_name
      return account.name if cash_accounts.ids.include?(account.id)
      if account.account_category.present?
        account.account_category.title
      else
        account.name
      end
    end
  end
end
