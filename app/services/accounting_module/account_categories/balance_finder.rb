module AccountingModule
  module AccountCategories
    class BalanceFinder
      attr_reader :account_category
      def initialize(args={})
        @account_category = args.fetch(:account_category)
      end
      def balance
        ids = []
        ids << account_category.sub_categories.pluck(:id)
        account_category.sub_categories.each do |sub_category|
          ids << sub_category.sub_categories.pluck(:id)
          sub_category.sub_categories.each do |sub1_category|
            ids << sub1_category.sub_categories.pluck(:id)
          end
        end
        AccountingModule::LevelOneAccountCategory.where(id: ids.uniq.compact.flatten)
      end
      def debits_balance
      end
      def credits_balance
      end
    end
  end
end
