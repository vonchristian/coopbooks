module Loans
  class MultiplePaymentFinder
    attr_reader :cart

    def initialize(args)
      @cart = args.fetch(:cart)
    end
    def total_cash_payment
      cart.voucher_amounts.where(account: Employees::EmployeeCashAccount.cash_accounts).total
    end
    def loan_ids
      cart_ids = cart.voucher_amounts.pluck(:account_id)
      loan_ids = []
      loan_ids << LoansModule::Loan.includes(:loan_product).where(receivable_account_id: cart_ids).ids
      loan_ids << LoansModule::Loan.where(interest_revenue_account_id: cart_ids).ids
      loan_ids << LoansModule::Loan.where(penalty_revenue_account_id: cart_ids).ids
      loan_ids.compact.flatten.uniq
    end

    def principal(loan)
      cart.voucher_amounts.where(account: loan.receivable_account).sum(&:amount)
    end

    def interest(loan)
      cart.voucher_amounts.where(account: loan.interest_revenue_account).sum(&:amount)
    end

    def penalty(loan)
      cart.voucher_amounts.where(account: loan.penalty_revenue_account).sum(&:amount)
    end
    def total_cash(loan)
      principal(loan) +
      interest(loan) +
      penalty(loan)
    end
  end
end
