module Loans
  class LoanGroupBalanceCalculator
    attr_reader :loan, :loan_group

    def initialize(loan:, loan_group:)
      @loan       = loan
      @loan_group = loan_group
    end
    
    def balance
      if loan.loan_group == loan_group
        loan.balance
      else
        0
      end
    end
  end
end
