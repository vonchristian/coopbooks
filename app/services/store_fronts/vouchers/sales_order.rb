module StoreFronts
  module Vouchers
    class SalesOrder
      attr_reader :order, :employee, :store_front, :cash_account, :cooperative

      def initialize(args)
        @order        = args.fetch(:order)
        @employee     = @order.employee
        @store_front  = @employee.store_front
        @cooperative  = @employee.cooperative
        @cash_account = @employee.default_cash_account
      end

      def create_voucher!

        voucher = cooperative.vouchers.build(
          office:              employee.office,
          preparer:            employee,
          date:                order.date,
          description:         "Sales order ##{order.reference_number}",
          reference_number:    order.reference_number,
          account_number:      SecureRandom.uuid,
          payee: order.customer
        )

        voucher.voucher_amounts.debit.build(
          amount:              order.total_cost_less_discount,
          account:             employee.default_cash_account,
          cooperative: employee.cooperative,
          recorder: employee
        )
        if order.discount > 0
        voucher.voucher_amounts.debit.build(
          amount:              order.discount,
          account:             order.sales_discount_account,
          cooperative: employee.cooperative,
          recorder: employee
        )
      end
        voucher.voucher_amounts.debit.build(
          amount:              order.cost_of_goods_sold,
          account:             order.cost_of_goods_sold_account,
          cooperative: employee.cooperative,
          recorder: employee
        )

        voucher.voucher_amounts.credit.build(
          amount:              order.total_cost,
          account:             order.sales_account,
          cooperative: employee.cooperative,
          recorder: employee
        )

        voucher.voucher_amounts.credit.build(
          amount:              order.cost_of_goods_sold,
          account:             store_front.merchandise_inventory_account,
          cooperative: employee.cooperative,
          recorder: employee
        )

        voucher.save!
        order.update!(voucher: voucher)
      end
    end
  end
end
