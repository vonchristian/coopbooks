module SavingsModule
  class AverageDailyBalance
    attr_reader :from_date, :to_date, :saving, :date_range
    def initialize(from_date:, to_date:, saving:)
      @from_date = from_date
      @to_date   = to_date
      @saving    = saving
      @date_range = @from_date..@to_date
    end

    def averaged_balance
      balance = BigDecimal("0")
        date_range.each do |date|
          balance += saving.balance(to_date: date.end_of_day)
        end
      balance / date_range.count
    end
  end
end
