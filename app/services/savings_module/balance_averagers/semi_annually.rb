module SavingsModule
  module BalanceAveragers
    class SemiAnnually
      attr_reader :saving, :to_date

      def initialize(args)
        @saving  = args.fetch(:saving)
        @to_date = args.fetch(:to_date)
      end

      def averaged_balance
        monthly_balances.sum / 6
      end


      def end_date

      end

      def start_date

      end

      def monthly_balances
        balances = []
        months   = []
        (start_date..end_date).each do |date|
          months << date.end_of_month
        end

        months.uniq.each do |month|
          balances <<  saving.balance(to_date: month.end_of_month)
        end
        balances
      end
    end
  end
end
