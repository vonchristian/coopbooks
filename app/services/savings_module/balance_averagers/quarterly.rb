module SavingsModule
  module BalanceAveragers
    class Quarterly
      attr_reader :saving, :to_date

      def initialize(saving:, to_date:)
        @saving  = saving
        @to_date = to_date
      end

      def averaged_balance
        monthly_balances / 3
      end

      def end_date
        if to_date.is_a?(Date)|| to_date.is_a?(Time)
          to_date.end_of_quarter
        else
          DateTime.parse(to_date).end_of_quarter
        end
      end

      def start_date
        if to_date.is_a?(Date) || to_date.is_a?(Time)
          to_date.beginning_of_quarter
        else
          DateTime.parse(to_date).beginning_of_quarter
        end
      end
      def months
        month   = []
        (start_date..end_date).each do |date|
          month << date.end_of_month
        end
        month.uniq
      end

      def monthly_balances
        balances = BigDecimal('0')
        months.each do |month|
          balances += saving.balance(to_date: month.end_of_month)
        end
        balances
      end
    end
  end
end
