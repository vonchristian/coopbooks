module SavingsModule
  class AccountClosing
    attr_reader :savings_account

    def initialize(args)
      @savings_account = args.fetch(:savings_account)
    end
    def close_account!
      savings_account.update_attributes!(
        archived: true,
        archived_at: savings_account.last_transaction_date
      )
    end
  end
end
