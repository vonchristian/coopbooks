module SavingsModule
  module InterestRateFinders
    class Daily
      attr_reader :saving_product

      def initialize(args)
        @saving_product = args.fetch(:saving_product)
      end

      def rate_divisor
        364.0
      end

      def applicable_rate
        saving_product.interest_rate / rate_divisor
      end 
    end
  end
end
