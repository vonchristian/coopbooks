module SavingsModule
  module InterestRateFinders
    class Annually
      attr_reader :saving_product

      def initialize(args)
        @saving_product = args.fetch(:saving_product)
      end

      def rate_divisor
        1.0
      end

      def applicable_rate
        saving_product.interest_rate / rate_divisor
      end 
    end
  end
end
