module SavingsModule
  class InterestCalculator
    attr_reader :saving, :saving_product, :from_date, :to_date
    def initialize(saving:, from_date:, to_date:)
      @saving         = saving
      @from_date      = from_date
      @to_date        = to_date
      @saving_product = @saving.saving_product
    end

    def calculate_interest
      averaged_balance * saving_product.applicable_rate
    end

    private
    def averaged_balance
      SavingsModule::AverageDailyBalance.new(from_date: from_date, to_date: to_date, saving: saving).averaged_balance
    end
  end
end
