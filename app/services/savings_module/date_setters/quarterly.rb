module SavingsModule
  module DateSetters
    class Quarterly
      attr_reader :saving_product, :date

      def initialize(saving_product:, date:)
        @saving_product = saving_product
        @date           = date
      end

      def start_date
        date.beginning_of_quarter
      end

      def end_date
        date.end_of_quarter
      end
    end
  end
end
