module Insights
  module Members
    class ProvinceCounter
      attr_reader :province, :cooperative, :members

      def initialize(province:, cooperative:)
        @province    = province
        @cooperative = cooperative
        @members     = province.members
      end

      def count
        total ||= members.joins(:memberships).
        where('memberships.cooperative_id' => cooperative.id).distinct.size
      end
    end
  end
end
