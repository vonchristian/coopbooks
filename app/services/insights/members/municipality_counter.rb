module Insights
  module Members
    class MunicipalityCounter
      attr_reader :municipality, :cooperative, :members

      def initialize(municipality:, cooperative:)
        @municipality = municipality
        @cooperative  = cooperative
        @members      = municipality.members
      end

      def count
        total ||= members.joins(:memberships).
        where('memberships.cooperative_id' => cooperative.id).distinct.size
      end
    end
  end
end
