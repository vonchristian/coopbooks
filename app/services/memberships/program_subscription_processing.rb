module Memberships
  class ProgramSubscriptionProcessing
    attr_reader :subscriber, :program, :office, :cooperative
    def initialize(args)
      @subscriber  = args.fetch(:subscriber)
      @program     = args.fetch(:program)
      @office      = args.fetch(:office)
      @cooperative = args.fetch(:cooperative)
    end
    def process!
      ActiveRecord::Base.transaction do
        create_subscription
      end
    end

    private
    def create_subscription
      subscription = subscriber.program_subscriptions.build(
        office:         office,
        cooperative:    cooperative,
        program:        program,
        account_number: SecureRandom.uuid)
        create_account(subscription)
        subscription.save!
    end

    def create_account(subscription)
      AccountCreators::ProgramSubscription.new(subscription: subscription).create_accounts!
    end
  end
end
