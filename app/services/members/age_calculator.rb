module Members
  class AgeCalculator
    attr_reader :date_of_birth
    def initialize(date_of_birth:)
      @date_of_birth = date_of_birth
    end

    def age
      ((Time.zone.now - date_of_birth.to_time) / 1.year.seconds).floor
    end
  end
end
