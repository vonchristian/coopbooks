module Members
  class MemberEntryMigration
    attr_reader :member
    def initialize(member:)
      @member = member
    end
    def migrate!
      migrate_loan_entries
      migrate_saving_entries
      migrate_share_capital_entries
      migrate_time_deposit_entries
      migrate_program_subscription_entries

    end
    private
    def migrate_loan_entries
      member.loans.accounts.each do |account|
        account.entries.each do |entry|
          member.member_entries.find_or_create_by(entry: entry, office: entry.office)
        end
      end
    end

    def migrate_saving_entries
      member.savings.liability_accounts.each do |account|
        account.entries.each do |entry|
          member.member_entries.find_or_create_by(entry: entry, office: entry.office)
        end
      end
    end

    def migrate_time_deposit_entries
      member.time_deposits.liability_accounts.each do |account|
        account.entries.each do |entry|
          member.member_entries.find_or_create_by(entry: entry, office: entry.office)
        end
      end
    end

    def migrate_share_capital_entries
      member.share_capitals.equity_accounts.each do |account|
        account.entries.each do |entry|
          member.member_entries.find_or_create_by(entry: entry, office: entry.office)
        end
      end
    end

    def migrate_program_subscription_entries
      member.program_subscriptions.accounts.each do |account|
        account.entries.each do |entry|
          member.member_entries.find_or_create_by(entry: entry, office: entry.office)
        end
      end
    end
  end
end
