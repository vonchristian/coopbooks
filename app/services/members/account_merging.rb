module Members
  class AccountMerging
    attr_reader :current_member, :old_member

    def initialize(current_member:, old_member:)
      @current_member = current_member
      @old_member     = old_member
    end

    def merge_accounts!
      ActiveRecord::Base.transaction do
        merge_accounts
      end
    end

    private
    def merge_accounts
        current_member.savings               << old_member.savings
        current_member.share_capitals        << old_member.share_capitals
        current_member.loans                 << old_member.loans
        current_member.time_deposits         << old_member.time_deposits
        current_member.entries               << old_member.entries
        current_member.program_subscriptions << old_member.program_subscriptions
    end
  end
end
