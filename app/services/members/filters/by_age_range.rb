module Members
  module Filters
    class ByAgeRange
      attr_reader :members, :start_num, :end_num

      def initialize(members:, start_num:, end_num:)
        @members   = members
        @start_num = start_num
        @end_num   = end_num
      end

      def age_range
        (start_num.to_f..end_num.to_f)
      end

      def filtered_members
        members.select{ |a| age_range.include?(a.age) }
      end
    end
  end
end
