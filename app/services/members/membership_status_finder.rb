module Members
  class MembershipStatusFinder
    attr_reader :member, :office, :membership

    def initialize(member:, office:)
      @member     = member
      @office     = office
      @membership = @member.office_memberships.where(office: @office).first
    end

    def status
      membership.status
    end

    def text_color
      if membership.active?
        'success'
      else
        'danger'
      end
    end
  end
end
