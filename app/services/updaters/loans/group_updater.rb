module Updaters
  module Loans
    class GroupUpdater
      def self.update_groups(date: Date.current)
        LoansModule::Loan.not_archived.each do |loan|
          LoansModule::Loans::LoanGroupUpdate.new(loan: loan, date: date).set_loan_group
        end
      end
    end
  end
end
