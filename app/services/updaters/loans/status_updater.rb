module Updaters
  module Loans
    class StatusUpdater
      def self.update_statuses!
        LoansModule::Loan.not_archived.each do |loan|
          LoansModule::Loans::StatusUpdate.new(loan: loan).set_status
        end
      end
    end
  end
end
