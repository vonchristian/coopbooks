module ShareCapitals
  class Opening
    include ActiveModel::Model
    attr_reader :voucher, :share_capital_application, :employee, :subscriber, :cooperative, :office
    def initialize(args)
      @share_capital_application = args.fetch(:share_capital_application)
      @employee                  = args.fetch(:employee)
      @cooperative               = @employee.cooperative
      @office                    = @employee.office
      @subscriber                = @share_capital_application.subscriber
    end

    def open_account!
      ActiveRecord::Base.transaction do
        create_share_capital
      end
    end

    def find_share_capital
      cooperative.share_capitals.find_by(account_number: share_capital_application.account_number)
    end

    private
    def create_share_capital
      share_capital = office.share_capitals.build(
        account_name:          subscriber.name,
        cooperative:           employee.cooperative,
        subscriber:            subscriber,
        account_number:        share_capital_application.account_number,
        date_opened:           share_capital_application.date_opened,
        share_capital_product: share_capital_application.share_capital_product,
        equity_account:        share_capital_application.equity_account)
        AccountCreators::ShareCapital.new(share_capital: share_capital).create_accounts!
        share_capital.save!
    end
  end

end
