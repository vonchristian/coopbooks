module ShareCapitals
  class InterestOnCapitalCalculator
    attr_reader :share_capital, :net_income_distributable

    def initialize(share_capital:, net_income_distributable:)
      @share_capital = share_capital
      @net_income_distributable = net_income_distributable
    end

    def interest
      share_capital.averaged_monthly_balances / net_income_distributable
    end
  end
end
