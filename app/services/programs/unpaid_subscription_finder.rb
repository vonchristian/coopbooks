module Programs
  class UnpaidSubscriptionFinder
    attr_reader :program, :office, :date, :subscriptions

    def initialize(program:, office:, date:)
      @program       = program
      @office        = office
      @date          = date
      @members       = @office.member_memberships
      @subscriptions = @office.program_subscriptions
      .where(program_id: @program.id).
      where(subscriber_id: @members.ids)
    end

    def unpaid_subscriptions
      ids = []
      subscriptions.each do |subscription|
        if subscription.unpaid?(date: date)
          ids << subscription.id
        end
      end
      office.program_subscriptions.where(id: ids.uniq.compact.flatten)
    end

    def members_with_unpaid_subscriptions
      ids = unpaid_subscriptions.pluck(:subscriber_id)
      office.members.where(id: ids.uniq.compact.flatten)
    end
  end
end
