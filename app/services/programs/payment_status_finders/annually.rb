module Programs
  module PaymentStatusFinders
    class Annually
      attr_reader :subscription, :date, :account, :program

      def initialize(args={})
        @subscription = args.fetch(:subscription)
        @date         = args.fetch(:date)
        @account      = @subscription.account
        @program      = @subscription.program
      end

      def paid?
        entry.present? && entry.total == program.amount
      end

      def entry
        account.entries.entered_on(from_date: start_date, to_date: end_date)
      end

      def start_date
        date.beginning_of_year
      end

      def end_date
        date.end_of_year
      end
    end
  end
end
