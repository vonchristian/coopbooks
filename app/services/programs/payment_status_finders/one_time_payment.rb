module Programs
  module PaymentStatusFinders
    class OneTimePayment
      attr_reader :subscription, :account

      def initialize(args={})
        @subscription = args.fetch(:subscription)
        @account      = @subscription.account
      end


      def paid?
        entry.present?
      end

      def entry
        account.entries.first
      end
    end
  end
end
