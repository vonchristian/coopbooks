module Savings
  class InterestCalculator
    attr_reader :amount, :rate
    def initialize(amount:, rate:)
      @amount = amount
      @rate   = rate
    end
    def calculate
      amount * rate
    end
  end
end
