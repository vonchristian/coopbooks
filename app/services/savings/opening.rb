module Savings
  class Opening
    include ActiveModel::Model
    attr_reader :voucher, :savings_account_application, :employee, :office

    def initialize(args)
      @savings_account_application = args.fetch(:savings_account_application)
      @employee                    = args.fetch(:employee)
      @office                      = @employee.office
    end

    def open_account!
      ActiveRecord::Base.transaction do
        create_savings_account
      end
    end

    def find_savings_account
      office.savings.find_by!(account_number: savings_account_application.account_number)
    end

    private

    def create_savings_account
      savings_account = office.savings.build(
        account_name:          find_depositor.name,
        cooperative:           employee.cooperative,
        office:                office,
        depositor:             find_depositor,
        account_number:        savings_account_application.account_number,
        date_opened:           savings_account_application.date_opened,
        saving_product:        savings_account_application.saving_product,
        liability_account:     savings_account_application.liability_account,
        saving_group:          find_saving_group)

      create_accounts(savings_account)
      savings_account.save!
      update_status(savings_account)
    end

    def create_accounts(savings_account)
      AccountCreators::Saving.new(saving: savings_account).create_accounts!
    end

    def update_status(savings_account)
      BalanceStatusChecker.new(account: savings_account, product: savings_account.saving_product).set_balance_status
    end

    def find_depositor
      savings_account_application.depositor
    end

    def find_saving_group
      office.saving_groups.order(start_num: :asc).first
    end
  end

end
