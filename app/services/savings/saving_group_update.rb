module Savings
  class SavingGroupUpdate
    attr_reader :saving, :office, :saving_groups

    def initialize(saving:)
      @saving        = saving
      @office        = @saving.office
      @saving_groups = @office.saving_groups
    end

    def set_group
      saving_groups.each do |saving_group|
        if saving_group.num_range.include?(saving.number_of_days_inactive)
          saving.update!(saving_group: saving_group)
        end
      end
    end
  end
end
