module Vouchers
  class ApproverFinder
    attr_reader :voucher, :office

    def initialize(voucher:)
      @voucher = voucher
      @office  = @voucher.office
    end

    def approver
      office.employees.branch_manager.last || office.employees.general_manager.first 
    end
  end
end
