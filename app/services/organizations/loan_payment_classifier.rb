module Organizations
  class LoanPaymentClassifier
    attr_reader :employee, :loan, :voucher_amounts

    def initialize(args)
      @employee        = args.fetch(:employee)
      @loan            = args.fetch(:loan)
      @voucher_amounts = @employee.voucher_amounts.with_no_vouchers
    end
    def principal_amount
      loan.voucher_amounts.with_no_vouchers.where(account: loan.principal_account).sum(&:amount)
    end

    def interest_amount
      loan.voucher_amounts.with_no_vouchers.where(account: loan.loan_product_interest_revenue_account).sum(&:amount)
    end
    def penalty_amount
      loan.voucher_amounts.with_no_vouchers.where(account: loan.loan_product_penalty_revenue_account).sum(&:amount)
    end
    def total_amount
      principal_amount +
      interest_amount +
      penalty_amount
    end
  end
end
