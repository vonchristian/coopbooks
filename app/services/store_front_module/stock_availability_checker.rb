module StoreFrontModule
  class StockAvailabilityChecker
    attr_reader :stock, :cart

    def initialize(stock:, cart:)
      @stock = stock
      @cart  = cart
    end

    def update_availability!
      if stock.available_stock_for_cart(cart) <= 0
        stock.update!(available: false)
      elsif
        stock.update!(available: true)
      end
    end
  end
end
