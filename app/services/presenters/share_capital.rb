module Presenters
  class ShareCapital
    attr_reader :share_capital, :entry, :equity_account

    def initialize(share_capital:, entry:)
      @share_capital = share_capital
      @entry  = entry
      @equity_account = @share_capital.equity_account
    end

    def chevron
      if entry.credit_amounts.accounts.include?(equity_account)
        'down'
      else
        'up'
      end
    end
    def text_color
      if entry.credit_amounts.accounts.include?(equity_account)
        'success'
      else
        'danger'
      end
    end
    def plus_or_minus_icon
      if entry.credit_amounts.accounts.include?(equity_account)
        'plus'
      else
        'minus'
      end
    end
  end
end
