module ApplicationHelper
  include Pagy::Frontend
  def short_time_ago(time, options={})
     timestamp = time.iso8601
     dist = DateTime.now.to_i - time.to_i
     # Quick (non-i18n friendly hack to always show weeks instead of months (since "m" is ambiguous)
     message =
       if dist.to_i < 6.5.days || dist.to_i > 364.5.days
         time_ago_in_words time
       else
         "#{(dist/1.week).round}w"
       end
     message
     # "<time class='short-timeago timeago' locale='en_abbrev' datetime='#{timestamp}'>#{message} ago</time>".html_safe # for use with jquery timeago
   end
end
