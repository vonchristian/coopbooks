class ShareCapitalsWithMinimumBalancesController < ApplicationController
  def index
    @pagy, @share_capitals = pagy(current_office.share_capitals.has_minimum_balances)
  end
end 
