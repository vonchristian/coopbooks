module ManagementModule
  module Settings
    class SavingProductsController < ApplicationController
      def index
        @saving_products = current_cooperative.saving_products
      end
      def new
        @saving_product = current_cooperative.saving_products.build
      end
      def create
        @saving_product = current_cooperative.saving_products.create(saving_product_params)
        if @saving_product.valid?
          @saving_product.save!
          redirect_to management_module_settings_saving_products_url, notice: 'Saving product saved successfully'
        else
          render :new
        end
      end

      private
      def saving_product_params
        params.require(:cooperatives_saving_product).
        permit(:name, :interest_rate, :minimum_balance, :interest_posting, :liability_account_category_id, :interest_expense_account_category_id, :closing_account_category_id)
      end
    end
  end
end 
