module ManagementModule
  module Settings
    class EmployeeRegistrationsController < ApplicationController
      def new
        @invitation = Employees::Registration.new
      end
      def create
        @invitation = Employees::Registration.new(registration_params)
        if @invitation.valid?
          @invitation.register!
          redirect_to management_module_settings_url, notice: 'Employee created successfully.'
        else
          render :new
        end
      end

      private
      def registration_params
        params.require(:employees_registration).
        permit(:first_name, :last_name, :email, :cooperative_id, :office_id, :password, :password_confirmation)
      end
    end
  end
end
