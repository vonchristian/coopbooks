module ManagementModule
  module Settings
    class LoanProductsController < ApplicationController

      def index
        @loan_products = current_cooperative.loan_products
      end

      def new
        @loan_product = LoansModule::LoanProductRegistration.new
      end

      def create
        @loan_product = LoansModule::LoanProductRegistration.new(loan_product_params)
        if @loan_product.valid?
          @loan_product.register!
          redirect_to management_module_settings_loan_products_url, notice: "Loan product created successfully."
        else
          render :new
        end
      end

      def show
        @loan_product = current_cooperative.loan_products.find(params[:id])
      end

      def edit
        @loan_product = current_cooperative.loan_products.find(params[:id])

      end

      def update
        @loan_product = current_cooperative.loan_products.find(params[:id])
        @loan_product.update(update_loan_product_params)
        if @loan_product.valid?
          @loan_product.save!
          redirect_to management_module_settings_loan_product_url(@loan_product), notice: 'Loan product updated successfully'
        else
          render :edit
        end
      end

      private
      def loan_product_params
        params.require(:loans_module_loan_product_registration).permit(
          :name,
          :description,
          :maximum_loanable_amount,
          :interest_rate,
          :penalty_rate,
          :loan_protection_plan_provider_id,
          :cooperative_id,
          :grace_period,
          :amortization_type_id,
          :interest_calculation_type,
          :prededuction_calculation_type,
          :prededuction_scope,
          :prededucted_rate,
          :prededucted_amount,
          :cooperative_id,
          :prededucted_number_of_payments,
          :receivable_account_category_id,
          :interest_revenue_account_category_id,
          :penalty_revenue_account_category_id)
      end

      def update_loan_product_params
        params.require(:cooperatives_loan_product).permit(
          :name,
          :description,
          :maximum_loanable_amount,
          :loan_protection_plan_provider_id,
          :grace_period,
          :amortization_type_id,
          :interest_calculation_type,
          :receivable_account_category_id,
          :interest_revenue_account_category_id,
          :penalty_revenue_account_category_id)
      end
    end
  end
end
