module ManagementModule
  module Settings
    class TimeDepositProductsController < ApplicationController
      respond_to :html, :json

      def new
        @time_deposit_product = current_cooperative.time_deposit_products.build
      end

      def create
        @time_deposit_product = current_cooperative.time_deposit_products.create(time_deposit_product_params)
        if @time_deposit_product.valid?
          @time_deposit_product.save!
          redirect_to management_module_settings_time_deposit_products_url,
          notice: "Time Deposit product saved successfully."
        else
          render :new
        end
      end

      def show
        @time_deposit_product = current_cooperative.time_deposit_products.find(params[:id])
      end

      private
      def time_deposit_product_params
        params.require(:cooperatives_time_deposit_product).
        permit(:name,
               :interest_rate,
               :minimum_deposit,
               :maximum_deposit,
               :number_of_days,
               :break_contract_fee,
               :break_contract_rate
               )
      end
    end
  end
end
