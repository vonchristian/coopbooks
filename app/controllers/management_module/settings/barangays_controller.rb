module ManagementModule
	module Settings
		class BarangaysController < ApplicationController

			def index
			end
      def new
        @barangay = Addresses::Barangay.new
      end
      def create
        @barangay = Addresses::Barangay.create(barangay_params)
        if @barangay.valid?
          @barangay.save!
          redirect_to management_module_settings_barangays_url, notice: 'Barangay saved successfully'
        else
          render :new
        end
      end

      private
      def barangay_params
        params.require(:addresses_barangay).
        permit(:name, :municipality_id)
      end 
		end
	end
end
