module ManagementModule
  module Settings
    class UsersController < ApplicationController
      def index
        @users = current_cooperative.employees 
      end
    end
  end
end
