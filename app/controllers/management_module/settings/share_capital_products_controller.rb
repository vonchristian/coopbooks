module ManagementModule
  module Settings
    class ShareCapitalProductsController < ApplicationController
      def index
        @share_capital_products = current_cooperative.share_capital_products
      end
      def new
        @share_capital_product = current_cooperative.share_capital_products.build
      end

      def create
        @share_capital_product = current_cooperative.share_capital_products.create(share_capital_product_params)
        if @share_capital_product.valid?
          @share_capital_product.save!
          redirect_to management_module_settings_share_capital_products_url, notice: 'Share Capital Product created successfully.'
        else
          render :new
        end
      end

      private
      def share_capital_product_params
        params.require(:cooperatives_share_capital_product).permit(
                        :name,
                        :cost_per_share,
                        :minimum_share,
                        :minimum_balance)
      end
    end
  end
end
