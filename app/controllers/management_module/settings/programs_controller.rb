module ManagementModule
  module Settings
    class ProgramsController < ApplicationController
      def index
        @programs = current_cooperative.programs
      end
      def new
        @program = current_cooperative.programs.build
      end
      def create
        @program = current_cooperative.programs.create(program_params)
        if @program.valid?
          @program.save!
          redirect_to management_module_settings_programs_url, notice: 'Program saved successfully'
        else
          render :new
        end
      end

      private
      def program_params
        params.require(:cooperatives_program).
        permit(:name, :description, :mode_of_payment, :amount, :account_category_id)
      end
    end
  end
end 
