module ManagementModule
  module Settings
    class OfficesController < ApplicationController
      def index
        @cooperative = current_cooperative
      end
      def new
        @cooperative = current_cooperative
        @office      = @cooperative.offices.build
      end
      def create
        @cooperative = current_cooperative
        @office      = @cooperative.offices.create(office_params)
        if @office.valid?
          @office.save!
          redirect_to management_module_settings_url, notice: "Office saved successfully"
        else
          render :new
        end
      end

      private
      def office_params
        params.require(:cooperatives_office).permit(:type, :contact_number, :address, :name)
      end
    end
  end
end
