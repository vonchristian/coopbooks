module Reports
  module MembersMasterlists
    class ByCivilStatusesController < ApplicationController
      def index
        @civil_status = params[:civil_status]
        @members = current_office.member_memberships.where(civil_status: @civil_status)
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
