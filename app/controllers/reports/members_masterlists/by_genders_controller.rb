module Reports
  module MembersMasterlists
    class ByGendersController < ApplicationController
      def index
        @members = current_office.members.where(sex: params[:sex])
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
