module Reports
  module MembersMasterlists
    class ByAgeRangesController < ApplicationController
      def index
        @filter = Members::Filtering::ByAgeRange.new
        @start_num = params[:starting_number]
        @end_num   = params[:ending_number]
        @members  = Members::Filters::ByAgeRange.new(start_num: @start_num, end_num: @end_num, members: current_office.member_memberships).filtered_members
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
