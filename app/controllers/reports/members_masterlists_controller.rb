module Reports
  class MembersMasterlistsController < ApplicationController
    def index
      @office  = params[:office_id] ? current_cooperative.offices.find(params[:office_id]) : current_office
      @members = @office.member_memberships
      respond_to do |format|
        format.html
        format.xlsx
      end
    end
  end
end
