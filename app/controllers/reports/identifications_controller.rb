module Reports
  class IdentificationsController < ApplicationController
    def index
      @office              = current_office
      @members             = current_office.member_memberships
      if params[:identity_provider_id].present?
        @identity_provider = IdentificationsModule::IdentityProvider.find(params[:identity_provider_id])
      end

      respond_to do |format|
        format.xlsx
        format.html
      end
    end
  end
end
