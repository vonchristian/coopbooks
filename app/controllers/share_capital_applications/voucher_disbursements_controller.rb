module ShareCapitalApplications
  class VoucherDisbursementsController < ApplicationController
    def create
      @share_capital_application = current_office.share_capital_applications.find(params[:share_capital_application_id])
      @voucher = current_office.vouchers.find(params[:voucher_id])
      ActiveRecord::Base.transaction do
        ShareCapitals::Opening.new(share_capital_application: @share_capital_application, employee: current_user, voucher: @voucher).open_account!
        @share_capital = current_office.share_capitals.find_by(account_number: @share_capital_application.account_number)
        Vouchers::EntryProcessing.new(updateable: @share_capital, voucher: @voucher, employee: current_user).process!
        BalanceStatusChecker.new(account: @share_capital, product: @share_capital.share_capital_product).set_balance_status

        redirect_to share_capital_url(id: @share_capital.id), notice: "Share capital account opened successfully."
      end
    end
  end
end
