module MembersModule
  module Reports
    class MembersBySexesController < ApplicationController
      def index
        sex = params[:sex] ? params[:sex] : Member.sexes.keys.first
        @members = current_office.member_memberships.where(sex: sex)
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
