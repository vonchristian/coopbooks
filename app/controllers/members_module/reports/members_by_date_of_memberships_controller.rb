module MembersModule
  module Reports
    class MembersByDateOfMembershipsController < ApplicationController
      def index
        @from_date = params[:from_date] ? Date.parse(params[:from_date]) : Date.current.beginning_of_year
        @to_date   = params[:to_date]   ? Date.parse(params[:to_date])   : Date.current.end_of_year
        date_range = @from_date..@to_date
        @pagy, @members = pagy(current_office.
                   member_memberships.
                   joins(:memberships).
                   where('memberships.cooperative_id' => current_cooperative.id).
                   where('memberships.membership_date' => date_range))
      end
    end
  end
end
