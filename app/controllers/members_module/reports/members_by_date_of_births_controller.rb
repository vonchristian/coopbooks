module MembersModule
  module Reports
    class MembersByDateOfBirthsController < ApplicationController
      def index
        @date           = params[:date] ? DateTime.parse(params[:date]) : Date.current
        @month          = @date.month
        @pagy, @members = pagy(current_office.member_memberships.where(birth_month: @month))
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
