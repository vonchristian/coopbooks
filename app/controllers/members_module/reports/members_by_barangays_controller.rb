module MembersModule
  module Reports
    class MembersByBarangaysController < ApplicationController
      def index
        @barangays        = Addresses::Barangay.all
        @barangay         = params[:barangay_id] ? Addresses::Barangay.find(params[:barangay_id]) : Addresses::Barangay.first
        @barangay_members = @barangay.members
        @members          = current_office.member_memberships.where(id: @barangay_members.ids.uniq)
        respond_to do |format|
          format.xlsx
          format.html
        end
      end
    end
  end
end
