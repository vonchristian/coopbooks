module MembersModule
  module Reports
    class MembersByAgesController < ApplicationController
      def index
        @start_num  = params[:start_num] ? params[:start_num] : 0
        @end_num    = params[:end_num] ? params[:end_num] : 0
        age_range   = @start_num..@end_num
        @pagy, @members = pagy(current_office.member_memberships.where(age: age_range))
      end
    end
  end
end
