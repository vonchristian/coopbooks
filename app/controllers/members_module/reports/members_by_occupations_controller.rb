module MembersModule
  module Reports
    class MembersByOccupationsController < ApplicationController
      def index
        @occupations = Occupation.all
        @occupation  = params[:occupation_id] ? Occupation.find(params[:occupation_id]) : Occupation.first
        @pagy, @members     = pagy(current_office.member_memberships.joins(:occupations).where('occupations.id' => @occupation.id))
      end
    end
  end
end
