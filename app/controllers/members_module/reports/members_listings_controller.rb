module MembersModule
  module Reports
    class MembersListingsController < ApplicationController
      def index
        @members = current_office.member_memberships.order(last_name: :asc)
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
