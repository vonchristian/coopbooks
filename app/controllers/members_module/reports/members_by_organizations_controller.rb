module MembersModule
  module Reports
    class MembersByOrganizationsController < ApplicationController
      def index
        @organizations  = current_office.organizations
        @organization   = params[:organization_id] ? current_office.organizations.find(params[:organization_id]) : current_office.organizations.first
        @pagy, @members = pagy(@organization.member_memberships)
        @all_members    = @organization.member_memberships
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
