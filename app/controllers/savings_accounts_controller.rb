class SavingsAccountsController < ApplicationController
  def index
    @office = current_office
    if params[:search].present?
      @pagy, @savings_accounts = pagy(@office.savings.text_search(params[:search]))
    else
      @pagy, @savings_accounts = pagy(@office.savings.includes(:saving_product, :liability_account, depositor: [:avatar_attachment => :blob]).order(:account_name), items: 25)
    end
    @offices = current_cooperative.offices
  end

  def show
    @savings_account = current_office.savings.find(params[:id])
    if params[:search].present?
      @pagy, @entries = pagy(@savings_account.entries.includes(:recorder).text_search(params[:search]))
    else
      @pagy, @entries = pagy(@savings_account.entries.includes(:recorder).order(entry_date: :desc).includes(:recorder))
    end
  end
end
