module Employees
  class CashCountLineItemsController < ApplicationController
    def new
      @employee = User.find(params[:employee_id])
      @cash_count = CashCountProcessing.new
      @bills = CashCounts::Bill.all
    end
    def create
      @bills = CashCounts::Bill.all
      @cash_count = CashCountProcessing.new(cash_count_params)
      if @cash_count.valid?
        @cash_count.process!
        redirect_to new_employee_cash_count_line_item_url(current_user), notice: 'added successfully.'
      else
        render :new
      end
    end

    private
    def cash_count_params
      params.require(:cash_count_processing).permit(
                      :bill_1000_count,
                      :bill_500_count,
                      :bill_200_count,
                      :bill_100_count,
                      :bill_50_count,
                      :bill_20_count,
                      :bill_10_count,
                      :bill_5_count,
                      :bill_1_count,
                      :bill_1_cent_count,
                      :checks_count,
                      :bill_1000_id,
                      :bill_500_id,
                      :bill_200_id,
                      :bill_100_id,
                      :bill_50_id,
                      :bill_20_id,
                      :bill_10_id,
                      :bill_5_id,
                      :bill_1_id,
                      :bill_1_id,
                      :bill_1_cent_id,
                      :checks_id,
                      :bill_1000_total,
                      :bill_500_total,
                      :bill_200_total,
                      :bill_100_total,
                      :bill_50_total,
                      :bill_20_total,
                      :bill_10_total,
                      :bill_5_total,
                      :bill_1_total,
                      :bill_1_cent_total,
                      :checks_total,
                      :total_count,
                      :cash_account_balance,
                      :difference,
                      :date,
                      :employee_id,
                      :cash_balance)
    end
  end
end
