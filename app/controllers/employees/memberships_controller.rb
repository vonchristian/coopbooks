module Employees
  class MembershipsController < ApplicationController
    def new
      @employee = current_cooperative.users.find(params[:employee_id])
      @membership = @employee.memberships.build
    end

    def create
       @employee = current_cooperative.users.find(params[:employee_id])
      @membership = @employee.memberships.create(membership_params)
      if @membership.valid?
        @membership.save!
        redirect_to employee_settings_url(@employee), notice: "Membership saved successfully."
      else
        render :new
      end
    end

    def edit
      @employee = current_cooperative.users.find(params[:employee_id])
      @membership = @employee.memberships.find(params[:id])
    end
    def update
      @employee   = current_cooperative.users.find(params[:employee_id])
      @membership = @employee.memberships.find(params[:id])
      @membership.update(membership_params)
      if @membership.valid?
        @membership.save!
        redirect_to employee_settings_url(@employee), notice: "Membership saved successfully."
      else
        render :new
      end
    end

    private
    def membership_params
      params.require(:cooperatives_membership).permit(:membership_category_id, :application_date, :approval_date, :account_number, :cooperative_id, :office_id)
    end
  end
end
