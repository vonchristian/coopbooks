module Employees
  module Reports
    class CashBookTransactionsController < ApplicationController
      def index
        @to_date      = Date.parse(params[:to_date])
        @employee     = current_office.employees.find(params[:employee_id])
        @cash_account = @employee.cash_accounts.find(params[:cash_account_id])
        respond_to do |format|
          format.pdf do
            pdf = CashBooks::TransactionsPdf.new(
              entries:      @entries,
              employee:     @employee,
              to_date:      @to_date,
              cash_account: @cash_account,
              view_context: view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Cash Book Report.pdf"
          end
        end
      end
    end
  end
end
