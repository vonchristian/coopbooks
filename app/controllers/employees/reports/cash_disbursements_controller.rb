module Employees
  module Reports
    class CashDisbursementsController < ApplicationController
      def index
        @employee = current_cooperative.users.find(params[:employee_id])
        @from_date = params[:from_date] ? Date.parse(params[:from_date]).beginning_of_day : Date.current.beginning_of_day
        @to_date = params[:to_date] ? Date.parse(params[:to_date]) : Date.curent.end_of_day

        @entries = @employee.cash_accounts.credit_entries.entered_on(from_date: @from_date, to_date: @to_date)
        respond_to do |format|
          format.pdf do
            pdf = CashBooks::DisbursementsPdf.new(
              entries: @entries,
              employee: @employee,
              from_date: @from_date,
              to_date: @to_date,
              title: "Cash Disbursements Journal",
              view_context: view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Cash Disbursement Report.pdf"
          end
        end
      end
    end
  end
end
