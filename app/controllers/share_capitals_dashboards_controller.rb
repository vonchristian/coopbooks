class ShareCapitalsDashboardsController < ApplicationController
  def index
    @share_capitals = current_office.share_capitals
    @accounts       = @share_capitals.equity_accounts.includes(:amounts)
  end
end
