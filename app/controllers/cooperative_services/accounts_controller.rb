module CooperativeServices
  class AccountsController < ApplicationController
    def index
      @cooperative_service = current_office.cooperative_services.find(params[:cooperative_service_id])
    end
    
    def new
      @cooperative_service = current_office.cooperative_services.find(params[:cooperative_service_id])
      if params[:search].present?
        @accounts = AccountingModule::Account.text_search(params[:search]).paginate(page: params[:page], per_page: 50)
      else
        @accounts = AccountingModule::Account.active.order(:code).paginate(page: params[:page], per_page: 50)
      end
    end
  end
end
