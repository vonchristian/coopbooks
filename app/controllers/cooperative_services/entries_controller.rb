module CooperativeServices
  class EntriesController < ApplicationController
    def index
      @cooperative_service = current_office.cooperative_services.
      find(params[:cooperative_service_id])
      @entries = @cooperative_service.entries
    end
  end
end
