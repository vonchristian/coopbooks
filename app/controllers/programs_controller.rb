class ProgramsController < ApplicationController
  def index
    @programs = current_office.programs
  end
  def show
    @program = current_office.programs.find(params[:id])
  end
end
