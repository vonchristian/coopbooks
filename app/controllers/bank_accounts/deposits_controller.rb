module BankAccounts
  class DepositsController < ApplicationController
    def new
      @bank_account = current_office.bank_accounts.find(params[:bank_account_id])
      @deposit = BankAccounts::Deposit.new
    end
    
    def create
      @bank_account = current_office.bank_accounts.find(params[:bank_account_id])
      @deposit = BankAccounts::Deposit.new(entry_params)
      if @deposit.valid?
        @deposit.create_voucher!
        redirect_to bank_account_voucher_url(id: @deposit.find_voucher.id, bank_account_id: @bank_account.id), notice: "Deposit transaction saved successfully."
      else
        render :new
      end
    end

    private

    def entry_params
      params.require(:bank_accounts_deposit).
      permit(:bank_account_id, :employee_id, :amount, :description,
      :reference_number, :account_number, :date, :payment_type,
       :offline_receipt, :cash_account_id, :account_number)
    end
  end
end
