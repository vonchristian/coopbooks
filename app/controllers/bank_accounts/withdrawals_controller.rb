module BankAccounts
  class WithdrawalsController < ApplicationController
    def new
      @bank_account = current_office.bank_accounts.find(params[:bank_account_id])
      @withdrawal = BankAccounts::Withdraw.new
    end
    def create
      @bank_account = current_office.bank_accounts.find(params[:bank_account_id])
      @withdrawal = BankAccounts::Withdraw.new(withdrawal_params)
      if @withdrawal.valid?
        @withdrawal.create_voucher!
        redirect_to bank_account_voucher_url(id: @withdrawal.find_voucher.id, bank_account_id: @bank_account.id), notice: "Entry saved successfully."
      else
        render :new
      end
    end

    private
    def withdrawal_params
      params.require(:bank_accounts_withdraw).
      permit(:bank_account_id, :employee_id, :amount, :description,
      :reference_number, :account_number, :date,
        :cash_account_id, :account_number)
    end
  end
end
