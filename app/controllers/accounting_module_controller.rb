class AccountingModuleController < ApplicationController
  def index
    @revenues = current_cooperative.accounts.revenues
    @expenses = current_cooperative.accounts.expenses
  end
end
