class LoanGroupsController < ApplicationController
  def show
    @loan_group = current_office.loan_groups.find(params[:id])
    @pagy, @loans = pagy(@loan_group.loans)
  end
end
