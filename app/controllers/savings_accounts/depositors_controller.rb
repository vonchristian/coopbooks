module SavingsAccounts
  class DepositorsController < ApplicationController
    def index
      @savings_account = current_office.savings.find(params[:savings_account_id])
      @depositors      = @savings_account.depositors 
    end
  end
end
