module SavingsAccounts
  class WithdrawalsController < ApplicationController
    def new
      @savings_account = current_cooperative.savings.find(params[:savings_account_id])
      @withdrawal = ::Savings::Withdraw.new
      authorize [:savings_accounts, :withdrawal]
    end
    def create
      @savings_account = current_cooperative.savings.find(params[:savings_account_id])
      @withdrawal = ::Savings::Withdraw.new(withdrawal_params)
      authorize [:savings_accounts, :withdrawal]
      if @withdrawal.valid?
        @withdrawal.create_voucher!
        redirect_to savings_account_withdrawal_voucher_path(savings_account_id: @savings_account.id, id: @withdrawal.find_voucher.id), notice: "Withdraw transaction created successfully."
      else
         render :new
      end
    end

    private
    def withdrawal_params
      params.require(:savings_withdraw).
      permit(:amount, :or_number, :date, :saving_id, :employee_id,   :cash_account_id, :account_number)
    end
  end
end
