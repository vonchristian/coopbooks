module SavingsAccounts
  class LockInPeriodsController < ApplicationController
    def new
      @savings_account = current_office.savings.find(params[:savings_account_id])
      @lock_in_period  = SavingsAccounts::LockInPeriod.new
    end

     def create
      @savings_account = current_office.savings.find(params[:savings_account_id])
      @lock_in_period  = SavingsAccounts::LockInPeriod.new(lock_in_period_params)
      if @lock_in_period.valid?
        @lock_in_period.process!
        redirect_to savings_account_settings_url(@savings_account), notice: 'Lock in period saved successfully'
      else
        render :new
      end
    end

    private
    def lock_in_period_params
      params.require(:savings_accounts_lock_in_period).
      permit(:savings_account_id, :effectivity_date, :maturity_date, :term)
    end
  end
end
