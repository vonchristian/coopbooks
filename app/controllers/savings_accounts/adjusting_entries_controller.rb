module SavingsAccounts
  class AdjustingEntriesController < ApplicationController
    def index
      @savings_account    = current_office.savings.find(params[:savings_account_id])
      @depositor          = @savings_account.depositor
      @loans              = @depositor.loans
      @subscriptions      = @depositor.program_subscriptions
      @voucher_processing = SavingsAccounts::AdjustingEntries::VoucherProcessing.new
    end
  end
end
