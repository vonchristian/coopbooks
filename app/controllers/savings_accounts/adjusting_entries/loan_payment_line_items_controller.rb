module SavingsAccounts
  module AdjustingEntries
    class LoanPaymentLineItemsController < ApplicationController
      def new
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @depositor       = @savings_account.depositor
        @loan            = @depositor.loans.find(params[:loan_id])
        @payment         = SavingsAccounts::AdjustingEntries::LoanPayment.new
      end

      def create
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @depositor       = @savings_account.depositor
        @loan            = @depositor.loans.find(params[:savings_accounts_adjusting_entries_loan_payment][:loan_id])
        @payment         = SavingsAccounts::AdjustingEntries::LoanPayment.new(payment_params)
        if @payment.valid?
          @payment.save_payment!
          redirect_to savings_account_adjusting_entries_path(@savings_account), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def payment_params
        params.require(:savings_accounts_adjusting_entries_loan_payment).
        permit(:cart_id, :employee_id, :loan_id, :savings_account_id, :principal_amount, :interest_amount, :penalty_amount)
      end
    end
  end
end
