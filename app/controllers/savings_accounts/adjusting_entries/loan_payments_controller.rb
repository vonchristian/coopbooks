module SavingsAccounts
  module AdjustingEntries
    class LoanPaymentsController < ApplicationController
      def new
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @depositor       = @savings_account.depositor
        @loans           = @depositor.loans
      end
    end
  end
end
