module SavingsAccounts
  module AdjustingEntries
    class LoanPaymentProcessingsController < ApplicationController
      def new
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @entry           = SavingsAccounts::AdjustingEntries::LoanPaymentProcessing.new
      end

       def create
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @entry           = SavingsAccounts::AdjustingEntries::LoanPaymentProcessing.new(payment_params)
        if @entry.valid?
          @entry.create_entry!
          redirect_to savings_account_url(@savings_account), notice: 'Adjusting entry saved successfully.'
        else
          render :new
        end
      end

      private
      def payment_params
        params.require(:savings_accounts_adjusting_entries_loan_payment_processing).
        permit(:date, :reference_number, :description, :cart_id, :employee_id)
      end
    end
  end
end


