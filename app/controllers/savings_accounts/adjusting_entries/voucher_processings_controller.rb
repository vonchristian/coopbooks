module SavingsAccounts
  module AdjustingEntries
    class VoucherProcessingsController < ApplicationController
      def create
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher_processing = SavingsAccounts::AdjustingEntries::VoucherProcessing.new(voucher_params)
        if @voucher_processing.valid?
          @voucher_processing.create_voucher!
          redirect_to savings_account_adjusting_entry_voucher_confirmation_url(savings_account_id: @savings_account.id, id: @voucher_processing.find_voucher.id), notice: 'Transaction created successfully.'
        else
          redirect_to savings_account_adjusting_entries_url(@savings_account), alert: 'Error'
        end
      end

      private
      def voucher_params
        params.require(:savings_accounts_adjusting_entries_voucher_processing).
        permit(:savings_account_id, :account_number, :date, :reference_number, :description, :cart_id, :employee_id)
      end
    end
  end
end
