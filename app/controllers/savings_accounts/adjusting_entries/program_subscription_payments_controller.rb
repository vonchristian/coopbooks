module SavingsAccounts
  module AdjustingEntries
    class ProgramSubscriptionPaymentsController < ApplicationController
      def new
        @savings_account      = current_office.savings.find(params[:savings_account_id])
        @program_subscription = current_office.program_subscriptions.find(params[:program_subscription_id])
        @payment              = SavingsAccounts::AdjustingEntries::ProgramSubscriptionPayment.new
      end
      def create
        @savings_account      = current_office.savings.find(params[:savings_account_id])
        @program_subscription = current_office.program_subscriptions.find(params[:savings_accounts_adjusting_entries_program_subscription_payment][:program_subscription_id])
        @payment              = SavingsAccounts::AdjustingEntries::ProgramSubscriptionPayment.new(amount_params)
        if @payment.valid?
          @payment.create_amounts!
          redirect_to savings_account_adjusting_entries_url(@savings_account), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def amount_params
        params.require(:savings_accounts_adjusting_entries_program_subscription_payment).
        permit(:cart_id, :savings_account_id, :program_subscription_id, :employee_id, :amount)
      end
    end
  end
end
