module SavingsAccounts
  module AdjustingEntries
    class AdjustingEntryVoucherAmountsController < ApplicationController
      def destroy
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher_amount = current_cart.voucher_amounts.find(params[:id])
        @voucher_amount.destroy
        redirect_to savings_account_adjusting_entries_url(@savings_account), notice: 'removed successfully'
      end
    end
  end
end
