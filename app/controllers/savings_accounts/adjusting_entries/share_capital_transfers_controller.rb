module SavingsAccounts
  module AdjustingEntries
    class ShareCapitalTransfersController < ApplicationController
      def new
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @share_capital   = current_office.share_capitals.find(params[:share_capital_id])
        @transfer        = SavingsAccounts::AdjustingEntries::ShareCapitalTransfer.new
      end

      def create
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @share_capital   = current_office.share_capitals.find(params[:savings_accounts_adjusting_entries_share_capital_transfer][:share_capital_id])
        @transfer        = SavingsAccounts::AdjustingEntries::ShareCapitalTransfer.new(amount_params)
        if @transfer.valid?
          @transfer.transfer_amounts!
          redirect_to savings_account_adjusting_entries_url(@savings_account), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def amount_params
        params.require(:savings_accounts_adjusting_entries_share_capital_transfer).
        permit(:cart_id, :employee_id, :savings_account_id, :share_capital_id, :amount)
      end
    end
  end
end
