module SavingsAccounts
  module AdjustingEntries
    class AdjustingEntryVoucherConfirmationsController < ApplicationController
      def show
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher         = current_office.vouchers.find(params[:id])
      end

      def create
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher         = current_office.vouchers.find(params[:voucher_id])
        Vouchers::EntryProcessing.new(voucher: @voucher, employee: current_user).process!
        redirect_to savings_account_url(@savings_account), notice: "Transaction confirmed successfully."
      end

      def destroy
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher         = current_office.vouchers.find(params[:id])
        if !@voucher.disbursed?
          @voucher.destroy
          redirect_to savings_account_adjusting_entries_url(@savings_account), notice: 'Transaction cancelled successfully.'
        end
      end
    end
  end
end
