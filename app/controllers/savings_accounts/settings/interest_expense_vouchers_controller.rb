module SavingsAccounts
  module Settings
    class InterestExpenseVouchersController < ApplicationController
      def show
        @savings_account = current_office.savings.find(params[:savings_account_id])
        @voucher         = current_office.vouchers.find(params[:id])
      end
    end
  end
end
