module SavingsAccounts
  module Settings
    class InterestExpensesController < ApplicationController
      def new
        @savings_account  = current_office.savings.find(params[:savings_account_id])
        @interest_expense = Savings::InterestExpense.new
      end

      def create
        @savings_account  = current_office.savings.find(params[:savings_account_id])
        @interest_expense = Savings::InterestExpense.new(expense_params)
        if @interest_expense.valid?
          @interest_expense.process!
          redirect_to savings_account_interest_expense_voucher_path(savings_account_id: @savings_account.id, id: @interest_expense.find_voucher.id), notice: 'created successfully'
        else
          render :new
        end
      end

      private
      def expense_params
        params.require(:savings_interest_expense).
        permit(:date, :reference_number, :description, :amount, :savings_account_id, :account_number, :employee_id)
      end 
    end
  end
end
