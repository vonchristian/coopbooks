require 'will_paginate/array'
class OrganizationsController < ApplicationController

  def index
    if params[:search].present?
      @pagy, @organizations = pagy(current_office.organizations.includes(:member_memberships, :employee_memberships, :avatar_attachment =>[:blob]).text_search(params[:search]))
    else
      @pagy, @organizations = pagy(current_office.organizations.includes(:member_memberships, :employee_memberships, :avatar_attachment =>[:blob]).order(:abbreviated_name))
    end
  end

  def new
    @organization = current_office.organizations.build
  end

  def create
    @organization = current_office.organizations.create(organization_params)
    if @organization.valid?
      @organization.save!
      redirect_to organization_url(@organization), notice: "Organization created successfully."
    else
      render :new
    end
  end

  def edit
    @organization = current_office.organizations.find(params[:id])
  end

  def update
    @organization = current_office.organizations.find(params[:id])
    if @organization.update(organization_params)
      redirect_to organization_url(@organization), notice: "Organization updated successfully."
    else
      render :new
    end
  end

  def show
    @organization = current_office.organizations.find(params[:id])
    if params[:membership_type].present?
      @members = @organization.member_memberships.select{|m| m.current_membership.membership_type == params[:membership_type]}.paginate(page: params[:page], per_page: 25)
    elsif params[:search].present?
      @members = @organization.member_memberships.text_search(params[:search]).paginate(page: params[:page], per_page: 25)
    else
      @members = @organization.member_memberships.uniq.paginate(page: params[:page], per_page: 25)
    end
  end

  private
  def organization_params
    params.require(:organization).permit(:name, :abbreviated_name, :avatar)
  end
end
