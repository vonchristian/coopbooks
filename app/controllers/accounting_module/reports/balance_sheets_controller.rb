module AccountingModule
  module Reports
    class BalanceSheetsController < ApplicationController
      def index
        first_entry = current_cooperative.entries.order('entry_date ASC').first
        @from_date  = first_entry ? DateTime.parse(first_entry.entry_date.strftime("%B %e, %Y")) : Time.zone.now
        @to_date    = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.zone.now
        @office     = params[:office_id] ? current_cooperative.offices.find(params[:office_id]) : current_office
        @cooperative = current_cooperative
        respond_to do |format|
          format.xlsx
          format.html # index.html.erb
          format.pdf do
            pdf = AccountingModule::Reports::BalanceSheetPdf.new(
              from_date:    @from_date,
              to_date:      @to_date,
              view_context: view_context,
              office:       @office,
              cooperative:  current_cooperative)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Balance Sheet.pdf"
          end
        end
      end
    end
  end
end
