require 'chronic'
module AccountingModule
  module Reports
    class IncomeStatementsController < ApplicationController
      def index
        @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Date.current.beginning_of_month
        @to_date   = params[:to_date]   ? DateTime.parse(params[:to_date]) : Date.current
        @office    = params[:office_id] ? current_cooperative.offices.find(params[:office_id]) : current_office
        @employee  = current_user

        respond_to do |format|
          format.html # index.html.erb
          format.xlsx
          format.pdf do
            pdf = AccountingModule::Reports::IncomeStatementPdf.new(
              from_date:    @from_date,
              to_date:      @to_date,
              revenues:     @revenues,
              expenses:     @expenses,
              view_context: view_context,
              office:       @office,
              employee:     @employee,
              cooperative:  current_cooperative)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Income Sheet.pdf"
          end
        end
      end
    end
  end
end
