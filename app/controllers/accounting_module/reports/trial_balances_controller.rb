module AccountingModule
  module Reports
    class TrialBalancesController < ApplicationController
      def index
        @from_date  = params[:from_date] ? DateTime.parse(params[:from_date]) : Date.current
        @to_date    = params[:to_date] ? DateTime.parse(params[:to_date]) : Date.current
        respond_to do |format|
          format.html
          format.xlsx
          format.pdf do
            pdf = AccountingModule::Reports::TrialBalancePdf.new(
              from_date:    @from_date,
              to_date:      @to_date,
              revenues:     @revenues,
              employee:     @employee,
              view_context: view_context,
              office:       current_office,
              cooperative:  current_cooperative)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Income Sheet.pdf"
          end
        end
      end
    end
  end
end
