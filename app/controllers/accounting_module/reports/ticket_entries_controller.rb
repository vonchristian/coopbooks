module AccountingModule
  module Reports
    class TicketEntriesController < ApplicationController
      def index
        @from_date      = params[:from_date] ? DateTime.parse(params[:from_date]) : Date.current.beginning_of_month
        @to_date        = params[:to_date]   ? DateTime.parse(params[:to_date]) : Date.current
        @office         = params[:office_id] ? current_cooperative.offices.find(params[:office_id]) : current_user.office
        @pagy, @entries = pagy(@office.entries.includes(:recorder).entered_on(from_date: @from_date, to_date: @to_date).distinct.order(entry_date: :desc))

        respond_to do |format|
          format.html
          format.xlsx
          format.pdf do
            pdf = AccountingModule::EntriesPdf.new(
              from_date:    @from_date,
              to_date:      @to_date,
              entries:      @office.entries.entered_on(from_date: @from_date, to_date: @to_date),
              updated_accounts: @office.accounts.updated_at(from_date: @from_date, to_date: @to_date),
              view_context: view_context,
              office:       @office,
              employee:     @employee,
              title: "Entries",
              cooperative:  current_cooperative)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Income Sheet.pdf"
          end

        end
      end
    end
  end
end
