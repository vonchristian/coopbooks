module AccountingModule
  module Reports
    class EquityChangesStatementsController < ApplicationController
      def index
        @categories = current_cooperative.account_categories.equities
        @from_date  = params[:to_date]   ? DateTime.parse(params[:to_date]) : Date.current
        @to_date    = params[:to_date]   ? DateTime.parse(params[:to_date]) : Date.current
      end
    end
  end
end
