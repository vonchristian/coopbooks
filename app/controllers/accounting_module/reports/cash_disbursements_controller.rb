module AccountingModule
  module Reports
    class CashDisbursementsController < ApplicationController
      def index
        @from_date = params[:from_date] ? Date.parse(params[:from_date]) : Date.current
        @to_date   = params[:to_date] ? Date.parse(params[:to_date]) : Date.current
        @cash_account = params[:cash_account_id] ? current_cooperative.cash_accounts.find(params[:cash_account_id]) : current_cooperative.cash_accounts.first

        if params[:from_date] && params[:to_date]
          @pagy, @entries = pagy(@cash_account.credit_entries.includes(:commercial_document, :recorder).entered_on(from_date: @from_date, to_date: @to_date))
        else
          @pagy, @entries = pagy(@cash_account.credit_entries.includes(:commercial_document, :recorder))
        end
      end
    end
  end
end
