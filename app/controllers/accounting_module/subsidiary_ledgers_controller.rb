module AccountingModule
  class SubsidiaryLedgersController < ApplicationController
    def index
      if params[:search].present?
        @pagy, @categories = pagy(current_office.account_categories.text_search(params[:search]))
      else
        @pagy, @categories = pagy(current_office.account_categories)
      end
    end
  end
end
