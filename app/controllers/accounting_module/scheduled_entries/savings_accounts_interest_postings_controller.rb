module AccountingModule
  module ScheduledEntries
    class SavingsAccountsInterestPostingsController < ApplicationController

      def index
        @saving_products = current_office.saving_products
      end

      def new
        @interest_expense_posting = AccountingModule::Entries::SavingsInterestExpenseEntry.new
        @saving_product           = current_office.saving_products.find(params[:saving_product_id])
        @pagy, @savings_accounts  = pagy(@saving_product.savings.includes(:depositor, :liability_account, :interest_expense_account))
        @from_date                  = params[:from_date] ? DateTime.parse(params[:to_date]) : Date.today.beginning_of_quarter
        @to_date                  = params[:to_date] ? DateTime.parse(params[:to_date]) : Date.today.end_of_quarter

      end
      def create
        @interest_expense_posting = AccountingModule::Entries::SavingsInterestExpenseEntry.new(voucher_params)
        @interest_expense_posting.process!
        redirect_to accounting_module_interest_expense_voucher_url(@interest_expense_posting.find_voucher), url: "Vouchers created succesfully."
      end

      private
      def voucher_params
        params.require(:accounting_module_entries_savings_interest_expense_entry).
        permit(:date, :cooperative_id, :account_number, :employee_id, :reference_number, :description)
      end
    end
  end
end
