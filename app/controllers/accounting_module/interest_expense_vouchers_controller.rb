module AccountingModule
  class InterestExpenseVouchersController < ApplicationController
    def show
      @voucher                 = Voucher.find(params[:id])
      @pagy, @voucher_amounts  = pagy(@voucher.voucher_amounts)
    end
  end
end
