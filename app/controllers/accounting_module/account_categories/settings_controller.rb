module AccountingModule
  module AccountCategories
    class SettingsController < ApplicationController
      def index
        @account_category = current_cooperative.account_categories.find(params[:account_category_id])
      end
    end
  end
end
