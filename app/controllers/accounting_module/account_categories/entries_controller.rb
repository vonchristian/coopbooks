module AccountingModule
  module AccountCategories
    class EntriesController < ApplicationController
      def index
        @account_category = current_cooperative.account_categories.find(params[:account_category_id])
        @pagy, @entries   = pagy(@account_category.entries.distinct)
      end
    end
  end
end
