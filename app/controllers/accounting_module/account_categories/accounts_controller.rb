module AccountingModule
  module AccountCategories
    class AccountsController < ApplicationController
      def index
        @account_category = current_cooperative.account_categories.find(params[:account_category_id])
        @pagy, @accounts   = pagy(@account_category.accounts)
      end
    end
  end
end
