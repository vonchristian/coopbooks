module AccountingModule
  module AccountCategories
    class SubCategoriesController < ApplicationController
      def index
        @account_category      = current_cooperative.account_categories.find(params[:account_category_id])
        @pagy, @sub_categories = pagy(@account_category.sub_categories)
      end
    end
  end
end 
