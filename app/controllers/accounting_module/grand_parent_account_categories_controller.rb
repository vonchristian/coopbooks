module AccountingModule
  class GrandParentAccountCategoriesController < ApplicationController
    def index
      @categories = current_cooperative.grand_parent_account_categories
    end
    def new
      @category = AccountingModule::GrandParentAccountCategoryRegistration.new
    end
    def create
      @category = AccountingModule::GrandParentAccountCategoryRegistration.new(category_params)
      if @category.valid?
        @category.register!
        redirect_to accounting_module_grand_parent_account_categories_url, notice: 'Category saved successfully.'
      else
        render :new
      end
    end

    private
    def category_params
      params.require(:accounting_module_grand_parent_account_category_registration).
      permit(:title, :code, :contra, :type, :cooperative_id)
    end 
  end
end
