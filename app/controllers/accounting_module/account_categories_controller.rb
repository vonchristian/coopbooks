module AccountingModule
  class AccountCategoriesController < ApplicationController
    def show
      @account_category = current_office.account_categories.find(params[:id])
    end
  end
end
