require 'pagy/extras/array'
module AccountingModule
  module Settings
    class AccountCategoriesController < ApplicationController
      def index
        @pagy, @account_categories = pagy_array(current_office.account_categories)
      end

      def new
        @account_category = AccountingModule::AccountCategoryRegistration.new
      end
      def create
        @account_category = AccountingModule::AccountCategoryRegistration.new(new_category_params)
        if @account_category.valid?
          @account_category.register!
          redirect_to accounting_module_account_categories_url, notice: 'Account Category created successfully'
        else
          render :new
        end
      end

      def show
        @account_category = current_office.account_categories.find(params[:id])
      end

      def edit
        @account_category = current_office.account_categories.find(params[:id])
      end

      def update
        @account_category = current_office.account_categories.find(params[:id])
        @account_category.update(category_params)
        if @account_category.valid?
          @account_category.save!
          redirect_to accounting_module_account_category_url(@account_category), notice: 'updated successfully'
        else
          render :edit
        end
      end

      private
      def new_category_params
        params.require(:accounting_module_account_category_registration).
        permit(:title, :type, :code, :office_id)
      end

      def category_params
          params.require(type_params).
          permit(:title, :type, :code, :contra)

      end
      def type_params
        @account_category.type.underscore.parameterize.gsub("-", "_").to_sym
      end
    end
  end
end
