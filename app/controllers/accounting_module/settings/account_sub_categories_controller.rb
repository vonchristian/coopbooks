module AccountingModule
  module Settings
    class AccountSubCategoriesController < ApplicationController
      def new
        @account_category     = current_cooperative.account_categories.find(params[:account_category_id])
        @account_sub_category = AccountingModule::AccountCategoryRegistration.new
      end
      def create
        @account_category     = current_cooperative.account_categories.find(params[:account_category_id])
        @account_sub_category = AccountingModule::AccountCategoryRegistration.new(sub_category_params)
        if @account_sub_category.valid?
          @account_sub_category.register!
          AccountingModule::SubCategoryRegistration.new(main_category: @account_category, sub_category: @account_sub_category.find_category).create_sub_category!
          redirect_to accounting_module_account_category_url(@account_category), notice: 'Sub Category created successfully'
        else
          render :new
        end
      end

      private
      def sub_category_params
        params.require(:accounting_module_account_category_registration).
        permit(:title, :office_id, :code, :type)
      end
    end
  end
end
