module AccountingModule
  module Dashboards
    class IncomeStatementDashboardsController < ApplicationController
      def index
        @accounts = current_office.accounts.includes(:entries, :amounts=>[:entry])
      end
    end
  end
end
