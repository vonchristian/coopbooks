module AccountingModule
  module Dashboards
    class AssetsDashboardsController < ApplicationController
      def index
        @assets      = current_office.accounts.assets
        @liabilities = current_office.accounts.liabilities
        @equities    = current_office.accounts.equities

      end
    end
  end
end
