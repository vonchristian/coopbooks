require 'will_paginate/array'
module AccountingModule
  class EntriesController < ApplicationController


    def index
      if params[:search].present?
        @pagy, @entries = pagy(current_office.entries.includes(:recorder).order(entry_date: :desc).order(reference_number: :desc).text_search(params[:search]))
      else
        @pagy, @entries = pagy(current_office.entries.includes(:recorder, :debit_amounts =>[:account =>[:level_one_account_category]], :credit_amounts =>[:account =>[:level_one_account_category]]).order(entry_date: :desc).order(reference_number: :desc))
      end
    end

    def new
      @line_item = Vouchers::VoucherAmountProcessing.new
    end

    def create
      @line_item = Vouchers::VoucherAmountProcessing.new(entry_params)
      if @line_item.valid?
        @line_item.save
        redirect_to new_accounting_module_entry_line_item_url, notice: "Entry saved successfully"
      else
        render :new
      end
    end

    def edit
      @entry = current_cooperative.entries.find(params[:id])
    end

    def update
      @entry = current_cooperative.entries.find(params[:id])
      @entry_form = AccountingModule::Entries::UpdateProcessing.new(edit_entry_params)
      if @entry_form.valid?
        @entry_form.process!
        redirect_to accounting_module_entry_url(@entry), notice: "Entry updated successfully"
      else
        render :edit
      end
    end

    def show
      @entry          = current_cooperative.entries.find(params[:id])
      @pagy, @amounts = pagy(@entry.amounts.includes(:account =>[:level_one_account_category]))
    end

    def destroy
      @entry = current_cooperative.entries.find(params[:id])
      @entry.destroy
      redirect_to accounting_module_entries_url, notice: "Entry destroyed successfully."
    end

    private
    def entry_params
      params.require(:accounting_module_entry_form).permit(:recorder_id, :amount, :debit_account_id, :credit_account_id, :entry_date, :description, :reference_number, :entry_type)
    end

    def edit_entry_params
      params.require(:accounting_module_entry).
      permit(:recorder_id, :reference_number, :description, :entry_date, :entry_id)
    end
  end
end
