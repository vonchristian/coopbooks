module AccountingModule
  class AccountsController < ApplicationController
    respond_to :html, :json
    before_action :set_type
    before_action :set_account, only: [:edit, :update]

    def index
      if params[:search].present?
        @pagy, @accounts = pagy(current_office.accounts.text_search(params[:search]))
      else
        @pagy, @accounts = pagy(current_office.accounts)
      end
    end

    def new
      @account = current_office.accounts.build
      authorize [:accounting_module, :account]
    end

    def create
      @account = current_office.accounts.create(account_params)
      authorize [:accounting_module, :account]
      if @account.valid?
        @account.save!
        redirect_to accounting_module_accounts_url, notice: 'Account created successfully'
      else
        render :new
      end
    end

    def show
      @account = current_office.accounts.find(params[:id])
    end

    def edit
      @account = type_class.find(params[:id])
      respond_modal_with @account
    end

    def update
      @account = current_office.accounts.find(params[:id])
      @account.update(account_params)
      respond_modal_with @account,
        location: accounting_module_account_url(@account)
    end

    private
    def set_type
       @type = type
    end

    def set_account
      @account = type_class.find(params[:id])
    end

    def type
        current_office.accounts.types.include?(params[:type]) ? params[:type] : "AccountingModule::Account"
    end

    def type_class
      type.constantize
    end

    def account_params
      if @account && @account.type == "AccountingModule::Asset"
        params.require(:accounting_module_asset).permit(:name, :code, :type, :contra,  :account_number, :account_category_id)
      elsif @account && @account.type == "AccountingModule::Equity"
        params.require(:accounting_module_equity).permit(:name, :code, :type, :contra, :account_number, :account_category_id)
      elsif @account &&  @account.type == "AccountingModule::Liability"
        params.require(:accounting_module_liability).permit(:name, :code, :type, :contra, :account_number, :account_category_id)
      elsif  @account &&  @account.type == "AccountingModule::Revenue"
        params.require(:accounting_module_revenue).permit(:name, :code, :type, :contra, :account_number, :account_category_id)
      elsif  @account &&  @account.type == "AccountingModule::Expense"
        params.require(:accounting_module_expense).permit(:name, :code, :type, :contra, :account_number, :account_category_id)
      else
        params.require(:accounting_module_account).permit(:name, :code, :type, :contra, :account_number, :account_category_id)

      end
    end
    def type_params
    end
  end
end
