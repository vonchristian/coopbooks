module AccountingModule
  class GeneralLedgersController < ApplicationController
    def index
      if params[:search].present?
        @pagy, @categories = pagy(current_office.account_categories.text_search(params[:search]))
      else
        @from_date         = params[:from_date] ? Date.parse(params[:from_date]) : Date.current
        @to_date           = params[:to_date] ? Date.parse(params[:to_date]) : Date.current

        @pagy, @categories = pagy(current_office.account_categories.order(code: :asc))
        @all_categories = current_office.account_categories.order(code: :asc)

      end

      respond_to do |format|
        format.html
        format.xlsx
      end
    end
  end
end
