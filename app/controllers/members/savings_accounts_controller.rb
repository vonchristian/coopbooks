module Members
  class SavingsAccountsController < ApplicationController
    def index
      @member           = current_cooperative.member_memberships.find(params[:member_id])
      @savings_accounts = @member.savings
    end
  end
end
