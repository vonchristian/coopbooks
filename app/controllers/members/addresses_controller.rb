module Members
  class AddressesController < ApplicationController
    respond_to :html, :json

    def new
      @member = current_office.member_memberships.find(params[:member_id])
      @address = Members::Address.new
    end

    def create
      @member = current_office.member_memberships.find(params[:member_id])
      @address = Members::Address.new(address_params)
      if @address.valid?
        @address.register!
        redirect_to member_path(@member), notice: 'Address updated successfully.'
      else
        render :new
      end
    end

    private
    def address_params
      params.require(:members_address).permit(:complete_address, :street_id, :barangay_id, :municipality_id, :province_id, :current, :member_id)
    end
  end
end
