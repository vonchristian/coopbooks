module Members
  class InfosController < ApplicationController
    def new
      @member      = current_office.member_memberships.find(params[:member_id])
      @update_info = Members::UpdateInfo.new
    end

    def create
      @member      = current_office.member_memberships.find(params[:member_id])
      @update_info = Members::UpdateInfo.new(member_params)
      if @update_info.valid?
        @update_info.process!
        redirect_to member_settings_url(@member), notice: 'Member updated successfully.'
      else
        render :new
      end
    end
    private

    def member_params
      params.require(:members_update_info).
      permit(:first_name, :middle_name, :last_name, :date_of_birth, :sex, :civil_status,
      :complete_address, :street_id, :barangay_id, :member_id,
      :contact_number, :cooperative_id, :avatar,
      :membership_date, :membership_category_id)
    end
  end
end
