module Members
  class MergingLineItemsController < ApplicationController
    def new
      @current_member = current_cooperative.member_memberships.find(params[:member_id])
      authorize [:members, :account_merging]
      if params[:search].present?
        @members = current_cooperative.member_memberships.where.not(id: @current_member.id).text_search(params[:search]).paginate(page: params[:page], per_page: 20)
      else
        @members = current_cooperative.member_memberships.where.not(id: @current_member.id).text_search(@current_member.last_name).paginate(page: params[:page], per_page: 20)
      end
    end
    def create
      @current_member = current_cooperative.member_memberships.find(params[:id])
      @old_member = current_cooperative.member_memberships.find(params[:old_member_id])
      Members::AccountMerging.new(current_member: @current_member, old_member: @old_member).merge_accounts!
      redirect_to member_url(@current_member), notice: 'Accounts merged successfully.'
    end

    def show
      @current_member = current_cooperative.member_memberships.find(params[:id])
      @old_member = current_cooperative.member_memberships.find(params[:old_member_id])
    end

    private
    def merging_line_item_params
      params.require(:members_account_merging).
      permit(:cart_id, :old_member_id)
    end
  end
end
