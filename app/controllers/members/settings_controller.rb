module Members
  class SettingsController < ApplicationController
    def index
      @member = current_office.member_memberships.find(params[:member_id])
    end
  end
end
