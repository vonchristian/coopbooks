module Members
  class ProgramSubscriptionsController < ApplicationController
    def create
      @member = current_cooperative.member_memberships.find(params[:member_id])
      @program = current_office.programs.find(params[:program_id])
      Memberships::ProgramSubscriptionProcessing.new(
        subscriber: @member,
        office: current_office,
        cooperative: current_cooperative,
        program: @program).process!
      redirect_to member_subscriptions_url(@member), notice: "Subscribed successfully."
    end
  end
end
