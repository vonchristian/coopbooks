module Members
  class LoansController < ApplicationController
    def index
      @member            = current_office.member_memberships.find(params[:member_id])
      @pagy, @loans      = pagy(@member.loans.includes(:loan_product))
      @loan_applications = @member.loan_applications.includes(:voucher, :loan_product).paginate(page: params[:page], per_page: 20)
    end
  end
end
