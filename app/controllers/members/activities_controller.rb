module Members
  class ActivitiesController < ApplicationController
    def index
      @member = current_office.member_memberships.find(params[:member_id])
      @pagy, @entries = pagy(@member.entries.includes(:recorder, :office).order(entry_date: :desc))
    end
  end
end
