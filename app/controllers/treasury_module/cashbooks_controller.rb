module TreasuryModule
  class CashbooksController < ApplicationController
    def index
      @cashbooks = current_office.cash_accounts
    end
  end
end 
