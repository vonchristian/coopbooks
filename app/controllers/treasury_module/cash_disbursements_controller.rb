module TreasuryModule
  class CashDisbursementsController < ApplicationController
    def index
      @cash_accounts = current_office.cash_accounts
      if params[:search].present?
        @pagy, @entries = pagy(@cash_accounts.credit_entries.text_search(params[:search]))
      else
        @pagy, @entries = pagy(@cash_accounts.credit_entries)
      end
    end
  end
end
