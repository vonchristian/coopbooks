module TreasuryModule
  class BankAccountsController < ApplicationController
    def index
      if params[:search].present?
        @bank_accounts = current_office.bank_accounts.text_search(params[:search])
      else
        @bank_accounts = current_office.bank_accounts
      end 
    end
  end
end
