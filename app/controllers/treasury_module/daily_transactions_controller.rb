module TreasuryModule
  class DailyTransactionsController < ApplicationController
    def index
      @employee      = current_user
      @cash_account  = @employee.default_cash_account
      @receipts      = @cash_account.debit_entries.entered_on(from_date: Date.current, to_date: Date.current).includes(:recorder)
      @disbursements = @cash_account.credit_entries.entered_on(from_date: Date.current, to_date: Date.current).includes(:recorder)
    end
  end
end
