module TreasuryModule
  class LoanPaymentsController < ApplicationController
    def index
      @entries = current_office.loan_products.payments
    end
  end
end
