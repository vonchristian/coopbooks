module TreasuryModule
  module CashReceiptsReports
    class CashReceiptsByCashAccountsController < ApplicationController
      def index
        if params[:cash_account_id].present?
          @cash_account = current_office.cash_accounts.find(params[:cash_account_id])
        else
          @cash_account = current_office.cash_accounts.first
        end
        @pagy, @entries  = pagy(@cash_account.entries)

        @cash_accounts = current_office.cash_accounts
      end

      def show
        @cash_account = current_office.cash_accounts.find(params[:id])
        @pagy, @entries       = pagy(@cash_account.entries)
      end
    end
  end
end
