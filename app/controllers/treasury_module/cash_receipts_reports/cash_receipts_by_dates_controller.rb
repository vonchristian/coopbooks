module TreasuryModule
  module CashReceiptsReports
    class CashReceiptsByDatesController < ApplicationController
      def index
        @cash_accounts = current_office.cash_accounts
        if params[:from_date] && params[:to_date]
          @from_date = Date.parse(params[:from_date])
          @to_date   = Date.parse(params[:to_date])
          @pagy, @entries = pagy(@cash_accounts.debit_entries.entered_on(from_date: @from_date, to_date: @to_date).order(entry_date: :desc))
        else
          @from_date = Date.current.beginning_of_month
          @to_date   = Date.current.end_of_month
          @pagy, @entries = pagy(@cash_accounts.debit_entries)
        end
      end
    end
  end
end
