module TreasuryModule
  module CashReceiptsReports
    class CashReceiptsByEmployeesController < ApplicationController
      def index
        if params[:employee_id].present?
          @employee = current_office.employees.with_cash_accounts.find(params[:employee_id])
        else
          @employee = current_office.employees.with_cash_accounts.first
        end
        @pagy, @entries  = pagy(@employee.entries)

        @employees = current_office.employees.with_cash_accounts
      end

      def show
        @cash_account = current_office.cash_accounts.find(params[:id])
        @pagy, @entries       = pagy(@cash_account.entries)
      end
    end
  end
end
