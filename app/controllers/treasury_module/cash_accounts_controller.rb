module TreasuryModule
  class CashAccountsController < ApplicationController
    def index
      if current_user.treasurer?
        @cash_accounts = current_office.cash_accounts
      else
        @cash_accounts = current_user.cash_accounts
      end
    end
    def show
      @employee     = current_user
      @cash_account = current_office.cash_accounts.find(params[:id])
      @pagy, @receipts = pagy(@cash_account.debit_entries.entered_on(from_date: Date.current, to_date: Date.current).includes(:recorder))
      @pagy, @disbursements = pagy(@cash_account.credit_entries.entered_on(from_date: Date.current, to_date: Date.current).includes(:recorder))

    end
  end
end
