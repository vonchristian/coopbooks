module TreasuryModule
  module CashAccounts
    class DisbursementsController < ApplicationController
      def index
        @cash_account       = current_office.cash_accounts.find(params[:cash_account_id])
        if params[:search].present?
        @pagy, @entries      = pagy(@cash_account.credit_entries.text_search(params[:search]).
          includes(:commercial_document).
          order(entry_date: :asc))
        else
        @pagy, @entries      = pagy(@cash_account.credit_entries.
        includes(:commercial_document, :recorder).
        order(entry_date: :asc))
      end
      end
    end
  end
end
