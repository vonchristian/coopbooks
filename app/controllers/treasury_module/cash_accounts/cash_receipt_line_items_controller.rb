module TreasuryModule
  module CashAccounts
    class CashReceiptLineItemsController < ApplicationController
      def new
        @cash_account           = current_user.cash_accounts.find(params[:cash_account_id])
        @cash_receipt_line_item = ::CashAccounts::Receipt.new
        @cash_receipt           = Vouchers::VoucherProcessing.new
        @voucher_amounts        = current_cart.voucher_amounts.includes(:account)
        authorize [:treasury_module, :cash_receipt]
      end

      def create
        @cash_account           = current_user.cash_accounts.find(params[:cash_account_id])
        @cash_receipt_line_item = ::CashAccounts::Receipt.new(cash_receipt_params)
        @cash_receipt           = Vouchers::VoucherProcessing.new
        @voucher_amounts        = current_user.voucher_amounts.includes(:account)

        authorize [:treasury_module, :cash_receipt]
        if @cash_receipt_line_item.valid?
          @cash_receipt_line_item.create_amounts!
          redirect_to new_treasury_module_cash_account_cash_receipt_line_item_url(cash_account_id: @cash_account.id), notice: "Added successfully"
        else
          render :new
        end
      end
      def destroy
        @cash_account = current_office.accounts.find(params[:id])
        @amount       = current_user.voucher_amounts.find(params[:amount_id])
        @amount.destroy
        redirect_to new_treasury_module_cash_account_cash_receipt_line_item_url(@cash_account), notice: "removed successfully"
      end

      private
      def cash_receipt_params
        params.require(:cash_accounts_receipt).
        permit(:amount, :account_id, :description, :employee_id, :cash_account_id, :account_number, :amount_type, :cart_id)
      end

    end
  end
end
