module TreasuryModule
  module CashAccounts
    class ReceiptsController < ApplicationController
      def index
        @from_date          = params[:from_date] ? Date.parse(params[:from_date]) : Date.current
        @to_date            = params[:from_date] ? Date.parse(params[:to_date]) : Date.current
        @cash_account       = current_office.cash_accounts.find(params[:cash_account_id])
        if params[:search].present?
        @pagy, @entries      = pagy(@cash_account.debit_entries.text_search(params[:search]).
          includes(:commercial_document).
          order(entry_date: :desc))
      else
        @pagy, @entries      = pagy(@cash_account.debit_entries.
        includes(:commercial_document, :recorder).
        order(entry_date: :desc, reference_number: :asc))
      end
      end
    end
  end
end
