class MembersDashboardsController < ApplicationController
  def index
    @members = current_office.member_memberships
  end
end
