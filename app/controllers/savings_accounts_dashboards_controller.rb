class SavingsAccountsDashboardsController < ApplicationController
  def index
    @savings_accounts = current_office.savings
    @liability_accounts = @savings_accounts.liability_accounts.includes(:amounts)
  end
end
