module LoanProducts
  class LoanProtectionPlanProvidersController < ApplicationController
    def edit
      @loan_product = current_office.loan_products.find(params[:loan_product_id])
    end
    def update
      @loan_product = current_office.loan_products.find(params[:loan_product_id])
      @loan_product.update(loan_protection_plan_provider_params)
      @loan_product.save!
      redirect_to office_loan_product_url(@loan_product), notice: 'Loan protection plan provider saved successfully.'
    end
    def loan_protection_plan_provider_params
      params.require(:cooperatives_loan_product).
      permit(:loan_protection_plan_provider_id)
    end
  end
end
