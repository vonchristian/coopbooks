module Insights
  module Members
    class SummariesController < ApplicationController
      def index
        @members = current_cooperative.members
      end
    end
  end
end
