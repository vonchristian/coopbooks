module Insights
  module Members
    class ByCivilStatusesController < ApplicationController
      def index
        @members = current_cooperative.members
      end
    end
  end
end
