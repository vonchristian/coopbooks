module Insights
  module Members
    class ByLocationsController < ApplicationController
      def index
        @members        = current_cooperative.members
        @barangays      = current_cooperative.barangays
        @municipalities = current_cooperative.municipalities
        @provinces      = current_cooperative.provinces
      end
    end
  end
end
