module Insights
  module Programs
    class SummariesController < ApplicationController
      def index
        @programs = current_cooperative.programs
      end
    end
  end
end 
