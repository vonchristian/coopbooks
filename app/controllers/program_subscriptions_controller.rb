class ProgramSubscriptionsController < ApplicationController
  def show
    @program_subscription = current_office.program_subscriptions.find(params[:id])
    @member = @program_subscription.subscriber
    @entries = @program_subscription.entries
  end
end
