class MembersController < ApplicationController
  layout 'application'

  respond_to :html, :json

  def index
    if params[:search].present?
      @pagy, @members = pagy(current_office.member_memberships.includes(:avatar_attachment).text_search(params[:search]).order(:last_name))
    else
      @pagy, @members = pagy(current_office.member_memberships.includes(:avatar_attachment =>[:blob]).order(:last_name))
    end
  end

  def show
    @member = current_office.member_memberships.find(params[:id])
    @address =@member.current_address
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Members::ProfileReportPdf.new(
          member: @member,
          cooperative: current_cooperative)
        send_data(pdf.render, type: 'application/pdf')
      end
    end
  end

  def edit
    @member      = current_office.member_memberships.find(params[:id])
    @update_info = Members::UpdateInfo.new
  end

  def update
    @member = current_office.member_memberships.find(params[:id])
    @update_info.Members::UpdateInfo.new(edit_member_params)
    if @update_info.valid?
      @update_info.process!
      redirect_to member_settings_url(@member), notice: 'Member updated successfully.'
    else
      render :edit
    end
  end

  # def destroy
  #   @member = current_cooperative.member_memberships.find(params[:id])
  #   if @member.savings.present? &&
  #      @member.time_deposits.present? &&
  #      @member.share_capitals.present? &&
  #      @member.loans.present?
  #      redirect_to member_url(@member), alert: "Savings, share capitals, time deposits and loans are still present."
  #    else
  #     @member.destroy
  #     redirect_to members_url, notice: "Member account deleted successfully."
  #   end
  # end

  private
  def member_params
    params.require(:member).permit(
      :civil_status, :membership_date,
      :first_name, :middle_name, :last_name,
      :sex, :date_of_birth, :avatar,
      :signature_specimen,
      tin_attributes: [:number])
  end
  def edit_member_params
    params.require(:members_update_info).
    permit(:first_name, :middle_name, :last_name, :date_of_birth, :sex, :civil_status,
    :complete_address, :street_id, :barangay_id, :municipality_id, :province_id, :member_id,
    :contact_number,
    :membership_date, :membership_category_id)
  end
end
