require 'will_paginate/array'
class BankAccountsController < ApplicationController
  def index
    if params[:search].present?
      @bank_accounts = current_office.bank_accounts.text_search(params[:search])
    else
      @bank_accounts = current_office.bank_accounts.paginate(page: params[:page], per_page: 25)
    end
  end

  def show
    @bank_account = current_office.bank_accounts.find(params[:id])
    @pagy, @entries = pagy(@bank_account.entries.order(entry_date: :asc))
  end
end
