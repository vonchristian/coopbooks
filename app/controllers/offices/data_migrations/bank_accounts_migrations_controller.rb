module Offices
  module DataMigrations
    class BankAccountsMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @registry = ::DataMigrations::BankAccountRegistry.new
      end
      def create
        @registry = ::DataMigrations::BankAccountRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_settings_url(current_office), notice: 'Bank accounts uploaded successfully.'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_bank_account_registry).
        permit(:spreadsheet, :employee_id)
      end
    end
  end
end
