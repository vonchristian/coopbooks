module Offices
  module DataMigrations
    class SavingsMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @registry = ::DataMigrations::SavingRegistry.new
      end
      def create
        @registry =  ::DataMigrations::SavingRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_settings_url(current_office), notice: 'Savings uploaded successfully'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_saving_registry).
        permit(:spreadsheet, :employee_id)
      end
    end
  end
end
