module Offices
  module DataMigrations
    class ShareCapitalsMigrationsController < ApplicationController
      def index
        @office = current_office
      end

      def new
        @office = current_office
        @registry = ::DataMigrations::ShareCapitalRegistry.new
      end
      def create
        @registry = ::DataMigrations::ShareCapitalRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_settings_url(current_office), notice: 'Share capital uploaded successfully.'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_share_capital_registry).
        permit(:spreadsheet, :employee_id)
      end
    end
  end
end
