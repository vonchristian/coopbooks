module Offices
  module DataMigrations
    class MembersMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @registry = ::DataMigrations::MemberRegistry.new
      end

      def create
        @office = current_office
        @registry = ::DataMigrations::MemberRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_members_migrations_path(@office), notice: 'uploaded successfully.'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_member_registry).
        permit(:spreadsheet, :employee_id, :office_id)
      end
    end
  end
end
