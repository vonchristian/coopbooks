module Offices
  module DataMigrations
    class TimeDepositsMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @registry = ::DataMigrations::TimeDepositRegistry.new
      end
      def create
        @registry = ::DataMigrations::TimeDepositRegistry.new(registry_params)
        if @registry.valid?
          ActiveRecord::Base.transaction do
            @registry.parse!
            redirect_to office_settings_url(current_office), notice: 'Bank accounts uploaded successfully.'
          end 
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_time_deposit_registry).
        permit(:spreadsheet, :employee_id)
      end
    end
  end
end
