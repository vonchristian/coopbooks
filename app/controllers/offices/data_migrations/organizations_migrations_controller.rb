module Offices
  module DataMigrations
    class OrganizationsMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @office   = current_office
        @registry = ::DataMigrations::OrganizationRegistry.new
      end

      def create
        @office   = current_office
        @registry = ::DataMigrations::OrganizationRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_settings_url(@office), notice: 'Organizations uploaded successfully'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_organization_registry).
        permit(:spreadsheet, :employee_id, :office_id)
      end
    end
  end
end
