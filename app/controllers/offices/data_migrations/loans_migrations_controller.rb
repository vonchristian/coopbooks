module Offices
  module DataMigrations
    class LoansMigrationsController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @registry = ::DataMigrations::LoanRegistry.new
      end
      def create
        @registry = ::DataMigrations::LoanRegistry.new(registry_params)
        if @registry.valid?
          @registry.parse!
          redirect_to office_settings_url(current_office), notice: 'Loans uploaded successfully'
        else
          render :new
        end
      end

      private
      def registry_params
        params.require(:data_migrations_loan_registry).
        permit(:spreadsheet, :employee_id)
      end
    end
  end
end 
