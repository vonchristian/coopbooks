module Offices
  module AccountingModule
    class ParentAccountCategoriesController < ApplicationController
      def index
        @categories = current_office.parent_account_categories
      end
      def new
        @category = ::AccountingModule::ParentAccountCategoryRegistration.new
      end

      def create
        @category = ::AccountingModule::ParentAccountCategoryRegistration.new(category_params)
        if @category.valid?
          @category.register!
          redirect_to office_parent_account_categories_url(current_office), notice: 'Category saved successfully.'
        else
          render :new
        end
      end

      def show
        @category = current_office.parent_account_categories.find(params[:id])
      end

      private
      def category_params
        params.require(:accounting_module_parent_account_category_registration).
        permit(:office_id, :grand_parent_account_category_id, :title, :code, :contra, :type)
      end
    end
  end
end
