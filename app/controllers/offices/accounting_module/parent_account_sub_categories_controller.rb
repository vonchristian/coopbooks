module Offices
  module AccountingModule
    class ParentAccountSubCategoriesController < ApplicationController
      def index
        @parent         = current_office.parent_account_categories.find(params[:parent_account_category_id])
        @sub_categories = @parent.parent_account_sub_categories
      end

      def new
        @parent = current_office.parent_account_categories.find(params[:parent_account_category_id])
        @category = ::AccountingModule::ParentAccountSubCategoryRegistration.new
      end
      def create
        @parent = current_office.parent_account_categories.find(params[:parent_account_category_id])
        @category = ::AccountingModule::ParentAccountSubCategoryRegistration.new(category_params)
        if @category.valid?
          @category.register!
          redirect_to office_parent_account_categories_url(current_office), notice: 'saved successfully'
        else
          render :new
        end
      end

      def show
        @parent_account_sub_category = current_office.parent_account_sub_categories.find(params[:id])
      end

      private
      def category_params
        params.require(:accounting_module_parent_account_sub_category_registration).
        permit(:office_id, :parent_account_category_id, :title, :code, :contra, :type)
      end
    end
  end
end
