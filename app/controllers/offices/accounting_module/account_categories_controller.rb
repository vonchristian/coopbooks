module Offices
  module AccountingModule
    class AccountCategoriesController < ApplicationController
      def new
        @parent_account_sub_category = current_office.parent_account_sub_categories.find(params[:parent_account_sub_category_id])
        @account_category            = ::AccountingModule::AccountCategoryRegistration.new
      end
      def create
        @parent_account_sub_category = current_office.parent_account_sub_categories.find(params[:parent_account_sub_category_id])
        @account_category            = ::AccountingModule::AccountCategoryRegistration.new(category_params)
        if @account_category.valid?
          @account_category.register!
          redirect_to office_account_category_url(id: @account_category.find_category.id), notice: 'Category saved successfully'
        else
          render :new
        end
      end

      private
      def category_params
        params.require(:accounting_module_account_category_registration).
        permit(:office_id, :parent_account_sub_category_id, :title, :code, :contra, :type)
      end
    end
  end
end
