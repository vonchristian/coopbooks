module Offices
  class LoansController < ApplicationController
    def index
      @office = current_cooperative.offices.find(params[:office_id])
      @pagy, @loans = pagy(@office.loans.order(created_at: :desc))
    end
  end
end
