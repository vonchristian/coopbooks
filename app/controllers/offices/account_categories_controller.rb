module Offices
  class AccountCategoriesController < ApplicationController
    def index
      @account_categories = current_office.account_categories
    end
    def show
      @account_category = current_office.account_categories.find(params[:id])
    end
    def edit
      @account_category = current_office.account_categories.find(params[:id])
    end
    def update
      @account_category = current_office.account_categories.find(params[:id])
      @account_category.update(account_category_params)
      if @account_category.valid?
        @account_category.save!
        redirect_to office_account_category_settings_url(account_category_id: @account_category.id), notice: 'Category updated successfully'
      else
        render :edit
      end
    end

    private
    def account_category_params
      params.require(category_type).
      permit(:title, :code, :type, :contra, :parent_account_sub_category_id)
    end
    
    def category_type
      @account_category.type.underscore.parameterize.gsub("-", "_").to_sym
    end
  end
end
