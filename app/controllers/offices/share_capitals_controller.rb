module Offices
  class ShareCapitalsController < ApplicationController
    def index
      @office = current_cooperative.offices.find(params[:office_id])
    end
  end
end 
