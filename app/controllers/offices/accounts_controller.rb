module Offices
  class AccountsController < ApplicationController
    def index
      @office    = current_cooperative.offices.find(params[:office_id])
      @accounts  = @office.accounts
      respond_to do |format|
        format.html
        format.xlsx
      end
    end
    def new
      @office  = current_cooperative.offices.find(params[:office_id])
      @account = ::AccountingModule::AccountRegistration.new
    end

    def create
      @office  = current_cooperative.offices.find(params[:office_id])
      @account = ::AccountingModule::AccountRegistration.new(account_params)
      if @account.valid?
        @account.register!
        redirect_to office_accounts_url(@office), notice: 'Account created successfully.'
      else
        render :new
      end
    end

    private
    def account_params
      params.require(:accounting_module_account_registration).
      permit(:name, :code, :contra, :type, :account_category_id, :office_id)
    end
  end
end
