module Offices
  module AccountCategories
    class AccountsController < ApplicationController
      def index
        @account_category = current_office.account_categories.find(params[:account_category_id])
        @pagy, @accounts = pagy(@account_category.accounts)
      end
      def new
        @account_category = current_office.account_categories.find(params[:account_category_id])
        @account          = ::AccountingModule::AccountRegistration.new
      end

      def create
        @account_category = current_office.account_categories.find(params[:account_category_id])
        @account          = ::AccountingModule::AccountRegistration.new(account_params)
        if @account.valid?
          @account.register!
          redirect_to office_account_category_url(id: @account_category.id), notice: 'saved successfully'
        else
          render :new
        end
      end

      private
      def account_params
        params.require(:accounting_module_account_registration).
        permit(:office_id, :account_category_id, :name, :code, :contra, :type)
      end
    end
  end
end
