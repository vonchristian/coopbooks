module Offices
  module AccountCategories
    class EntriesController < ApplicationController
      def index
        @account_category = current_office.account_categories.find(params[:account_category_id])
        @pagy, @entries   = pagy(@account_category.entries)
      end
    end
  end
end 
