module Offices
  module Settings
    class ShareCapitalProductsController < ApplicationController
      def index
        @share_capital_products = current_office.share_capital_products
      end
      def new
        @office = current_cooperative.offices.find(params[:office_id])
        @office_share_capital_product = @office.office_share_capital_products.build
      end
      def create
        @office = current_office
        @office_share_capital_product = @office.office_share_capital_products.create(share_capital_product_params)
        if @office_share_capital_product.valid?
          @office_share_capital_product.save!
          redirect_to office_share_capital_products_url(@office), notice: 'saved successfully.'
        else
          render :new
        end
      end
      def show
        @share_capital_product = current_office.share_capital_products.find(params[:id])
      end

      private
      def share_capital_product_params
        params.require(:offices_office_share_capital_product).
        permit(:share_capital_product_id, :equity_account_category_id, :interest_payable_account_category_id, :temporary_account_id)
      end
    end
  end
end
