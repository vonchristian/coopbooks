module Offices
  module Settings
    class TimeDepositProductsController < ApplicationController

      def new
        @time_deposit_product = current_office.time_deposit_products.build
      end

      def create
        @time_deposit_product = current_office.time_deposit_products.create(time_deposit_product_params)
        if @time_deposit_product.valid?
          @time_deposit_product.save!
          redirect_to office_settings_url(current_office), notice: "Time Deposit product created successfully."
        else
          render :new
        end
      end

      def show
        @time_deposit_product = current_office.time_deposit_products.find(params[:id])
      end

      private
      def time_deposit_product_params
        params.require(:cooperatives_time_deposit_product).
        permit(:name,
               :interest_rate,
               :minimum_deposit,
               :maximum_deposit,
               :number_of_days,
               :break_contract_fee,
               :break_contract_rate,
               :liability_account_category_id,
               :interest_expense_account_category_id,
               :break_contract_account_category_id,
               :office_id
               )
      end
    end
  end
end
