module Offices
  module Settings
    class LoanProductsController < ApplicationController
      def index
        @office        = current_office
        @loan_products = @office.loan_products
      end

      def new
        @office       = current_office
        @office_loan_product = @office.office_loan_products.build
      end

      def create
        @office       = current_office
        @office_loan_product = @office.office_loan_products.create(loan_product_params)
        if @office_loan_product.valid?
          @office_loan_product.save!
          redirect_to office_settings_url(current_office), notice: 'Loan product saved successfully.'
        else
          render :new
        end
      end

      def show
        @office = current_office
        @loan_product = @office.loan_products.find(params[:id])
        @office_loan_product = @office.office_loan_products.find_by!(loan_product: @loan_product)
        @loan_product_charges = @office_loan_product.loan_product_charges
      end

      def edit
        @office       = current_office
        @loan_product = @office.loan_products.find(params[:id])
      end

      def update
        @office       = current_office
        @loan_product = @office.loan_products.find(params[:id])
        @loan_product.update(update_loan_product_params)
        if @loan_product.valid?
          @loan_product.save!
          redirect_to office_loan_product_url(office_id: @office.id, id: @loan_product.id), notice: 'updated successfully'
        else
          render :edit
        end
      end

      private
      def loan_product_params
        params.require(:offices_office_loan_product).
        permit(:loan_product_id, :receivable_account_category_id, :interest_revenue_account_category_id, :penalty_revenue_account_category_id, :loan_protection_plan_provider_id, :amortization_type_id, :temporary_account_id)
      end
    end
  end
end
