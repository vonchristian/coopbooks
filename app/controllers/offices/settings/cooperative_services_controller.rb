module Offices
  module Settings
    class CooperativeServicesController < ApplicationController
      def index
        @office = current_office
      end
      def new
        @office              = current_office
        @cooperative_service = @office.cooperative_services.build
      end

      def create
        @office              = current_office
        @cooperative_service = @office.cooperative_services.create(service_params)
        if @cooperative_service.valid?
          @cooperative_service.save!
          redirect_to office_cooperative_services_url(@office), notice: 'Cooperative service created successfully.'
        else
          render :new
        end
      end

      def show
        @cooperative_service = current_office.cooperative_services.find(params[:id])
      end

      private
      def service_params
        params.require(:cooperatives_cooperative_service).
        permit(:title)
      end
    end
  end
end
