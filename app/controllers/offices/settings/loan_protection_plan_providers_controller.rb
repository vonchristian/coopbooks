module Offices
  module Settings
    class LoanProtectionPlanProvidersController < ApplicationController
      def new
        @office   = current_office
        @provider = @office.loan_protection_plan_providers.build
      end
      def create
        @office   = current_office
        @provider = @office.loan_protection_plan_providers.create(provider_params)
        if @provider.valid?
          @provider.save!
          redirect_to office_settings_url(current_office), notice: 'Provider created successfully.'
        else
          render :new
        end
      end

      private
      def provider_params
        params.require(:loans_module_loan_protection_plan_provider).
        permit(:business_name, :rate, :payable_account_id)
      end
    end
  end
end 
