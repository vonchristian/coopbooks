module Offices
  module Settings
    class ProgramsController < ApplicationController
      def index
        @programs = current_office.programs
      end
      def new
        @program = Offices::ProgramSubscription.new
      end
      def create
        @program = Offices::ProgramSubscription.new(program_params)
        if @program.valid?
          @program.subscribe!
          redirect_to office_settings_url(current_office), notice: 'Program added successfully'
        else
          render :new
        end
      end

      def show
        @program = current_office.programs.find(params[:id])
      end

      def edit
        @program = current_office.programs.find(params[:id])
      end

      def update
        @program = current_office.programs.find(params[:id])
        @program.update(program_params)
        if @program.valid?
          @program.save!
          redirect_to office_program_url(office_id: current_office.id, id: @program.id), notice: 'updated successfully'
        else
          render :edit
        end
      end

      private
      def program_params
        params.require(:offices_program_subscription).
        permit(:program_id, :account_category_id, :office_id)
      end
    end
  end
end
