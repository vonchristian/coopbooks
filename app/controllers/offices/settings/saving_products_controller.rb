module Offices
  module Settings
    class SavingProductsController < ApplicationController
      def index
        @saving_products = current_office.saving_products
      end
      def new
        @office = current_cooperative.offices.find(params[:office_id])
        @saving_product = @office.office_saving_products.build
      end

      def create
        @office = current_cooperative.offices.find(params[:office_id])
        @saving_product = @office.office_saving_products.create(saving_product_params)
        if @saving_product.valid?
          @saving_product.save!
          redirect_to office_settings_url(@office), notice: 'Saving Product created successfully.'
        else
          render :new
        end
      end

      def show
        @saving_product = current_office.saving_products.find(params[:id])
      end
      def edit
        @saving_product = current_office.saving_products.find(params[:id])
      end

      def update
        @saving_product = current_office.saving_products.find(params[:id])
        @saving_product.update(update_saving_product_params)
        if @saving_product.valid?
          @saving_product.save!
          redirect_to office_saving_product_url(office_id: current_office.id, id: @saving_product.id), notice: 'updated successfully'
        else
          render :edit
        end
      end

      private
      def saving_product_params
        params.require(:offices_office_saving_product).
        permit(:saving_product_id, :liability_account_category_id, :interest_expense_account_category_id, :temporary_account_id, :closing_account_category_id)
      end
    end
  end
end
