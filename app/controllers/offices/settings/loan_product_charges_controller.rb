module Offices
  module Settings
    class LoanProductChargesController < ApplicationController
      def new
        @loan_product = current_office.loan_products.find(params[:loan_product_id])
        @office_loan_product = current_office.office_loan_products.find_by(loan_product_id: @loan_product.id)
        @charge = @office_loan_product.loan_product_charges.build
      end
      def create
        @loan_product = current_office.loan_products.find(params[:loan_product_id])
        @office_loan_product = current_office.office_loan_products.find_by(loan_product_id: @loan_product.id)
        @charge = @office_loan_product.loan_product_charges.create(charge_params)
        if @charge.valid?
          @charge.save!
          redirect_to office_loan_product_url(office_id: current_office.id, id: @loan_product.id), notice: 'Charge created successfully'
        else
          render :new
        end
      end

      private
      def charge_params
        params.require(:loans_module_loan_products_loan_product_charge).
        permit(:name, :charge_type, :amount, :rate, :account_id)
      end
    end
  end
end 
