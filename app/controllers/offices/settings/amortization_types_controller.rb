module Offices
  module Settings
    class AmortizationTypesController < ApplicationController
      def new
        @office            = current_office
        @amortization_type = current_office.amortization_types.build
      end
      def create
        @office            = current_office
        @amortization_type = current_office.amortization_types.create(amortization_type_params)
        if @amortization_type.valid?
          @amortization_type.save!
          redirect_to office_settings_url(current_office), notice: 'Amortization Type created successfully'
        else
          render :new
        end
      end

      private
      def amortization_type_params
        params.require(:loans_module_amortization_type).
        permit(:calculation_type, :repayment_calculation_type, :name)
      end
    end
  end
end
