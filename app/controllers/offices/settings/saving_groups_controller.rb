module Offices
  module Settings
    class SavingGroupsController < ApplicationController

      def new
        @saving_group = current_office.saving_groups.build
      end

      def create
        @saving_group = current_office.saving_groups.create(saving_group_params)
        if @saving_group.valid?
          @saving_group.save!
          redirect_to office_saving_groups_url(current_office), notice: 'Saving group saved successfully.'
        else
          render :new
        end
      end

      private
      def saving_group_params
        params.require(:offices_saving_group).
        permit(:title, :start_num, :end_num)
      end
    end
  end
end
