class SavingsAccountsBelowMinimumBalancesController < ApplicationController
  def index
    @pagy, @savings_accounts = pagy(current_office.savings.below_minimum_balance)
  end
end
