class ShareCapitalsBelowMinimumBalancesController < ApplicationController
  def index
    @pagy, @share_capitals = pagy(current_office.share_capitals.below_minimum_balance)
  end
end 
