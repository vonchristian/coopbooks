module StoreFrontModule
  class StoreController < ApplicationController
    def index
      if params[:search].present?
        @stocks = current_store_front.stocks.processed.available.text_search(params[:search])
        @products = current_store_front.products.text_search(params[:search])
      end
      @sales_line_item = StoreFrontModule::LineItems::SalesLineItemProcessing.new

    end
  end
end
