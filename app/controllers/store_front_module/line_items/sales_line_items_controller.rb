module StoreFrontModule
  module LineItems
    class SalesLineItemsController < ApplicationController
      def new
        if params[:search].present?
          @pagy, @stocks   = pagy(current_store_front.stocks.processed.available.text_search(params[:search]))
          @pagy, @products = pagy(current_store_front.products.text_search(params[:search]))
        end
        @cart = current_cart
        @sales_line_item = StoreFrontModule::LineItems::SalesLineItemProcessing.new
        @sales_order = StoreFrontModule::Orders::SalesOrderProcessing.new
        @sales_line_items = @cart.sales_line_items.includes(:stock).order(created_at: :desc)
      end
      def create
        @cart = current_cart
        @sales_line_item = StoreFrontModule::LineItems::SalesLineItemProcessing.new(line_item_params)
        @stock = current_store_front.stocks.find(params[:store_front_module_line_items_sales_line_item_processing][:stock_id])
        if @sales_line_item.valid?
          @sales_line_item.process!
          StoreFrontModule::StockAvailabilityChecker.new(stock: @stock, cart: current_cart).update_availability!
          redirect_to new_store_front_module_sales_line_item_url, notice: "Added to cart."

        else
          render :new
        end
      end
      def destroy
        @line_item = StoreFronts::LineItems::SalesLineItem.find(params[:id])
        @line_item.destroy
        StoreFrontModule::StockAvailabilityChecker.new(stock: @line_item.stock, cart: current_cart).update_availability!
        redirect_to new_store_front_module_sales_line_item_url
      end
      private
      def line_item_params
        params.require(:store_front_module_line_items_sales_line_item_processing).
        permit(:unit_of_measurement_id,
               :quantity,
               :unit_cost,
               :total_cost,
               :product_id,
               :barcode,
               :cart_id,
               :employee_id,
               :store_front_id,
               :stock_id)

      end
    end
  end
end
