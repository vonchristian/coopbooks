module StoreFrontModule
  module Suppliers
    class ProductSelectionsController < ApplicationController
      def new
        @supplier = current_store_front.suppliers.find(params[:supplier_id])
        @products = current_store_front.products
        @purchase_order_processing = StoreFronts::Orders::PurchaseOrderProcessing.new
        @product_selection   = StoreFrontModule::Suppliers::ProductSelection.new
      end
      def create
        @supplier = current_store_front.suppliers.find(params[:supplier_id])
        redirect_to new_store_front_module_supplier_purchase_line_item_url(
          @supplier, product_id: params[:store_front_module_suppliers_product_selection][:product_id],
          unit_of_measurement_id: params[:store_front_module_suppliers_product_selection][:unit_of_measurement_id])

      end
    end
  end
end
