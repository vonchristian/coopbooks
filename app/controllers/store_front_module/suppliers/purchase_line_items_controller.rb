module StoreFrontModule
  module Suppliers
    class PurchaseLineItemsController < ApplicationController
      def new
        @supplier = current_store_front.suppliers.find(params[:supplier_id])
        @product  = current_store_front.products.find(params[:product_id])
        @cart     = current_cart
        @line_item = StoreFrontModule::LineItems::PurchaseLineItemProcessing.new

        @purchase_order_processing = StoreFronts::Orders::PurchaseOrderProcessing.new

        @purchase_line_items = @cart.purchase_line_items.includes(:unit_of_measurement, :product).order(created_at: :desc)
      end
      def create
        @supplier = current_store_front.suppliers.find(params[:supplier_id])
        @product  = current_store_front.products.find(params[:store_front_module_line_items_purchase_line_item_processing][:product_id])

        @cart = current_cart
        @line_item = StoreFrontModule::LineItems::PurchaseLineItemProcessing.new(line_item_params)
        if @line_item.valid?
          @line_item.process!
          redirect_to new_store_front_module_supplier_product_selection_url(@supplier), notice: "Added to cart."
        else
          render :new
        end
      end
      private
      def line_item_params
        params.require(:store_front_module_line_items_purchase_line_item_processing).
        permit(:quantity, :unit_cost, :total_cost, :barcode, :unit_of_measurement_id, :product_id, :expiry_date, :cart_id, :store_front_id)
      end
    end
  end
end
