module StoreFrontModule
  module Suppliers
    class PurchaseOrderProcessingsController < ApplicationController
      def create
        @supplier = current_store_front.suppliers.find(params[:supplier_id])
        @purchase_order_processing = StoreFronts::Orders::PurchaseOrderProcessing.new(purchase_order_params)
        if @purchase_order_processing.valid?
          @purchase_order_processing.process!
          redirect_to "/", notice: 'save'
        else
          redirect_to new_store_front_module_supplier_product_selection_url(@supplier), alert: 'Error'
        end
      end

      private
      def purchase_order_params
        params.require(:store_fronts_orders_purchase_order_processing).
        permit(:cart_id, :voucher_id, :employee_id, :store_front_id, :date, :reference_number, :supplier_id)
      end
    end
  end
end
