module StoreFrontModule
  module Products
    class SettingsController < ApplicationController
      def index
        @product = current_store_front.products.find(params[:product_id])
      end
    end
  end
end
