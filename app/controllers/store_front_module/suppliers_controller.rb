module StoreFrontModule
  class SuppliersController < ApplicationController
    def index
      if params[:search].present?
        @suppliers = current_store_front.suppliers.text_search(params[:search]).paginate(:page => params[:page], :per_page => 50)
      else
        @suppliers = current_store_front.suppliers.paginate(:page => params[:page], :per_page => 50)
      end
    end
    def new
      @supplier = current_store_front.suppliers.build
    end
    def create
      @supplier = current_store_front.suppliers.create(supplier_params)
    end
    def show
      @supplier = current_store_front.suppliers.find(params[:id])

    end

    private
    def supplier_params
      params.require(:store_fronts_supplier).permit(:first_name, :last_name, :contact_number, :business_name, :address)
    end
  end
end
