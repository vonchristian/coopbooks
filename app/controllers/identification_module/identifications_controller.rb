module IdentificationModule
  class IdentificationsController < ApplicationController
    def index
      @identifications = IdentificationModule::Identification.all
    end
    def new
      @identifiable = params[:identifiable_type].constantize.find(params[:identifiable_id])
      @identification = @identifiable.identifications.build
    end
    def create
      @identifiable = params[:identifications_module_identification][:identifiable_type].constantize.find(params[:identifications_module_identification][:identifiable_id])
      @identification = @identifiable.identifications.create(identification_params)
      if @identification.valid?
        @identification.save
        redirect_to member_url(@identifiable), notice: 'Identification saved successfully.'
      else
        render :new
      end
    end

    def edit
      @identification = IdentificationsModule::Identification.find(params[:id])
    end

    def update
      @identification = IdentificationsModule::Identification.find(params[:id])
      @identification.update(identification_params)
      if @identification.valid?
        @identification.save!
        redirect_to member_url(id: @identification.identifiable_id), notice: 'Identification updated successfully'
      else
        render :edit
      end 
    end
    private
    def identification_params
      params.require(:identifications_module_identification).
      permit(:identifiable_id, :identifiable_type, :identity_provider_id, :issuance_date, :expiry_date, :number, :photo)
    end
  end
end
