class VouchersController < ApplicationController
  def index
    if params[:search].present?
      @pagy, @vouchers = pagy(current_office.vouchers.includes(:preparer).order(reference_number: :desc, date: :desc).text_search(params[:search]))
    else
      @pagy, @vouchers = pagy(current_office.vouchers.includes(:payee, :preparer).order(reference_number: :desc, date: :desc))
    end
  end
  def show
    @voucher = current_office.vouchers.find(params[:id])
    @voucher_amounts = @voucher.voucher_amounts.includes(:account =>[:account_category, :cooperative])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = VoucherPdf.new(
          voucher: @voucher,
          view_context: view_context)
        send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Voucher.pdf"
      end
    end
  end
  def destroy
    @voucher = current_office.vouchers.find(params[:id])
    if !@voucher.disbursed?
      @voucher.destroy
      redirect_to "/", notice: "Voucher cancelled successfully."
    else
      redirect_to "/", alert: "Cannot delete voucher. Already disbursed."
    end
  end
end
