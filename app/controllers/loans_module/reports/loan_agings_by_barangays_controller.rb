module LoansModule
  module Reports
    class LoanAgingsByBarangaysController < ApplicationController
      def index
        @loan_groups  = current_office.loan_groups
        @loan_group   = params[:loan_group_id] ? current_office.loan_groups.find(params[:loan_group_id]) : current_office.loan_groups.order(start_num: :asc).first
        @barangays    = Addresses::Barangay.all.includes(:municipality)
        @barangay     = params[:barangay_id] ? Addresses::Barangay.find(params[:barangay_id]) : Addresses::Barangay.first
        @pagy, @loans = pagy(@barangay.loans.where(office: current_office).includes(:loan_product, :receivable_account, :borrower =>[:avatar_attachment =>[:blob]]))
        @all_loans    = @barangay.loans.where(office: current_office).where(loan_group: @loan_group)

        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
