module LoansModule
  module Reports
    class LoanMaturitiesController < ApplicationController
      def index

        @from_date     = params[:from_date] ? Date.parse(params[:from_date]) : Date.today.beginning_of_month
        @to_date       = params[:to_date] ? Date.parse(params[:to_date]) : Date.today.end_of_month
        @pagy, @loans  = pagy(current_office.loans.matured_on(from_date: @from_date, to_date: @to_date))
        @all_loans     = current_office.loans.matured_on(from_date: @from_date, to_date: @to_date)
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
