module LoansModule
  module Reports
    class LoanCollectionsController < ApplicationController
      def index
        @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : DateTime.now.at_beginning_of_month
        @to_date   = params[:to_date] ? DateTime.parse(params[:to_date]).end_of_day : DateTime.now.end_of_day
        @entries   = LoansModule::Payments::EntriesAggregator.new(office: current_office, from_date: @from_date, to_date: @to_date).entries

        @loans  = current_office.loans.updated_at(from_date: @from_date, to_date: @to_date)
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
