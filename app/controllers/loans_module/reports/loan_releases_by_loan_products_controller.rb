module LoansModule
  module Reports
    class LoanReleasesByLoanProductsController < ApplicationController
      def index
        @from_date     = params[:from_date] ? DateTime.parse(params[:from_date]) : DateTime.now.at_beginning_of_month
        @to_date       = params[:to_date]   ? DateTime.parse(params[:to_date])   : Date.current.end_of_month
        @loan_products = current_office.loan_products
        @loan_product  = params[:loan_product_id] ? current_office.loan_products.find(params[:loan_product_id]) : current_office.loan_products.first
        @loans         = current_office.loans.where(loan_product: @loan_product).disbursed_on(from_date: @from_date, to_date: @to_date)
      end
    end
  end
end
