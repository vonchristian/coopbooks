module LoansModule
  module Reports
    class LoanAgingsByLoanProductsController < ApplicationController
      def index
        @loan_products  = current_office.loan_products
        @loan_groups    = current_office.loan_groups
        @loan_product   = params[:loan_product_id] ? current_office.loan_products.find(params[:loan_product_id]) : current_office.loan_products.first
        @loan_group     = params[:loan_group_id] ? current_office.loan_groups.find(params[:loan_group_id]) : current_office.loan_groups.order(start_num: :asc).first
        @pagy, @loans   = pagy(@loan_group.loans.where(loan_product: @loan_product).includes(:loan_product, :receivable_account, :borrower =>[:avatar_attachment =>[:blob]]))
        @all_loans      = @loan_group.loans.where(loan_product: @loan_product)

        respond_to do |format|
          format.xlsx
          format.html
        end
      end
    end
  end
end
