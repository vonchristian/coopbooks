module LoansModule
  module Reports
    class LoanReleasesController < ApplicationController
      def index
        @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : DateTime.now.at_beginning_of_month
        @to_date   = params[:to_date] ? DateTime.parse(params[:to_date]) : Date.current.end_of_month
        @loans     = current_office.loans.order(disbursement_date: :desc).disbursed_on(from_date: @from_date, to_date: @to_date)
        @cooperative = current_cooperative
        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
