module LoansModule
  module Reports
    class LoanAgingsSummariesController < ApplicationController
      def index
        @loans       = current_office.loans
        @loan_groups = current_office.loan_groups.order(start_num: :asc)
      end
    end
  end
end
