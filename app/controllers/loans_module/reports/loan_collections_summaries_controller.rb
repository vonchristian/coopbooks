module LoansModule
  module Reports
    class LoanCollectionsSummariesController < ApplicationController
      def index
        @date = params[:date] ? Date.parse(params[:date]) : Date.current
      end
    end
  end
end
