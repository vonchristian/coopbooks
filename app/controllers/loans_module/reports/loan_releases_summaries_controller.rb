module LoansModule
  module Reports
    class LoanReleasesSummariesController < ApplicationController
      def index
        @date = params[:date] ? Date.parse(params[:date]) : Date.current 
      end
    end
  end
end
