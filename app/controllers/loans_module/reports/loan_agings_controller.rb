module LoansModule
  module Reports
    class LoanAgingsController < ApplicationController
      def index
        @loan_groups  = current_office.loan_groups.order(start_num: :asc)
        @loan_group   = params[:loan_group_id] ? current_office.loan_groups.find(params[:loan_group_id]) : current_office.loan_groups.order(start_num: :asc).first
        @pagy, @loans = pagy(@loan_group.loans.includes(:loan_product, :receivable_account, :borrower =>[:avatar_attachment =>[:blob]]))
        @all_loans    = @loan_group.loans.includes(:loan_product, :receivable_account, :borrower =>[:avatar_attachment =>[:blob]])

        respond_to do |format|
          format.xlsx
          format.html
        end
      end
    end
  end
end
