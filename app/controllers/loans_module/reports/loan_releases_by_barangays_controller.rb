module LoansModule
  module Reports
    class LoanReleasesByBarangaysController < ApplicationController
      def index
        @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : DateTime.now.at_beginning_of_month
        @to_date   = params[:to_date] ? DateTime.parse(params[:to_date]) : Date.current.end_of_month
        @barangays = Addresses::Barangay.all
        @barangay  = params[:barangay_id] ? Addresses::Barangay.find(params[:barangay_id]) : Addresses::Barangay.first
        @loans     = @barangay.loans.where(office: current_office)
      end
    end
  end
end
