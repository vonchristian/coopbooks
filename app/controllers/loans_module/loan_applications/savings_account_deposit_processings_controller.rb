
module LoansModule
  module LoanApplications
    class SavingsAccountDepositProcessingsController < ApplicationController
      respond_to :html, :json

      def new
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @savings_account = current_office.savings.find(params[:saving_id])
        @deposit         = LoansModule::LoanApplications::Saving.new
      end

      def create
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @savings_account = current_office.savings.find(params[:loans_module_loan_applications_saving][:savings_account_id])
        @deposit         = LoansModule::LoanApplications::Saving.new(deposit_params)
        if @deposit.valid?
          @deposit.process!
          redirect_to new_loans_module_loan_application_voucher_url(@loan_application), notice: 'Savings deposit added successfully'
        else
          render :new
        end
      end

      private
      def deposit_params
        params.require(:loans_module_loan_applications_saving).
        permit(:amount, :loan_application_id, :savings_account_id, :cart_id)
      end
    end
  end
end
