module LoansModule
  module LoanApplications
    class ProgramSubscriptionsController < ApplicationController
      def new
        @loan_application     = current_office.loan_applications.find(params[:loan_application_id])
        @program              = current_office.programs.find(params[:program_id])
        @program_subscription = LoansModule::LoanApplications::ProgramSubscription.new
      end
      def create
        @loan_application     = current_office.loan_applications.find(params[:loan_application_id])
        @program              = current_office.programs.find(params[:loans_module_loan_applications_program_subscription][:program_id])
        @program_subscription = LoansModule::LoanApplications::ProgramSubscription.new(program_subscription_params)
        if @program_subscription.valid?
          @program_subscription.process!
          redirect_to new_loans_module_loan_application_voucher_url(@loan_application), notice: 'added successfully'
        else
          render :new
        end
      end
      def program_subscription_params
        params.require(:loans_module_loan_applications_program_subscription).
        permit(:program_id, :amount, :loan_application_id)
      end
    end
  end
end
