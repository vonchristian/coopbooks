module LoansModule
  module LoanApplications
    class ProgramSubscriptionPaymentsController < ApplicationController
      def new
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @borrower = @loan_application.borrower
        @program_subscription = @borrower.program_subscriptions.find(params[:program_subscription_id])
        @payment = LoansModule::LoanApplications::ProgramSubscriptionPayment.new
      end
      def create
        @loan_application = current_office.loan_applications.find(params[:loans_module_loan_applications_program_subscription_payment][:loan_application_id])
         @borrower = @loan_application.borrower
        @program_subscription = @borrower.program_subscriptions.find(params[:loans_module_loan_applications_program_subscription_payment][:program_subscription_id])
        @payment          = LoansModule::LoanApplications::ProgramSubscriptionPayment.new(program_params)
        if @payment.valid?
          @payment.process!
          redirect_to new_loans_module_loan_application_voucher_url(@loan_application), notice: 'Program payment added successfully.'
        else
          render :new
        end
      end

      private

      def program_params
        params.require(:loans_module_loan_applications_program_subscription_payment).
        permit(:amount, :employee_id, :loan_application_id, :program_subscription_id)
      end
    end
  end
end
