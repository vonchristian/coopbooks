module LoansModule
  module LoanApplications
    class SavingsAccountOpeningsController < ApplicationController
      def new
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @opening          = LoansModule::LoanApplications::SavingsAccountOpening.new
      end

      def create
         @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @opening          = LoansModule::LoanApplications::SavingsAccountOpening.new(opening_params)
        if @opening.valid?
          @opening.create_application!
          redirect_to new_loans_module_loan_application_voucher_url(@loan_application), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def opening_params
        params.require(:loans_module_loan_applications_savings_account_opening).
        permit(:cart_id, :employee_id, :saving_product_id, :amount, :loan_application_id, :account_number)
      end
    end
  end
end
