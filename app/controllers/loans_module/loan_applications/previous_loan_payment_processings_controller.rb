module LoansModule
  module LoanApplications
    class PreviousLoanPaymentProcessingsController < ApplicationController
      respond_to :html, :json

      def new
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @loan             = current_office.loans.find(params[:loan_id])
        @payment          = LoansModule::LoanApplications::PreviousLoanPayment.new
        respond_modal_with @payment
      end

      def create
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @loan             = current_office.loans.find(params[:loans_module_loan_applications_previous_loan_payment][:loan_id])
        @payment          = LoansModule::LoanApplications::PreviousLoanPayment.new(payment_params)
        @payment.process!
        respond_modal_with @payment,
          location: new_loans_module_loan_application_voucher_url(@loan_application)
      end

      private
      def payment_params
        params.require(:loans_module_loan_applications_previous_loan_payment).
        permit(:principal_amount, :interest_amount, :penalty_amount, :employee_id, :loan_application_id, :loan_id, :cart_id)
      end
    end
  end
end
