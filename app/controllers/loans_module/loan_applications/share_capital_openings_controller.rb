module LoansModule
  module LoanApplications
    class ShareCapitalOpeningsController < ApplicationController
      def new
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @opening          = LoansModule::LoanApplications::ShareCapitalOpening.new
      end

      def create
        @loan_application = current_office.loan_applications.find(params[:loan_application_id])
        @opening          = LoansModule::LoanApplications::ShareCapitalOpening.new(opening_params)
        if @opening.valid?
          @opening.create_application!
          redirect_to new_loans_module_loan_application_voucher_url(@loan_application), notice: 'added successfully.'
        else
          render :new
        end
      end

      private
      def opening_params
        params.require(:loans_module_loan_applications_share_capital_opening).
        permit(:cart_id, :loan_application_id, :share_capital_product_id, :amount, :account_number, :employee_id)
      end
    end
  end
end
