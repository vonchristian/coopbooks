module LoansModule
  module LoanApplications
    class GuarantorsController < ApplicationController
      def new
        @pagy, @members = pagy(current_office.member_memberships)
        @loan_application  = current_office.loan_applications.find(params[:loan_application_id])
        @guarantor        = LoansModule::LoanApplications::GuarantorProcessing.new
      end

      def create
        @pagy, @members = pagy(current_office.member_memberships)
        @loan_application  = current_office.loan_applications.find(params[:loan_application_id])
        @guarantor        = LoansModule::LoanApplications::GuarantorProcessing.new(guarantor_params)
        if @guarantor.valid?
          @guarantor.process!
          redirect_to new_loans_module_loan_application_guarantor_url(@loan_application), notice: 'Loan application guarantor saved successfully.'
        else
          render :new
        end
      end

      private

      def guarantor_params
        params.require(:loans_module_loan_applications_guarantor_processing).
        permit(:loan_application_id, :office_id, :guarantor_id)
      end 
    end
  end
end
