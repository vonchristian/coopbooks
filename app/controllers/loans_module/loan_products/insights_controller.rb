module LoansModule
  module LoanProducts
    class InsightsController < ApplicationController
      def index
        @loan_product = current_office.loan_products.find(params[:loan_product_id])
      end
    end
  end
end
