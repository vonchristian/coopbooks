module LoansModule
  class LoanApplicationProductSelectionsController < ApplicationController
    def new
      @loan_products = current_office.loan_products
      @borrower      = params[:borrower_type].constantize.find(params[:borrower_id])
    end
  end
end
