module LoansModule
  class LoanApplicationsController < ApplicationController
    def index
      if params[:search].present?
        @pagy, @loan_applications = pagy(current_office.
        loan_applications.includes(:cart, :loan_product, voucher: [:entry], borrower: [:avatar_attachment =>[:blob]]).
        text_search(params[:search]))
      else
        @pagy, @loan_applications = pagy(current_office.
        loan_applications.includes(:cart, :loan_product, voucher: [:entry], borrower: [:avatar_attachment =>[:blob]]))
      end
    end

    def new
      @borrower         = params[:borrower_type].constantize.find(params[:borrower_id])
      @loan_product     = current_office.loan_products.find(params[:loan_product_id])
      @loan_application = LoansModule::LoanApplicationProcessing.new
    end

    def create
      @loan_product     = current_office.loan_products.find(params[:loans_module_loan_application_processing][:loan_product_id])

      @borrower = params[:loans_module_loan_application_processing][:borrower_type].constantize.find(params[:loans_module_loan_application_processing][:borrower_id])
      @loan_application = LoansModule::LoanApplicationProcessing.new(loan_params)
      if @loan_application.process!
        # @loan_application.process!
        redirect_to new_loans_module_loan_application_voucher_url(@loan_application.find_loan_application), notice: "Loan application saved successfully."
      else
        render :new
      end
    end
    def show
      @loan_application = current_office.loan_applications.find(params[:id])
      @voucher_amounts = @loan_application.voucher.voucher_amounts
    end

    def destroy
      @loan_application = current_office.loan_applications.find(params[:id])
      LoansModule::LoanApplications::Cancellation.new(loan_application: @loan_application).cancel!
      redirect_to loans_url, notice: "Loan application cancelled successfully"
    end

    private
    def loan_params
      params.require(:loans_module_loan_application_processing).permit(
                  :cooperative_id,
                  :borrower_id,
                  :borrower_type,
                  :term,
                  :purpose,
                  :loan_product_id,
                  :loan_amount,
                  :application_date,
                  :mode_of_payment,
                  :application_date,
                  :account_number,
                  :preparer_id,
                  :interest_rate,
                  :interest_calculation_type,
                  :interest_prededuction_rate,
                  :cart_id)
    end
  end
end
