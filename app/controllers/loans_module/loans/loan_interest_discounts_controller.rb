module LoansModule
  module Loans
    class LoanInterestDiscountsController < ApplicationController
      respond_to :html, :json

      def new
        @loan = current_cooperative.loans.find(params[:loan_id])
        @loan_discount = @loan.loan_discounts.interest.build
        respond_modal_with @loan_discount
      end

      def create
        @loan = current_cooperative.loans.find(params[:loan_id])
        @loan_discount = @loan.loan_discounts.interest.create(loan_discount_params)
        if @loan_discount.valid?
          @loan_discount.save!
          redirect_to loans_module_loan_interests_url(@loan), notice: 'Loan discount saved successfully.'
        else
          render :new
        end
      end


      private
      def loan_discount_params
        params.require(:loans_module_loans_loan_discount).
        permit(:date, :amount, :description, :computed_by_id)
      end
    end
  end
end

