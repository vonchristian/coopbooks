module LoansModule
  module Loans
    class InterestsController < ApplicationController
      def index
        @loan = current_office.loans.find(params[:loan_id])
        @interests_and_discounts = @loan.loan_interests.includes(:employee)
        @discounts               = @loan.loan_discounts.interest.includes(:employee)
      end
    end
  end
end
