module LoansModule
  module Loans
    class AutoPaySetupsController < ApplicationController
      def new
        @loan = current_office.loans.find(params[:loan_id])
        @auto_pay = ::Loans::AutoPaySetup.new
      end

      def create
        @loan = current_office.loans.find(params[:loan_id])
        @auto_pay = ::Loans::AutoPaySetup.new(auto_pay_setup_params)
        if @auto_pay.valid?
          @auto_pay.process!
          redirect_to loan_settings_path(@loan), notice: 'Loan auto pay enabled successfully'
        else
          render :new
        end
      end

      private
      def auto_pay_setup_params
        params.require(:loans_auto_pay_setup).
        permit(:office_id, :loan_id, :savings_account_id)
      end
    end
  end
end
