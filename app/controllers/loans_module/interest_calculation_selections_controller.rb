module LoansModule
  class InterestCalculationSelectionsController < ApplicationController
    def new
      @borrower         = params[:borrower_type].constantize.find(params[:borrower_id])
      @loan_product     = current_office.loan_products.find(params[:loan_product_id])
    end
  end
end
