module LoansModule
  module Settings
    class LoanGroupsController < ApplicationController
      def new
        @loan_group = current_office.loan_groups.build
      end
      def create
        @loan_group = current_office.loan_groups.create(loan_group_params)
        if @loan_group.valid?
          @loan_group.save!
          redirect_to loans_module_settings_url,
          notice: 'Loan group created successfully'
        else
          render :new
        end
      end

      private
      def loan_group_params
        params.require(:loans_module_loan_group).
        permit(:title, :start_num, :end_num)
      end
    end
  end
end
