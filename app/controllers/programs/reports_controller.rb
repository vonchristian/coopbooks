module Programs
  class ReportsController < ApplicationController
    def index
      @program = current_office.programs.find(params[:program_id])
    end
  end
end 
