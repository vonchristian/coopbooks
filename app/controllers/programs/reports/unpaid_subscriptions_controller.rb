module Programs
  module Reports
    class UnpaidSubscriptionsController < ApplicationController
      def index
        @program = current_office.programs.find(params[:program_id])
        @date    = params[:date] ? Date.parse(params[:date]) : Date.current
        @pagy, @members = pagy(Programs::UnpaidSubscriptionFinder.new(program: @program, office: current_office, date: @date).members_with_unpaid_subscriptions)
        @all_members = Programs::UnpaidSubscriptionFinder.new(program: @program, office: current_office, date: @date).members_with_unpaid_subscriptions

        respond_to do |format|
          format.html
          format.xlsx
        end
      end
    end
  end
end
