module ShareCapitals
  module AdjustingEntries
    class AdjustingEntryVoucherConfirmationsController < ApplicationController
      def show
        @share_capital = current_office.share_capitals.find(params[:share_capital_id])
        @voucher = current_office.vouchers.find(params[:id])
      end
       def create
        @voucher         = current_cooperative.vouchers.find(params[:voucher_id])
        @share_capital   = current_office.share_capitals.find(params[:share_capital_id])
        Vouchers::EntryProcessing.new(voucher: @voucher, employee: current_user).process!
        redirect_to share_capital_url(@share_capital), notice: "Transaction confirmed successfully."
      end

      def destroy
        @share_capital = current_office.share_capitals.find(params[:share_capital_id])
        @voucher       = current_office.vouchers.find(params[:id])
        if !@voucher.disbursed?
          @voucher.destroy
          redirect_to share_capital_adjusting_entries_url(@share_capital), notice: 'Transaction cancelled successfully.'
        end
      end
    end
  end
end
