module ShareCapitals
  module AdjustingEntries
    class ProgramSubscriptionPaymentsController < ApplicationController
      def new
        @share_capital        = current_office.share_capitals.find(params[:share_capital_id])
        @program_subscription = current_office.program_subscriptions.find(params[:program_subscription_id])
        @payment              = ShareCapitals::AdjustingEntries::ProgramSubscriptionPayment.new
      end
      def create
        @share_capital        = current_office.share_capitals.find(params[:share_capital_id])
        @payment              = ShareCapitals::AdjustingEntries::ProgramSubscriptionPayment.new(payment_params)
        if @payment.valid?
          @payment.create_amounts!
          redirect_to share_capital_adjusting_entries_url(@share_capital), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def payment_params
        params.require(:share_capitals_adjusting_entries_program_subscription_payment).
        permit(:cart_id, :share_capital_id, :employee_id, :program_subscription_id, :amount)
      end
    end
  end
end
