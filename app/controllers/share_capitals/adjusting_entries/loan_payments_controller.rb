module ShareCapitals
  module AdjustingEntries
    class LoanPaymentsController < ApplicationController
      def new
        @share_capital = current_office.share_capitals.find(params[:share_capital_id])
        @loan          = current_office.loans.find(params[:loan_id])
        @payment       = ShareCapitals::AdjustingEntries::LoanPayment.new
      end

      def create
        @share_capital = current_office.share_capitals.find(params[:share_capital_id])
        @payment       = ShareCapitals::AdjustingEntries::LoanPayment.new(payment_params)
        if @payment.valid?
          @payment.create_amounts!
          redirect_to share_capital_adjusting_entries_url(@share_capital), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def payment_params
        params.require(:share_capitals_adjusting_entries_loan_payment).
        permit(:cart_id, :employee_id, :loan_id, :share_capital_id, :principal_amount, :interest_amount, :penalty_amount)
      end
    end
  end
end
