module ShareCapitals
  module AdjustingEntries
    class AdjustingEntryVoucherProcessingsController < ApplicationController
      def create
        @share_capital = current_office.share_capitals.find(params[:share_capital_id])
        @voucher_processing = ShareCapitals::AdjustingEntries::VoucherProcessing.new(voucher_params)
        if @voucher_processing.valid?
          @voucher_processing.create_voucher!
          redirect_to share_capital_adjusting_entry_voucher_confirmation_url(share_capital_id: @share_capital.id, id: @voucher_processing.find_voucher.id), notice: 'Transaction created successfully'
        else
          redirect_to share_capital_adjusting_entries_url(@share_capital), alert: 'Error'
        end
      end

      private
      def voucher_params
        params.require(:share_capitals_adjusting_entries_voucher_processing).
        permit(:date, :reference_number, :description, :cart_id, :employee_id, :share_capital_id, :account_number)
      end
    end
  end
end
