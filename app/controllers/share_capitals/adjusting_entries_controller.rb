module ShareCapitals
  class AdjustingEntriesController < ApplicationController
    def index
      @share_capital = current_office.share_capitals.find(params[:share_capital_id])
      @subscriber = @share_capital.subscriber
      @loans = @subscriber.loans
      @subscriptions = @subscriber.program_subscriptions
      @voucher_processing = ShareCapitals::AdjustingEntries::VoucherProcessing.new
    end
  end
end
