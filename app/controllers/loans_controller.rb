class LoansController < ApplicationController
  def index
    if params[:search].present?
      @pagy, @loans = pagy(current_office.loans.not_archived.
        includes(:loan_product, :receivable_account, borrower: [:avatar_attachment =>[:blob]]).
        text_search(params[:search]))
    else
      @pagy, @loans = pagy(current_office.loans.
      includes(:loan_product, :receivable_account, borrower: [:avatar_attachment =>[:blob]]).
      not_archived)
    end
  end

  def show
    @loan = current_office.loans.find(params[:id])
    @pagy, @amortization_schedules = pagy(@loan.amortization_schedules.order(date: :asc))
    respond_to do |format|
      format.html
      format.pdf do
        pdf = StatementOfAccounts::LoanPdf.new(
        loan:         @loan,
        view_context: view_context)
        send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Statement of Account.pdf"
      end
    end
  end
end
