class BelowMinimumBalanceShareCapitalsController < ApplicationController
  def index
    if params[:search].present?
      @pagy, @share_capitals = pagy(current_office.share_capitals.below_minimum_balance.text_search(params[:search]))
    else
      @pagy, @share_capitals = pagy(current_office.share_capitals.below_minimum_balance.includes(:equity_account, :share_capital_product, :subscriber => [:avatar_attachment => [:blob]]))
    end
    respond_to do |format|
      format.html
      format.xlsx
    end 
  end
end
