class SavingsAccountsWithMinimumBalancesController < ApplicationController
  def index
    @pagy, @savings_accounts = pagy(current_office.savings.has_minimum_balances)
  end
end
