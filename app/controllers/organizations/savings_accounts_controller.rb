module Organizations
  class SavingsAccountsController < ApplicationController
    def index
      @organization = current_office.organizations.find(params[:organization_id])
    end
  end
end
