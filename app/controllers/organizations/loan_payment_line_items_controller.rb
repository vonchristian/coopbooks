module Organizations
  class LoanPaymentLineItemsController < ApplicationController
    def index
      @organization = current_office.organizations.find(params[:organization_id])
      if params[:search].present?
        @pagy, @loans = pagy(@organization.member_loans.text_search(params[:search]))
      else
        @pagy, @loans = pagy(@organization.member_loans.order(created_at: :desc))
      end
      @loans_with_payment_line_items = loans
    end
    def new
      @organization           = current_office.organizations.find(params[:organization_id])
      @loan                   = @organization.member_loans.find(params[:loan_id])
      @loan_payment_line_item = Organizations::LoanPaymentLineItemProcessing.new
    end
    def create
      @organization           = current_office.organizations.find(params[:organization_id])
      @loan_payment_line_item = Organizations::LoanPaymentLineItemProcessing.new(loan_payment_line_item_params)
      if @loan_payment_line_item.valid?
        @loan_payment_line_item.process!
        redirect_to organization_loan_payment_line_items_url(@organization), notice: 'added successfully.'
      else
        render :new
      end
    end
    def loans
      ids = current_user.voucher_amounts.pluck(:commercial_document_id)
      current_cooperative.loans.where(id: ids)
    end

    private
    def loan_payment_line_item_params
      params.require(:organizations_loan_payment_line_item_processing).
      permit(:principal_amount, :interest_amount, :penalty_amount, :loan_id, :employee_id, :organization_id, :cooperative_id)
    end
  end
end
