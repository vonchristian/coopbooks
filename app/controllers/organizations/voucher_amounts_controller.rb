module Organizations
  class VoucherAmountsController < ApplicationController
    def destroy
      @organization = current_cooperative.organizations.find(params[:organization_id])
      @voucher_amount = current_user.voucher_amounts.find(params[:id])
      @voucher_amount.destroy
      redirect_to organization_loan_payment_line_items_url(@organization), alert: 'removed successfully'
    end
  end
end 
