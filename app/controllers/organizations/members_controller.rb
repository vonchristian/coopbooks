require 'will_paginate/array'
module Organizations
  class MembersController < ApplicationController

    def index
      @organization = current_office.organizations.find(params[:organization_id])
      if params[:membership_type].present?
        @members = @organization.member_memberships.select{|m| m.current_membership.membership_type == params[:membership_type]}.paginate(page: params[:page], per_page: 25)
      elsif params[:search].present?
        @members = @organization.member_memberships.text_search(params[:search]).paginate(page: params[:page], per_page: 25)
      else
        @members = @organization.member_memberships.uniq.paginate(page: params[:page], per_page: 25)
      end
    end

    def new
      @organization = current_office.organizations.find(params[:organization_id])
      @member = Organizations::MembershipProcessing.new(organization_id: @organization.id)
      if params[:search].present?
        @pagy, @members = pagy(current_cooperative.members.text_search(params[:search]))
      else
        @pagy, @members = pagy(current_cooperative.members)
      end
    end
    def create
      @organization = current_office.organizations.find(params[:organization_id])
      @member = Organizations::MembershipProcessing.new(
        member_params.merge(
          office_id: current_office.id,
          organization_id: @organization.id
        )
      )
      @member.process!
      redirect_to new_organization_member_url(@organization), notice: "Member added successfully."
    end

    private
    def member_params
      params.require(:organizations_membership_processing).permit(:organization_membership_id, :organization_membership_type)
    end
  end
end
