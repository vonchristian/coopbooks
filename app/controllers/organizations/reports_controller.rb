module Organizations
  class ReportsController < ApplicationController
    def index
      @organization = current_office.organizations.find(params[:organization_id])

      respond_to do |format|
        format.html
        format.xlsx
        
      end
    end
  end
end
