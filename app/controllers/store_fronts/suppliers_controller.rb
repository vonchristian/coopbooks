module StoreFronts
  class SuppliersController < ApplicationController
    def index
      @store_front      = current_office.store_fronts.find(params[:store_front_id])
      @pagy, @suppliers = pagy(@store_front.suppliers)
    end

    def new
      @store_front = current_office.store_fronts.find(params[:store_front_id])
      @supplier    = @store_front.suppliers.build
    end

    def create
      @store_front = current_office.store_fronts.find(params[:store_front_id])
      @supplier    = @store_front.suppliers.create(supplier_params)
      if @supplier.valid?
        @supplier.save!
        redirect_to store_front_suppliers_url(store_front_id: @store_front.id, id: @supplier.id), notice: 'Supplier saved successfully.'
      else
        render :new
      end
    end

    def show
      @store_front = current_office.store_fronts.find(params[:store_front_id])
      @supplier    = @store_front.suppliers.find(params[:id])
    end

    private
    def supplier_params
      params.require(:store_fronts_supplier).
      permit(:business_name, :first_name, :last_name, :contact_number, :address)
    end
  end
end
