module StoreFronts
  module Suppliers
    class PurchaseProcessingsController < ApplicationController
      def create
        @purchase_processing = StoreFronts::Orders::PurchaseOrderProcessing.new(purchase_params)
        if @purchase_processing.valid?
          @purchase_processing.process!
          redirect_to store_front_supplier_url(store_front_id: params[:store_fronts_orders_purchase_order_processing][:store_front_id], id: params[:store_fronts_orders_purchase_order_processing][:supplier_id]), notice: 'saved successfully'
        else
          render :new
        end
      end

      private
      def purchase_params
        params.require(:store_fronts_orders_purchase_order_processing).
        permit(:cart_id, :date, :voucher_id, :supplier_id, :employee_id, :store_front_id)
      end
    end
  end
end
