module StoreFronts
  module Suppliers
    class ProductSelectionsController < ApplicationController
      def new
        @store_front         = current_office.store_fronts.find(params[:store_front_id])
        @supplier            = @store_front.suppliers.find(params[:supplier_id])
        @pagy, @products     = pagy(@store_front.products)
        @purchase_processing = StoreFronts::Orders::PurchaseOrderProcessing.new
      end
    end
  end
end
