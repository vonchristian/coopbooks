module StoreFronts
  module Suppliers
    class PurchasesController < ApplicationController
      def new
        @store_front = current_office.store_fronts.find(params[:store_front_id])
        @supplier    = @store_front.suppliers.find(params[:supplier_id])
        @product     = @store_front.products.find(params[:product_id])
        @purchase    = StoreFronts::LineItems::PurchaseProcessing.new
      end

      def create
        @store_front = current_office.store_fronts.find(params[:store_front_id])
        @supplier    = @store_front.suppliers.find(params[:supplier_id])
        @purchase    = StoreFronts::LineItems::PurchaseProcessing.new(purchase_params)
        if @purchase.valid?
          @purchase.process!
          redirect_to new_store_front_supplier_product_selection_url(store_front_id: @store_front.id, supplier_id: @supplier.id), notice: 'added successfully'
        else
          render :new
        end
      end

      private
      def purchase_params
        params.require(:store_fronts_line_items_purchase_processing).
        permit(:cart_id, :product_id, :unit_of_measurement_id, :quantity, :unit_cost, :total_cost, :barcode)
      end
    end
  end
end
