module TimeDeposits
  class InterestExpensesController < ApplicationController
    def new
      @time_deposit     = current_office.time_deposits.find(params[:time_deposit_id])
      @interest_expense = TimeDeposits::InterestExpense.new
    end

    def create
      @time_deposit = current_office.time_deposits.find(params[:time_deposit_id])
      @interest_expense = TimeDeposits::InterestExpense.new(interest_expense_params)
      if @interest_expense.valid?
        @interest_expense.process!
        redirect_to time_deposit_interest_expense_voucher_url(time_deposit_id: @time_deposit.id, id: @interest_expense.find_voucher.id), notice: 'created successfully'
      else
        render :new
      end
    end

    private
    def interest_expense_params
      params.require(:time_deposits_interest_expense).
      permit(:date, :reference_number, :account_number, :description, :amount, :time_deposit_id, :employee_id)
    end
  end
end
