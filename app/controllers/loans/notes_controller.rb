module Loans
  class NotesController < ApplicationController
    respond_to :html, :json

    def index
      @loan = current_office.loans.find(params[:loan_id])
      @notes = @loan.notes.paginate(page: params[:page], per_page: 25)
      @note = @loan.notes.build
    end

    def new
      @loan = current_office.loans.find(params[:loan_id])
      @note = @loan.notes.build
    end

    def create
      @loan = current_office.loans.find(params[:loan_id])
      @note = @loan.notes.create(note_params)
      if @note.valid?
        @note.save!
        redirect_to loan_notes_url(@loan), notice: 'Note saved successfully'
      else
        render :new
      end 
    end

    private
    def note_params
      params.require(:note).
      permit(:date, :title, :content, :noter_id)
    end
  end
end
