require 'will_paginate/array'
module Loans
  class PaymentsController < ApplicationController
    def index
      @loan = current_cooperative.loans.find(params[:loan_id])
      if params[:search].present?
        @pagy, @payments = pagy(@loan.loan_payments.includes(:recorder).text_search(params[:search]).order(entry_date: :desc))
      else
      @pagy, @payments = pagy(@loan.loan_payments.includes(:recorder).order(entry_date: :desc))
    end
    end
    def new
      @loan = current_cooperative.loans.find(params[:loan_id])
      @payment = LoansModule::Loans::PaymentVoucher.new
    end
    def create
      @loan = current_cooperative.loans.find(params[:loan_id])
      @payment = LoansModule::Loans::PaymentVoucher.new(payment_params)
      if @payment.valid?
        @payment.create_payment_voucher!
        redirect_to loan_payment_voucher_url(schedule_id: @payment.schedule_id, loan_id: @loan.id, id: @payment.find_voucher.id), notice: "Payment voucher created successfully."
      else
        render :new
      end
    end

    private
    def payment_params
      params.require(:loans_module_loans_payment_voucher).
      permit(:principal_amount, :interest_amount, :penalty_amount, :amortization_schedule_id, :description, :employee_id, :loan_id, :reference_number, :date, :cash_account_id, :account_number)
    end
    # def payment_processor_params
    #   @loan.payment_processor.to_s.underscore.gsub("/", "_").to_sym
    # end
  end
end
