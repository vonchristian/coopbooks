module Loans
  module PaymentVouchers
    class ConfirmationsController < ApplicationController
      def create
        @loan    = current_cooperative.loans.find(params[:loan_id])
        @voucher = Voucher.find(params[:payment_voucher_id])
        create_entry
        update_paid_at
        redirect_to loan_payments_url(@loan), notice: "Payment saved successfully."
      end

      def create_entry
        Vouchers::EntryProcessing.new(
          voucher:    @voucher,
          employee:   current_user,
          updateable: @loan
        ).process!
      end

      def update_paid_at
        LoansModule::Loans::PaidAtUpdater.new(loan: @loan, date: @voucher.date).update_paid_at!
      end
    end
  end
end
