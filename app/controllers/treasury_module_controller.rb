class TreasuryModuleController < ApplicationController
  def index
    @employee      = current_user
    @cash_accounts = current_user.cash_accounts
    @receipts      = @cash_accounts.debit_entries
  end
end
