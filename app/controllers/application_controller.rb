class ApplicationController < ActionController::Base
  include Pundit
  include Pagy::Backend
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_action :authenticate_user!
  rescue_from Pundit::NotAuthorizedError, with: :permission_denied
  helper_method :current_cooperative, :current_cart, :multiple_transaction_cart, :current_office, :current_store_front

  private
  def current_cart
    Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
    cart = Cart.create!(employee: current_user)
    session[:cart_id] = cart.id
    cart
  end


  def current_cooperative
    @current_cooperative ||= current_user.cooperative
  end

  def current_office
    @current_office ||= current_user.office
  end

  def current_store_front
    @current_store_front ||= current_user.store_front
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end


  def permission_denied
    redirect_to "/", alert: 'Sorry but you are not allowed to access this page.'
  end
end
