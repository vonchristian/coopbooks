class SavingGroupsController < ApplicationController
  def show
    @saving_group = current_office.saving_groups.find(params[:id])
    @pagy, @savings_accounts = pagy(@saving_group.savings)
  end
end
