class OccupationRegistrationsController < ApplicationController
	def new
		@occupation = Occupation.new
	end
	def create
		@occupation = Occupation.create(occupation_params)
	end

	private
	def occupation_params
		params.require(:occupation).permit(:title)
	end
end
