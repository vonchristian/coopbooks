class MembershipApplicationsController < ApplicationController
  def new
    @membership = MembershipApplication.new
  end

  def create
    @membership = MembershipApplication.new(membership_params)
    if @membership.register!
      redirect_to member_url(@membership.find_member), notice: "Member information saved successfully"
    else
      render :new
    end
  end


  private
  def membership_params
    params.require(:membership_application).permit(
      :first_name,
      :middle_name,
      :last_name,
      :date_of_birth,
      :account_number,
      :civil_status,
      :sex,
      :contact_number,
      :email,
      :office_id,
      :cooperative_id,
      :membership_date,
      :membership_category_id,
      :complete_address,
      :barangay_id,
      :occupation_ids => [])
  end
end
