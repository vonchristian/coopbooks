module Portfolios
	class LoansController < ApplicationController

		def index
      @office = current_cooperative.offices.find(params[:office_id])
        if params[:loan_product].present?
          @loan_product = @office.loan_products.find(params[:loan_product])
          @loans = @loan_product.loans
        else
      	  @loans = @office.loans
        end
        @to_date = Date.parse(params[:to_date])
      respond_to do |format|
	      format.html
	      format.xlsx
	    end
		end
	end
end
