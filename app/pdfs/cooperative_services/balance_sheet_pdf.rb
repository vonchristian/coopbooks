module CooperativeServices
  class BalanceSheetPdf < Prawn::Document
    attr_reader :accounts, :from_date, :to_date, :view_context, :cooperative
    def initialize(args={})
      super(margin: 40, page_size: "A4", page_layout: :portrait)
      @accounts            = args[:accounts]
      @cooperative         = @cooperative_service.cooperative
      @from_date           = args[:from_date]
      @to_date             = args[:to_date]
      @view_context        = args[:view_context]
      heading
      accounts_details
    end

    private
    def price(number)
      view_context.number_to_currency(number, :unit => "P ")
    end

    def heading
      bounding_box [300, 770], width: 50 do
        image "#{Rails.root}/app/assets/images/#{cooperative.abbreviated_name.downcase}_logo.jpg", width: 50, height: 50
      end
      bounding_box [360, 770], width: 170 do
          text "#{cooperative.abbreviated_name }", style: :bold, size: 20
          text "#{cooperative.name.try(:upcase)}", size: 8
          text "#{cooperative.address}", size: 8
      end
      bounding_box [0, 770], width: 300 do
        text "BALANCE SHEET", style: :bold, size: 12
        text "#{cooperative_service.title}", size: 11, style: :bold
        text "As of: #{to_date.strftime("%B %e, %Y")}", size: 10
      end
      move_down 20
      stroke do
        move_down 5
        stroke_color 'CCCCCC'
        line_width 0.2
        stroke_horizontal_rule
        move_down 15
      end
    end

    def accounts_details

    end
  end
end
