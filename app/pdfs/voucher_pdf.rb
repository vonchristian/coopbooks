require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/prawn_outputter'
class VoucherPdf < Prawn::Document
  attr_reader :voucher, :cooperative, :view_context, :title, :approver, :office
  def initialize(args={})
    super(margin: 50, page_size: "LEGAL", page_layout: :portrait)
    @title        = args[:title] || "Cash Disbursement Voucher"
    @voucher      = args.fetch(:voucher)
    @cooperative  = @voucher.cooperative
    @office       = @voucher.office
    @view_context = args.fetch(:view_context)
    @approver     = Vouchers::ApproverFinder.new(voucher: voucher).approver
    heading
    payee_details
    voucher_details
    signatory_details
    font Rails.root.join("app/assets/fonts/open_sans_regular.ttf")

  end

  private
  def price(number)
    view_context.number_to_currency(number, :unit => "P ")
  end
  def heading

    bounding_box [425, 940], width: 50 do
      qr_code
    end

    bounding_box [0, 920], width: 350 do
      text "#{title.upcase}", style: :bold, size: 12
      move_down 5
      text "#{cooperative.name.upcase}", size: 10
      text "#{office.name}", size: 10
      text "#{office.address} | #{office.contact_number}", size: 10
    end
    move_down 15
    stroke do
      stroke_color '24292E'
      line_width 1
      stroke_horizontal_rule
      move_down 15
    end
  end
  def payee_details
    text "VOUCHER DETAILS", style: :bold, size: 10
    move_down 5
    table([["", "Payee:", "<b>#{voucher.payee.try(:name).try(:upcase)}</b>"]], cell_style: { inline_format: true, size: 10}, column_widths: [20, 100, 130]) do
      cells.borders = []
    end

    table([["", "Address:", "#{voucher.payee.current_address.try(:complete_address)}"]], cell_style: { inline_format: true, size: 10}, column_widths: [20, 100, 200]) do
      cells.borders = []
    end

    table([["", "Contact #:", "#{voucher.payee.current_contact.try(:number)}"]], cell_style: { inline_format: true, size: 10 }, column_widths: [20, 100, 200]) do
      cells.borders = []
    end

    table([["", "Description:", "#{voucher.description}", "", ""]], cell_style: { inline_format: true, size: 10, font: "Helvetica", :padding => [2, 3, 2, 3]}, column_widths: [20, 100, 200]) do
      cells.borders = []
    end
    table([["", "Office:", "#{voucher.payee.try(:current_organization).try(:name)}"]], cell_style: { inline_format: true, size: 10}, column_widths: [20, 100, 200]) do
      cells.borders = []
    end

    table([["", "Reference #:", "#{voucher.reference_number}"]], cell_style: { inline_format: true, size: 10}, column_widths: [20, 100, 200]) do
      cells.borders = []
    end
  end


  def debit_amount_for(amount)
    if amount.debit?
      amount.amount
    end
  end

  def credit_amount_for(amount)
    if amount.credit?
      amount.amount
    end
  end

  def qr_code
    print_qr_code("https://www.coopcatalyst.co/vouchers/#{voucher.id}", extent: 90, stroke: false)
  end

  def voucher_details
    move_down 20
    table([["DEBIT", "ACCOUNT", "CREDIT"]], cell_style: { inline_format: true, size: 10 },  column_widths: [130, 220, 130]) do
      row(0).font_style = :bold
    end
      voucher.voucher_amounts.debit.order(amount_type: :desc).each do |amount|
        table([["#{price(debit_amount_for(amount))}", "#{amount.account.try(:display_name)}", "#{price(credit_amount_for(amount))}"]], cell_style: { inline_format: true, size: 10}, column_widths: [130, 220, 130]) do
          # cells.borders = []
          column(0).align = :right
          column(2).align = :right
        end
      end
      voucher.voucher_amounts.credit.order(amount_type: :desc).each do |amount|
        table([["#{price(debit_amount_for(amount))}", "#{amount.account.try(:display_name)}", "#{price(credit_amount_for(amount))}"]], cell_style: { inline_format: true, size: 10 }, column_widths: [130, 220, 130]) do
          # cells.borders = []
          column(0).align = :right
          column(2).align = :right
        end
      end
      table([["#{price(voucher.voucher_amounts.debit.sum(&:amount))}", "", "#{price(voucher.voucher_amounts.credit.sum(&:amount))}"]], cell_style: { inline_format: true, size: 10 },  column_widths: [130, 220, 130]) do
        # cells.borders = []
        row(0).font_style = :bold
        column(0).align = :right
        column(2).align = :right
      end

  end
  def signatory_details
    move_down 10
    table([["PREPARED BY", "APPROVED BY", "DISBURSED BY", "RECEIVED BY"]],
      cell_style: { inline_format: true, size: 10, font: "Helvetica", :padding => [2, 4, 2, 4]},
      column_widths: [120, 120, 120, 120]) do
        cells.borders = []
    end
    move_down 30
    table(signatory, cell_style: { inline_format: true, size: 9, font: "Helvetica", :padding => [2, 4, 2, 4]}, column_widths: [110, 110, 110, 110]) do
      cells.borders = []
      row(0).font_style = :bold
    end
  end

  def signatory
    [["#{voucher.preparer.first_middle_and_last_name.to_s.try(:upcase)}",
      "#{approver.first_middle_and_last_name.to_s.upcase}",
      "#{voucher.disburser.try(:first_middle_and_last_name).try(:upcase)}",
      "#{voucher.payee.first_middle_and_last_name.try(:upcase)}"]] +
    [["#{voucher.preparer_current_occupation.try(:titleize)}",
      "#{approver.current_occupation.to_s.titleize}",
      "#{voucher.disburser.try(:designation)}",
      "Payee"]]
  end
end
