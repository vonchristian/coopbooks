module AccountingModule
  module Reports
    class BalanceSheetPdf < Prawn::Document
      attr_reader :assets, :liabilities, :equity, :view_context, :to_date, :cooperative, :office
      def initialize(args={})
        super(margin: 40, page_size: [612, 988], page_layout: :portrait)
        @from_date    = args[:from_date]
        @to_date      = args[:to_date]

        @view_context = args[:view_context]
        @cooperative  = args[:cooperative]
        @office       = args[:office]
        heading
        assets_table
        liabilities_and_equity_table
      end

      private

      def price(number)
        view_context.number_to_currency(number, :unit => "P ")
      end

      def heading
        # bounding_box [300, 770], width: 50 do
        #   image "#{Rails.root}/app/assets/images/#{cooperative.abbreviated_name.downcase}_logo.jpg", width: 50, height: 50
        # end
        bounding_box [360, 920], width: 200 do
            text "#{cooperative.abbreviated_name }", style: :bold, size: 20
            text "#{cooperative.name.try(:upcase)}", size: 8
            text "#{cooperative.address}", size: 8
        end
        bounding_box [0, 920], width: 400 do
          text "BALANCE SHEET",  size: 14, style: :bold
          text "As of: #{to_date.strftime("%B %e, %Y")} ",  size: 10
          text "#{office.name}", size: 10
        end
        move_down 30
        stroke do
          stroke_color '24292E'
          line_width 1
          stroke_horizontal_rule
          move_down 5
        end
      end
      def assets_table
        table([["ASSETS"]], cell_style: {padding: [2,2], inline_format: true, size: 10},
        column_widths: [230, 100]) do
          cells.borders = []
          column(0).font_style = :bold
        end
        cooperative.grand_parent_account_categories.assets.order(code: :asc).each do |grand_parent_account_category|
          table([["#{grand_parent_account_category.title}"]], cell_style: {padding: [2,2], inline_format: true, size: 10},
          column_widths: [230, 100]) do
            cells.borders = []
          end
          grand_parent_account_category.parent_account_categories.each do |parent_account_category|
            table([["","#{parent_account_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10},
            column_widths: [10, 320, 100]) do
              cells.borders = []
              column(2).align = :right
            end
            parent_account_category.parent_account_sub_categories.each do |parent_account_sub_category|
              table([["", "", "#{parent_account_sub_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10},
              column_widths: [10, 10, 310, 100]) do
                cells.borders = []
                column(3).align = :right
              end

              parent_account_sub_category.account_categories.each do |account_category|
                table([["","", "", "#{account_category.title}", price(account_category.balance(to_date: @to_date))]], cell_style: { padding: [2,2], inline_format: true, size: 10},
                column_widths: [10, 10, 10, 300, 100]) do
                  cells.borders = []
                  column(4).align = :right
                end
              end
            end
            stroke_horizontal_rule
            table([["", "Total #{parent_account_category.title}",price(parent_account_category.balance(to_date: @to_date)) ]], cell_style: { padding: [2, 2], inline_format: true, size: 10},
            column_widths: [10, 320, 100]) do
              cells.borders = []
              column(2).align = :right
            end
          end
          stroke_horizontal_rule

            table([["Total #{grand_parent_account_category.title}",price(grand_parent_account_category.balance(to_date: @to_date)) ]], cell_style: { padding: [2, 2], inline_format: true, size: 10},
            column_widths: [330, 100]) do
              cells.borders = []
              column(1).align = :right
            end
          end
            stroke_horizontal_rule
            table([["TOTAL ASSETS", price(cooperative.grand_parent_account_categories.assets.balance(to_date: @to_date))]], cell_style: {padding: [2,2], inline_format: true, size: 10},
            column_widths: [330, 100]) do
              cells.borders = []
              column(0).font_style = :bold
              column(1).align = :right
              column(1).font_style = :bold

            end
            move_down 10
      end

      def liabilities_and_equity_table
        net_surplus = cooperative.grand_parent_account_categories.revenues.balance(from_date: @to_date.beginning_of_year, to_date: @to_date) - cooperative.grand_parent_account_categories.expenses.balance(from_date: @to_date.beginning_of_year, to_date: @to_date)
        table([["LIABILITIES, EQUITY AND RESERVE"]], cell_style: {padding: [2,2], inline_format: true, size: 10},
        column_widths: [230, 100]) do
          cells.borders = []
          column(0).font_style = :bold
        end

        cooperative.grand_parent_account_categories.liabilities.order(code: :asc).each do |grand_parent_account_category|
          table([["#{grand_parent_account_category.title}"]], cell_style: {padding: [2,2], inline_format: true, size: 10},
          column_widths: [230, 100]) do
            cells.borders = []
          end

          grand_parent_account_category.parent_account_categories.each do |parent_account_category|
            table([["","#{parent_account_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10},
            column_widths: [10, 320, 100]) do
              cells.borders = []
              column(2).align = :right
            end
            parent_account_category.parent_account_sub_categories.each do |parent_account_sub_category|
              table([["","", "#{parent_account_sub_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10},
              column_widths: [10, 10, 310, 100]) do
                cells.borders = []
              end
              parent_account_sub_category.account_categories.each do |account_category|
                table([["","", "", "#{account_category.title}", price(account_category.balance(to_date: @to_date))]], cell_style: { padding: [2,2], inline_format: true, size: 10},
                column_widths: [10, 10, 10, 300, 100]) do
                  cells.borders = []
                  column(4).align = :right
                end
              end
            end
            stroke_horizontal_rule
            table([["", "Total #{parent_account_category.title}", price(parent_account_category.balance(to_date: @to_date))]], cell_style: { padding: [2, 2], inline_format: true, size: 10 },
            column_widths: [10, 320, 100]) do
              cells.borders = []
              column(2).align = :right
          end
          end
          stroke_horizontal_rule

            table([["Total #{grand_parent_account_category.title}", price(grand_parent_account_category.balance(to_date: @to_date))]], cell_style: { padding: [2, 2], inline_format: true, size: 10 },
            column_widths: [330, 100]) do
              cells.borders = []
              column(1).align = :right
          end
        end
        cooperative.grand_parent_account_categories.equities.order(code: :asc).each do |grand_parent_account_category|
          table([["#{grand_parent_account_category.title}"]], cell_style: {padding: [2,2], inline_format: true, size: 10 },
          column_widths: [230, 100]) do
            cells.borders = []
          end
          grand_parent_account_category.parent_account_categories.each do |parent_account_category|
            table([["","#{parent_account_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10 },
            column_widths: [10, 320, 100]) do
              cells.borders = []
              column(2).align = :right
            end
            parent_account_category.parent_account_sub_categories.each do |parent_account_sub_category|
              table([["","", "#{parent_account_sub_category.title}"]], cell_style: { padding: [2,2], inline_format: true, size: 10 },
              column_widths: [10, 10, 310, 100]) do
                cells.borders = []
              end
              parent_account_sub_category.account_categories.each do |account_category|
                table([["","", "", "#{account_category.title}", price(account_category.balance(to_date: @to_date))]], cell_style: { padding: [2,2], inline_format: true, size: 10},
                column_widths: [10, 10, 10, 300, 100]) do
                  cells.borders = []
                  column(4).align = :right
                end
              end
            end
            stroke_horizontal_rule
            table([["", "Total #{parent_account_category.title}", price(parent_account_category.balance(to_date: @to_date))]], cell_style: { padding: [2, 2], inline_format: true, size: 10 },
            column_widths: [10, 330, 100]) do
              cells.borders = []
              column(2).align = :right
            end
          end
          stroke_horizontal_rule

          table([["Total #{grand_parent_account_category.title}", price(grand_parent_account_category.balance(to_date: @to_date))]], cell_style: { padding: [2, 2], inline_format: true, size: 10 },
          column_widths: [330, 100]) do
            cells.borders = []
            column(1).align = :right
          end
        end
        table([["Undivided Net Surplus/Loss", price(net_surplus)]], cell_style: { padding: [2, 2], inline_format: true, size: 10 },
        column_widths: [330, 100]) do
          cells.borders = []
          column(1).align = :right
        end

        stroke_horizontal_rule
        table([["TOTAL LIABILITIES AND EQUITY", price(cooperative.grand_parent_account_categories.liabilities.balance(to_date: @to_date) + cooperative.grand_parent_account_categories.equities.balance(to_date: @to_date) + net_surplus)]], cell_style: {padding: [2,2], inline_format: true, size: 10 },
        column_widths: [330, 100]) do
          cells.borders = []
          column(0).font_style = :bold
          column(1).font_style = :bold

          column(1).align = :right

        end
      end
    end
  end
end
