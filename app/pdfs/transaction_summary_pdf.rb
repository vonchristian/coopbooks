require 'prawn/icon'
class TransactionSummaryPdf < Prawn::Document
  attr_reader :employee, :date, :view_context, :office, :cooperative
  def initialize(args={})
    super(margin: 30, page_size: "A4", page_layout: :portrait)
    @employee     = args.fetch(:employee)
    @office       = @employee.office
    @cooperative  = @employee.cooperative
    @date         = args[:date]
    @view_context = args[:view_context]
    heading
    cash_books
    loans_summary
    font Rails.root.join("app/assets/fonts/open_sans_light.ttf")
  end
  private
  def price(number)
    view_context.number_to_currency(number, :unit => "P ")
  end
  def heading
    bounding_box [250, 770], width: 50 do
      image "#{Rails.root}/app/assets/images/#{cooperative.abbreviated_name.downcase}_logo.jpg", width: 50, height: 50
    end
    bounding_box [310, 770], width: 400 do
        text "#{cooperative.abbreviated_name }", style: :bold, size: 20
        text "#{cooperative.name.try(:upcase)}", size: 10
        text "#{cooperative.address}", size: 8
    end
    bounding_box [0, 770], width: 400 do
      text "TRANSACTIONS SUMMARY", style: :bold, size: 12
      text "Date Covered: #{date.strftime("%B %e, %Y")}", size: 10
      move_down 2
      text "Employee: #{employee.full_name}", size: 10
    end
    move_down 20
    stroke do
      stroke_color '24292E'
      line_width 0.5
      stroke_horizontal_rule
      move_down 5
    end
  end
  def cash_books
    text "Cash Accounts",  color: "4A86CF", style: :bold, size: 10
    table(cash_books_data, header: true, cell_style: { size: 10 }, column_widths: [100, 100, 110, 110, 110]) do
      cells.borders = []
      column(2).align = :right
      column(3).align = :right
      column(4).align = :right
    end
    stroke do
      stroke_color 'CCCCCC'
      line_width 0.2
      stroke_horizontal_rule
      move_down 15
    end
  end

  def cash_books_data
    [["Account", "Beginning Balance", "Debits", "Credits", "Ending Balance"]] +
    employee.cash_accounts.map{|account| [account.name, price(account.balance(to_date: date.yesterday.end_of_day)), price(account.debits_balance(to_date: date)), price(account.credits_balance(to_date: date)), price(account.balance(to_date: date))] }
  end
  def loans_summary
    text "Loans ",  color: "4A86CF", style: :bold, size: 10
    table(loans_data, header: true, cell_style: { size: 10 }, column_widths: [100, 100, 110, 110, 110]) do
      cells.borders = []
      column(2).align = :right
      column(3).align = :right
      column(4).align = :right
    end
    stroke do
      stroke_color 'CCCCCC'
      line_width 0.2
      stroke_horizontal_rule
      move_down 15
    end
  end
  def loans_data
    [["Account", "Beginning Balance", "Debits", "Credits", "Ending Balance"]] +
    office.loan_products.receivable_account_categories.map{|account| [account.name, price(account.balance(to_date: date.yesterday.end_of_day)), price(account.debits_balance(to_date: date)), price(account.credits_balance(to_date: date)), price(account.balance(to_date: date))] }
  end
end
