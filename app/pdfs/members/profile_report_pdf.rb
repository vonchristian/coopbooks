module Members
  class ProfileReportPdf < Prawn::Document
    attr_reader :member, :cooperative
    def initialize(args={})
      super(margin: 40, page_size: 'A4', page_layout: :portrait)
      @member = args.fetch(:member)
      @cooperative = args.fetch(:cooperative)
      heading
      members_data
    end
    def heading
      bounding_box [300, 770], width: 50 do
        image "#{Rails.root}/app/assets/images/#{cooperative.abbreviated_name.downcase}_logo.jpg", width: 50, height: 50
      end
      bounding_box [360, 770], width: 200 do
          text "#{cooperative.abbreviated_name }", style: :bold, size: 20
          text "#{cooperative.name.try(:upcase)}", size: 8
          text "#{cooperative.address}", size: 8
      end
      bounding_box [0, 770], width: 400 do
        text "MEMBER'S DATA SHEET",  size: 14, style: :bold

      end 
      move_down 30
      stroke do
        stroke_color '24292E'
        line_width 1
        stroke_horizontal_rule
        move_down 5
      end
    end
    def members_data
      table([['First Name', @member.first_name]])
    end
  end
end
