module CashBooks
  class TransactionsPdf < Prawn::Document
    attr_reader :employee, :from_date, :to_date, :view_context, :cooperative, :cash_account, :credit_account_categories, :debit_account_categories, :updated_account_categories, :cash_count_report, :office
    def initialize(args)
      super(margin: 40, page_size: [612, 988], page_layout: :portrait)
      @employee     = args.fetch(:employee)
      @office       = @employee.office
      @cooperative  = @employee.cooperative
      @cash_account = args.fetch(:cash_account)
      @to_date      = args.fetch(:to_date)
      @from_date    = @to_date.beginning_of_day
      @view_context = args[:view_context]
      @credit_account_categories  = AccountingModule::CashAccountTransactionConsolidator.new(cash_account: @cash_account, date: @to_date, office: office).credit_account_categories
      @debit_account_categories   = AccountingModule::CashAccountTransactionConsolidator.new(cash_account: @cash_account, date: @to_date, office: office).debit_account_categories
      @updated_account_categories = AccountingModule::CashAccountTransactionConsolidator.new(cash_account: @cash_account, date: @to_date, office: office).updated_account_categories
      @cash_count_report          = @employee.cash_count_reports.created_at(from_date: @from_date, to_date: @to_date.end_of_day).last
      heading
      bounding_box [0, 840], width: 290 do
        cash_account_details
      end
      bounding_box [295, 840], width: 210 do
        cash_counts_summary

      end
      cash_receipts
      cash_disbursements
      accounts_summary
    end

    private
    def price(number)
      view_context.number_to_currency(number, :unit => "P ")
    end

    def display_commercial_document_for(entry)
      entry.commercial_document.try(:abbreviated_name) || entry.commercial_document.name
    end

    def heading
      bounding_box [300, 920], width: 50 do
        image "#{Rails.root}/app/assets/images/#{cooperative.abbreviated_name.downcase}_logo.jpg", width: 50, height: 50
      end
      bounding_box [360, 920], width: 200 do
          text "#{cooperative.abbreviated_name }", style: :bold, size: 20
          text "#{cooperative.name.try(:upcase)}", size: 8
          text "#{cooperative.address}", size: 8
      end
      bounding_box [0, 920], width: 400 do
        text "TELLER'S BLOTTER", style: :bold, size: 14
        move_down 5
        table([["Employee:", "#{employee.first_middle_and_last_name}"]],
          cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
          column_widths: [150, 150]) do
          cells.borders = []
          column(1).font_style = :bold
        end
        table([["Date:", "#{to_date.strftime("%B %e, %Y")}"]],
          cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
          column_widths: [150, 150]) do
          cells.borders = []
          column(1).font_style = :bold
        end
         table([["Cash Account:", cash_account.name]],
          cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
          column_widths: [150, 150]) do
          cells.borders = []
          column(1).font_style = :bold
        end
      end
      move_down 10
      stroke do
        stroke_color '24292E'
        line_width 1
        stroke_horizontal_rule
        move_down 5
      end
    end


    def credit_amount_for(account_category)
      AccountingModule::CashAccounts::EntryAmountCalculator.new(date: to_date, account_category: account_category, cash_account: cash_account).payment
    end


    def debit_amount_for(account_category)
      AccountingModule::CashAccounts::EntryAmountCalculator.new(date: to_date, account_category: account_category, cash_account: cash_account).disbursement
    end

    def total_debits
      ids = []
      debit_account_categories.each do |account_category|
        ids << account_category.accounts.ids
        ids << account_category.sub_categories.map{ |sub| sub.accounts.ids }
      end
      final = ids.uniq.compact.flatten
      cash_account.entries.entered_on(from_date: from_date, to_date: to_date).map{|entry| entry.amounts.where(account_id: final).total }.sum
    end

    def total_credits
      ids = []
      credit_account_categories.each do |account_category|
        ids << account_category.accounts.ids
        ids << account_category.sub_categories.map{ |sub| sub.accounts.ids }
      end
      final = ids.uniq.compact.flatten
      cash_account.entries.entered_on(from_date: from_date, to_date: to_date).map{|entry| entry.amounts.where(account_id: final).total }.sum
    end

    def total_payment_for(account_category)
      if account_category.type == "AccountingModule::AccountCategories::Asset"
        price(account_category.balance(to_date: to_date.yesterday.end_of_day) - credit_amount_for(account_category))
      else
        price(account_category.balance(to_date: to_date.yesterday.end_of_day) + credit_amount_for(account_category))
      end
    end

    def total_disbursement_for(account_category)
      if account_category.type == "AccountingModule::AccountCategories::Liability" ||
         account_category.type == "AccountingModule::AccountCategories::Equity"

        price(account_category.balance(to_date: to_date) - debit_amount_for(account_category))
      else
        price(account_category.balance(to_date: to_date) + debit_amount_for(account_category))
      end
    end



    def cash_account_details

        text 'TRANSACTION SUMMARY', size: 10, style: :bold
        move_down 5
        table([["Cash Transfer from Vault/Cashier:", price(cash_account.balance(to_date: to_date.yesterday.end_of_day))]],
            cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
            column_widths: [200, 90]) do
            cells.borders = []
            column(1).align = :right

          end
          move_down 10
        text 'ADD: CASH RECEIPTS', size: 10, style: :bold
        move_down 5
        if !debit_account_categories.present?
          table([["", "No cash receipts"]],
              cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
              column_widths: [10, 180, 120]) do
              cells.borders = []
              column(2).align = :right
            end
         else
           debit_account_categories.order(code: :asc).each do |account_category|
              if credit_amount_for(account_category) > 0
                table([["", account_category.title, price(credit_amount_for(account_category))]], cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
                column_widths: [10, 180, 100]) do
                  cells.borders = []
                  column(2).align = :right
                end
             end
           end

            move_down 5
            stroke do
              stroke_color '24292E'
              line_width 1
              stroke_horizontal_rule
              move_down 5
            end

            table([["", "TOTAL CASH RECEIPT", price(total_debits)]],
              cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
              column_widths: [10, 180, 100]) do
                row(0).font_style = :bold
              cells.borders = []
              column(2).align = :right
            end
          end
          move_down 10
          stroke do
            stroke_color '24292E'
            line_width 1
            stroke_horizontal_rule
            move_down 10
          end
        text 'LESS: CASH DISBURSEMENTS', size: 10, style: :bold
        move_down 5
        if !credit_account_categories.present?
          text 'No cash disbursements', size: 10
        else

          credit_account_categories.order(code: :asc).each do |account_category|
            if debit_amount_for(account_category) > 0
              table([["", account_category.title, price(debit_amount_for(account_category))]], cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
              column_widths: [10, 180, 100]) do
                cells.borders = []
                column(2).align = :right
              end
            end
          end
            move_down 5
            stroke do
              stroke_color '24292E'
              line_width 0.5
              stroke_horizontal_rule
              move_down 5
            end
            table([["", "TOTAL CASH DISBURSEMENT", price(total_credits)]],
              cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
              column_widths: [10, 180, 100]) do
              cells.borders = []
              row(0).font_style = :bold
              column(2).align = :right
            end
          end
          move_down 5
            stroke do
              stroke_color '24292E'
              line_width 1
              stroke_horizontal_rule
              move_down 10
            end

            table([["Ending Balance", price(cash_account.balance(to_date: to_date))]],
              cell_style: {padding: [0,0,1,0], inline_format: true, size: 10},
              column_widths: [200, 90]) do
              cells.borders = []
              column(1).align = :right
              row(-1).font_style = :bold
            end
            move_down 10



  end

    def accounts_summary
      start_new_page
        text 'ACCOUNTS SUMMARY', size: 10, style: :bold
        text "#{to_date.strftime('%B %e, %Y')}", size: 9
        stroke_horizontal_rule
        if updated_account_categories.present?
          total_beginning_balance = BigDecimal('0')
          total_debits_balance    = BigDecimal('0')
          total_credits_balance   = BigDecimal('0')
          total_ending_balance    = BigDecimal('0')
          updated_account_categories.each do |account_category|
            total_beginning_balance += account_category.balance(to_date: to_date.yesterday.end_of_day)
            total_debits_balance    += account_category.debits_balance(from_date: from_date, to_date: to_date)
            total_credits_balance   += account_category.credits_balance(from_date: from_date, to_date: to_date)
            total_ending_balance    += account_category.balance(to_date: to_date)
          end
          table([["Code", "Account", 'Beginning Balance', "Debit", "Credit", 'Ending Balance']] +
            updated_account_categories.order(code: :asc).map{|account_category| [
              account_category.code,
              account_category.title,
              price(account_category.balance(to_date: to_date.yesterday.end_of_day)),
              price(account_category.debits_balance(from_date: from_date, to_date: to_date)),
              price(account_category.credits_balance(from_date: from_date, to_date: to_date)),
              price(account_category.balance(to_date: to_date))
              ]},
            column_widths: [50,150, 80, 80, 80, 80], cell_style: { size: 9} ) do
           column(2).align = :right
           column(3).align = :right
           column(4).align = :right
           column(5).align = :right

           row(0).font_style = :bold
           cells.borders = []
          end
          stroke do
            stroke_color '24292E'
            line_width 1
            stroke_horizontal_rule
            move_down 5
          end
          table([["Total", "", price(total_beginning_balance), price(total_debits_balance), price(total_credits_balance), price(total_ending_balance)]],
            column_widths: [150, 50, 80, 80, 80], cell_style: { size: 10} ) do

           row(0).font_style = :bold
           cells.borders = []
           column(2).align = :right
           column(3).align = :right
           column(4).align = :right
           column(5).align = :right
         end
      end
    end


    def total_debits
      cash_account.debit_entries.entered_on(from_date: from_date, to_date: to_date).map{|entry| entry.debit_amounts.where(account: cash_account).total }.sum
    end

    def total_credits
      cash_account.credit_entries.entered_on(from_date: from_date, to_date: to_date).map{|entry| entry.credit_amounts.where(account: cash_account).total }.sum
    end

    def cash_counts_summary
      text 'CASH COUNT REPORT', style: :bold, size: 10
      move_down 10
      if cash_count_report.present?
        cash_count_report_details
      end
    end

    def cash_count_report_details
      if cash_count_report.cash_counts.present?
        table [["Bill", "QTY", "Subtotal"]] +
              cash_count_report.cash_counts.map{|a| [ a.name, a.quantity, price(a.subtotal)]} +
              [["<b>TOTAL</b>", "", price(cash_count_report.total_bills)]] +
              [["Cash Balance", "", price(cash_count_report.cash_balance)]] +
              [["Overage/Shortage", "", price(cash_count_report.overage_shortage)]],

              cell_style: { inline_format: true, size: 9} do
          row(0).font_style= :bold
          column(2).align = :right

        end
      end
    end

    def cash_receipts
      start_new_page
      text "CASH RECEIPTS - #{to_date.strftime("%B %e, %Y")}", size: 10, style: :bold
      stroke_horizontal_rule
      move_down 10
      debit_entries         = cash_account.debit_entries.entered_on(from_date: from_date, to_date: to_date)
      loan_payment_data     = AccountingModule::EntriesAggregator::Loan.new(entries: debit_entries, office: office).credit_entries
      savings_deposit_data  = AccountingModule::EntriesAggregator::Saving.new(entries: debit_entries, office: office).credit_entries
      capital_build_up_data = AccountingModule::EntriesAggregator::ShareCapital.new(entries: debit_entries, office: office).credit_entries
      bank_withdrawals_data = AccountingModule::EntriesAggregator::BankAccount.new(entries: debit_entries, office: office).credit_entries
      other_revenues_data   = AccountingModule::EntriesAggregator::Revenue.new(entries: debit_entries, office: office).credit_entries

      if loan_payment_data.present?
        text 'Loan Payments', style: :bold, size: 10
        table([['Borrower', 'OR#', 'Principal', 'Interest', 'Penalty', 'Total']], column_widths: [150, 50, 80, 80, 80, 80], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
        end
        total_principals    = BigDecimal("0")
        total_interests     = BigDecimal("0")
        total_penalties      = BigDecimal("0")
        total_cash_payments = BigDecimal("0")


        loan_payment_data.order(reference_number: :desc).uniq.each do |entry|
          office.loans.updated_at(from_date: from_date, to_date: to_date).distinct.each do |loan|
            if LoansModule::Payments::Classifier.new(loan: loan, entry: entry).total_cash_payment > 0
              total_principals    += LoansModule::Payments::Classifier.new(loan: loan, entry: entry).principal
              total_interests     += LoansModule::Payments::Classifier.new(loan: loan, entry: entry).interest
              total_penalties     += LoansModule::Payments::Classifier.new(loan: loan, entry: entry).penalty
              total_cash_payments += LoansModule::Payments::Classifier.new(loan: loan, entry: entry).total_cash_payment



            table(
              [[
              "#{loan.borrower_name}",
              "#{entry.reference_number}",
              price(LoansModule::Payments::Classifier.new(loan: loan, entry: entry).principal),
              price(LoansModule::Payments::Classifier.new(loan: loan, entry: entry).interest),
              price(LoansModule::Payments::Classifier.new(loan: loan, entry: entry).penalty),
              price(LoansModule::Payments::Classifier.new(loan: loan, entry: entry).total_cash_payment)
              ]], column_widths: [150, 50, 80, 80, 80, 80], cell_style: {padding: [2,3,2,3], inline_format: true, size: 9}) do
              column(2).align = :right
              column(3).align = :right
              column(4).align = :right
              column(5).align = :right

            end
            end
          end
        end
        table([['TOTAL', '', price(total_principals), price(total_interests), price(total_penalties), price(total_cash_payments)]], column_widths: [150, 50, 80, 80, 80, 80], cell_style: { size: 9 }) do
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
          row(0).font_style = :bold

        end

      end
      move_down 10
      if savings_deposit_data.present?
        text 'Savings Deposits', style: :bold, size: 10
        table([["Member", "OR #", "Total"]] +
        savings_deposit_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(savings_deposit_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 160, 160], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end
      if bank_withdrawals_data.present?
        text 'Bank Withdrawals', style: :bold, size: 10
        table([["Employee", "OR #", "Total"]] +
        bank_withdrawals_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(bank_withdrawals_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 120, 100, 100], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end

      if other_revenues_data.present?
        text 'Other Revenues', style: :bold, size: 10
        table([["Member", 'Accounts', "OR #", "Total"]] +
        other_revenues_data.map{|a| [a.commercial_document_name, a.account_categories.pluck(:title).join("/"), a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(other_revenues_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 160, 160], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end

      move_down 10
      if capital_build_up_data.present?
        text 'Capital Build Ups', style: :bold, size: 10
        table([["Member", "OR #", "Total"]] +
        capital_build_up_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(capital_build_up_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 160, 160], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end
      move_down 10
      office.programs.each do |program|

        program_payments_data = AccountingModule::EntriesAggregator::ProgramSubscription.new(office: office, entries: debit_entries, program_subscriptions: office.program_subscriptions.where(program:program)).credit_entries
        if program_payments_data.present?
          text "#{program.name} Payments" , size: 10, style: :bold
          table([["Member", "OR #", "Total"]] +
          program_payments_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
          [["Total", "", price(program_payments_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 160, 160], cell_style: { size: 9 }) do
            row(0).font_style= :bold
            row(-1).font_style= :bold

            row(0).background_color = 'DDDDDD'
            column(2).align = :right

          end
        end
      end
      move_down 5
      stroke_horizontal_rule
      table([["TOTAL CASH RECEIPTS", price(total_debits)]], column_widths: [200, 320], cell_style: { size: 10}) do
        row(0).font_style = :bold
        cells.borders = []
        column(1).align = :right
      end
    end

    def cash_disbursements
      start_new_page
      text "CASH DISBURSEMENTS - #{to_date.strftime("%B %e, %Y")}", size: 10, style: :bold
      stroke_horizontal_rule
      move_down 10
      credit_entries                 = cash_account.credit_entries.entered_on(from_date: from_date, to_date: to_date)
      loan_releases_data             = AccountingModule::EntriesAggregator::Loan.new(entries: credit_entries, office: office).debit_entries
      savings_withdrawals_data       = AccountingModule::EntriesAggregator::Saving.new(entries: credit_entries, office: office).debit_entries
      share_capital_withdrawals_data = AccountingModule::EntriesAggregator::ShareCapital.new(entries: credit_entries, office: office).debit_entries
      loan_releases                  = office.loans.disbursed_on(from_date: from_date, to_date: to_date)
      bank_deposits_data             = AccountingModule::EntriesAggregator::BankAccount.new(entries: credit_entries, office: office).debit_entries
      expenses_data                  = AccountingModule::EntriesAggregator::Expense.new(entries: credit_entries, office: office).debit_entries

      if loan_releases_data.present?
        text 'Loan Releases', style: :bold, size: 10
        table([["Member", "CDV #", "Loan Product", "Loan Amount", "Net Proceed"]] +
        loan_releases.map{ |loan| [loan.borrower_name, loan.voucher.try(:reference_number), loan.loan_product_name, price(loan.loan_amount), price(loan.net_proceed)]} +
        [["Total", "", "", "", price(loan_releases_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [150, 50, 100, 100, 100], cell_style: { size: 9 }) do
          row(0).font_style       = :bold
          row(-1).font_style      = :bold
          row(0).background_color = 'DDDDDD'
          column(3).align         = :right
          column(4).align         = :right
        end
      end
      move_down 10
      if savings_withdrawals_data.present?
        text 'Savings Withdrawals', style: :bold, size: 10
        table([["Member", "CDV #", "Total"]] +
        savings_withdrawals_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(savings_withdrawals_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 100, 200], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end
      move_down 10
      if share_capital_withdrawals_data.present?
        text 'Share Capital Withdrawals', style: :bold, size: 10
        table([["Member", "OR #", "Total"]] +
        share_capital_withdrawals_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(share_capital_withdrawals_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 100, 200], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right

        end
      end
      move_down 10

      if bank_deposits_data.present?
        text 'Bank Deposits', style: :bold, size: 10
        move_down 5
        table([["Employee", "CDV #", "Total"]] +
        bank_deposits_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", price(bank_deposits_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 100, 200], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right
        end
      end


      if expenses_data.present?
        text 'Expenses', style: :bold, size: 10
        move_down 5
        table([["Employee", 'Accounts', "CDV #", "Total"]] +
        expenses_data.map{|a| [a.commercial_document_name, a.account_categories.except_cash_account_categories.pluck(:title).join("/"), a.reference_number, price(a.total_cash_amount)]} +
        [["Total", "", "", price(expenses_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [150, 150, 100, 100], cell_style: { size: 9 }) do
          row(0).font_style= :bold
          row(-1).font_style= :bold

          row(0).background_color = 'DDDDDD'
          column(2).align = :right
          column(3).align = :right


        end
      end
      move_down 10
      office.programs.each do |program|

        program_disbursements_data = AccountingModule::EntriesAggregator::ProgramSubscription.new(office: office, entries: credit_entries, program_subscriptions: office.program_subscriptions.where(program: program)).credit_entries
        if program_disbursements_data.present?
          text "#{program.name} Disbursements" , size: 10, style: :bold
          table([["Member", "CDV #", "Total"]] +
          program_disbursements_data.map{|a| [a.commercial_document_name, a.reference_number, price(a.total_cash_amount)]} +
          [["Total", "", price(program_disbursements_data.map{|a| a.total_cash_amount}.sum)]], column_widths: [200, 100, 200], cell_style: { size: 9 }) do
            row(0).font_style= :bold
            row(-1).font_style= :bold

            row(0).background_color = 'DDDDDD'
            column(2).align = :right

          end
        end
      end
      move_down 5
      stroke_horizontal_rule
      table([["TOTAL CASH DISBURSEMENT", price(total_credits)]], column_widths: [200, 300], cell_style: { size: 10}) do
        row(0).font_style = :bold
        cells.borders = []
        column(1).align = :right
      end
    end
  end
end
