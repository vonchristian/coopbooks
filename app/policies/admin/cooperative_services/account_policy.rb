module Admin
  module CooperativeServices
    class AccountPolicy < ApplicationPolicy
      def new?
        user.branch_manager? || user.general_manager? || user.accountant? || user.bookkeeper?
      end
      def create?
        new?
      end
    end
  end
end
