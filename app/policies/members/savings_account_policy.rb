module Members
  class SavingsAccountPolicy < ApplicationPolicy
    def new?
      user.teller? || user.treasurer?
    end
    def create?
      new?
    end
    def destroy?
      false
    end
  end
end
