module LoansModule
  module LoanApplications
    class DisbursementPolicy < ApplicationPolicy

      def new?
        user.teller?
      end
    end
  end
end
