module AccountingModule
  module Entries
    class EntryDateValidator
      attr_reader :entry, :date

      def initialize(entry:, date:)
        @entry = entry
        @date  = date
      end

      def valid?
        date_range = entry.created_at.beginning_of_day..entry.created_at.end_of_day
        if date_range.include?(date)
          true
        else
          false
        end
      end

    end
  end
end
