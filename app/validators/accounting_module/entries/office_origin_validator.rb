module AccountingModule
  module Entries
    class OfficeOriginValidator
      attr_reader :entry, :office, :employee

      def initialize(entry:, office:)
        @entry    = entry
        @employee = entry.recorder
        @office   = office
      end

      def valid?
        entry.office == employee.office &&
        entry.cooperative.offices.include?(office)
      end
    end
  end
end
