module AccountingModule
  module Entries
    class ArchivedAtValidator
      attr_reader :entry, :archived_at

      def initialize(entry:, archived_at:)
        @entry       = entry
        @archived_at = archived_at
      end
      def valid?
        date_range = entry.created_at.beginning_of_day..entry.created_at.end_of_day
        if date_range.include?(archived_at)
          true
        else
          false
        end
      end

    end
  end
end
