module AccountingModule
  module Entries
    class RecorderOriginValidator
      attr_reader :entry, :recorder

      def initialize(entry:, recorder:)
        @entry    = entry
        @recorder = recorder
      end

      def valid?
        entry.office      == recorder.office &&
        entry.cooperative == recorder.cooperative
      end
    end
  end
end
