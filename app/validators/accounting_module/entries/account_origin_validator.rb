module AccountingModule
  module Entries
    class AccountOriginValidator
      attr_reader :entry, :account, :office

      def initialize(entry:, account:)
        @entry       = entry
        @account     = account
        @office      = entry.office
      end

      def valid?
        office.accounts.include?(account)
      end
    end
  end
end
