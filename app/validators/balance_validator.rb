
class BalanceValidator < ActiveModel::Validator
  def validate(record)
    if !record.cooperative.offices.ids.include?(record.office_id)
      record.errors.add(:base, "invalid office origin")
    end
  end
end
