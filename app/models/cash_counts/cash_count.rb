module CashCounts
  class CashCount < ApplicationRecord
    belongs_to :bill
    belongs_to :cash_count_report

    delegate :bill_amount, :name, to: :bill

    def self.total
      sum(&:subtotal)
    end

    def subtotal
      bill_amount * quantity
    end
  end
end
