module CashCounts
  class Bill < ApplicationRecord
    validates :bill_amount, :name, presence: true
    validates :bill_amount, numericality: true
    validates :name, uniqueness: true
    has_many :cash_counts
  end
end
