module CashCounts
  class CashCountReport < ApplicationRecord
    belongs_to :employee, class_name: "User", foreign_key: 'employee_id'
    has_many :cash_counts, dependent: :destroy
    has_many :bills, through: :cash_counts

    validates :date, presence: true

    def self.created_at(args={})
      from_date = args[:from_date]
      to_date   = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('date' => (date_range.start_date..date_range.end_date))
    end
    def total_bills
      cash_counts.total
    end
    def overage_shortage
      cash_balance - total_bills
    end
  end
end
