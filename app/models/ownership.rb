class Ownership < ApplicationRecord
  belongs_to :owner, polymorphic: true
  belongs_to :ownable, polymorphic: true

  def archived?
    archived_at.present?
  end
end
