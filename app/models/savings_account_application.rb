class SavingsAccountApplication < ApplicationRecord
  belongs_to :cart, optional: true
  belongs_to :cooperative
  belongs_to :office,            class_name: "Cooperatives::Office"
  belongs_to :depositor,         polymorphic: true
  belongs_to :saving_product,    class_name: "Cooperatives::SavingProduct"
  belongs_to :liability_account, class_name: 'AccountingModule::Account'
  validates :account_number,     presence: true, uniqueness: true

  delegate :name, to: :depositor, prefix: true
end
