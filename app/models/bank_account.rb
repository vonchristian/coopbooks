class BankAccount < ApplicationRecord
  audited
  include PgSearch::Model
  pg_search_scope :text_search, against: [:bank_name, :bank_address, :account_number]
  has_one_attached :avatar
  belongs_to :cooperative
  belongs_to :office,                            class_name: "Cooperatives::Office"
  belongs_to :cash_account,                      class_name: "AccountingModule::Account"
  belongs_to :interest_revenue_account,          class_name: "AccountingModule::Account"

  validates :bank_name, :bank_address, :account_number, presence: true
  validates :bank_name, uniqueness: { scope: :office_id, case_sensitive: false }
  validate :unique_cash_account
  before_save :set_default_image

  delegate :entries, :balance, :debits_balance, :credits_balance, :last_transaction_date, to: :cash_account
  delegate :balance, to: :interest_revenue_account, prefix: true
  def self.cash_accounts
    ids = pluck(:cash_account_id)
    AccountingModule::Account.where(id: ids.uniq.compact.flatten)
  end
  def name
    bank_name
  end

  private
  def set_default_image
    if avatar.attachment.blank?
      self.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'bank.png')), filename: 'default-image.png', content_type: 'image/png')
    end
  end

  def unique_cash_account
    errors.add(:cash_account_id, 'Already taken') if BankAccount.pluck(:cash_account_id).include?(:cash_account_id)
  end
end
