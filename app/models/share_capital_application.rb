class ShareCapitalApplication < ApplicationRecord
  belongs_to :cart,                  optional: true
  belongs_to :subscriber,            polymorphic: true
  belongs_to :equity_account,        class_name: 'AccountingModule::Account'
  belongs_to :share_capital_product, class_name: "Cooperatives::ShareCapitalProduct"
  belongs_to :cooperative
  belongs_to :office,                class_name: "Cooperatives::Office"
end
