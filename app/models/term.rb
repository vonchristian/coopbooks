class Term < ApplicationRecord
  validates :term, presence: true, numericality: true #no of months
  validates :effectivity_date, :maturity_date, presence: true
  validates :account_number, presence: true, uniqueness: true

  def self.past_due
    where('maturity_date < ?', Date.current)
  end

  def self.current
    order(effectivity_date: :desc).first
  end

  def days_elapsed
    ((Time.zone.now - effectivity_date)/86400.0).to_i
  end

  def number_of_months
    term
  end

  def matured?
    maturity_date <= Time.zone.now
  end

  def is_past_due?
    matured?
  end

  def past_due?
    maturity_date < Time.zone.now
  end

  def grace_period_past_due?(grace_period = 0)
    maturity_date < Time.zone.now + grace_period.days
  end

  def number_of_days_past_due
    number = ((Time.zone.now - maturity_date)/86400.0).to_i
    if number.negative?
      0
    else
      number
    end
  end

  def number_of_days_before_maturity
    number = ((Time.zone.now - maturity_date)/86400.0).to_i
    if number.positive?
      0
    else
      number
    end
  end

  def remaining_term
    ((maturity_date - Time.zone.now)/86400.0).to_i
  end

  def terms_elapsed # months
    (Time.zone.now.year * 12 + Time.zone.now.month) - (effectivity_date.year * 12 + effectivity_date.month)
  end
end
