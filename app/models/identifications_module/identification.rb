module IdentificationsModule
  class Identification < ApplicationRecord
    belongs_to :identifiable,      polymorphic: true
    belongs_to :identity_provider, class_name: "IdentificationsModule::IdentityProvider"
    has_many_attached :photos

    validates :issuance_date, :expiry_date, :number, presence: true

    delegate :name, to: :identity_provider, prefix: true
  end
end
