class StoreFront < ApplicationRecord
  audited
  belongs_to :office,                            class_name: 'Cooperatives::Office'
  belongs_to :merchandise_inventory_account,     class_name: 'AccountingModule::Account'
  belongs_to :receivable_category,               class_name: 'AccountingModule::LevelOneAccountCategory', foreign_key: 'receivable_category_id'
  belongs_to :cost_of_goods_sold_category,       class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'cost_of_goods_sold_category_id'
  belongs_to :inventory_category,                class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'inventory_category_id'
  belongs_to :sales_category,                    class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'sales_category_id'
  belongs_to :sales_return_category,             class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'sales_return_category_id'
  belongs_to :spoilage_category,                 class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'spoilage_category_id'
  belongs_to :sales_discount_category,           class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'sales_discount_category_id'
  belongs_to :purchase_return_category,          class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'purchase_return_category_id'
  belongs_to :internal_use_category,             class_name: "AccountingModule::LevelOneAccountCategory", foreign_key: 'internal_use_category_id'
  has_many :products,                            class_name: "StoreFronts::Product"
  has_many :orders,                              class_name: "StoreFronts::Order"
  has_many :line_items,                          through: :orders, class_name: "StoreFronts::LineItem"
  has_many :store_front_accounts,                class_name: 'StoreFronts::StoreFrontAccount'
  has_many :customers,                           through: :store_front_accounts
  has_many :suppliers,                           class_name: 'StoreFronts::Supplier'
  has_many :employee_store_front_accounts,       class_name: 'StoreFronts::Accounts::EmployeeStoreFrontAccount'
  has_many :employees,                           through: :employee_store_front_accounts, class_name: 'User', foreign_key: 'employee_id'
  has_many :ledger_accounts,                     class_name: 'AccountingModule::LedgerAccount', as: :ledgerable
  has_many :accounts,                            through: :ledger_accounts, class_name: 'AccountingModule::Account'
  has_many :purchase_orders,                     class_name: 'StoreFronts::Orders::PurchaseOrder'
  has_many :purchase_line_items,                 class_name: 'StoreFronts::LineItems::PurchaseLineItem', through: :purchase_orders, source: :line_items
  has_many :stocks,                              class_name: 'StoreFronts::Stock'
  has_many :sales_orders,                        class_name: 'StoreFronts::Orders::SalesOrder'
  validates :name, :address,  presence: true
  validates :name, uniqueness: true
end
