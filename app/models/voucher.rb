#ensure debits and credits are equal
class Voucher < ApplicationRecord
  include PgSearch::Model
  include Taggable
  has_secure_token
  pg_search_scope :text_search, :against => [:reference_number, :description]
  multisearchable against: [:number, :description]

  belongs_to :cooperative
  belongs_to :office,        class_name: "Cooperatives::Office"
  belongs_to :entry,         class_name: "AccountingModule::Entry", foreign_key: 'entry_id', optional: true
  belongs_to :payee,         polymorphic: true
  belongs_to :preparer,      class_name: "User", foreign_key: 'preparer_id'
  belongs_to :disburser,     class_name: "User", foreign_key: 'disburser_id', optional: true
  has_many :voucher_amounts, class_name: "Vouchers::VoucherAmount", dependent: :destroy

  delegate :name, :abbreviated_name, :address, :contact_number, to: :cooperative, prefix: true
  delegate :full_name, :current_occupation, to: :preparer, prefix: true
  delegate :full_name, :current_occupation, to: :disburser, prefix: true, allow_nil: true
  delegate :name, to: :payee, prefix: true, allow_nil: true
  delegate :avatar, to: :payee, allow_nil: true

  validates :account_number, presence: true, uniqueness: true

  validates :description, presence: true
  before_save :set_default_date
  # validate :has_credit_amounts?
  # validate :has_debit_amounts?
  # validate :amounts_cancel?

  def self.entries
    ids = pluck(:entry_id)
    AccountingModule::Entry.where(id: ids)
  end

  def self.disbursed_by(employee_id:)
    entries.where(recorder_id: employee_id)
  end
  def self.disbursement_vouchers
    joins(:voucher_amounts).where('voucher_amounts.amount_type' == 'credit').where('voucher_amounts.account_id' => Employees::EmployeeCashAccount.cash_accounts)
  end

  def self.loan_disbursement_vouchers
    vouchers = LoansModule::LoanApplication.pluck(:voucher_id)
    where(id: vouchers)
  end

  def self.contains_cash_accounts
    vouchers = Vouchers::VoucherAmount.contains_cash_accounts.pluck(:voucher_id)
    where(id: vouchers)
  end

  def cancelled?
    cancelled_at.present?
  end

  def total_cash_amount
    voucher_amounts.total_cash_amount
  end

  def name
    payee_name
  end

  def self.unused
    where(entry_id: nil)
  end

  def total
    if disbursed?
      entry.debit_amounts.sum(&:amount)
    else
      voucher_amounts.sum(&:amount)
    end
  end

  def number_and_total
    "#{reference_number} - #{total}"
  end

  def self.disbursed
    where.not(entry_id: nil)
  end

  def self.disbursed_on(args={})
    if args[:from_date] && args[:to_date]
      range = DateRange.new(from_date: args[:from_date], to_date: args[:to_date])
      disbursed.joins(:entry).where('entries.entry_date' => (range.start_date..range.end_date))
    else
      disbursed
    end
  end


  def disbursed?
    entry.present?
  end

  def self.latest
    all.pluck(:number).max
  end


  def contains_cash_accounts?
    voucher_amounts.contains_cash_accounts.present?
  end


  private
  def set_default_date
    self.date ||= Time.zone.now
  end
  def has_credit_amounts?
    errors[:base] << "Voucher must have at least one credit amount" if self.voucher_amounts.credit.blank?
  end

  def has_debit_amounts?
    errors[:base] << "Voucher must have at least one debit amount" if self.voucher_amounts.debit.blank?
  end

  def amounts_cancel?
    errors[:base] << "The credit and debit amounts are not equal" if voucher_amounts.credit.total != voucher_amounts.debit.total
  end
end
