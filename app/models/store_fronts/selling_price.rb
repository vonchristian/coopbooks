module StoreFronts
  class SellingPrice < ApplicationRecord
    belongs_to :store_front
    belongs_to :variant
  end
end
