module StoreFronts
  class Stock < ApplicationRecord
    include PgSearch::Model
    pg_search_scope :text_search, against: [:barcode]
    belongs_to :product
    belongs_to :store_front
    has_one :purchase,          class_name: 'StoreFronts::LineItems::PurchaseLineItem'
    has_many :sales,            class_name: 'StoreFronts::LineItems::SalesLineItem'
    has_many :stock_transfers,  class_name: 'StoreFronts::LineItems::StockTransferLineItem'
    has_many :internal_uses,    class_name: 'StoreFronts::LineItems::InternalUseLineItem'
    has_many :spoilages,        class_name: 'StoreFronts::LineItems::SpoilageLineItem'

    delegate :unit_of_measurement, :unit_cost, :total_cost, :quantity, to: :purchase
    delegate :name, :has_conversion?, to: :product
    delegate :code,  to: :unit_of_measurement, prefix: true
    delegate :price, to: :unit_of_measurement

    def self.processed
      joins(:purchase).where.not('line_items.order_id' => nil)
    end

    def self.available
      where(available: true)
    end

    def available_stock_for_cart(cart)
      balance -
      sales.where(cart: cart).sum(&:quantity)
    end

    def balance
      quantity     -
      sales.total           -
      stock_transfers.total -
      internal_uses.total   -
      spoilages.total
    end

  end
end
