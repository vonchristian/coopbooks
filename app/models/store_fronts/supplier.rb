module StoreFronts
  class Supplier < ApplicationRecord
    include Addressing
    include Contacting

    belongs_to :store_front
    has_many :supplier_vouchers, class_name: 'Suppliers::SupplierVoucher'
    has_many :vouchers, through: :supplier_vouchers
    has_many :purchase_orders,   class_name: 'StoreFronts::Orders::PurchaseOrder'
    validates :first_name, :last_name, :contact_number, :address, :business_name, presence: true
    def name
      business_name
    end
    def first_middle_and_last_name #report signatory

        "#{first_name.titleize} #{last_name.titleize}"
    end
  end
end
