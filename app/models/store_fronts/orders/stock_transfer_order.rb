module StoreFronts
  module Orders
    class StockTransferOrder < ApplicationRecord
      belongs_to :employee,                class_name: 'User'
      belongs_to :voucher
      belongs_to :origin_store_front,      class_name: 'StoreFront'
      belongs_to :destination_store_front, class_name: 'StoreFront'
    end
  end
end
