module StoreFronts
  module Orders
    class PurchaseOrder < ApplicationRecord
      belongs_to :store_front
      belongs_to :supplier, polymorphic: true
      belongs_to :voucher
      belongs_to :employee,          class_name: 'User', foreign_key: 'employee_id'
      has_many :line_items, class_name: 'StoreFronts::LineItems::PurchaseLineItem', as: :order

      validates :date, presence: true
    end
  end
end
