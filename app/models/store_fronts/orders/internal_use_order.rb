module StoreFronts
  module Orders
    class InternalUseOrder < ApplicationRecord
      belongs_to :voucher 
      belongs_to :store_front
      belongs_to :employee,            class_name: 'User'
      belongs_to :requesting_employee, class_name: 'User'
      has_many   :line_items,          class_name: 'StoreFronts::LineItems::InternalUseLineItem', as: :order
      validates :date, presence: true
    end
  end
end
