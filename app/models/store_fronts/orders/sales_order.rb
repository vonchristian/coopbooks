module StoreFronts
  module Orders
    class SalesOrder < ApplicationRecord
      belongs_to :customer, polymorphic: true
      belongs_to :store_front
      belongs_to :voucher,                    optional: true
      belongs_to :sales_account,              class_name: 'AccountingModule::Account'
      belongs_to :sales_discount_account,     class_name: 'AccountingModule::Account'
      belongs_to :cost_of_goods_sold_account, class_name: 'AccountingModule::Account'
      belongs_to :receivable_account,         class_name: 'AccountingModule::Account', optional: true
      belongs_to :employee,                   class_name: 'User'
      has_many   :line_items,                 class_name: 'StoreFronts::LineItems::SalesLineItem', as: :order

      validates :date, presence: true
      delegate :office, to: :store_front
      delegate :name, to: :customer, prefix: true
      def reference_number
        SecureRandom.uuid
      end
      def total_cost_less_discount
        total_cost - discount
      end
      def cost_of_goods_sold
        line_items.cost_of_goods_sold
      end
    end
  end
end
