module StoreFronts
  class Variant < ApplicationRecord
    belongs_to :product
    has_many :purchase_prices, class_name: 'StoreFronts::PurchasePrice'
    has_many :selling_prices,  class_name: 'StoreFronts::SellingPrice'
  end
end
