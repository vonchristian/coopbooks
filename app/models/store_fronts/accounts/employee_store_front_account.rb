module StoreFronts
  module Accounts
    class EmployeeStoreFrontAccount < ApplicationRecord
      belongs_to :employee, class_name: 'User', foreign_key: 'employee_id'
      belongs_to :store_front
    end
  end
end
