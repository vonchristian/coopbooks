module StoreFronts
  module Accounts
    class CustomerAccount < ApplicationRecord
      belongs_to :customer, polymorphic: true
      belongs_to :store_front
    end
  end
end
