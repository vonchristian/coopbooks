module StoreFronts
  module Suppliers
    class SupplierVoucher < ApplicationRecord
      belongs_to :supplier
      belongs_to :voucher
    end
  end
end
