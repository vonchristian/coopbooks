module StoreFronts
  class PurchasePrice < ApplicationRecord
    belongs_to :variant
    belongs_to :store_front
  end
end
