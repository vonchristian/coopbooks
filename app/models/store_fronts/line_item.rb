module StoreFronts
  class LineItem < ApplicationRecord
    include PgSearch::Model
    pg_search_scope :text_search, associated_against: { barcodes: [:code] }
    belongs_to :product
    belongs_to :line_itemable, polymorphic: true, optional: true
    belongs_to :unit_of_measurement, class_name: 'StoreFronts::UnitOfMeasurement'
    belongs_to :cart,                optional: true
    belongs_to :order,               polymorphic: true, optional: true
    belongs_to :stock,               class_name: 'StoreFronts::Stock', optional: true
    has_many :barcodes,              dependent: :destroy

    validates :quantity, :unit_cost, :total_cost, presence: true, numericality: true

    delegate :code,                   to: :unit_of_measurement,prefix: true
    delegate :conversion_multiplier,  to: :unit_of_measurement
    delegate :name, :barcode, to: :stock, prefix: true
    def self.total
      processed.sum(&:quantity)
    end

    def self.processed
      where.not(order_id: nil).where.not(order_type: nil)
    end

    def self.with_orders
      processed
    end

    def self.unprocessed
      where(order_id: nil).where(order_type: nil)
    end

    def self.total_converted_quantity
      sum(&:converted_quantity)
    end

    def self.total_cost
      sum(:total_cost)
    end


    def processed?
      order.present? && order.processed?
    end

    def converted_quantity
      quantity * conversion_multiplier
    end

    def normalized_barcodes
      barcodes.pluck(:code).join(",")
    end
  end
end
