module StoreFronts
  class StoreFrontAccount < ApplicationRecord
    belongs_to :customer, polymorphic: true
    belongs_to :store_front
  end
end
