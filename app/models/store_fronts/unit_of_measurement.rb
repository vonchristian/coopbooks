module StoreFronts
  class UnitOfMeasurement < ApplicationRecord
    belongs_to :product
    has_many :mark_up_prices, class_name: 'StoreFronts::MarkUpPrice'
    delegate :price, to: :recent_mark_up_price

    def recent_mark_up_price 
      mark_up_prices.recent
    end
  end
end
