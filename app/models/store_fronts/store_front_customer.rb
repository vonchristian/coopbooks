module StoreFronts
  class StoreFrontCustomer < ApplicationRecord
    belongs_to :store_front
    belongs_to :customer, polymorphic: true
  end
end
