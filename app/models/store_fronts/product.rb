module StoreFronts
  class Product < ApplicationRecord
    include PgSearch::Model
    multisearchable against: [:name]
    pg_search_scope :text_search,               against: [:name]
    pg_search_scope :text_search_with_barcode,  against: [:name],
                                                associated_against:  { :stocks => [:barcode] }
    belongs_to :store_front
    belongs_to :product_category,               class_name: "StoreFronts::ProductCategory", optional: true
    has_many :unit_of_measurements
    has_many :stocks
    has_many :purchase_line_items, class_name: 'StoreFronts::LineItems::PurchaseLineItem'

    has_one :master_variant,
            -> { where is_master: true },
            inverse_of: :product,
            class_name: 'StoreFronts::Variant'

    has_many :variants,
                    -> { where is_master: false },
                    inverse_of: :product,
                    class_name: 'StoreFronts::Variant'

    validates :name, presence: true, uniqueness: { scope: :store_front_id }
  end
end
