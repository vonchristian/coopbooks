module StoreFronts
  module LineItems
    class SalesLineItem < LineItem
      def self.cost_of_goods_sold
        sum(&:cost_of_goods_sold)
      end

      def cost_of_goods_sold
        stock.unit_cost
      end
    end
  end
end
