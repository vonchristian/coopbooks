module StoreFronts
  class ProductCategory < ApplicationRecord
    belongs_to :store_front
  end
end
