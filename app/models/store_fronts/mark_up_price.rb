module StoreFronts
  class MarkUpPrice < ApplicationRecord
    belongs_to :unit_of_measurement, class_name: "StoreFronts::UnitOfMeasurement"

    before_save :set_default_date

    validates :price, presence: true, numericality: { greater_than: 0.01 }

    def self.current
      order(date: :desc).first
    end
    def self.recent
      current
    end

    private
    def set_default_date
      todays_date = ActiveRecord::Base.default_timezone == :utc ? Time.now.utc : Time.now
      self.date ||= todays_date
    end
  end
end
