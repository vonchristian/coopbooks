module Cooperatives
  class MembershipCategory < ApplicationRecord
    audited
    belongs_to :cooperative
    validates :title, presence: true, uniqueness: { scope: :cooperative_id }
  end
end
