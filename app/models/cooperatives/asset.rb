module Cooperatives
  class Asset < ApplicationRecord
    audited
    belongs_to :asset_account, class_name: "AccountingModule::Accounts::Asset", foreign_key: 'account_id'
    belongs_to :office,        class_name: "Cooperatives::Office"
    belongs_to :cooperative
  end
end
