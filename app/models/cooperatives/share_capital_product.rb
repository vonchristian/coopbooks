module Cooperatives
  class ShareCapitalProduct < ApplicationRecord
    audited
    extend Metricable
    extend Totalable
    extend VarianceMonitoring

    enum balance_averaging_type: [:monthly]
    belongs_to :cooperative
    has_many :subscribers, class_name: "MembershipsModule::ShareCapital"

    validates :name, :cost_per_share, :minimum_share, presence: true
    validates :name, uniqueness: { scope: :cooperative_id, case_sensitive: false }
    validates :cost_per_share, :minimum_share, numericality: true
  end
end
