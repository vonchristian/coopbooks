module Cooperatives
  class CooperativeService < ApplicationRecord
    belongs_to :office,        class_name: 'Cooperatives::Office'
    has_many :ledger_accounts, class_name: "AccountingModule::LedgerAccount", as: :ledgerable
    has_many :accounts,        class_name: "AccountingModule::Account", through: :ledger_accounts
    has_many :voucher_amounts, class_name: "Vouchers::VoucheAmunt", as: :commercial_document

    validates :title, presence: true, uniqueness: true

    def entries
      accounts.entries
    end
  end
end
