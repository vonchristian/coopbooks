module Cooperatives
	class Program < ApplicationRecord
    audited
    enum mode_of_payment: [:one_time_payment, :annually, :quarterly, :semi_annually,  :monthly, :semi_monthly, :weekly, :daily]
    belongs_to :cooperative
    has_many :program_subscriptions,    class_name: "MembershipsModule::ProgramSubscription", inverse_of: :program
    has_many :member_subscribers,       through: :program_subscriptions, source: :subscriber, source_type: "Member"
    has_many :employee_subscribers,     through: :program_subscriptions, source: :subscriber, source_type: "User"
    has_many :organization_subscribers, through: :program_subscriptions, source: :subscriber, source_type: "Organization"
    has_many :office_programs,          class_name: 'Offices::OfficeProgram'
    has_many :offices,                  through: :office_programs, class_name: 'Cooperatives::Office'


    validates :name, :description, presence: true, uniqueness: { scope: :cooperative_id }
    validates :mode_of_payment, presence: true
    validates :amount, presence: true, numericality: true


    def subscribers
      employee_subscribers +
      member_subscribers +
      organization_subscribers
    end

    def payment_status_finder
      ("Programs::PaymentStatusFinders::" + mode_of_payment.titleize.gsub(" ", "")).constantize
    end

    def self.default_programs
    	where(default_program: true)
    end

    def paid?(args={})
      payment_status_finder.new(args.merge(program: self)).paid?
    end

    def subscribed?(subscriber)
      subscribers.pluck(:id).include?(subscriber.id)
    end
	end
end
