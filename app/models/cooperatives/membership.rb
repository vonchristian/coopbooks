module Cooperatives
  class Membership < ApplicationRecord
    audited
    include PgSearch::Model
    pg_search_scope :text_search, against: [:search_term]

    belongs_to :cooperative
    belongs_to :cooperator,          polymorphic: true
    belongs_to :membership_category, class_name: "Cooperatives::MembershipCategory"
    has_many :office_memberships,    class_name: 'Offices::OfficeMembership'
    validates :cooperator_id,  :cooperator_type, :cooperative_id, :membership_category_id, presence: true
    validates :cooperator_id,  uniqueness: { scope: :cooperative_id }
    validates :account_number, presence: true, uniqueness: true

    delegate :name,  to: :cooperative,         prefix: true
    delegate :title, to: :membership_category, prefix: true

    def self.for_cooperative(cooperative)
      where(cooperative: cooperative)
    end

    def self.approved_at(args={})
      from_date  = args[:from_date]
      to_date    = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('membership_date' => date_range.range)
    end
  end
end
