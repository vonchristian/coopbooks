 module Cooperatives
	class SavingProduct < ApplicationRecord
    audited
    extend Totalable
    extend Metricable
    extend VarianceMonitoring
	  enum interest_posting: [:daily, :weekly, :monthly, :quarterly, :semi_annually, :annually]

    belongs_to :cooperative

    has_many   :savings,                           class_name: 'MembershipsModule::Saving'

	  validates :interest_rate,
              :minimum_balance,
              numericality: { greater_than_or_equal_to: 0.01 },
              presence: true
	  validates :interest_posting, presence: true
	  validates :name, presence: true, uniqueness: { scope: :office_id }
    validates :minimum_balance, presence: true, numericality: true

    def self.accounts_opened(args={})
      SavingProductQuery.new.accounts_opened(args)
    end

    def self.total_savings
      sum(&:total_savings)
    end

    def total_savings
      savings.count
    end

    def interest_earned_posting_status_finder
      ("SavingsModule::InterestEarnedPostingStatusFinders::" + interest_posting.titleize.gsub(" ", "")).constantize
    end

    def balance_averager
      ("SavingsModule::BalanceAveragers::" + interest_posting.titleize.gsub(" ", "")).constantize
    end


    def self.liability_accounts
      accounts = pluck(:liability_account_id)
      AccountingModule::Liability.where(id: accounts)
    end
    def self.accounts
      liability_accounts
    end

    def self.total_balances(args={})
      liability_accounts.balance(args)
    end

    def applicable_rate
      applicable_rate_finder.new(saving_product: self).applicable_rate
    end

    def self.no_interests 
      where(no_interest: true)
    end

    def applicable_rate_finder
      ("SavingsModule::InterestRateFinders::" + interest_posting.titleize.gsub(" ", "")).constantize
    end

    def date_setter
      ("SavingsModule::DateSetters::" + interest_posting.titleize.gsub(" ", "")).constantize
    end
	end
end
