module Cooperatives
  module AreaOfOperations
    class OfficeMunicipality < ApplicationRecord
      belongs_to :office,       class_name: 'Cooperatives::Office'
      belongs_to :municipality, class_name: 'Addresses::Municipality'
    end
  end
end
