module Cooperatives
  module AreaOfOperations
    class OfficeBarangay < ApplicationRecord
      belongs_to :office,   class_name: 'Cooperatives::Office'
      belongs_to :barangay, class_name: 'Addresses::Barangay'
    end
  end
end
