module Cooperatives
  module AreaOfOperations
    class OfficeProvince < ApplicationRecord
      belongs_to :province, class_name: 'Addresses::Province'
      belongs_to :office,   class_name: 'Cooperatives::Office'

      validates :province_id, uniqueness: { scope: :office_id }
    end
  end
end
