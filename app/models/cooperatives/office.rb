module Cooperatives
  class Office < ApplicationRecord
    audited
    belongs_to :cooperative
    has_many :employees,                      class_name: "User"
    has_many :loans,                          class_name: "LoansModule::Loan"
    has_many :amortization_schedules,         class_name: "LoansModule::AmortizationSchedule"
    has_many :savings,                        class_name: "MembershipsModule::Saving"
    has_many :time_deposits,                  class_name: "MembershipsModule::TimeDeposit"
    has_many :share_capitals,                 class_name: "MembershipsModule::ShareCapital"
    has_many :entries,                        class_name: "AccountingModule::Entry"
    has_many :bank_accounts,                  class_name: "BankAccount"
    has_many :loan_applications,              class_name: "LoansModule::LoanApplication"
    has_many :vouchers
    has_many :accounts,                       class_name: "AccountingModule::Account"
    has_many :office_saving_products,         class_name: 'Offices::OfficeSavingProduct'
    has_many :saving_products,                class_name: "Cooperatives::SavingProduct", through: :office_saving_products
    has_many :office_share_capital_products,  class_name: 'Offices::OfficeShareCapitalProduct'
    has_many :share_capital_products,         through: :office_share_capital_products, class_name: "Cooperatives::ShareCapitalProduct"
    has_many :office_time_deposit_products,   class_name: 'Offices::OfficeTimeDepositProduct'
    has_many :time_deposit_products,          class_name: "Cooperatives::TimeDepositProduct", through: :office_time_deposit_products
    has_many :office_loan_products,           class_name: 'Offices::OfficeLoanProduct'
    has_many :loan_products,                  class_name: "Cooperatives::LoanProduct", through: :office_loan_products
    has_many :office_programs,                class_name: 'Offices::OfficeProgram'
    has_many :programs,                       class_name: "Cooperatives::Program", through: :office_programs
    has_many :office_memberships,             class_name: 'Offices::OfficeMembership'
    has_many :memberships,                    class_name: 'Cooperatives::Membership', through: :office_memberships
    has_many :member_memberships,             through: :memberships, source: :cooperator, source_type: "Member"
    has_many :members,                        through: :memberships, source: :cooperator, source_type: "Member"
    has_many :organization_memberships,       through: :memberships, source: :cooperator, source_type: 'Organization'
    has_many :membership_categories,          class_name: 'Cooperatives::MembershipCategory'
    has_many :interest_configs,               class_name: 'LoansModule::LoanProducts::InterestConfig'
    has_many :amortization_types,             class_name: 'LoansModule::AmortizationType'
    has_many :loan_protection_plan_providers, class_name: 'LoansModule::LoanProtectionPlanProvider'
    has_many :cooperative_services,           class_name: 'Cooperatives::CooperativeService'
    has_many :savings_account_applications
    has_many :share_capital_applications
    has_many :time_deposit_applications
    has_many :program_subscriptions,          class_name: 'MembershipsModule::ProgramSubscription'
    has_many :savings_account_registries,     class_name: 'Registries::SavingsAccountRegistry'
    has_many :organizations
    has_many :bank_accounts
    has_many :employee_cash_accounts,         class_name: 'Employees::EmployeeCashAccount'
    has_many :cash_accounts,                  class_name: 'AccountingModule::Account', through: :employee_cash_accounts, source: :cash_account
    has_many :loan_groups,                    class_name: 'LoansModule::LoanGroup'
    has_many :office_provinces,               class_name: 'Cooperatives::AreaOfOperations::OfficeProvince'
    has_many :office_municipalities,          class_name: 'Cooperatives::AreaOfOperations::OfficeMunicipality'
    has_many :office_barangays,               class_name: 'Cooperatives::AreaOfOperations::OfficeBarangay'
    has_many :provinces,                      class_name: 'Addresses::Province', through: :office_provinces
    has_many :municipalities,                 class_name: 'Addresses::Municipality', through: :office_municipalities
    has_many :barangays,                      class_name: 'Addresses::Barangay', through: :office_barangays
    has_many :store_fronts
    has_many :level_one_account_categories,   class_name: 'AccountingModule::LevelOneAccountCategory'

    has_many :saving_groups,                  class_name: 'Offices::SavingGroup'
    validates :name, uniqueness: { scope: :cooperative_id }
    validates :name, :type, :contact_number, :address, presence: true


    def normalized_type
      type.to_s.gsub("Cooperatives::Offices::", "")
    end


    def self.types
      ["Cooperatives::Offices::MainOffice",
       "Cooperatives::Offices::SatelliteOffice",
       "Cooperatives::Offices::BranchOffice"]
    end
  end
end
