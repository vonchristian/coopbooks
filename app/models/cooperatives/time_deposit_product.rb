module Cooperatives
	class TimeDepositProduct < ApplicationRecord
    audited
    extend Totalable
    extend Metricable
    extend VarianceMonitoring

    belongs_to :cooperative

    validates :name,
              :minimum_deposit,
              :maximum_deposit,
              :interest_rate,
              :break_contract_fee,
              :break_contract_rate,
              :number_of_days,
              presence: true

    validates :break_contract_fee,
              :break_contract_rate,
              :minimum_deposit,
              :maximum_deposit,
              :interest_rate,
              :number_of_days,
              numericality: true

    validates :name,
              uniqueness: { scope: :cooperative_id }



    def amount_range
      minimum_deposit..maximum_deposit
    end

    def amount_range_and_days
      "#{name} - #{amount_range} #{number_of_days} days"
    end

    def monthly_interest_rate
      months = number_of_days / 30
      interest_rate / months
    end

    def rate_for_not_matured
      interest_rate / number_of_days
    end
	end
end
