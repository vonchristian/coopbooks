module Cooperatives
  class LoanProduct < ApplicationRecord
    audited
    extend Totalable
    extend LoansModule::PastDuePercentage
    enum interest_calculation_type: [:add_on, :prededucted]
    belongs_to :receivable_account_category,       class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :interest_revenue_account_category, class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :penalty_revenue_account_category,  class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :amortization_type,                 class_name: "LoansModule::AmortizationType"
    belongs_to :loan_protection_plan_provider,     class_name: "LoansModule::LoanProtectionPlanProvider", optional: true
    # belongs_to :office,                            class_name: 'Cooperatives::Office'
    belongs_to :cooperative
    # belongs_to :receivable_account,                class_name: 'AccountingModule::Asset'
    belongs_to :temporary_account,                 class_name: 'AccountingModule::Accounts::Asset', optional: true
    # belongs_to :interest_revenue_account,          class_name: 'AccountingModule::Revenue'
    # belongs_to :penalty_revenue_account,           class_name: 'AccountingModule::Revenue'
    # belongs_to :unearned_interest_account,         class_name: 'AccountingModule::Asset'
    has_many   :interest_configs,                  class_name: "LoansModule::LoanProducts::InterestConfig", dependent: :destroy
    has_many   :penalty_configs,                   class_name: "LoansModule::LoanProducts::PenaltyConfig",dependent: :destroy
    has_many   :loan_product_charges,              class_name: "LoansModule::LoanProducts::LoanProductCharge",dependent: :destroy
    has_many   :loans,                             class_name: "LoansModule::Loan"
    has_many   :member_borrowers,                  through: :loans, source: :borrower, source_type: 'Member'
    has_many   :employee_borrowers,                through: :loans, source: :borrower, source_type: 'User'
    has_many   :organization_borrowers,            through: :loans, source: :borrower, source_type: 'Organization'
    has_many   :interest_predeductions,            class_name: "LoansModule::LoanProducts::InterestPrededuction"
    has_many   :loan_applications,                 class_name: "LoansModule::LoanApplication"

    delegate :calculation_type, to: :amortization_type, prefix: true
    delegate :rate,
             :annual_rate,
             :calculation_type,
             :prededuction_type,
             :prededucted_rate,
             :amortization_type,
             :rate_type,
             to: :current_interest_config, prefix: true
    delegate :amortizer, :amortizeable_principal_calculator, to: :amortization_type
    delegate :rate, :rate_in_percent, to: :current_penalty_config, prefix: true, allow_nil: true

    delegate :monthly_interest_rate,
             to: :current_interest_config
    delegate :business_name, to: :loan_protection_plan_provider, prefix: true, allow_nil: true


    delegate :scheduler, to: :amortization_type, prefix: true
    delegate :calculation_type, to: :current_interest_prededuction, prefix: true

    validates :name, presence: true, uniqueness: { scope: :office_id, case_sensitive: false }
    validates :maximum_loanable_amount, numericality: true

    delegate :calculation_type, :rate, :rate_in_percent, :number_of_payments, to: :current_interest_prededuction, prefix: true, allow_nil: true

    # def self.receivable_accounts
    #   ids = pluck(:receivable_account_id)
    #   AccountingModule::Asset.where(id: ids.compact.uniq.flatten)
    # end
    # def self.receivable_account_categories
    #   ids = pluck(:receivable_account_category_id)
    #   AccountingModule::AccountCategory.where(id: ids)
    # end
    # def self.payments
    #   entry_ids = []
    #   entry_ids << receivable_accounts.credit_entries.pluck(:id)
    #   AccountingModule::Entry.where(id: entry_ids.compact.flatten.uniq)
    # end


    # def self.interest_revenue_accounts
    #   ids = pluck(:interest_revenue_account_id)
    #   AccountingModule::Revenue.where(id: ids)
    # end


    def self.active
      where(active: true)
    end
    def self.receivable_account_categories
      ids = pluck(:receivable_account_category_id)
      AccountingModule::AccountCategory.where(id: ids.uniq.compact.flatten)
    end

    # def payment_processor
    #   ("LoansModule::PaymentProcessors::" + current_interest_config_calculation_type.titleize.gsub(" ", "")+"Interest").constantize
    # end
    #
    def interest_calculator
      if current_interest_config.prededucted?
        ("LoansModule::InterestCalculators::" + current_interest_prededuction_calculation_type.titleize.gsub(" ", "") + amortization_type.calculation_type.titleize.gsub(" ", "")).constantize
      elsif current_interest_config.add_on?
        ("LoansModule::InterestCalculators::" + current_interest_config_calculation_type.titleize.gsub(" ", "") + amortization_type.calculation_type.titleize.gsub(" ", "")).constantize
      end
    end
    #
    # def loan_processor
    #   if current_interest_config.prededucted?
    #     ("LoansModule::LoanProcessors::" + current_interest_prededuction_calculation_type.titleize.gsub(" ", "") + amortization_type.calculation_type.titleize.gsub(" ", "")).constantize
    #   elsif current_interest_config.add_on? || current_interest_config.accrued?
    #     ("LoansModule::LoanProcessors::" + current_interest_config_calculation_type.titleize.gsub(" ", "") + amortization_type.calculation_type.titleize.gsub(" ", "")).constantize
    #   end
    # end
    # #
    def annual_interest_calculator
      ("LoansModule::AnnualInterestCalculators::" + amortization_type.calculation_type.titleize.gsub(" ", "")).constantize
    end

    def current_interest_config
      interest_configs.current
    end

    def current_penalty_config
      penalty_configs.current
    end

    def current_interest_prededuction
      interest_predeductions.current
    end
  end
end
