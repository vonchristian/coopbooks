module BalanceMonitoring
  def below_minimum_balance
    where(has_minimum_balance: false)
  end

  def has_minimum_balances
    where(has_minimum_balance: true)
  end
end
