class EmployeeOriginValidator < ActiveModel::Validator
  def validate(record)
    
    if !record.office.employees.include?(record.employee)
      record.errors.add(:base, "invalid recorder origin")
    end
  end
end
