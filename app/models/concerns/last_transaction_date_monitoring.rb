module LastTransactionDateMonitoring
  def active_at(args={})
    if args[:from_date] && args[:to_date]
      from_date = args[:from_date]
      to_date   = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('last_transaction_date' => date_range.range)
    end
  end
end
