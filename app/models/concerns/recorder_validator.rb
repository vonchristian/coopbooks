class RecorderValidator < ActiveModel::Validator
  def validate(record)
    if !record.cooperative.users.ids.include?(record.recorder_id)
      record.errors.add(:base, "invalid recorder origin")
    end
    if !record.office.employees.ids.include?(record.recorder_id)
      record.errors.add(:base, "invalid recorder origin")
    end
  end
end
