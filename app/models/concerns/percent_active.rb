module PercentActive
  def percent_active(args={})
    if active_at(args).present?
      (active_at(args).count / self.count.to_f) * 100
    else
      0
    end
  end
end
