module Addressing
  extend ActiveSupport::Concern
  included do
    has_many :addresses, as: :addressable
    has_many :provinces,      through: :addresses
    has_many :municipalities, through: :addresses
    has_many :barangays,      through: :addresses



    delegate :details, :complete_address, :barangay_name, :municipality_name, :province_name, :street_name, to: :current_address, prefix: true
  end

  def current_address
    addresses.recent || NullAddress.new
  end
end
