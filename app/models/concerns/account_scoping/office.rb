module AccountScoping
  module Office
    extend ActiveSupport::Concern
    included do
      belongs_to :office, class_name: 'Cooperatives::Office'
    end
  end
end
