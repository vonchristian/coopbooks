module AccountScoping
  module Municipality
    extend ActiveSupport::Concern
    included do
      has_one :municipality_scope, class_name: 'AccountScopes::MunicipalityScope', as: :account
      delegate :municipality, to: :municipality_scope, allow_nil: true
      delegate :name,     to: :municipality, prefix: true, allow_nil: true
    end
  end
end
