module AccountScoping
  module Organization
    extend ActiveSupport::Concern
    included do
      has_one :organization_scope, class_name: 'AccountScopes::OrganizationScope', as: :account
      delegate :organization, to: :organization_scope, allow_nil: true
      delegate :name,     to: :organization, prefix: true, allow_nil: true
    end
  end
end
