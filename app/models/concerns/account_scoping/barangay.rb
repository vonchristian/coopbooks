module AccountScoping
  module Barangay
    extend ActiveSupport::Concern
    included do
      has_one :barangay_scope, class_name: 'AccountScopes::BarangayScope', as: :account
      delegate :barangay, to: :barangay_scope, allow_nil: true
      delegate :name,     to: :barangay, prefix: true, allow_nil: true
    end
  end
end
