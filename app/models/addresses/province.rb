module Addresses
  class Province < ApplicationRecord
    has_many :municipalities, class_name: "Addresses::Municipality"
    has_many :barangays,      through: :municipalities, class_name: "Addresses::Barangay"
    has_many :addresses
    has_many :members,       through: :addresses, source: :addressable, source_type: 'Member'
    has_many :organizations, through: :addresses, source: :addressable, source_type: 'Organization'

    validates :name, presence: true, uniqueness: true
  end
end
