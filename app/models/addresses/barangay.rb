module Addresses
  class Barangay < ApplicationRecord
    include PgSearch::Model

    pg_search_scope( :search, :against => [:name], :associated_against => { :municipality => [:name] },
                    using: { tsearch: { prefix: true } } )

    has_one_attached :avatar
    belongs_to :municipality
    has_many :streets
    has_many :addresses
    has_many :members,               through: :addresses, source: :addressable, source_type: "Member"
    has_many :organizations,         through: :addresses, source: :addressable, source_type: "Organization"
    has_many :barangay_scopes,       class_name: 'AccountScopes::BarangayScope'
    has_many :loans,                 through: :barangay_scopes, source: :account, source_type: 'LoansModule::Loan'
    has_many :savings,               through: :barangay_scopes, source: :account, source_type: 'MembershipsModule::Saving'
    has_many :share_capitals,        through: :barangay_scopes, source: :account, source_type: 'MembershipsModule::ShareCapital'
    has_many :time_deposits,         through: :barangay_scopes, source: :account, source_type: 'MembershipsModule::TimeDeposit'
    has_many :program_subscriptions, through: :barangay_scopes, source: :account, source_type: 'MembershipsModule::ProgramSubscription'

    validates :name, presence: true, uniqueness: { scope: :municipality_id }

    before_save :set_default_avatar

    delegate :name, to: :municipality, prefix: true
    delegate :province, to: :municipality
    def name_and_municipality
      "#{name}, #{municipality_name}"
    end

    private
    def set_default_avatar
      if avatar.attachment.blank?
        self.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'default_business_logo.jpg')), filename: 'default-image.jpg', content_type: 'image/jpg')
      end
    end
  end
end
