module Addresses
  class Municipality < ApplicationRecord
    belongs_to :province
    has_many :barangays
    has_many :streets
    has_many :addresses
    has_many :members, through: :addresses, source: :addressable, source_type: 'Member'
    has_many :municipality_scopes, class_name: 'AccountSubCategory::MunicipalityScope'
    has_many :loans,          through: :municipality_scopes, source: :account, source_type: 'LoansModule::Loan'
    has_many :savings,        through: :municipality_scopes, source: :account, source_type: 'MembershipsModule::Saving'
    has_many :share_capitals, through: :municipality_scopes, source: :account, source_type: 'MembershipsModule::ShareCapital'
    has_many :time_deposits,  through: :municipality_scopes, source: :account, source_type: 'MembershipsModule::TimeDeposit'



    validates :name, presence: true, uniqueness: { scope: :province_id }

  end
end
