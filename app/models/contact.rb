class Contact < ApplicationRecord
  audited
  belongs_to :contactable, polymorphic: true
  validates :number, presence: true
  def self.current
    order(created_at: :desc).first || NullContact.new
  end
end
