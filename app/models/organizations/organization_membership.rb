module Organizations
  class OrganizationMembership < ApplicationRecord
    belongs_to :organization
    belongs_to :cooperator, polymorphic: true
  end
end
