 module AccountingModule
  module UpdatedAtFinder
    def updated_at(from_date:, to_date:)
      from_date  = from_date
      to_date    = to_date
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      joins(:entries).where('entries.entry_date' => date_range.start_date..date_range.end_date)
    end
  end
end
