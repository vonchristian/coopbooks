module AccountingModule
  class LevelTwoAccountCategory < ApplicationRecord
    audited
    class_attribute :normal_credit_balance

    belongs_to :office,                  class_name: 'Cooperatives::Office'
    belongs_to :parent_account_category, class_name: 'AccountingModule::ParentAccountCategory'
    has_many :account_categories,        class_name: 'AccountingModule::AccountCategory'
    has_many :accounts,                  through: :account_categories, class_name: 'AccountingModule::Account'
    has_many :debit_amounts,             through: :accounts, class_name: 'AccountingModule::DebitAmount'
    has_many :credit_amounts,            through: :accounts, class_name: 'AccountingModule::CreditAmount'
    has_many :entries,                   through: :accounts, class_name: 'AccountingModule::Entry'
    scope :assets,      -> { where(type: 'AccountingModule::ParentAccountSubCategories::Asset') }
    scope :liabilities, -> { where(type: 'AccountingModule::ParentAccountSubCategories::Liability') }
    scope :equities,    -> { where(type: 'AccountingModule::ParentAccountSubCategories::Equity') }
    scope :revenues,    -> { where(type: 'AccountingModule::ParentAccountSubCategories::Revenue') }
    scope :expenses,    -> { where(type: 'AccountingModule::ParentAccountSubCategories::Expense') }

    def self.trial_balance(args={})
      return raise(NoMethodError, "undefined method 'trial_balance'") unless self.new.class == AccountingModule::ParentAccountSubCategory
      assets.balance(args) -
      (liabilities.balance(args) +
      equities.balance(args) +
      revenues.balance(args) -
      expenses.balance(args))
    end

    def self.balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.balance(options)
          else
            accounts_balance += account.balance(options)
          end
        end
      end
      accounts_balance
    end

    def self.debits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.debits_balance(options)
          else
            accounts_balance += account.debits_balance(options)
          end
        end
      end
      accounts_balance
    end

    def self.credits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.credits_balance(options)
          else
            accounts_balance += account.credits_balance(options)
          end
        end
      end
      accounts_balance
    end
    def balance(options={})
      return raise(NoMethodError, "undefined method 'balance'") if self.class == AccountingModule::ParentAccountSubCategory
      if self.normal_credit_balance ^ contra
        credits_balance(options) - debits_balance(options)
      else
        debits_balance(options) - credits_balance(options)
      end
    end

    def credits_balance(args={})
      credit_amounts.balance(args)
    end

    def debits_balance(args={})
      debit_amounts.balance(args)
    end
  end
end
