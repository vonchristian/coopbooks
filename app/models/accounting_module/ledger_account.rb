module AccountingModule
  class LedgerAccount < ApplicationRecord
    belongs_to :ledgerable, polymorphic: true
    belongs_to :account, class_name: 'AccountingModule::Account'

    validates :account_id, uniqueness: { scope: :ledgerable_id }
  end
end
