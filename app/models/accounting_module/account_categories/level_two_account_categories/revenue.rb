module AccountingModule
  module AccountCategories
    module LevelTwoAccountCategories
      class Revenue < AccountCategory
        self.normal_credit_balance = true

      end
    end
  end
end
