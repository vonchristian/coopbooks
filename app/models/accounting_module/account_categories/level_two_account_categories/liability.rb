module AccountingModule
  module AccountCategories
    module LevelTwoAccountCategories
      class Liability < AccountCategory
        self.normal_credit_balance = true

      end
    end
  end
end
