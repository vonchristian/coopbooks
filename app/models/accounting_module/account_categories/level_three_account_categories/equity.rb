module AccountingModule
  module AccountCategories
    module LevelThreeAccountCategories
      class Equity < AccountCategory
        self.normal_credit_balance = true
      end
    end
  end
end
