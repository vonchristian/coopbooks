module AccountingModule
  class Account < ApplicationRecord
    audited
    extend ProfitPercentage
    extend AccountingModule::UpdatedAtFinder
    include PgSearch::Model

    pg_search_scope :text_search, :against => [:name, :code]
    multisearchable against: [:name, :code]

    class_attribute :normal_credit_balance
    belongs_to :office,                     class_name: 'Cooperatives::Office'
    belongs_to :level_one_account_category, class_name: 'AccountingModule::LevelOneAccountCategory'
    has_many :amounts,                      class_name: "AccountingModule::Amount"
    has_many :credit_amounts,               class_name: 'AccountingModule::Amounts::CreditAmount'
    has_many :debit_amounts,                class_name: 'AccountingModule::Amounts::DebitAmount'
    has_many :entries,                      through: :amounts, source: :entry, class_name: "AccountingModule::Entry"
    has_many :credit_entries,               through: :credit_amounts, source: :entry, class_name: 'AccountingModule::Entry'
    has_many :debit_entries,                through: :debit_amounts, source: :entry, class_name: 'AccountingModule::Entry'
    has_many :account_budgets

    validates :type, :name, :code, :account_number, presence: true
    validates :name, uniqueness:  { scope: :office_id, case_sensitive: true }
    validates :code, uniqueness: { scope: :office_id, case_sensitive: true }

    scope :assets,      -> { where(type: 'AccountingModule::Accounts::Asset') }
    scope :liabilities, -> { where(type: 'AccountingModule::Accounts::Liability') }
    scope :equities,    -> { where(type: 'AccountingModule::Accounts::Equity') }
    scope :revenues,    -> { where(type: 'AccountingModule::Accounts::Revenue') }
    scope :expenses,    -> { where(type: 'AccountingModule::Accounts::Expense') }

    before_save :set_default_account_number

    delegate :title, to: :level_one_account_category, prefix: true, allow_nil: true

    def self.balance(options={})
      accounts_balance ||= BigDecimal('0')
      self.all.each do |account|
        if account.contra?
          accounts_balance -= account.balance(options)
        else
          accounts_balance += account.balance(options)
        end
      end
      accounts_balance
    end

    def self.debits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |account|
        if account.contra?
          accounts_balance -= account.debits_balance(options)
        else
          accounts_balance += account.debits_balance(options)
        end
      end
      accounts_balance
    end

    def self.credits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |account|
        if account.contra?
          accounts_balance -= account.credits_balance(options)
        else
          accounts_balance += account.credits_balance(options)
        end
      end
      accounts_balance
    end



    def self.except_cash_accounts
      ids = Employees::EmployeeCashAccount.pluck(:cash_account_id)
      where.not(id: ids.uniq)
    end

    def self.cash_accounts
      ids = Employees::EmployeeCashAccount.pluck(:cash_account_id)
      where(id: ids.uniq)
    end



    def last_transaction_date
      return created_at if entries.any?
      entries.recent.try(:entry_date)
    end


    def self.updated_by(employee)
      includes(:entries).where('entries.recorder_id' => employee.id)
    end

    def account_name
      name
    end

    def display_name
      level_one_account_category.title
    end

    def normalized_type
      type.gsub("AccountingModule::Accounts::", "")
    end

    def self.types
      ["AccountingModule::Accounts::Asset",
       "AccountingModule::Accounts::Equity",
       "AccountingModule::Accounts::Liability",
       "AccountingModule::Accounts::Expense",
       "AccountingModule::Accounts::Revenue"]
     end



    def self.except_account(args={})
      account = args[:account]
      where.not(id: account)
    end

    def self.amounts
      AccountingModule::Amount.where(account_id: self.ids)
    end

    def self.entries
      ids = amounts.pluck(:entry_id)
      AccountingModule::Entry.where(id: ids.compact.uniq.flatten)
    end

    def self.credit_entries
      ids = AccountingModule::Amounts::CreditAmount.where(account_id: self.ids).pluck(:entry_id)
      AccountingModule::Entry.where(id: ids.uniq.compact.flatten)
    end

    def self.debit_entries
      ids = AccountingModule::Amounts::DebitAmount.where(account_id: self.ids).pluck(:entry_id)
      AccountingModule::Entry.where(id: ids.uniq.compact.flatten)
    end




    def self.trial_balance(args={})
      return raise(NoMethodError, "undefined method 'trial_balance'") unless self.new.class == AccountingModule::Account
        assets.balance(args) -
        (liabilities.balance(args) +
        equities.balance(args) +
        revenues.balance(args) -
        expenses.balance(args))
    end

    def self.net_surplus(args={})
      revenues.balance(args) -
      expenses.balance(args)
    end

    def self.total_equity_and_liabilities(args={})
      equities.balance(args) +
      liabilities.balance(args)
    end




    def current_account_budget
      account_budgets.current
    end

    def default_last_transaction_date
      last_transaction_date || updated_at
    end

    def balance(options={})
      return raise(NoMethodError, "undefined method 'balance'") if self.class == AccountingModule::Account
      if self.normal_credit_balance ^ contra
        credits_balance(options) - debits_balance(options)
      else
        debits_balance(options) - credits_balance(options)
      end
    end

    def credits_balance(args={})
      credit_amounts.balance(args)
    end

    def debits_balance(args={})
      debit_amounts.balance(args)
    end

    private
    def set_default_account_number
      self.account_number ||= SecureRandom.uuid
    end
  end
end
