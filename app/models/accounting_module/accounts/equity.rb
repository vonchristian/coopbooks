module AccountingModule
  module Accounts
    class Equity < Account
      has_many :share_capital_applications, foreign_key: 'equity_account_id', dependent: :nullify
      self.normal_credit_balance = true

      def balance(options={})
        super(options)
      end

      def self.balance(options={})
        super(options)
      end
    end
  end 
end
