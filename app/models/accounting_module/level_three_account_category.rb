module AccountingModule
  class ParentAccountCategory < ApplicationRecord
    audited
    class_attribute :normal_credit_balance
    belongs_to :office,                        class_name: 'Cooperatives::Office'
    belongs_to :grand_parent_account_category, class_name: 'AccountingModule::GrandParentAccountCategory'
    has_many :parent_account_sub_categories,   class_name: 'AccountingModule::ParentAccountSubCategory'
    has_many :account_categories,              class_name: 'AccountingModule::AccountCategory', through: :parent_account_sub_categories
    has_many :accounts,                        class_name: 'AccountingModule::Account', through: :parent_account_sub_categories
    has_many :amounts,                         through: :accounts, class_name: 'AccountingModule::Amount'
    has_many :debit_amounts,                   through: :accounts, class_name: 'AccountingModule::DebitAmount'
    has_many :credit_amounts,                  through: :accounts, class_name: 'AccountingModule::CreditAmount'
    has_many :entries,                         through: :accounts, class_name: 'AccountingModule::Entry'
    has_many :debit_entries,                   through: :accounts, class_name: 'AccountingModule::Entry'
    has_many :credit_entries,                  through: :accounts, class_name: 'AccountingModule::Entry'

    validates :title, :code, :type, presence: true

    scope :assets,      -> { where(type: 'AccountingModule::ParentAccountCategories::Asset') }
    scope :liabilities, -> { where(type: 'AccountingModule::ParentAccountCategories::Liability') }
    scope :equities,    -> { where(type: 'AccountingModule::ParentAccountCategories::Equity') }
    scope :revenues,    -> { where(type: 'AccountingModule::ParentAccountCategories::Revenue') }
    scope :expenses,    -> { where(type: 'AccountingModule::ParentAccountCategories::Expense') }

    def self.trial_balance(args={})
      return raise(NoMethodError, "undefined method 'trial_balance'") unless self.new.class == AccountingModule::ParentAccountCategory
      assets.balance(args) -
      (liabilities.balance(args) +
      equities.balance(args) +
      revenues.balance(args) -
      expenses.balance(args))
    end

    def self.balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.balance(options)
          else
            accounts_balance += account.balance(options)
          end
        end
      end
      accounts_balance
    end

    def self.debits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.debits_balance(options)
          else
            accounts_balance += account.debits_balance(options)
          end
        end
      end
      accounts_balance
    end

    def self.credits_balance(options={})
      accounts_balance = BigDecimal('0')
      self.all.each do |category|
        category.accounts.each do |account|
          if account.contra?
            accounts_balance -= account.credits_balance(options)
          else
            accounts_balance += account.credits_balance(options)
          end
        end
      end
      accounts_balance
    end
    def balance(options={})
      return raise(NoMethodError, "undefined method 'balance'") if self.class == AccountingModule::ParentAccountCategory
      if self.normal_credit_balance ^ contra
        credits_balance(options) - debits_balance(options)
      else
        debits_balance(options) - credits_balance(options)
      end
    end

    def credits_balance(args={})
      credit_amounts.balance(args)
    end

    def debits_balance(args={})
      debit_amounts.balance(args)
    end
  end
end
