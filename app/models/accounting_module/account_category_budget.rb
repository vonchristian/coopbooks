module AccountingModule
  class AccountCategoryBudget < ApplicationRecord
    belongs_to :account_category
    validates :year, :amount, presence: true
    validates :year, numericality: { only_integer: true }
    validates :amount, numericality: true
  end
end
