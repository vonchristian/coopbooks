module AccountingModule
  class EntryTransaction < ApplicationRecord
    belongs_to :entry,    class_name: 'AccountingModule::Entry'
    belongs_to :customer, polymorphic: true

    validates :entry_id, uniqueness: { scope: :customer_id }
  end
end
