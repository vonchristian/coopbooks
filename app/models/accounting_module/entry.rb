module AccountingModule
  class Entry < ApplicationRecord
    audited
    include PgSearch::Model
    include Taggable
    validates_with RecorderValidator
    pg_search_scope :text_search, :against => [:reference_number, :description]
    multisearchable against: [:reference_number, :description]

    enum payment_type: [:cash, :check]

    has_one    :voucher,               foreign_key: 'entry_id', dependent: :nullify
    belongs_to :official_receipt,      optional: true
    belongs_to :commercial_document,   polymorphic: true
    belongs_to :office,                class_name: "Cooperatives::Office"
    belongs_to :cooperative
    belongs_to :recorder,              class_name: "User", foreign_key: 'recorder_id'
    has_many   :credit_amounts,        class_name: 'AccountingModule::Amounts::CreditAmount', dependent: :destroy
    has_many   :debit_amounts,         class_name: 'AccountingModule::Amounts::DebitAmount', dependent: :destroy
    has_many   :credit_accounts,       class_name: 'AccountingModule::Account', through: :credit_amounts, source: :account
    has_many   :debit_accounts,        class_name: 'AccountingModule::Account', through: :debit_amounts,  source: :account
    has_many   :amounts,               class_name: "AccountingModule::Amount", dependent: :destroy
    has_many   :accounts,              class_name: "AccountingModule::Account", through: :amounts
    has_many   :member_entries,        class_name: 'MembershipsModule::MemberEntry', dependent: :destroy
    validates :description, presence: true
    validates :office_id, :cooperative_id, :recorder_id, presence: true

    validate :has_credit_amounts?
    validate :has_debit_amounts?
    validate :amounts_cancel?

    accepts_nested_attributes_for :credit_amounts, :debit_amounts, allow_destroy: true

    before_save :set_default_date, :set_default_time

    delegate :name,  :first_and_last_name, to: :recorder, prefix: true, allow_nil: true
    delegate :name,  to: :cooperative, prefix: true
    delegate :name,  to: :office, prefix: true
    delegate :name,  to: :commercial_document, prefix: true, allow_nil: true


    def ascending_order #for sorting entries in reports
      reference_number.to_i
    end

    def self.recent
      order(entry_date: :desc).first
    end

    def self.oldest
      order(entry_date: :desc).last
    end

    def self.loan_payments
      ids = amounts.for_loans.pluck(:entry_id).uniq.flatten
      where(id: ids)
    end

    def self.except_loan_disbursements
      ids = amounts.for_loans.pluck(:entry_id).uniq.flatten
      where.not(id: ids)
    end



    def self.amounts
      AccountingModule::Amount.where(entry_id: self.ids)
    end


    def self.accounts
      accounts = amounts.pluck(:account_id)
      AccountingModule::Account.where(id: accounts)
    end


    def account_categories
      accounts.account_categories
    end


    def self.without_cash_accounts
      ids = (self.all - where(id: amounts.with_cash_accounts.pluck(:entry_id).uniq)).pluck(:id)
      where(id: ids)
    end

    def self.with_cash_accounts
      ids = amounts.with_cash_accounts.pluck(:entry_id)
      where(id: ids)
    end

    def self.not_archived
      where(archived_at: nil)
    end

    def self.archived
      where.not(archived_at: nil)
    end


    def self.entered_on(args={})
      from_date = args.fetch(:from_date)
      to_date   = args.fetch(:to_date)
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('entry_date' => date_range.start_date..date_range.end_date)
    end

    def self.total
      all.map{|a| a.total }.sum
    end

    def self.total_cash_amount
      sum(&:total_cash_amount)
    end

    def total
      credit_amounts.total
    end

    def total_cash_amount
      amounts.total_cash_amount
    end

    def amounts_valid?
      debit_amounts.total == credit_amounts.total
    end

    def contains_cash_account?
      amounts.with_cash_accounts.present?
    end

    def accounts
      amounts.accounts
    end





    def display_commercial_document
      if commercial_document.try(:member).present?
        commercial_document.try(:member).try(:full_name)
      elsif commercial_document.try(:borrower).present?
        commercial_document.try(:borrower).try(:full_name)
      else
        commercial_document.try(:name)
      end
    end

    private

    def set_default_date
      if entry_date.blank?
        todays_date = ActiveRecord::Base.default_timezone == :utc ? Time.now.utc : Time.now
        self.entry_date = todays_date
      end
    end

    def set_default_time
      if entry_time.blank?
        self.entry_time = self.created_at
      end
    end

    def has_credit_amounts?
      errors[:base] << "Entry must have at least one credit amount" if self.credit_amounts.blank?
    end

    def has_debit_amounts?
      errors[:base] << "Entry must have at least one debit amount" if self.debit_amounts.blank?
    end

    def amounts_cancel?
      errors[:base] << "The credit and debit amounts are not equal" if credit_amounts.balance_for_new_record != debit_amounts.balance_for_new_record
    end
  end
end
