module Offices
  class OfficeLoanProduct < ApplicationRecord
    belongs_to :office,                            class_name: 'Cooperatives::Office'
    belongs_to :loan_product,                      class_name: 'Cooperatives::LoanProduct'
    belongs_to :receivable_account_category,       class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :interest_revenue_account_category, class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :penalty_revenue_account_category,  class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :loan_protection_plan_provider,     class_name: 'LoansModule::LoanProtectionPlanProvider', optional: true
    belongs_to :amortization_type,                 class_name: 'LoansModule::AmortizationType'
    belongs_to :temporary_account,                 class_name: 'AccountingModule::Account'
    has_many :loan_product_charges,                class_name: 'LoansModule::LoanProducts::LoanProductCharge'
    validates :loan_product_id, presence: true, uniqueness: { scope: :office_id }

    def self.receivable_account_categories
      ids = pluck(:receivable_account_category_id)
      ::AccountingModule::LevelOneAccountCategory.where(id: ids.uniq.compact.flatten)
    end
  end
end
