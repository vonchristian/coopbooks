module Offices
  class OfficeMembership < ApplicationRecord
    belongs_to :office,     class_name: 'Cooperatives::Office'
    belongs_to :membership, class_name: 'Cooperatives::Membership'
    enum status: [:active, :inactive]
  end
end
