module Offices
  class OfficeShareCapitalProduct < ApplicationRecord
    belongs_to :office,                            class_name: 'Cooperatives::Office'
    belongs_to :share_capital_product,             class_name: 'Cooperatives::ShareCapitalProduct'
    belongs_to :equity_account_category,           class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :interest_payable_account_category, class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :temporary_account,                 class_name: 'AccountingModule::Account'

    validates :share_capital_product_id, presence: true, uniqueness: { scope: :office_id }
  end
end
