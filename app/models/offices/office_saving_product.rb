module Offices
  class OfficeSavingProduct < ApplicationRecord
    belongs_to :saving_product,                    class_name: 'Cooperatives::SavingProduct'
    belongs_to :office,                            class_name: 'Cooperatives::Office'
    belongs_to :liability_account_category,        class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :interest_expense_account_category, class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :closing_account_category,          class_name: 'AccountingModule::LevelOneAccountCategory'
    belongs_to :temporary_account,                 class_name: 'AccountingModule::Account'

    validates :saving_product_id, uniqueness: { scope: :office_id }
  end
end
