module Offices
  class OfficeProgram < ApplicationRecord
    belongs_to :office,           class_name: 'Cooperatives::Office'
    belongs_to :program,          class_name: 'Cooperatives::Program'
    belongs_to :level_one_account_category, class_name: 'AccountingModule::LevelOneAccountCategory', foreign_key: 'account_category_id'

    validates :account_category_id, uniqueness: true
    validates :program_id, uniqueness: { scope: :office_id }
  end
end
