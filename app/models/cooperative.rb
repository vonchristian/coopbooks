class Cooperative < ApplicationRecord
  audited
  has_one_attached :logo
  has_many :merchants
  has_many :offices,                        class_name: "Cooperatives::Office"
  has_many :main_offices,                   class_name: "Cooperatives::Offices::MainOffice"
  has_many :branch_offices,                 class_name: "Cooperatives::Offices::BranchOffice"
  has_many :satellite_offices,              class_name: "Cooperatives::Offices::SatelliteOffice"
  has_many :store_fronts
  has_many :accounts,                       class_name: "AccountingModule::Account"
  has_many :memberships,                    class_name: "Cooperatives::Membership"
  has_many :member_memberships,             through: :memberships, source: :cooperator, source_type: 'Member'
  has_many :members,                        through: :memberships, source: :cooperator, source_type: 'Member'
  has_many :bank_accounts
  has_many :loans,                          class_name: "LoansModule::Loan"
  has_many :entries,                        class_name: "AccountingModule::Entry"
  has_many :amounts,                        class_name: "AccountingModule::Amount", through: :entries
  has_many :debit_amounts,                  class_name: "AccountingModule::Amounts::DebitAmount", through: :entries
  has_many :credit_amounts,                 class_name: "AccountingModule::Amounts::CreditAmount", through: :entries
  has_many :organizations
  has_many :vouchers
  has_many :voucher_amounts,                class_name: "Vouchers::VoucherAmount"
  has_many :users
  has_many :employees,                      class_name: "User"
  has_many :saving_products,                class_name: "Cooperatives::SavingProduct"
  has_many :loan_products,                  class_name: "Cooperatives::LoanProduct"
  has_many :interest_configs,               through: :loan_products
  has_many :time_deposit_products,          class_name: "Cooperatives::TimeDepositProduct"
  has_many :share_capital_products,         class_name: "Cooperatives::ShareCapitalProduct"
  has_many :programs,                       class_name: "Cooperatives::Program"
  has_many :program_subscriptions,          through: :programs, class_name: "MembershipsModule::ProgramSubscription"
  has_many :savings,                        class_name: "MembershipsModule::Saving"
  has_many :share_capitals,                 class_name: "MembershipsModule::ShareCapital"
  has_many :time_deposits,                  class_name: "MembershipsModule::TimeDeposit"
  has_many :barangays,                      class_name: "Addresses::Barangay"
  has_many :municipalities,                 class_name: "Addresses::Municipality"
  has_many :loan_applications,              class_name: "LoansModule::LoanApplication"
  has_many :employee_cash_accounts,         class_name: "Employees::EmployeeCashAccount"
  has_many :cash_accounts,                  class_name: "AccountingModule::Account", through: :employee_cash_accounts, source: :cash_account
  has_many :amortization_schedules,         class_name: "LoansModule::AmortizationSchedule"
  has_many :beneficiaries
  has_many :savings_account_applications
  has_many :share_capital_applications
  has_many :time_deposit_applications
  has_many :purchase_line_items,            class_name: "StoreFronts::LineItems::PurchaseLineItem"
  has_many :sales_orders,                   class_name: "StoreFronts::Orders::SalesOrder", through: :store_fronts
  has_many :loan_protection_plan_providers, class_name: "LoansModule::LoanProtectionPlanProvider"
  has_many :net_income_distributions
  has_many :level_one_account_categories,   class_name: 'AccountingModule::LevelOneAccountCategory', through: :offices
  has_many :membership_categories,           class_name: 'Cooperatives::MembershipCategory'
  has_many :cooperative_services, through: :offices
  has_many :suppliers, through: :store_fronts

  has_many :barangays,                       through: :offices, class_name: 'Addresses::Barangay'
  has_many :municipalities,                  through: :offices, class_name: 'Addresses::Municipality'
  has_many :provinces,                       through: :offices, class_name: 'Addresses::Province'

  validates :name, :abbreviated_name, presence: true
  validates :name, uniqueness: true
  validates :registration_number, presence: true, uniqueness: true
  def avatar
    logo
  end
end
