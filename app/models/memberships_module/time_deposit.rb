module MembershipsModule
  class TimeDeposit < ApplicationRecord
    audited
    include PgSearch::Model
    include AccountScoping::Barangay
    pg_search_scope :text_search, against: [:account_number, :depositor_name]

    belongs_to :cooperative
    belongs_to :term
    belongs_to :liability_account,        class_name: "AccountingModule::Accounts::Liability"
    belongs_to :interest_expense_account, class_name: 'AccountingModule::Accounts::Expense'
    belongs_to :depositor,                polymorphic: true, touch: true
    belongs_to :office,                   class_name: "Cooperatives::Office"
    belongs_to :time_deposit_product,     class_name: "Cooperatives::TimeDepositProduct"
    belongs_to :organization,             optional: true
    has_many :ownerships,                 as: :ownable
    has_many :depositors,                 through: :ownerships, source: :owner

    delegate :deposit_date, :maturity_date, :matured?, to: :term, prefix: true
    delegate :name, :interest_rate, :interest_expense_account, :break_contract_fee, to: :time_deposit_product, prefix: true
    delegate :full_name, :name, :first_and_last_name, to: :depositor, prefix: true
    delegate :name, to: :office, prefix: true
    delegate :name, :avatar, to: :depositor

    delegate :balance, to: :liability_account
    delegate :matured?, :days_elapsed, to: :term
    before_save :set_depositor_name
    def self.liability_accounts
      ids = pluck(:liability_account_id)
      AccountingModule::Accounts::Liability.where(id: ids)
    end

    def self.deposited_on(args={})
      from_date = args[:from_date]
      to_date   = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where(date_deposited: date_range)
    end

    def entries
      liability_account.entries
    end

    def can_be_extended?
      !withdrawn? && term.matured?
    end

    def withdrawn?
      withdrawn_at.present?
    end

    def self.not_withdrawn
      where(withdrawn_at: nil)
    end

    def member?
      depositor.regular_member?
    end

    def non_member?
      !depositor.regular_member?
    end

    def self.matured
      joins(:term).where('terms.maturity_date < ?', Date.current)
    end
    def amount_deposited
      balance
    end

    def earned_interests
      interest_expense_account.debits_balance
    end

    def computed_break_contract_amount
      TimeDeposits::BreakContractFeeCalculator.new(time_deposit: self).compute
    end

    def computed_earned_interests
      TimeDeposits::InterestCalculator.new(time_deposit: self).compute
    end

    private
    def set_depositor_name
      self.depositor_name ||= self.depositor_full_name
    end
  end
end
