module MembershipsModule
  class ShareCapital < ApplicationRecord
    audited
    extend  PercentActive
    extend BalanceMonitoring
    extend AccountingModule::UpdatedAtFinder
    include PgSearch::Model
    include InactivityMonitoring
    include AccountScoping::Barangay
    include AccountScoping::Municipality



    pg_search_scope :text_search, :against => [:account_number, :account_name]
    multisearchable against: [:account_number, :account_name]

    belongs_to :equity_account,              class_name: "AccountingModule::Account"
    belongs_to :interest_on_capital_account, class_name: "AccountingModule::Account"
    belongs_to :subscriber,                  polymorphic: true
    belongs_to :cooperative
    belongs_to :office,                      class_name: "Cooperatives::Office"
    belongs_to :share_capital_product,       class_name: "Cooperatives::ShareCapitalProduct"
    belongs_to :organization,                optional: true
    has_many :entries,                       class_name: 'AccountingModule::Entry', through: :equity_account


    delegate :name,
            :default_product?,
            :cost_per_share,
            :minimum_balance,
             to: :share_capital_product, prefix: true
    delegate :name, to: :office, prefix: true
    delegate :name, :current_address_complete_address, :current_contact_number, to: :subscriber, prefix: true
    delegate :avatar, :name, to: :subscriber
    delegate :balance, to: :equity_account

    def self.total_balances(args={})
      equity_accounts.balance(args)
    end

    def self.inactive(options={})
      updated_at(options)
    end

    def self.equity_accounts
      ids = pluck(:equity_account_id)
      AccountingModule::Account.where(id: ids)
    end

    def self.interest_on_capital_accounts
      ids = pluck(:interest_on_capital_account_id)
      AccountingModule::Account.where(id: ids)
    end

    def capital_build_ups(args={})
      entries
    end

    def deficit
      balance - share_capital_product_minimum_balance
    end


    def average_monthly_balance(args = {})
      first_entry_date = Date.today - 999.years
      date = args[:date]
      balance(from_date: first_entry_date, to_date: date.beginning_of_month.last_month.end_of_month, commercial_document: self) +
      capital_build_ups(commercial_document: self, from_date: date.beginning_of_month, to_date: (date.beginning_of_month + 14.days)).sum(&:amount)
    end

    def averaged_monthly_balances
      months = []
      (DateTime.now.beginning_of_year..DateTime.now.end_of_year).each do |month|
        months << month.beginning_of_month
      end
      balances = []
      months.uniq.each do |month|
        balances << average_monthly_balance(date: month.beginning_of_month)
      end
      balances.sum / balances.size
    end


    def self.subscribed_shares
      all.sum(&:shares)
    end

    def shares
      balance / share_capital_product_cost_per_share
    end

    def balance(args={})
      equity_account.balance(args)
    end

    def dividends_earned
      share_capital_product_interest_payable_account.balance
    end

    def name
      account_name
    end

    def computed_interest
      ShareCapitals::InterestOnCapitalCalculator.new(share_capital: self).interest
    end

    def latest_transaction
      entries.recent
    end

    def last_transaction_date
      latest_transaction.try(:entry_date) || created_at
    end
  end
end
