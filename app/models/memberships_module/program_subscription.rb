 module MembershipsModule
	class ProgramSubscription < ApplicationRecord
    audited
    include AccountScoping::Office
    include AccountScoping::Barangay
    belongs_to :cooperative
    belongs_to :account,    class_name: 'AccountingModule::Account'
    belongs_to :program,    class_name: "Cooperatives::Program"
	  belongs_to :subscriber, polymorphic: true

    validates :account_number, presence: true, uniqueness: true
    validates :program_id, uniqueness: { scope: :subscriber_id }

    delegate :name,
             :amount,
             :description,
             :payment_status_finder,
             :one_time_payment?,
             to: :program
    delegate :name, to: :subscriber, prefix: true
    delegate :entries,
             :balance,
             :credits_balance,
             :debits_balance, to: :account


    def self.accounts
      ids = pluck(:account_id)
      AccountingModule::Account.where(id: ids)
    end

	  def self.unpaid(args={})
    end

    def self.paid(args={})
    end

    def unpaid?(args={})
      !paid?(args)
    end

	  def paid?(args={})
      payment_status_finder.new(args.merge(subscription: self)).paid?
	  end
	end
end
