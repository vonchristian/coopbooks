module MembershipsModule
  class Saving < ApplicationRecord
    audited
    extend Metricable
    extend BalanceMonitoring
    extend AccountingModule::UpdatedAtFinder
    include PgSearch::Model
    include InactivityMonitoring
    include AccountScoping::Barangay
    include AccountScoping::Municipality
    include AccountScoping::Organization


    extend  PercentActive

    pg_search_scope :text_search, against: [:account_number, :account_name]
    multisearchable against: [:account_number, :account_name]
    belongs_to :cooperative
    belongs_to :liability_account,        class_name: "AccountingModule::Accounts::Liability"
    belongs_to :interest_expense_account, class_name: 'AccountingModule::Accounts::Expense', foreign_key: 'interest_expense_account_id'
    belongs_to :organization,             optional: true
    belongs_to :depositor,                polymorphic: true
    belongs_to :municipality,             class_name: 'Addresses::Municipality', optional: true
    belongs_to :saving_product,           class_name: "Cooperatives::SavingProduct"
    belongs_to :office,                   class_name: "Cooperatives::Office"
    belongs_to :saving_group,             class_name: 'Offices::SavingGroup'
    has_many   :ownerships,               as: :ownable
    has_many :member_depositors,          through: :ownerships, source: :owner, source_type: 'Member'
    has_many :organization_depositors,    through: :ownerships, source: :owner, source_type: 'Organization'
    has_many :entry_transactions,         class_name: 'AccountingModule::EntryTransaction', as: :customer
    has_many :entries,                    through: :liability_account
    has_many :durations,                  as: :termable
    has_many :lock_in_periods,            through: :durations, source: :term

    has_many :credit_entries,             through: :liability_account, class_name: 'AccountingModule::Entry'
    has_many :debit_entries,              through: :liability_account, class_name: 'AccountingModule::Entry'


    validates :liability_account_id, :interest_expense_account_id, uniqueness: true
    validates :account_name, presence: true

    delegate :name, :current_address_complete_address, :current_contact_number, :current_occupation, to: :depositor, prefix: true
    delegate :name,
             :applicable_rate,
             :closing_account,
             :closing_account_fee,
             :closing_account,
             :interest_rate,
             :quarterly_interest_rate,
             to: :saving_product, prefix: true
    delegate :name, to: :office, prefix: true
    delegate :avatar, to: :depositor
    delegate :dormancy_number_of_days, :balance_averager, to: :saving_product
    delegate :balance, to: :liability_account

    def depositors
      member_depositors + organization_depositors
    end

    def overdrawn?
      balance.negative?
    end

    def last_transaction
      entries.recent
    end

    def last_transaction_date
      if last_transaction.present?
        last_transaction.entry_date
      else
        created_at
      end
    end

    def self.interest_earning_accounts
      joins(:saving_product).where('saving_products.no_interest' => false)
    end

    def self.non_interest_earning_accounts
      joins(:saving_product).where('saving_products.no_interest' => true)
    end

    def self.liability_accounts
      ids = pluck(:liability_account_id)
      AccountingModule::Accounts::Liability.where(id: ids)
    end

    def self.interest_expense_accounts
      ids = pluck(:interest_expense_account_id)
      AccountingModule::Accounts::Expense.where(id: ids)
    end

    def self.total_balances(args={})
      liability_accounts.balance(args)
    end


    def self.averaged_balance(args={})
      balances = BigDecimal('0')
      months   = []
      (args[:from_date]..args[:to_date]).each do |date|
        months << date.end_of_month
      end

      months.uniq.each do |month|
        balances += Cooperatives::SavingProduct.total_balance(to_date: month.end_of_month)
      end
      balances
    end

    def self.inactive(args={})
      updated_at(args)
    end

    def closed?
      closed_at.present?
    end

    def dormant?
      dormancy_number_of_days < number_of_days_inactive
    end

    def interest_posted?(args={})
      interest_expense_account.entries.entered_on(args).present?
    end

    def self.top_savers(args={})
      limiting_num = args[:limiting_num] || 10
      savers ||= all.to_a.sort_by(&:balance).reverse.first(limiting_num)
      savers
    end

    def name
      depositor_name
    end

    def name_and_balance
      "#{name} - #{balance.to_f}"
    end

    def deposits
      credit_entries
    end

    def withdrawals
      debit_entries
    end

    def interests_earned(args={})
      interest_expense_account.debits_balance(args)
    end

    def can_withdraw?
      !closed? && balance > 0.0
    end

    def averaged_balance(args={})
      balance_averager.new(saving: self, to_date: args.fetch(:to_date)).averaged_balance
    end

  end
end
