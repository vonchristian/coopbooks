module LoansModule
  class GuaranteedLoan < ApplicationRecord
    belongs_to :guarantor, polymorphic: true
    belongs_to :loan, class_name: 'LoansModule::Loan'

    validates :loan_id, uniqueness: { scope: :guarantor_id }
  end
end
