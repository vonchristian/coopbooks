module LoansModule
  class LoanTerm < ApplicationRecord
    belongs_to :loan
    belongs_to :term
  end
end
