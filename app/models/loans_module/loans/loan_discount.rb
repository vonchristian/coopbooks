module LoansModule
  module Loans
    class LoanDiscount < ApplicationRecord
      enum discount_type: [:interest, :penalty]
      belongs_to :loan
      belongs_to :employee, class_name: "User", foreign_key: 'computed_by_id'
      delegate :name, to: :employee, prefix: true
      def self.total
        sum(:amount)
      end
    end
  end
end
