module LoansModule
  module Loans
    class LoanAutoPay < ApplicationRecord
      belongs_to :loan,            class_name: 'LoansModule::Loan'
      belongs_to :savings_account, class_name: 'MembershipsModule::Saving'

      validates :savings_account_id, uniqueness: { scope: :loan_id }
      before_save :savings_account_of_borrower?

      delegate :borrower, to: :loan

      def self.active
        where(active: true)
      end

      def self.active_loan_ids
        active.pluck(:loan_id)
      end

      private
      def savings_account_of_borrower?
        if !self.borrower.savings.ids.include?(self.savings_account_id)
          errors[:savings_account_id] << 'Savings account not of borrower'
          return false
        end
      end
    end
  end
end
