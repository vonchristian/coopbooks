module LoansModule
  module Loans
    class LoanAccount < ApplicationRecord
      belongs_to :loan,    class_name: 'LoansModule::Loan'
      belongs_to :account, class_name: 'AccountingModule::Account'

      validates :account_id, uniqueness: { scope: :loan_id }

      def self.accounts
        ids = pluck(:account_id)
        AccountingModule::Account.where(id: ids.uniq)
      end
    end
  end
end
