module LoansModule
  module Loans
    class LoanAging < ApplicationRecord
      belongs_to :loan,       class_name: 'LoansModule::Loan'
      belongs_to :loan_group, class_name: 'LoansModule::LoanGroup'

      validates :loan_group_id, uniqueness: { scope: :loan_id }

      def self.current
        order(date: :desc).first
      end
    end
  end
end
