module LoansModule
  module Loans
    module Interest
      def total_interest_payments
        interest_revenue_account.credits_balance
      end

      def balance
        interest_revenue_account.balance
      end

    end
  end
end
