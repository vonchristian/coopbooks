module LoansModule
  module Loans
    module Penalty
      def penalties_debits_balance
         loan_penalties.total
      end
      def penalties_balance
        loan_penalties.total
      end
      def total_penalty_payments
        penalty_revenue_account.credits_balance
      end
    

      def compute(loan, schedule)
        #compute daily loan penalty
        loan.unpaid_balance_for(schedule) * (rate / 30)
      end

      def rate
       loan.loan_product.penalty_rate
      end
    end
  end
end
