module LoansModule
  class LoanProtectionPlanProvider < ApplicationRecord
    belongs_to :office,          class_name: 'Cooperatives::Office'
    belongs_to :payable_account, class_name: "AccountingModule::Account"
    has_many   :loan_products,   class_name: "Cooperatives::LoanProduct"
    has_many   :loans,           through: :loan_products, class_name: "LoansModule::Loan"

    validates :business_name, presence: true, uniqueness: { scope: :office_id }
    validates :rate, presence: true, numericality: true
    delegate :name, to: :payable_account, prefix: true

    def amount_for(loan_application)
      LoansModule::LoanProtectionCalculator.new(loan_application: loan_application, plan_provider: self).calculate
    end

  end
end
