module LoansModule
  class Loan < ApplicationRecord
    enum status: [:current_loan, :past_due]
    audited
    extend PercentActive
    include PgSearch::Model
    include LoansModule::Loans::Amortization
    include InactivityMonitoring
    include Taggable
    include AccountScoping::Barangay
    include AccountScoping::Municipality
    include AccountScoping::Organization

    pg_search_scope :text_search, :against => [:borrower_full_name, :tracking_number, :account_number]
    multisearchable against: [:borrower_full_name]
    enum mode_of_payment: [:daily, :weekly, :monthly, :semi_monthly, :quarterly, :semi_annually, :lumpsum]
    has_one    :loan_auto_pay,             class_name: 'LoansModule::Loans::LoanAutoPay'
    belongs_to :cooperative
    belongs_to :receivable_account,        class_name: 'AccountingModule::Account'
    belongs_to :interest_revenue_account,  class_name: 'AccountingModule::Account'
    belongs_to :penalty_revenue_account,   class_name: 'AccountingModule::Account'
    belongs_to :unearned_interest_account, class_name: 'AccountingModule::Account', optional: true
    belongs_to :loan_application,          class_name: "LoansModule::LoanApplication", optional: true
    belongs_to :voucher,                   optional: true
    belongs_to :office,                 class_name: "Cooperatives::Office"
    belongs_to :archived_by,            class_name: "User", foreign_key: 'archived_by_id', optional: true
    belongs_to :borrower,               polymorphic: true
    belongs_to :loan_product,           class_name: "Cooperatives::LoanProduct"
    belongs_to :street,                 class_name: "Addresses::Street",  optional: true
    belongs_to :barangay,               class_name: "Addresses::Barangay",  optional: true
    belongs_to :municipality,           class_name: "Addresses::Municipality",optional: true
    belongs_to :organization,           optional: true

    has_many :amortization_schedules
    has_many :notices,                  class_name: "LoansModule::Notice",  as: :notified
    has_many :loan_interests,           class_name: "LoansModule::Loans::LoanInterest"
    has_many :loan_penalties,           class_name: "LoansModule::Loans::LoanPenalty"
    has_many :loan_discounts,           class_name: "LoansModule::Loans::LoanDiscount"

    has_many :notes,                    as: :noteable
    has_many :loan_terms,               class_name: 'LoansModule::LoanTerm'
    has_many :durations,                as: :termable
    has_many :terms,                    through: :loan_terms

    has_many :loan_co_makers,           class_name: "LoansModule::LoanCoMaker"
    has_many :member_co_makers,         through: :loan_co_makers, source: :co_maker, source_type: "Member"
    has_many :loan_accounts,            class_name: 'LoansModule::Loans::LoanAccount'
    has_many :accounts,                 through: :loan_accounts, class_name: 'AccountingModule::Account'
    has_many :entries,                  through: :accounts
    has_many :principal_entries,        through: :receivable_account, source: :entries
    has_many :interest_entries,         through: :interest_revenue_account, source: :entries
    has_many :penalty_entries,          through: :penalty_revenue_account, source: :entries
    has_many :loan_agings,              class_name: 'LoansModule::Loans::LoanAging'
    has_many :loan_groups,              through: :loan_agings, class_name: 'LoansModule::LoanGroup'
    has_many :guaranteed_loans,         class_name: 'LoansModule::GuaranteedLoan'
    has_many :member_guarantors,        through: :guaranteed_loans, source: :guarantor, source_type: 'Member'
    delegate :name, :address, :contact_number, to: :cooperative, prefix: true
    delegate :effectivity_date, :is_past_due?, :number_of_days_past_due, :remaining_term, :terms_elapsed, :maturity_date, to: :current_term, allow_nil: true
    delegate :name, :age, :contact_number, :current_address, :current_address_complete_address, :current_contact_number,  :first_name, to: :borrower,  prefix: true, allow_nil: true
    delegate :name,  to: :loan_product, prefix: true
    delegate :payment_processor, to: :loan_product
    delegate :interest_rate,
             :penalty_rate,:monthly_interest_rate,
             to: :loan_product, prefix: true

    delegate :maximum_loanable_amount, to: :loan_product
    delegate :avatar, to: :borrower

    delegate :name, to: :office, prefix: true
    delegate :number_of_months, to: :current_term, prefix: true
    delegate :term, to: :current_term
    delegate :balance, to: :receivable_account
    delegate :loan_group, to: :current_loan_aging
    validates :borrower_full_name, presence: true

    def self.auto_pay_enabled
      ids = LoansModule::Loans::LoanAutoPay.active_loan_ids
      where(id: ids)
    end

    def balance_for_loan_group(loan_group)
      ::Loans::LoanGroupBalanceCalculator.new(loan: self, loan_group: loan_group).balance
    end

    def last_transaction
      entries.recent
    end

    def current_loan_aging
      loan_agings.current
    end

    def last_transaction_date
      last_transaction.try(:entry_date) || created_at
    end

    def paid?
      balance.zero? && paid_at.present?
    end

    def self.paid_loans
      where.not(paid_at: nil)
    end

    def self.accounts
      loan_accounts.accounts
    end

    def self.loan_accounts
      LoansModule::Loans::LoanAccount.where(loan_id: self.ids.uniq)
    end

    def self.principal_balance
      receivable_accounts.balance
    end

    def self.receivable_accounts
      ids = pluck(:receivable_account_id)
      AccountingModule::Accounts::Asset.where(id: ids)
    end

    def self.interest_revenue_accounts
      ids = pluck(:interest_revenue_account_id)
      AccountingModule::Accounts::Revenue.where(id: ids)
    end

    def self.penalty_revenue_accounts
      ids = pluck(:penalty_revenue_account_id)
      AccountingModule::Accounts::Revenue.where(id: ids)
    end

    def self.payments
      entry_ids = []
      entry_ids = receivable_accounts.credit_entries.pluck(:id)
      entry_ids = interest_revenue_accounts.credit_entries.pluck(:id)
      entry_ids = penalty_revenue_accounts.credit_entries.pluck(:id)
      AccountingModule::Entry.where(id: entry_ids.uniq.compact.flatten)
    end



    def net_proceed #move to loan application
      if loan_application
        loan_application.net_proceed
      else
        0
      end
    end

    def total_loan_interests
      loan_interests.total_interests
    end

    def interest_rate
      if loan_application.present?
        loan_application.interest_rate
      else
        loan_product.current_interest_config_rate
      end
    end

    def loan_payments
      entry_ids = []
      entry_ids << receivable_account.credit_entries.pluck(:id)
      entry_ids << interest_revenue_account.credit_entries.pluck(:id)
      entry_ids <<  penalty_revenue_account.credit_entries.pluck(:id)
      AccountingModule::Entry.where(id: entry_ids.uniq.compact.flatten)
    end

    def badge_color
      return 'danger' if past_due?
      return 'success' if current_loan?
    end

    def total_deductions(args={})
      amortized_principal_for(from_date: args[:from_date], to_date: args[:to_date]) -
      amortized_interest_for(from_date: args[:from_date], to_date: args[:to_date]) +
      arrears(from_date: application_date, to_date: args[:from_date].yesterday.end_of_day)
    end

    def current_term
      terms.current
    end

    def name
      borrower_full_name
    end

    def self.active
      not_archived
    end

    def self.archived
      where.not(archived_at: nil)
    end

    def self.not_archived
      where(archived_at: nil)
    end

    def self.not_forwarded
      where(forwarded_loan: false)
    end

    def self.forwarded_loans
      where(forwarded_loan: true)
    end


    def self.current_loans
      not_matured
    end

    def self.not_matured
        disbursed.joins(:terms).where('terms.maturity_date > ?', Date.today)
    end

    def self.past_due_loans
      disbursed.not_archived.joins(:terms).where('terms.maturity_date < ?', Date.today)
    end

    def self.past_due_loans_on(args={})
      from_date = args.fetch(:from_date)
      to_date   = args.fetch(:to_date)
      range     = DateRange.new(from_date: from_date, to_date: to_date)
      disbursed.not_archived.
      joins(:terms).where('terms.maturity_date' => range.range)
    end

    def self.disbursed
      where.not(disbursement_date: nil)
    end

    def self.disbursed_on(args={})
      from_date = args[:from_date]
      to_date   = args[:to_date]
      range     = DateRange.new(from_date: from_date, to_date: to_date)
      where(disbursement_date: range.start_date..range.end_date)
    end

    def self.updated_at(args={})
      from_date  = args[:from_date]
      to_date    = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      joins(:entries).where('entries.entry_date' => date_range.start_date..date_range.end_date)
    end

    def self.matured
      joins(:terms).where('terms.maturity_date < ?', Date.today)
    end

    def self.matured_on(args={})
      range = DateRange.new(from_date: args[:from_date], to_date: args[:to_date])
      self.joins(:terms).where('terms.maturity_date' => range.start_date..range.end_date)
    end

    def voucher_amounts_excluding_loan_amount_and_net_proceed
      accounts = []
      Employees::EmployeeCashAccount.cash_accounts.each do |account|
        accounts << account
      end
      accounts << loan_product_current_account
      voucher_amounts.excluding_account(account: accounts )
    end

    def current?
      !is_past_due?
    end

    def name
      borrower_name
    end

    def balance_for(schedule)
      loan_amount - amortization_schedules.principal_for(schedule: schedule)
    end

    def payments_total
      principal_payments +
      total_interest_payments +
      penalty_payments
    end

    def loan_interests_balance
      total_loan_interests -
      total_interest_discounts -
      total_interest_payments
    end

    def total_interest_payments
      interest_revenue_account.balance
    end

    def total_interest_discounts
      loan_discounts.interest.total
    end

    def loan_penalties_balance(args={})
      total_loan_penalties -
      total_penalty_discounts -
      total_penalty_payments
    end

    def total_loan_penalties
      loan_penalties.total_amount
    end

    def total_penalty_discounts
      loan_discounts.penalty.total
    end
    def total_penalty_payments
      penalty_revenue_account.balance
    end

    def status_color
      if is_past_due?
        'danger'
      elsif paid?
        'success'
      else
        'gray'
      end
    end

    def status_text
      if is_past_due?
        'Past Due'
      elsif paid?
        'Paid'
      else
        'Current'
      end
    end


    def loan_penalty_computation
      daily_rate = loan_product.current_penalty_config_rate / 30.0
      (balance * daily_rate) * number_of_days_past_due
    end

    def loan_interest_computation
      daily_rate = loan_product_interest_rate / 30.0
      (principal_balance * daily_rate) * number_of_days_past_due
    end

    def current_amortized_principal
      amortization_schedules.scheduled_for(from_date: Date.today.beginning_of_month, to_date: Date.today.end_of_month).sum(&:principal)
    end

    def current_amortized_interest
      amortization_schedules.scheduled_for(from_date: Date.today.beginning_of_month, to_date: Date.today.end_of_month).sum(&:interest)
    end

    def amortized_principal
      if forwarded_loan == false
        amortized_principal_for(from_date: loan_application.first_amortization_date.beginning_of_month, to_date: loan_application.first_amortization_date.end_of_month)
      else
        0
      end
    end

    def amortized_interest
      if forwarded_loan == false
        amortized_interest_for(from_date: loan_application.first_amortization_date.beginning_of_month, to_date: loan_application.first_amortization_date.end_of_month)
      else
        0
      end
    end

    def archived?
      archived_at.present?
    end

    private
    def set_borrower_full_name
      self.borrower_full_name = self.borrower.full_name
    end
  end
end
