module LoansModule
	class AmortizationSchedule < ApplicationRecord
    enum payment_status: [:fully_paid, :partially_paid, :missed_payment]
    belongs_to :cooperative
    belongs_to :loan,                optional: true
    belongs_to :loan_application,    optional: true
    belongs_to :office,              class_name: "Cooperatives::Office"
    has_many :notes,                 as: :noteable
    has_many :amortization_payments, class_name: 'LoansModule::Amortizations::AmortizationPayment'
    has_many :entries,               class_name: 'AccountingModule::Entry', through: :amortization_payments

    validates :date, :principal, :interest, :total_repayment, presence: true
    validates :principal, :interest, :total_repayment, numericality: true

    delegate :borrower, :loan_product_name, to: :loan
    delegate :avatar, :name, :current_contact_number, :current_address_complete_address, to: :borrower, prefix: true

    def self.for_loans
      where.not(loan_id: nil)
    end

    def self.total_principal
      sum(:principal)
    end

    def self.total_interest
      sum(:interest)
    end


    def self.total_principal_for(args={})
      from_date = args.fetch(:from_date)
      to_date   = args.fetch(:to_date)
      scheduled_for(from_date: from_date, to_date: to_date).sum(&:principal)
    end

    def total_payments
      payment_entries.sum {|e| e.debit_amounts.total}
    end

    def self.total_repayment
      sum(&:total_payments)
    end


    def self.no_payments
      ids = LoansModule::Amortizations::AmortizationPayment.pluck(:amortization_schedule_id)
      where.not(id: ids)
    end

    def self.total_interest
      sum(:interest)
    end

    def self.latest
      order(date: :asc).last
    end

    def self.oldest
      order(date: :asc).first
    end

    def self.by_oldest_date
      order(date: :asc)
    end

    def self.total_principal_balance(args={})
      to_date   = args.fetch(:to_date)
      from_date = args.fetch(:from_date)
      scheduled_for(from_date: from_date, to_date: to_date).sum(&:principal)
    end

    def self.total_interest_for(args={})
      to_date   = args.fetch(:to_date)
      from_date = args.fetch(:from_date)
      scheduled_for(from_date: from_date, to_date: to_date).sum(&:interest)
    end

    def self.principal_for(args={})
      schedule  = args.fetch(:schedule)
      from_date = oldest.date
      to_date   = schedule.date
      select { |a| (from_date.beginning_of_day..to_date.end_of_day).cover?(a.date) }.sum(&:principal)
    end

    def self.total_amortization_for(args={})
      schedule  = args.fetch(:schedule)
      from_date = oldest.date
      to_date   = schedule.date
      select { |a| (from_date.beginning_of_day..to_date.end_of_day).cover?(a.date) }.sum(&:total_repayment)
    end

    def self.total_amortization(args={})
      scheduled_for(args).sum(&:total_amortization)
    end

    def self.scheduled_for(args={})
      from_date  = args.fetch(:from_date)
      to_date    = args.fetch(:to_date)
			date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('date' => date_range.range)
    end


    def total_amortization
       principal +
       interest_computation
    end

    def interest_computation
      if prededucted_interest?
        0
      else
        interest
      end
    end

    def previous_schedule
      from_date = self.class.oldest.date
      to_date   = self.date
      count     = self.class.select { |a| (from_date.beginning_of_day..to_date.end_of_day).cover?(a.date) }.count
      self.class.by_oldest_date.take(count-1).last
    end
	end
end
