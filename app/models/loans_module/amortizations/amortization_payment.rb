module LoansModule
  module Amortizations
    class AmortizationPayment < ApplicationRecord
      belongs_to :amortization_schedule, class_name: 'LoansModule::AmortizationSchedule'
      belongs_to :entry,                 class_name: 'AccountingModule::Entry'

      delegate :loan, to: :amortization_schedule
      # validate :entry_is_for_loan?
      private

      def entry_is_for_loan?
        result = []
        loan.accounts.each do |account|
          result << entry.amounts.accounts.pluck(:id).include?(account.id)
        end
        return errors[:entry_id] << "is not a loan payment" if !result.uniq.include?(true)
      end
    end
  end
end
