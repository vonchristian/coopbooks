module LoansModule
  module LoanProducts
    class LoanProductCharge < ApplicationRecord
      enum charge_type: [:amount_based, :percent_based]
      belongs_to :office_loan_product, class_name: "Offices::OfficeLoanProduct"
      belongs_to :account, class_name: 'AccountingModule::Account'
      validates :name, :account_id, :amount, :rate, presence: true
      validates :rate, :amount, numericality: true
      delegate :name, to: :account, prefix: true

      def self.except_capital_build_up
        where.not(account: Cooperatives::ShareCapitalProduct.pluck(:equity_account_id))
      end

      def charge_amount(args={})
        charge_calculator.new(args.merge(charge: self)).calculate
      end

      def charge_calculator
        ("LoansModule::LoanProductChargeCalculators::" + charge_type.titleize.gsub(" ", "")).constantize
      end

      def rate_in_percent
        rate * 100
      end
    end
  end
end
