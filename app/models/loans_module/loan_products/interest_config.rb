module LoansModule
  module LoanProducts
    class InterestConfig < ApplicationRecord
      extend Totalable
      enum calculation_type: [:add_on, :prededucted]

      belongs_to :loan_product,                     class_name: "Cooperatives::LoanProduct"


      validates :rate,  presence: true
      validates :rate, numericality: true

      def self.current
        all.order(created_at: :desc).first
      end

      def compute_interest(args={})
        LoansModule::InterestComputation.new(
          loan_amount: args[:amount],
          rate: args[:rate] || monthly_interest_rate,
          term: args[:term]
        ).compute
      end

      def monthly_interest_rate
        rate / 12.0
      end
    end
  end
end
