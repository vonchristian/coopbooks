module LoansModule
  class GuaranteedLoanApplication < ApplicationRecord
    belongs_to :loan_application, class_name: 'LoansModule::LoanApplication'
    belongs_to :guarantor, polymorphic: true
  end
end
