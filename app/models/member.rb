class Member < ApplicationRecord
  audited
  include PgSearch::Model
  include Contacting
  include Addressing
  extend PercentActive
  extend BirthdayMonitoring
  extend LastTransactionDateMonitoring

  pg_search_scope :text_search, :against => [ :first_name, :middle_name, :last_name]
  multisearchable against: [:first_name, :last_name, :middle_name]
  enum sex: [:male, :female]
  enum civil_status: [:single, :married, :widow, :divorced]

  has_one_attached :signature_specimen
  has_one_attached :avatar

  has_one :member_account #for devise login
  has_many :voucher_amounts,          class_name: "Vouchers::VoucherAmount", as: :commercial_document
  has_many :memberships,              class_name: "Cooperatives::Membership", as: :cooperator, dependent: :destroy
  has_many :savings,                  class_name: "MembershipsModule::Saving", as: :depositor
  has_many :share_capitals,           class_name: "MembershipsModule::ShareCapital", as: :subscriber
  has_many :time_deposits,            class_name: "MembershipsModule::TimeDeposit", as: :depositor
  has_many :program_subscriptions,    class_name: "MembershipsModule::ProgramSubscription", as: :subscriber
  has_many :programs,                 class_name: 'Cooperatives::Program', through: :program_subscriptions
  has_many :member_occupations,       class_name: "MembershipsModule::MemberOccupation", dependent: :destroy
  has_many :occupations,              through: :member_occupations
  has_many :loans,                    class_name: "LoansModule::Loan", as: :borrower
  has_many :subscribed_programs,      class_name: "Cooperatives::Program", through: :program_subscriptions, source: :program
  has_many :sales,                    class_name: "StoreFronts::Orders::SalesOrder", as: :customer
  has_many :organization_memberships, class_name: "Organizations::OrganizationMembership",   as: :cooperator
  has_many :organizations,            through: :organization_memberships
  has_many :relationships,            as: :relationee
  has_many :relations,                as: :relationer
  has_many :beneficiaries, dependent: :destroy
  has_many :loan_applications,            class_name: "LoansModule::LoanApplication", as: :borrower
  has_many :share_capital_applications,   as: :subscriber
  has_many :savings_account_applications, as: :depositor
  has_many :time_deposit_applications,    as: :depositor
  has_many :identifications, class_name: "IdentificationsModule::Identification", as: :identifiable
  has_many :store_front_accounts, as: :customer, class_name: 'StoreFronts::StoreFrontAccount'
  has_many :office_memberships,       class_name: 'Offices::OfficeMembership', through: :memberships
  has_many :entry_transactions,       class_name: 'AccountingModule::EntryTransaction', as: :customer
  has_many :entries,                  class_name: 'AccountingModule::Entry', through: :entry_transactions

  validates :last_name, :first_name, presence: true, on: :update
  delegate :name, to: :current_organization, prefix: true, allow_nil: true
  before_save :update_birth_date_fields, :normalize_name
  def date_of_membership_for(cooperative:)
    membership = memberships.where(cooperative: cooperative).last
    if membership
      membership.membership_date
    end
  end
  def self.retired
    where.not(retired_at: nil)
  end

  def self.for_cooperative(args={})
    joins(:memberships).where('memberships.cooperative_id' => args[:cooperative].id)
  end

  def self.created_at(args={})
    if args[:from_date] && args[:to_date]
      from_date = args[:from_date]
      to_date   = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      where('created_at' => date_range.range)
    end
  end

  def self.active_at(args={})
    if args[:from_date] && args[:to_date]
      from_date = args[:from_date]
      to_date   = args[:to_date]
      date_range = DateRange.new(from_date: from_date, to_date: to_date)
      joins(:entries).where('entries.entry_date' => date_range.start_date..date_range.end_date)
    end
  end

  def self.inactive_at(args={})
    where.not(id: self.active_at(args))
  end

  def retired?
    retired_at.present?
  end

  def current_organization
    organizations.current
  end


  def name_and_details #for select2 referencing
    "#{full_name} (#{current_membership.membership_type.try(:titleize)})"
  end

  def name
    full_name
  end

  def latest_purchase_date
    if sales_orders.present?
      sales_orders.order(created_at: :asc).last.date
    else
      "No Purchases yet"
    end
  end

  def current_occupation
    occupations.current
  end

  def recommended_co_makers
    Member.where(last_name: self.last_name)
  end

  def recommended_relationships
    Member.where(last_name: self.last_name)
  end

  def full_name
    "#{last_name}, #{first_name} #{middle_name}"
  end

  def first_and_last_name
    "#{first_name} #{last_name}"
  end

  def first_middle_and_last_name #for pdf signatories
    if middle_name.present?
      "#{first_name.titleize} #{middle_name.first.upcase}. #{last_name.titleize}"
    else
      "#{first_name.titleize} #{last_name.titleize}"
    end
  end

  def activity_status(args={})
    office    = args.fetch(:office)
    from_date = args.fetch(:from_date)
    to_date   = args.fetch(:to_date)
    activity  = entries.where(office: office).entered_on(from_date: from_date, to_date: to_date)
    if activity.present?
      'Active'
    else
      'Inactive'
    end
  end

  def activity_status_text_color(args={})
    if activity_status(args) == 'Active'
      'success'
    else
      'danger'
    end
  end

  def default_last_transaction_date
    last_transaction_date || updated_at
  end

  private
  def set_fullname
    self.fullname = self.full_name #used for slugs
  end

  def update_birth_date_fields
    if date_of_birth_changed?
      self.birth_month = date_of_birth ? date_of_birth.month : nil
      self.birth_day   = date_of_birth ? date_of_birth.day : nil
      self.birth_year  = date_of_birth ? date_of_birth.year : nil
    end
  end

  def normalize_name
    self.first_name  = Propercaser.new(text: self.first_name).propercase
    self.middle_name = Propercaser.new(text: self.middle_name || "").propercase
    self.last_name   = Propercaser.new(text: self.last_name).propercase
  end
end
