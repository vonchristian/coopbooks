class Cart < ApplicationRecord
  belongs_to :employee,                   class_name: 'User', foreign_key: 'user_id'
  has_many :cash_counts,                  class_name: 'CashCounts::CashCount'
  has_many :loan_applications,            class_name: 'LoansModule::LoanApplication', dependent: :nullify
  has_many :share_capital_applications,   dependent: :nullify
  has_many :savings_account_applications, dependent: :nullify
  has_many :voucher_amounts,              class_name: 'Vouchers::VoucherAmount', dependent: :nullify
  has_many :purchase_line_items,          class_name: 'StoreFronts::LineItems::PurchaseLineItem', dependent: :destroy
  has_many :sales_line_items,             class_name: 'StoreFronts::LineItems::SalesLineItem', dependent: :destroy

  def total_cost
    line_items.sum(&:total_cost)
  end

end
