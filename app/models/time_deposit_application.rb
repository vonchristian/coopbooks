class TimeDepositApplication < ApplicationRecord
  belongs_to :cooperative
  belongs_to :office,               class_name: "Cooperatives::Office"
  belongs_to :depositor,            polymorphic: true
  belongs_to :time_deposit_product, class_name: "Cooperatives::TimeDepositProduct"
end
