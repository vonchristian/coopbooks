class Relationship < ApplicationRecord
  enum relationship_type: [:father, :mother, :son, :daughter, :grandmother, :grandfather, :brother, :sister]
  belongs_to :relationee, polymorphic: true
  belongs_to :relationer, polymorphic: true

  validates :relationee_id, :relationer_id, :relationer_type, :relationship_type, presence: true

  delegate :name, to: :relationer, prefix: true, allow_nil: true
  delegate :name, to: :relationee, prefix: true, allow_nil: true
end
