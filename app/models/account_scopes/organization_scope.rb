module AccountScopes
  class OrganizationScope < ApplicationRecord
    belongs_to :organization
    belongs_to :account, polymorphic: true

    validates :account_id, uniqueness: { scope: :organization_id }
  end
end
