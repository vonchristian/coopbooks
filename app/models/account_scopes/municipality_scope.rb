module AccountScopes
  class MunicipalityScope < ApplicationRecord
    belongs_to :municipality, class_name: 'Addresses::Municipality'
    belongs_to :account,      polymorphic: true

    validates :account_id, uniqueness: { scope: :municipality_id }
  end
end
