module AccountScopes
  class BarangayScope < ApplicationRecord
    belongs_to :account, polymorphic: true
    belongs_to :barangay, class_name: 'Addresses::Barangay'

    validates :account_id, uniqueness: { scope: :barangay_id }
  end
end
