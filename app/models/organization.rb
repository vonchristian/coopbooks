class Organization < ApplicationRecord
  audited
  include PgSearch::Model
  include Addressing
  include Contacting
  include LastTransactionDateMonitoring
  pg_search_scope :text_search, against: [:name, :abbreviated_name]

  has_one_attached :avatar
  belongs_to :office,                 class_name: 'Cooperatives::Office'
  has_many :organization_memberships, class_name: "Organizations::OrganizationMembership"
  has_many :member_memberships,       through: :organization_memberships, source: :cooperator, source_type: "Member"
  has_many :employee_memberships,     through: :organization_memberships, source: :cooperator, source_type: "User"
  has_many :loans,                    class_name: "LoansModule::Loan", as: :borrower
  has_many :savings,                  class_name: "MembershipsModule::Saving", as: :depositor
  has_many :share_capitals,           class_name: "MembershipsModule::ShareCapital", as: :subscriber
  has_many :time_deposits,            class_name: "MembershipsModule::TimeDeposit", as: :depositor
  has_many :member_savings,           class_name: "MembershipsModule::Saving", as: :depositor
  has_many :organization_scopes,      class_name: 'AccountScopes::OrganizationScope'
  has_many :member_time_deposits,     class_name: "MembershipsModule::TimeDeposit", through: :organization_scopes, source: :account, source_type: 'MembershipsModule::TimeDeposit'
  has_many :member_share_capitals,    class_name: "MembershipsModule::ShareCapital", through: :organization_scopes, source: :account, source_type: 'MembershipsModule::ShareCapital'
  has_many :member_savings,           class_name: "MembershipsModule::Saving", through: :organization_scopes, source: :account, source_type: 'MembershipsModule::Saving'
  has_many :member_loans,             class_name: "LoansModule::Loan", through: :organization_scopes, source: :account, source_type: 'LoansModule::Loan'
  has_many :entry_transactions,       class_name: 'AccountingModule::EntryTransaction', as: :customer
  has_many :entries,                  through: :entry_transactions, class_name: 'AccountingModule::Entry'

  validates :name,  presence: true, uniqueness: { scope: :office_id }

  before_save :set_default_image

  def self.current
    order(created_at: :desc).first
  end

  def members
    member_memberships + employee_memberships
  end

  private
  def set_default_image
    if avatar.attachment.blank?
      self.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'default_business_logo.jpg')), filename: 'default-image.jpg', content_type: 'image/jpg')
    end
  end
end
