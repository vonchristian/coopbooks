module Savings
  class Deposit
    include ActiveModel::Model
    attr_accessor :saving_id, :employee_id, :amount, :or_number, :account_number, :description, :date, :offline_receipt, :cash_account_id
    validates :amount, presence: true, numericality: { greater_than: 0.01 }
    validates :or_number, :saving_id, :account_number, :employee_id, :description, :date, :cash_account_id, presence: true

    def create_voucher!
      if valid?
        ActiveRecord::Base.transaction do
          create_deposit_voucher
          remove_cart_reference
        end
      end
    end

    def find_voucher
      find_employee.office.vouchers.find_by(account_number: account_number)
    end

    def find_saving
      find_employee.office.savings.find(saving_id)
    end
    def find_employee
      User.find(employee_id)
    end

    private
    def create_deposit_voucher
      voucher = Voucher.new(
        payee:            find_saving.depositor,
        office:           find_employee.office,
        cooperative:      find_employee.cooperative,
        preparer:         find_employee,
        description:      description,
        reference_number: or_number,
        account_number:   account_number,
        date:             date)
      voucher.voucher_amounts.debit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     debit_account,
        amount:      amount)
      voucher.voucher_amounts.credit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     credit_account,
        amount:      amount)
      voucher.save!
    end

    def remove_cart_reference
      find_voucher.voucher_amounts.each do |amount|
        amount.cart_id = nil
        amount.save!
      end
    end


    def debit_account
      find_employee.cash_accounts.find(cash_account_id)
    end

    def credit_account
      find_saving.liability_account
    end
  end
end
