module Savings
  class Closing
    include ActiveModel::Model
    include ActiveModel::Validations
    attr_accessor :savings_account_id, :amount, :closing_account_fee,
    :reference_number, :date, :employee_id, :cash_account_id, :account_number

    validates :amount,  presence: true, numericality: true
    validates :savings_account_id, :reference_number, :date, :cash_account_id, :employee_id, :account_number, presence: true
    validate :ensure_amount_less_than_current_cash_on_hand
    validate :ensure_amount_is_less_than_balance

    def create_voucher!
      if valid?
        ActiveRecord::Base.transaction do
          create_voucher
        end
      end
    end

    def find_voucher
      Voucher.find_by!(account_number: account_number)
    end

    private

    def find_savings_account
      MembershipsModule::Saving.find_by_id(savings_account_id)
    end

    def create_voucher
      voucher = Voucher.new(
        office: find_employee.office,
        cooperative: find_employee.cooperative,
        preparer: find_employee,
        description: 'Closing of savings account',
        reference_number: reference_number,
        account_number: account_number,
        date: date,
        payee: find_savings_account.depositor
      )
      voucher.voucher_amounts.debit.build(
        cooperative: find_employee.cooperative,
        account:     debit_account,
        amount:      find_savings_account.balance,
        recorder: find_employee
      )
      voucher.voucher_amounts.credit.build(
        cooperative: find_employee.cooperative,
        account:     cash_account,
        amount:      amount,
        recorder: find_employee
      )
      if !closing_account_fee.nil? && !closing_account_fee.to_d.zero?
        voucher.voucher_amounts.credit.build(
          account: closing_fee_account,
          amount: closing_account_fee,
          cooperative: find_employee.cooperative,
          recorder: find_employee
        )
      end
      voucher.save!
    end

    def closing_fee_account
      find_savings_account.saving_product_closing_account
    end

    def cash_account
      find_employee.cash_accounts.find(cash_account_id)
    end

    def debit_account
      find_savings_account.liability_account
    end

    def find_employee
      User.find(employee_id)
    end

    def ensure_amount_is_less_than_balance
      errors[:amount] << "Amount exceeded balance"  if amount.to_f > find_savings_account.balance
    end

    def ensure_amount_less_than_current_cash_on_hand
      errors[:amount] << "Amount exceeded current cash on hand" if amount.to_f > cash_account.balance
    end
  end

end
