module Savings
  class InterestExpense
    include ActiveModel::Model
    attr_accessor :date, :reference_number, :description, :amount, :savings_account_id, :employee_id, :account_number

    validates :date, :reference_number, :description, :amount, :savings_account_id, :employee_id, :account_number, presence: true
    validates :amount, numericality: { greater_than: 0 }
    
    def process!
      if valid?
        ActiveRecord::Base.transaction do
          create_voucher
        end
      end
    end

    def find_voucher
      find_office.vouchers.find_by!(account_number: account_number)
    end

    private
    def create_voucher
      voucher = find_office.vouchers.build(
        date:             date,
        account_number:   account_number,
        cooperative:      find_office.cooperative,
        payee:            find_savings_account.depositor,
        reference_number: reference_number,
        description:      description,
        preparer:         find_employee)

      voucher.voucher_amounts.credit.build(
          description: description,
          voucher:     voucher,
          amount:      amount,
          account:     find_savings_account.liability_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)

          voucher.voucher_amounts.debit.build(
              description: description,
              voucher:     voucher,
              amount:      amount,
              account:     find_savings_account.interest_expense_account,
              recorder:    find_employee,
              cooperative: find_employee.cooperative)
        voucher.save!
    end

    def find_employee
      User.find(employee_id)
    end

    def find_office
      find_employee.office
    end

    def find_savings_account
      find_office.savings.find(savings_account_id)
    end
  end
end
