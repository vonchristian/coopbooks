module Savings
  class Withdraw
    include ActiveModel::Model
    attr_accessor :saving_id, :employee_id, :amount, :or_number, :account_number, :date, :offline_receipt, :cash_account_id, :account_number
    validates :amount, presence: true, numericality: { greater_than: 0.01 }
    validates :or_number, :saving_id, :employee_id, :account_number, :cash_account_id, :date, presence: true
    # validate :amount_does_not_exceed_balance?

    def create_voucher!
      ActiveRecord::Base.transaction do
        create_deposit_voucher
      end
    end

    def find_voucher
     find_office.vouchers.find_by!(account_number: account_number)
    end

    def find_saving
      find_office.savings.find(saving_id)
    end
    def find_employee
      User.find(employee_id)
    end
    def find_office
      find_employee.office
    end
    private
    def create_deposit_voucher
      voucher = Voucher.new(
        payee:            find_saving.depositor,
        office:           find_employee.office,
        cooperative:      find_employee.cooperative,
        preparer:         find_employee,
        description:      "Savings withdrawal.",
        reference_number: or_number,
        account_number:   account_number,
        date:             date)
      voucher.voucher_amounts.debit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     debit_account,
        amount:      amount)
      voucher.voucher_amounts.credit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     credit_account,
        amount:      amount)
      voucher.save!
    end


    def credit_account
      find_employee.cash_accounts.find(cash_account_id)
    end

    def debit_account
      find_saving.liability_account
    end

    def amount_does_not_exceed_balance?
      errors[:amount] << "Exceeded available balance" if amount.to_f > find_saving.balance
    end
  end
end
