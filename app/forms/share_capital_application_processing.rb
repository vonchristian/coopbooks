class ShareCapitalApplicationProcessing
  include ActiveModel::Model
  attr_accessor :subscriber_id, :subscriber_type, :share_capital_product_id,
  :date_opened, :amount, :reference_number, :description, :employee_id, :account_number,
  :cash_account_id, :voucher_account_number
  validates :share_capital_product_id, :subscriber_id, :subscriber_type,
            :date_opened, :amount, :reference_number, :description, :employee_id,
            :account_number, :cash_account_id, :voucher_account_number,  presence: true
  validates :amount, numericality: true

  def process!
    ActiveRecord::Base.transaction do
      create_equity_account
      create_share_capital_application
    end
  end

  def find_share_capital_application
    find_office.share_capital_applications.find_by(account_number: account_number)
  end

  def find_voucher
    find_office.vouchers.find_by!(account_number: voucher_account_number)
  end

  private
  def create_equity_account
    find_office.accounts.equities.create!(
      account_number: account_number,
      name:           "#{find_share_capital_product.name} - (#{find_subscriber.full_name} - #{account_number})",
      code:           account_number,
      account_category: find_equity_account_category
    )
  end

  def create_share_capital_application
    share_capital_application = find_office.share_capital_applications.create!(
      share_capital_product: find_share_capital_product,
      subscriber:            find_subscriber,
      date_opened:           date_opened,
      account_number:        account_number,
      initial_capital:       amount,
      cooperative:           find_employee.cooperative,
      office:                find_office,
      equity_account:        find_equity_account)

    create_voucher(share_capital_application)
  end

  def create_voucher(share_capital_application)
    voucher = find_office.vouchers.new(
    account_number:   voucher_account_number,
    payee:            find_subscriber,
    preparer:         find_employee,
    cooperative:      find_employee.cooperative,
    description:      description,
    reference_number: reference_number,
    date:             date_opened)

    voucher.voucher_amounts.debit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     cash_account,
      amount:      amount)

    voucher.voucher_amounts.credit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     credit_account,
      amount:      amount)

    voucher.save!
  end

  def credit_account
    find_office.accounts.equities.find_by!(account_number: account_number)
  end

  def cash_account
    find_employee.cash_accounts.find(cash_account_id)
  end

  def find_employee
    User.find(employee_id)
  end

  def find_office
    find_employee.office
  end

  def find_subscriber
    subscriber_type.constantize.find(subscriber_id)
  end

  def find_share_capital_product
    find_office.share_capital_products.find(share_capital_product_id)
  end
  def find_equity_account
    find_office.accounts.equities.find_by!(account_number: account_number)
  end
  def find_equity_account_category
    find_office.office_share_capital_products.where(share_capital_product: find_share_capital_product).last.equity_account_category
  end
end
