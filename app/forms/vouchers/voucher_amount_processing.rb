module Vouchers
  class VoucherAmountProcessing
    include ActiveModel::Model
    attr_accessor :amount, :account_id, :description, :amount_type, :employee_id, :cash_account_id, :amount_type, :employee_id, :cart_id
    validates :amount, :account_id, :cart_id, presence: true
    validates :amount, numericality: true
    def save
      ActiveRecord::Base.transaction do
        create_voucher_amount
      end
    end
    private
    def create_voucher_amount
      find_cart.voucher_amounts.create!(
        cooperative: find_employee.cooperative,
        recorder_id: employee_id,
        amount:      amount,
        account_id:  account_id,
        amount_type: set_amount_type(amount_type),
        description: description
        )
    end
    def create_cash_account_line_item
      voucher_amounts = find_cart.voucher_amounts.where(account: cash_account)
      if voucher_amounts.present?
        voucher_amounts.destroy_all
        find_cart.voucher_amounts.create!(
          recorder_id: employee_id,
          amount:      find_cart.voucher_amounts.sum(&:amount),
          account:     cash_account,
          amount_type: amount_type_contra,
          description: description,
          cooperative: find_employee.cooperative

          )
      else
        find_cart.voucher_amounts.create!(
          amount:      amount,
          account:     cash_account,
          amount_type: amount_type_contra,
          description: description,
          cooperative: find_employee.cooperative,
          recorder_id: employee_id,

          )
      end
    end

    def find_employee
      User.find(employee_id)
    end

    def cash_account
      find_employee.cash_accounts.find(cash_account_id)
    end
    def find_cart
      Cart.find(cart_id)
    end

    def amount_type_contra
      if amount_type == 'credit'
        'debit'
      elsif amount_type == 'debit'
        'credit'
      end
    end

    def find_account
      find_employee.cooperative.accounts.find(account_id)
    end

    def set_amount_type(amount_type)
      if find_account.is_a?(AccountingModule::Revenue)
        'credit'
      else
        amount_type
      end
    end
  end
end
