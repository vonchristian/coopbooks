module Vouchers
  class EntryProcessing
    include ActiveModel::Model
    attr_reader :voucher, :employee, :updateable, :cooperative
    def initialize(args)
      @voucher     = args[:voucher]
      @employee    = args[:employee]
      @cooperative = @employee.cooperative
      @updateable  = args[:updateable]
    end
    def process!
      ActiveRecord::Base.transaction do
        create_entry
      end
    end

    private
    def create_entry
      entry = AccountingModule::Entry.new(
        office:              voucher.office,
        cooperative:         cooperative,
        commercial_document: voucher.payee,
        description:         voucher.description,
        recorder:            voucher.preparer,
        reference_number:    voucher.reference_number,
        entry_date:          voucher.date
      )

      voucher.voucher_amounts.debit.each do |amount|
        entry.debit_amounts.build(
          account:             amount.account,
          amount:              amount.amount
        )
      end

      voucher.voucher_amounts.credit.each do |amount|
        entry.credit_amounts.build(
          account:             amount.account,
          amount:              amount.amount
        )
      end

      entry.save!
      voucher.update!(entry: entry, disburser: employee)
      add_entry_to_member_entries(entry)
    end

    def find_recent_entry
      cooperative.entries.recent
    end

    def add_entry_to_member_entries(entry)
      voucher.payee.entries << entry
    end
  end
end
