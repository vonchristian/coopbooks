module Vouchers
  class VoucherProcessing
    include ActiveModel::Model
    attr_accessor :date, :reference_number, :description, :payee_id, :employee_id, :account_number, :cash_account_id, :cart_id
    validates :date, :description, :payee_id, :reference_number,  presence: true

    def process!
      ActiveRecord::Base.transaction do
        create_voucher
        remove_cart_reference
      end
    end

    def find_voucher
      Voucher.find_by(account_number: account_number)
    end

    private
    def create_voucher
       voucher = Voucher.create!(
        payee:            find_payee,
        office:           find_employee.office,
        cooperative:      find_employee.cooperative,
        preparer:         find_employee,
        description:      description,
        reference_number: reference_number,
        account_number:   account_number,
        date:             date
      )
      voucher.voucher_amounts << find_cart.voucher_amounts

    end
    def find_employee
      User.find(employee_id)
    end
    def find_payee
      Payee.find_by_id(payee_id)
    end

    def find_cart
      Cart.find(cart_id)
    end

    def remove_cart_reference
      find_cart.voucher_amounts.each do |voucher_amount|
        voucher_amount.cart_id = nil
        voucher_amount.save
      end
    end
  end
end
