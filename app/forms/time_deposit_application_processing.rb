class TimeDepositApplicationProcessing
  include ActiveModel::Model
  attr_accessor :time_deposit_product_id, :depositor_id, :depositor_type,
  :cash_account_id, :reference_number, :date, :amount, :description, :term,
  :employee_id, :voucher_account_number, :account_number, :beneficiaries

  validates :reference_number,
            :time_deposit_product_id,
            :amount,
            :date,
            :description,
            :term,
            :cash_account_id,
            :depositor_type,
            :depositor_id,
            :account_number,
            :voucher_account_number,
            :employee_id,
            presence: true
  validates :term, numericality: { integer: true }
  validates :amount, numericality: true

  def process!
    ActiveRecord::Base.transaction do
      create_liability_account
      create_time_deposit_application
    end
  end

  def find_voucher
    find_office.vouchers.find_by!(account_number: voucher_account_number)
  end
  def find_time_deposit_application
    find_office.time_deposit_applications.find_by!(account_number: account_number)
  end

  private
  def create_liability_account
    find_office.accounts.liabilities.create!(
      account_number: account_number,
      name: "#{find_time_deposit_product.name} - (#{find_depositor.full_name} - #{account_number})",
      code: account_number,
      account_category: find_liability_account_category
    )
  end

  def create_time_deposit_application
    time_deposit_application = find_office.time_deposit_applications.create!(
      time_deposit_product_id: time_deposit_product_id,
      depositor_id:            depositor_id,
      depositor_type:          depositor_type,
      date_deposited:          date,
      account_number:          account_number,
      amount:                  amount,
      term:                    term,
      cooperative:             find_employee.cooperative,
      beneficiaries:           beneficiaries
    )
    create_voucher(time_deposit_application)
  end

  def create_voucher(time_deposit_application)
    voucher = find_office.vouchers.build(
      account_number: voucher_account_number,
      payee_id:         depositor_id,
      payee_type:       depositor_type,
      preparer:         find_employee,
      cooperative:      find_employee.cooperative,
      description:      description,
      reference_number: reference_number,
      date:             date)

    voucher.voucher_amounts.debit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     cash_account,
      amount:      amount
    )
    voucher.voucher_amounts.credit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     credit_account,
      amount:      amount)

    voucher.save!
  end

  def credit_account
    find_office.accounts.find_by!(account_number: account_number)
  end

  def cash_account
    find_employee.cash_accounts.find(cash_account_id)
  end

  def find_employee
    User.find(employee_id)
  end

  def find_office
    find_employee.office
  end

  def find_time_deposit_product
    find_office.time_deposit_products.find(time_deposit_product_id)
  end

  def find_liability_account_category
    find_office.office_time_deposit_products.where(time_deposit_product: find_time_deposit_product).last.liability_account_category
  end 
  def find_depositor
    depositor_type.constantize.find(depositor_id)
  end

end
