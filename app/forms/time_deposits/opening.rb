module TimeDeposits
  class Opening
    include ActiveModel::Model
    attr_reader :voucher, :time_deposit_application, :employee, :office
    def initialize(args)
      @voucher = args[:voucher]
      @time_deposit_application = args[:time_deposit_application]
      @employee = args[:employee]
      @office    = @employee.office
    end

    def process!
      ActiveRecord::Base.transaction do
        create_time_deposit
      end
    end

    private
    def create_time_deposit
      term = Term.create!(
        account_number:   SecureRandom.uuid,
        term:             time_deposit_application.term,
        effectivity_date: voucher.date,
        maturity_date:    (voucher.date.to_date + (time_deposit_application.term.to_i.months))
      )

      time_deposit = office.time_deposits.build(
        term:                 term,
        depositor_name:       find_depositor.name,
        cooperative:          employee.cooperative,
        office:               employee.office,
        depositor:            find_depositor,
        account_number:       time_deposit_application.account_number,
        date_deposited:       time_deposit_application.date_deposited,
        time_deposit_product: time_deposit_application.time_deposit_product,
        certificate_number:   set_certificate_number)
        create_accounts(time_deposit)
        time_deposit.save!

      time_deposit.update(term: term)
    end

    def create_accounts(time_deposit)
      AccountCreators::TimeDeposit.new(time_deposit: time_deposit).create_accounts!
    end

    def find_depositor
      time_deposit_application.depositor
    end

    def entry_date
      voucher.accounting_entry.entry_date
    end

    def set_certificate_number
      date_deposited = time_deposit_application.date_deposited
      datestamp = date_deposited.strftime("%Y")
      recent_annual_time_deposits = employee.cooperative.time_deposits.where(date_deposited: Date.today.beginning_of_year..Date.today.end_of_year)
      Date.today.strftime("%Y").to_s << ("-" + (recent_annual_time_deposits.count+1).to_s)
    end
  end
end
