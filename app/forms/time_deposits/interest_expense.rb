module TimeDeposits
  class InterestExpense
    include ActiveModel::Model
    attr_accessor :date, :reference_number, :account_number, :description, :amount, :time_deposit_id, :employee_id

    validates :date, :reference_number, :account_number, :description, :amount, :time_deposit_id, :employee_id, presence: true
    validates :amount, numericality: true
    def find_voucher
      find_employee.office.vouchers.find_by(account_number: account_number)
    end

    def process!
      if valid?
        ActiveRecord::Base.transaction do
          create_voucher
        end
      end
    end

    private
    def create_voucher
        voucher = find_employee.office.vouchers.build(
          date:             date,
          account_number:   account_number,
          cooperative:      find_employee.cooperative,
          payee:            find_time_deposit.depositor,
          reference_number: reference_number,
          description:      description,
          preparer:         find_employee)

        voucher.voucher_amounts.credit.build(
            description: description,
            voucher:     voucher,
            amount:      amount,
            account:     find_time_deposit.liability_account,
            recorder:    find_employee,
            cooperative: find_employee.cooperative)

            voucher.voucher_amounts.debit.build(
                description: description,
                voucher:     voucher,
                amount:      amount,
                account:     find_time_deposit.interest_expense_account,
                recorder:    find_employee,
                cooperative: find_employee.cooperative)
          voucher.save!
        end
        def find_employee
          User.find(employee_id)
        end
        def find_time_deposit
          find_employee.office.time_deposits.find(time_deposit_id)
        end
    end
  end
