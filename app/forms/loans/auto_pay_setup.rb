module Loans
  class AutoPaySetup
    include ActiveModel::Model
    attr_accessor :loan_id, :savings_account_id, :office_id

    validates :loan_id, :savings_account_id, presence: true

    def process!
      if valid?
        ActiveRecord::Base.transaction do
          create_loan_auto_pay
        end
      end
    end

    private
    def create_loan_auto_pay
      find_loan.create_loan_auto_pay(active: true, savings_account_id: savings_account_id)
    end

    def find_loan
      find_office.loans.find(loan_id)
    end

    def find_office
      Cooperatives::Office.find(office_id)
    end
  end
end
