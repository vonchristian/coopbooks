module Members
  class UpdateInfo
    include ActiveModel::Model
    attr_accessor :first_name, :middle_name, :last_name, :date_of_birth, :sex, :civil_status,
    :complete_address, :street_id, :barangay_id, :member_id,
    :contact_number, :cooperative_id, :avatar,
    :membership_date, :membership_category_id

    validates :first_name, :last_name, :middle_name, :sex, :civil_status, :date_of_birth,
    :contact_number, :complete_address, :barangay_id,
    :membership_date, :membership_category_id, presence: true

    def process!
      if valid?
        ActiveRecord::Base.transaction do
          update_personal_info
          update_contact_number
          update_address
          update_membership_info
        end
      end
    end

    private
    def update_personal_info
      find_member.update!(
        avatar: avatar,
      first_name:    first_name,
      middle_name:   middle_name,
      last_name:     last_name,
      date_of_birth: date_of_birth,
      sex:           sex,
      civil_status:  civil_status)
    end

    def update_contact_number
    end

    def update_address
      find_member.current_address.update!(
        barangay:         find_barangay,
        municipality:     find_barangay.municipality,
        province:         find_barangay.province,
        complete_address: complete_address
      )
    end

    def update_membership_info
      if find_membership.present?
        find_membership.update(
          membership_category_id: membership_category_id,
          membership_date: membership_date
        )
      end
    end

    def find_member
      Member.find(member_id)
    end
    def find_cooperative
      Cooperative.find(cooperative_id)
    end
    def find_barangay
      Addresses::Barangay.find(barangay_id)
    end
    def find_membership
      find_member.memberships.where(cooperative: find_cooperative).last
    end

  end
end
