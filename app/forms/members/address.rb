module Members
  class Address
    include ActiveModel::Model
    attr_accessor :complete_address, :street_id, :barangay_id, :municipality_id, :province_id, :current, :member_id
    validates :complete_address, :barangay_id, :municipality_id, :province_id, :member_id, presence: true

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_address
          update_member_accounts
        end
      end
    end

    private

    def create_address
      find_member.addresses.create!(
      complete_address: complete_address,
      street_id:        street_id,
      barangay_id:      barangay_id,
      municipality_id:  municipality_id,
      province_id:      province_id)
    end

    def find_member
      Member.find(member_id)
    end

    def update_member_accounts
      update_loans
      update_savings
      update_share_capitals
      update_time_deposits
    end

    def find_barangay
      Addresses::Barangay.find(barangay_id)
    end

    def update_loans
      find_member.loans.each do |account|
        find_barangay.barangay_scopes.find_or_create_by(account: account)
      end
    end

    def update_savings
      find_member.savings.each do |account|
        find_barangay.barangay_scopes.find_or_create_by(account: account)
      end
    end

    def update_share_capitals
      find_member.share_capitals.each do |account|
        find_barangay.barangay_scopes.find_or_create_by(account: account)
      end
    end

    def update_time_deposits
      find_member.time_deposits.each do |account|
        find_barangay.barangay_scopes.find_or_create_by(account: account)
      end
    end
  end
end
