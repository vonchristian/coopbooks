module Organizations
  class LoanPaymentLineItemProcessing
    include ActiveModel::Model
    attr_accessor :principal_amount, :interest_amount, :penalty_amount, :loan_id, :employee_id, :cooperative_id, :organization_id
    validates :principal_amount, :interest_amount, :penalty_amount, numericality: { greater_than_or_equal_to: 0 }
    def process!
      create_principal_amount
      create_interest_amount
      create_penalty_amount
    end
    def create_principal_amount
      if principal_amount.to_f > 0
        find_employee.voucher_amounts.credit.create!(
          amount: principal_amount.to_f,
          account: find_loan.principal_account,
          commercial_document: find_loan,
          description: 'Loan principal payment'
        )
      end
    end

    def create_interest_amount
      if interest_amount.to_f > 0
        find_employee.voucher_amounts.credit.create!(
          amount: interest_amount.to_f,
          account: find_loan.loan_product_interest_revenue_account,
          commercial_document: find_loan,
          description: 'Loan interest payment'
        )
      end
    end

    def create_penalty_amount
      if penalty_amount.to_f > 0
        find_employee.voucher_amounts.credit.create!(
          amount: penalty_amount.to_f,
          account: find_loan.loan_product_penalty_revenue_account,
          commercial_document: find_loan,
          description: 'Loan penalty payment'
        )
      end
    end


    def find_employee
      find_cooperative.employees.find(employee_id)
    end

    def find_cooperative
      Cooperative.find(cooperative_id)
    end
    def find_loan
      find_organization.member_loans.find(loan_id)
    end
    def find_organization
      find_cooperative.organizations.find(organization_id)
    end
  end
end
