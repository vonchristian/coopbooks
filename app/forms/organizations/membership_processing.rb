module Organizations
	class MembershipProcessing
		include ActiveModel::Model
	  attr_accessor :organization_membership_id,
	  :organization_membership_type,
	  :organization_id,
	  :office_id

	  def process!
	  	if valid?
		  	ActiveRecord::Base.transaction do
		      add_member
		      add_member_accounts_to_organization
		    end
		  end
	  end

	  def find_organization
	  	find_office.organizations.find(organization_id)
	  end


	  private
	  def add_member
	  	if find_member.organization_memberships.present?
	  		find_member.organization_memberships.destroy_all
	  	end
	  	find_organization.organization_memberships.create!(
	  		cooperator_id:   organization_membership_id,
	  		cooperator_type: organization_membership_type
	  	)
	  end

	  def find_member
	  	find_office.member_memberships.find(organization_membership_id)
	  end

    def find_office
      Cooperatives::Office.find(office_id)
    end

	  def add_member_accounts_to_organization
	  	find_organization.member_savings        << find_member.savings
	  	find_organization.member_share_capitals << find_member.share_capitals
	  	find_organization.member_loans          << find_member.loans
	  	find_organization.member_time_deposits  << find_member.time_deposits
	  end
	end
end
