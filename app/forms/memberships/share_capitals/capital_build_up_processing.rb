module Memberships
  module ShareCapitals
    class CapitalBuildUpProcessing
      include ActiveModel::Model
      attr_accessor :or_number, :amount, :date, :description, :share_capital_id, :employee_id, :cash_account_id, :account_number
      validates :amount, presence: true, numericality: { greater_than: 0.01 }
      validates :or_number, presence: true

      def save
        ActiveRecord::Base.transaction do
          create_deposit_voucher
        end
      end

      def find_voucher
        find_employee.office.vouchers.find_by(account_number: account_number)
      end

      def find_share_capital
        find_employee.office.share_capitals.find(share_capital_id)
      end


      private
      def create_deposit_voucher
        voucher = Voucher.new(
          payee:            find_share_capital.subscriber,
          office:           find_employee.office,
          cooperative:      find_employee.cooperative,
          preparer:         find_employee,
          description:      description,
          reference_number: or_number,
          account_number:   account_number,
          date:             date)

        voucher.voucher_amounts.debit.build(
          cooperative: find_employee.cooperative,
          recorder:    find_employee,
          account:     cash_account,
          amount:      amount)
          
        voucher.voucher_amounts.credit.build(
          cooperative: find_employee.cooperative,
          recorder:    find_employee,
          account:     credit_account,
          amount:      amount)
        voucher.save!
      end

      def cash_account
        find_employee.cash_accounts.find(cash_account_id)
      end

      def credit_account
        find_share_capital.equity_account
      end

      def find_subscriber
        find_share_capital.subscriber
      end

      def find_employee
        User.find(employee_id)
      end

    end
  end
end
