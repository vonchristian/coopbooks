class CashCountProcessing
  include ActiveModel::Model
  attr_accessor :bill_1000_count,
                :bill_500_count,
                :bill_200_count,
                :bill_100_count,
                :bill_50_count,
                :bill_20_count,
                :bill_10_count,
                :bill_5_count,
                :bill_1_count,
                :bill_1_cent_count,
                :checks_count,
                :bill_1000_id,
                :bill_500_id,
                :bill_200_id,
                :bill_100_id,
                :bill_50_id,
                :bill_20_id,
                :bill_10_id,
                :bill_5_id,
                :bill_1_id,
                :bill_1_id,
                :bill_1_cent_id,
                :checks_id,
                :bill_1000_total,
                :bill_500_total,
                :bill_200_total,
                :bill_100_total,
                :bill_50_total,
                :bill_20_total,
                :bill_10_total,
                :bill_5_total,
                :bill_1_total,
                :bill_1_cent_total,
                :checks_total,
                :total_count,
                :cash_account_balance,
                :difference,
                :date,
                :employee_id,
                :cash_balance

  validates :bill_1000_count, presence: true


  def process!
    create_cash_count
  end

  def create_cash_count
    report      = CashCounts::CashCountReport.create!(employee_id: employee_id, date: date, cash_balance: cash_balance)
    bill_1000   = CashCounts::Bill.find(bill_1000_id)
    bill_500    = CashCounts::Bill.find(bill_500_id)
    bill_200    = CashCounts::Bill.find(bill_200_id)
    bill_100    = CashCounts::Bill.find(bill_100_id)
    bill_50     = CashCounts::Bill.find(bill_50_id)
    bill_20     = CashCounts::Bill.find(bill_20_id)
    bill_10     = CashCounts::Bill.find(bill_10_id)
    bill_5      = CashCounts::Bill.find(bill_5_id)
    bill_1      = CashCounts::Bill.find(bill_1_id)
    bill_1_cent = CashCounts::Bill.find(bill_1_cent_id)
    checks      = CashCounts::Bill.find(checks_id)



    bill_1000.cash_counts.create!(cash_count_report: report, quantity: bill_1000_count)
    bill_500.cash_counts.create!(cash_count_report: report, quantity: bill_500_count)
    bill_200.cash_counts.create!(cash_count_report: report, quantity: bill_200_count)
    bill_100.cash_counts.create!(cash_count_report: report, quantity: bill_100_count)
    bill_50.cash_counts.create!(cash_count_report: report, quantity: bill_50_count)
    bill_20.cash_counts.create!(cash_count_report: report, quantity: bill_20_count)
    bill_10.cash_counts.create!(cash_count_report: report, quantity: bill_10_count)
    bill_5.cash_counts.create!(cash_count_report: report, quantity: bill_5_count)
    bill_1.cash_counts.create!(cash_count_report: report, quantity: bill_1_count)
    bill_1_cent.cash_counts.create!(cash_count_report: report, quantity: bill_1_cent_count)
    checks.cash_counts.create!(cash_count_report: report, quantity: checks_count)


  end
end
