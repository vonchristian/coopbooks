module ShareCapitals
  module AdjustingEntries
    class ProgramSubscriptionPayment
      include ActiveModel::Model
      attr_accessor :employee_id, :cart_id, :share_capital_id, :program_subscription_id, :amount
      validates :employee_id, :cart_id, :share_capital_id, :program_subscription_id, :amount, presence: true
      validates :amount, numericality: true

      def create_amounts!
        if valid?
          ActiveRecord::Base.transaction do
            create_program_subscription_amount
          end
        end
      end

      private
      def create_program_subscription_amount
        find_cart.voucher_amounts.credit.create!(
          account:     find_program_subscription.account,
          amount:      amount,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
      end
      def find_cart
        Cart.find(cart_id)
      end
      def find_program_subscription
        MembershipsModule::ProgramSubscription.find(program_subscription_id)
      end
      def find_employee
        User.find(employee_id)
      end
    end
  end
end
