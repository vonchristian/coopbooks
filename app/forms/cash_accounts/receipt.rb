module CashAccounts
  class Receipt
    include ActiveModel::Model
    attr_accessor :amount, :account_id, :description, :amount_type, :employee_id, :cash_account_id, :amount_type, :employee_id, :cart_id
    validates :amount, :cash_account_id,:account_id, :cart_id, :employee_id, presence: true
    validates :amount, numericality: true

    def create_amounts!
      ActiveRecord::Base.transaction do
        create_voucher_amount
        create_cash_amount
      end
    end

    private
    def create_voucher_amount
      find_cart.voucher_amounts.credit.create!(
        cooperative: find_employee.cooperative,
        recorder_id: employee_id,
        amount:      amount,
        account_id:  account_id,
        description: description
        )
    end

    def create_cash_amount
      voucher_amounts = find_cart.voucher_amounts.where(account: cash_account)
      if voucher_amounts.present?
        voucher_amounts.destroy_all
        find_cart.voucher_amounts.debit.create!(
          recorder_id: employee_id,
          amount:      find_cart.voucher_amounts.sum(&:amount),
          account:     cash_account,
          description: description,
          cooperative: find_employee.cooperative

          )
      else
        find_cart.voucher_amounts.debit.create!(
          amount:       amount,
          account:     cash_account,
          description: description,
          cooperative: find_employee.cooperative,
          recorder_id: employee_id,

          )
      end
    end

    def find_cart
      Cart.find(cart_id)
    end

    def find_employee
      User.find(employee_id)
    end

    def cash_account
      find_employee.cash_accounts.find(cash_account_id)
    end
  end
end
