class SavingsAccountApplicationProcessing
  include ActiveModel::Model
  attr_accessor :saving_product_id, :depositor_id, :depositor_type,
  :cash_account_id, :reference_number, :date, :amount, :description,
  :employee_id, :voucher_account_number, :account_number

  validates :reference_number,
            :amount,
            :date,
            :saving_product_id,
            :cash_account_id,
            :account_number,
            :depositor_id,
            :depositor_type,
            :description,
            :employee_id,
            :voucher_account_number,
            presence: true

  def process!
    ActiveRecord::Base.transaction do
      create_liability_account
      create_savings_account_application
    end
  end

  def find_voucher
    find_office.vouchers.find_by!(account_number: voucher_account_number)
  end

  def find_savings_account_application
    find_office.savings_account_applications.find_by!(account_number: account_number)
  end

  private
  def create_liability_account
    account = find_office.accounts.liabilities.create!(
    name:           "#{find_saving_product.name} - (#{find_depositor.name} - #{account_number}",
    code:           SecureRandom.uuid,
    account_number: SecureRandom.uuid,
    account_category: find_liability_account_category)
  end

  def create_savings_account_application
    savings_account_application = find_office.savings_account_applications.create!(
      saving_product_id: saving_product_id,
      depositor_id:      depositor_id,
      depositor_type:    depositor_type,
      date_opened:       date,
      account_number:    account_number,
      initial_deposit:   amount,
      cooperative:       find_employee.cooperative,
      liability_account: find_liability_account
    )
    create_voucher(savings_account_application)
  end
  def create_voucher(savings_account_application)
    voucher = Voucher.new(
      account_number:   voucher_account_number,
      payee_id:         depositor_id,
      payee_type:       depositor_type,
      preparer:         find_employee,
      office:           find_office,
      cooperative:      find_employee.cooperative,
      description:      description,
      reference_number: reference_number,
      date:             date)

    voucher.voucher_amounts.debit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     cash_account,
      amount:      amount)

    voucher.voucher_amounts.credit.build(
      cooperative: find_employee.cooperative,
      recorder:    find_employee,
      account:     find_liability_account,
      amount:      amount)
    voucher.save!
  end

  def find_liability_account
    find_office.accounts.find_by!(name: "#{find_saving_product.name} - (#{find_depositor.name} - #{account_number}")
  end
  def cash_account
    find_employee.cash_accounts.find(cash_account_id)
  end

  def find_depositor
    depositor_type.constantize.find(depositor_id)
  end

  def find_office
    find_employee.office
  end

  def find_employee
    User.find(employee_id)
  end

  def find_saving_product
    find_office.saving_products.find(saving_product_id)
  end
  def find_liability_account_category
    find_office.office_saving_products.where(saving_product: find_saving_product).last.liability_account_category
  end
end
