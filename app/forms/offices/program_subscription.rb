module Offices
  class ProgramSubscription
    include ActiveModel::Model
    attr_accessor :office_id, :program_id, :account_category_id

    validates :office_id, :program_id, :account_category_id, presence: true

    def subscribe!
      if valid?
        ActiveRecord::Base.transaction do
          create_office_program
        end
      end
    end

    private
    def create_office_program
      find_office.office_programs.create!(
        account_category_id: account_category_id,
        program_id: program_id)
    end
    def find_office
      Cooperatives::Office.find(office_id)
    end
  end
end 
