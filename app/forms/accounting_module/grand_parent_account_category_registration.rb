module AccountingModule
  class GrandParentAccountCategoryRegistration
    include ActiveModel::Model

    attr_accessor :title, :code, :type, :contra, :cooperative_id

    validates :title, :code, :type, :cooperative_id, presence: true

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_category
        end
      end
    end

    private
    def create_category
      find_cooperative.grand_parent_account_categories.create!(
        title: title,
        code: code,
        contra: contra,
        type: normalized_type
      )
    end

    def find_cooperative
      Cooperative.find(cooperative_id)
    end

    def normalized_type
      "AccountingModule::GrandParentAccountCategories::#{type}"
    end
  end
end
