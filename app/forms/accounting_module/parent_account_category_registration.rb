module AccountingModule
  class ParentAccountCategoryRegistration
    include ActiveModel::Model

    attr_accessor :office_id, :grand_parent_account_category_id, :title, :code, :contra, :type
    validates     :office_id, :grand_parent_account_category_id, :title, :code, :type, presence: true

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_category
        end
      end
    end

    private
    def create_category
      find_office.parent_account_categories.create!(
        title: title,
        code: code,
        grand_parent_account_category_id: grand_parent_account_category_id,
        contra: contra,
        type: normalized_type
      )
    end
    def find_office
      Cooperatives::Office.find(office_id)
    end
    def normalized_type
      "AccountingModule::ParentAccountCategories::#{type}"
    end
  end
end 
