module AccountingModule
  module Entries
    class SavingsInterestExpenseEntry
      include ActiveModel::Model

      attr_accessor :date, :employee_id, :description, :reference_number, :account_number

      validates :date, :description, :reference_number, :account_number, :employee_id, presence: true

      def find_voucher
        find_employee.office.vouchers.find_by!(account_number: account_number)
      end

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher
          end 
        end
      end

      def create_voucher
        voucher = Voucher.new(
          account_number:   account_number,
          payee:            find_employee,
          preparer:         find_employee,
          office:           find_employee.office,
          cooperative:      find_employee.cooperative,
          description:      description,
          reference_number: reference_number,
          date:             date
        )
        find_employee.office.savings.has_minimum_balances.each do |saving|
          if !saving.interest_posted?(from_date: from_date_for(date, saving.saving_product), to_date: to_date_for(date, saving.saving_product))
            voucher.voucher_amounts.debit.build(
            recorder:    find_employee,
            cooperative: find_employee.cooperative,
            account:     saving.interest_expense_account,
            amount:      computed_interest(saving))
            voucher.voucher_amounts.credit.build(
            recorder:    find_employee,
            cooperative: find_employee.cooperative,
            account:     saving.liability_account,
            amount:      computed_interest(saving))
          end
        end
        voucher.save!
      end
      def find_employee
        User.find(employee_id)
      end
      def find_cooperative
        find_employee.cooperative
      end
      def computed_interest(saving)
        Savings::InterestCalculator.new(amount: saving.averaged_balance(to_date: date), rate: saving.saving_product.applicable_rate).calculate
      end
      def from_date_for(date, saving_product)
        saving_product.date_setter.new(saving_product: saving_product, date: date.to_date).start_date
      end
      def to_date_for(date, saving_product)
        saving_product.date_setter.new(saving_product: saving_product, date: date.to_date).end_date
      end
    end
  end
end
