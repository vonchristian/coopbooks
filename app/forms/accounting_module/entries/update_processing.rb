module AccountingModule
  module Entries
    class UpdateProcessing
      include ActiveModel::Model
      attr_accessor :entry_date, :description, :reference_number, :entry_id
      validates :entry_date, :description, :reference_number, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            update_entry!
            update_loan!
          end
        end
      end

      private
      def update_entry!
        find_entry.update!(
          entry_date:       entry_date,
          description:      description,
          reference_number: reference_number
        )
        if find_entry.voucher.present?
          find_entry.voucher.update(
            date:             entry_date,
            description:      description,
            reference_number: reference_number
          )
        end
      end

      def find_entry
        AccountingModule::Entry.find(entry_id)
      end
      def update_loan!
        if find_loan.present?
          find_loan.update!(disbursement_date: entry_date)
        end
      end

      def find_loan
        voucher = find_entry.voucher
        if voucher.present?
          LoansModule::Loan.where(voucher_id: voucher.id).last
        end
      end

    end
  end
end
