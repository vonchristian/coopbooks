module AccountingModule
  class AccountCategoryRegistration
    include ActiveModel::Model
    attr_accessor :title, :type, :code, :office_id, :parent_account_sub_category_id

    validates :title, :type, :code, :office_id, :parent_account_sub_category_id, presence: true
    validate :unique_code?, :unique_title?

    def find_category
      find_office.account_categories.find_by!(code: code)
    end

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_category
        end
      end
    end

    private

    def create_category
      find_office.account_categories.create!(
        title: title,
        type: normalized_type,
        parent_account_sub_category_id: parent_account_sub_category_id,
        code: code)
    end

    def normalized_type
      "AccountingModule::AccountCategories::#{type}"
    end

    def find_office
      Cooperatives::Office.find(office_id)
    end

    private
    def unique_code?
      errors[:code] << "Code has already been taken" if find_office.account_categories.where(code: code).present?
    end

    def unique_title?
      errors[:title] << "Title has already been taken" if find_office.account_categories.where(title: title).present?
    end
  end
end
