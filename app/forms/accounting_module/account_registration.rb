module AccountingModule
  class AccountRegistration
    include ActiveModel::Model
    attr_accessor :name, :code, :type, :contra, :account_category_id, :office_id

    validates :name, :code, :type, :account_category_id, presence: true

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_account
        end
      end
    end

    private

    def create_account
      account = find_office.accounts.create!(
        name:                name,
        code:                code,
        contra:              contra,
        type:                normalized_type,
        account_number:      SecureRandom.uuid,
        account_category_id: account_category_id)
    end




    def find_office
      Cooperatives::Office.find(office_id)
    end

    def normalized_type
      "AccountingModule::#{type}"
    end
  end
end
