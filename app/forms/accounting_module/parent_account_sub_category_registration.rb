module AccountingModule
  class ParentAccountSubCategoryRegistration
    include ActiveModel::Model

    attr_accessor :office_id, :parent_account_category_id, :title, :code, :contra, :type
    validates     :office_id, :parent_account_category_id, :title, :code, :type, presence: true

    def register!
      if valid?
        ActiveRecord::Base.transaction do
          create_category
        end
      end
    end

    private
    def create_category
      find_office.parent_account_sub_categories.create!(
        title: title,
        code: code,
        parent_account_category_id: parent_account_category_id,
        contra: contra,
        type: normalized_type
      )
    end
    def find_office
      Cooperatives::Office.find(office_id)
    end
    def normalized_type
      "AccountingModule::ParentAccountSubCategories::#{type}"
    end
  end
end
