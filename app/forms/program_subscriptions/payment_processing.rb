
module ProgramSubscriptions
  class PaymentProcessing
    include ActiveModel::Model
    attr_accessor :program_subscription_id, :cash_account_id, :employee_id, :amount, :reference_number, :account_number, :date, :description, :member_id
    validates :amount, presence: true, numericality: true
    validates :reference_number, :description, :date, :cash_account_id, presence: true
    
    def save
      ActiveRecord::Base.transaction do
        create_voucher
      end
    end
    def find_voucher
      find_office.vouchers.find_by!(account_number: account_number)
    end
    private
    def create_voucher
      voucher = find_office.vouchers.build(
        payee: find_member,
        office: find_employee.office,
        cooperative: find_employee.cooperative,
        preparer: find_employee,
        description: description,
        reference_number: reference_number,
        account_number: account_number,
        date: date)

      voucher.voucher_amounts.debit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     debit_account,
        amount:      amount)

      voucher.voucher_amounts.credit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     credit_account,
        amount:      amount)
      voucher.save!
    end

    def debit_account
     find_employee.cash_accounts.find(cash_account_id)
    end

    def credit_account
      find_program_subscription.account
    end

    def find_program_subscription
      find_office.program_subscriptions.find(program_subscription_id)
    end
    def find_member
      Member.find(member_id)
    end

    def find_employee
      User.find(employee_id)
    end
    def find_office
      find_employee.office
    end
  end
end
