module BankAccounts
  class Withdraw
    include ActiveModel::Model
    attr_accessor :bank_account_id,
                  :employee_id,
                  :amount,
                  :description,
                  :reference_number,
                  :account_number,
                  :date,
                  :cash_account_id

    validates :amount, numericality: { greater_than: 0.01 }
    validates :reference_number,
              :bank_account_id,
              :employee_id,
              :amount,
              :description,
              :account_number,
              :date,
              :cash_account_id,
              presence: true


    def create_voucher!
      if valid?
        ActiveRecord::Base.transaction do
          create_withdraw_voucher
        end
      end
    end

    def find_voucher
      find_office.vouchers.find_by!(account_number: account_number)
    end

    def find_bank_account
      find_office.bank_accounts.find(bank_account_id)
    end

    def find_employee
      User.find(employee_id)
    end

    private
    def create_withdraw_voucher
      voucher = find_office.vouchers.build(
        payee:            find_employee,
        cooperative:      find_employee.cooperative,
        preparer:         find_employee,
        description:      description,
        reference_number: reference_number,
        account_number:   account_number,
        date:             date)
      voucher.voucher_amounts.debit.build(
        recorder:    find_employee,
        cooperative: find_employee.cooperative,
        account:     debit_account,
        amount:      amount)
      voucher.voucher_amounts.credit.build(
        recorder:    find_employee,
        cooperative: find_employee.cooperative,
        account:     credit_account,
        amount:      amount)
      voucher.save!
    end

    def debit_account
      find_employee.cash_accounts.find(cash_account_id)
    end

    def credit_account
      find_bank_account.cash_account
    end

    def find_office
      find_employee.office
    end

    def ensure_not_exceed_balance
      errors[:amount] << "must be less than or equal to balance" if amount > find_bank_account.balance
    end

  end
end
