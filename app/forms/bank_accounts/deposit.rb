module BankAccounts
  class Deposit
    include ActiveModel::Model
    attr_accessor :bank_account_id,
                  :employee_id,
                  :amount,
                  :description,
                  :reference_number,
                  :account_number,
                  :date,
                  :cash_account_id,
                  :account_number

    validates     :amount,
                  numericality: { greater_than: 0.01 }

    validates     :reference_number,
                  :amount,
                  :bank_account_id,
                  :employee_id,
                  :description,
                  :date,
                  :cash_account_id,
                  :account_number,
                  presence: true

    def create_voucher!
      if valid?
        ActiveRecord::Base.transaction do
          create_deposit_voucher
        end
      end
    end

    def find_voucher
      Voucher.find_by!(account_number: account_number)
    end

    def find_bank_account
      find_office.bank_accounts.find(bank_account_id)
    end

    private
    def create_deposit_voucher
      voucher = Voucher.new(
        payee:            find_employee,
        office:           find_employee.office,
        cooperative:      find_employee.cooperative,
        preparer:         find_employee,
        description:      description,
        reference_number: reference_number,
        account_number:   account_number,
        date:             date)

      voucher.voucher_amounts.debit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     debit_account,
        amount:      amount)

      voucher.voucher_amounts.credit.build(
        cooperative: find_employee.cooperative,
        recorder:    find_employee,
        account:     credit_account,
        amount:      amount)
      voucher.save!
    end

    def credit_account
      find_employee.cash_accounts.find(cash_account_id)
    end

    def debit_account
      find_bank_account.cash_account
    end

    def find_office
      find_employee.office
    end

    def find_employee
      User.find(employee_id)
    end
  end
end
