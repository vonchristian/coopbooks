class MembershipApplication
  include ActiveModel::Model
  attr_accessor :first_name,
                :middle_name,
                :last_name,
                :date_of_birth,
                :account_number,
                :civil_status,
                :sex,
                :contact_number,
                :email,
                :office_id,
                :cooperative_id,
                :membership_date,
                :complete_address,
                :barangay_id,
                :membership_category_id,
                :occupation_ids

  validates     :first_name,
                :middle_name,
                :last_name,
                :sex,
                :civil_status,
                :date_of_birth,
                :cooperative_id,
                :office_id,
                :contact_number,
                :complete_address,
                :barangay_id,
                :membership_date,
                :membership_category_id,
                :account_number,
                presence: true

  validate      :unique_full_name

  def register!
    if valid?
      ActiveRecord::Base.transaction do
        create_member
        create_membership
        create_contact
        create_address
        create_occupations
      end
    end
  end

  def find_member
    Member.find_by!(account_number: account_number)
  end

  private
  def create_member
    member = Member.create!(
    account_number: account_number,
    first_name:     Propercaser.new(text: first_name).propercase,
    middle_name:    Propercaser.new(text: middle_name).propercase,
    last_name:      Propercaser.new(text: last_name).propercase,
    civil_status:   civil_status,
    sex:            sex,
    date_of_birth:  date_of_birth,
    contact_number: contact_number,
    email:          email,
    last_transaction_date: Date.current)
    member.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'default.png')), filename: 'default.png')
  end

  def create_membership
    membership = Cooperatives::Membership.create!(
      cooperator:          find_member,
      cooperative:         find_cooperative,
      account_number:      SecureRandom.uuid,
      membership_category: find_membership_category,
      membership_date:       membership_date
    )
    Offices::OfficeMembership.create!(office: find_office, membership: membership)
  end

  def find_membership_category
    find_cooperative.membership_categories.find(membership_category_id)
  end
  def create_contact
    find_member.contacts.create!(number: contact_number)
  end

  def create_address
    find_member.addresses.create!(
      complete_address: complete_address,
      barangay:      find_barangay,
      municipality:  find_barangay.municipality,
      province:      find_barangay.municipality.province
    )
  end

  def create_occupations
    if occupation_ids.present?
      occupation_ids.each do |occupation_id|
        occupation = Occupation.find_by(id: occupation_id)
        find_member.member_occupations.find_or_create_by(occupation: occupation)
      end
    end
  end

  def find_cooperative
    Cooperative.find(cooperative_id)
  end

  def find_office
    find_cooperative.offices.find(office_id)
  end

  def find_barangay
    Addresses::Barangay.find(barangay_id)
  end

  def unique_full_name
    errors[:last_name]   << "Member already registered" if Member.find_by(first_name: first_name, middle_name: middle_name, last_name: last_name).present?
    errors[:first_name]  << "Memberalready registered" if  Member.find_by(first_name: first_name, middle_name: middle_name, last_name: last_name).present?
    errors[:middle_name] << "Member already registered" if Member.find_by(first_name: first_name, middle_name: middle_name, last_name: last_name).present?
  end
end
