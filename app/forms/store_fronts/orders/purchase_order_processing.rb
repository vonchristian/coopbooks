module StoreFronts
  module Orders
    class PurchaseOrderProcessing
      include ActiveModel::Model
      attr_accessor :date, :cart_id, :supplier_id, :voucher_id, :store_front_id, :employee_id, :reference_number

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_purchase
          end
        end
      end

      private
      def create_purchase
        purchase_order = find_supplier.purchase_orders.create!(
          date:           date,
          account_number: SecureRandom.uuid,
          supplier:       find_supplier,
          employee_id:    employee_id,
          store_front_id: store_front_id,
          voucher:        find_voucher)
          purchase_order.line_items << find_cart.purchase_line_items
          find_cart.purchase_line_items.each do |line_item|
            line_item.cart_id = nil
            line_item.save
          end
      end
      def find_employee
        User.find(employee_id)
      end
      def find_voucher
        Voucher.find(voucher_id)
      end
      def find_cart
        Cart.find(cart_id)
      end


      def find_supplier
        StoreFronts::Supplier.find(supplier_id)
      end
    end
  end
end
