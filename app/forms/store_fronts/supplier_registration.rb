module StoreFronts
  class SupplierRegistration
    include ActiveModel::Model
    attr_accessor :business_name, :first_name, :last_name, :contact_number, :address, :store_front_id

    def process!
      if valid?
        ActiveRecord::Base.transaction do
          find_or_create_supplier
        end
      end
    end

    private
    def find_or_create_supplier
    supplier = find_store_front.suppliers.build(
        business_name: business_name,
        first_name: first_name,
        last_name: last_name,
        contact_number: contact_number,
        address: address,
        store_front_id: store_front_id
      )
      create_accounts(supplier)
    end

    def create_accounts(supplier)
      AccountCreators::Supplier.new(supplier: supplier).create_accounts!
    end
    def find_store_front
      StoreFront.find(store_front_id)
    end 
  end
end
