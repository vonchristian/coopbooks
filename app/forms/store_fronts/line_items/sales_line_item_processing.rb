module StoreFronts
  module LineItems
    class SalesLineItemProcessing
     include ActiveModel::Model
      attr_accessor :unit_of_measurement_id,
                    :quantity,
                    :cart_id,
                    :product_id,
                    :unit_cost,
                    :total_cost,
                    :cart_id,
                    :barcode,
                    :employee_id,
                    :store_front_id,
                    :stock_id

      validates :quantity, numericality: { greater_than: 0.1 }
      validate :ensure_quantity_is_less_than_or_equal_to_available_quantity?

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_sales_line_item
          end
        end
      end

      private
      def create_sales_line_item
        find_cart.sales_line_items.create!(
          stock:    find_stock,
          product:  find_stock.product,
          quantity: quantity
        )
      end

      def find_stock
        find_store_front.stocks.find(stock_id)
      end

      def ensure_quantity_is_less_than_or_equal_to_available_quantity?
        errors[:quantity] << "exceeded available quantity" if converted_quantity.to_f > find_stock.balance
      end
    end
  end
end
