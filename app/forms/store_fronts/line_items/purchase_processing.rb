module StoreFronts
  module LineItems
    class PurchaseProcessing
      include ActiveModel::Model
      attr_accessor :quantity, :unit_cost, :total_cost, :product_id, :barcode, :cart_id, :unit_of_measurement_id, :store_front_id
      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_purchase
          end
        end
      end

      private
      def create_purchase
        stock = find_product.stocks.create!(store_front: find_product.store_front)
        purchase = find_cart.purchase_line_items.create!(
          stock:                  stock,
          product_id:             product_id,
          unit_of_measurement_id: unit_of_measurement_id,
          quantity:               quantity,
          unit_cost:              unit_cost,
          total_cost:             total_cost)

        if barcode.present?
          purchase.barcodes.create(code: barcode)
        end
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_product
        StoreFronts::Product.find(product_id)
      end
    end
  end
end
