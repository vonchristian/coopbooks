module SavingsAccounts
  module AdjustingEntries
    class LoanPayment
      include ActiveModel::Model
      attr_accessor :principal_amount, :interest_amount, :penalty_amount, :cart_id, :savings_account_id, :loan_id, :employee_id

      validates :principal_amount, :interest_amount, :penalty_amount,
      :cart_id, :savings_account_id, :loan_id, :employee_id, presence: true

      validates :principal_amount, :interest_amount, :penalty_amount, numericality: true

      def save_payment!
        if valid?
          ActiveRecord::Base.transaction do
            create_principal_amount
            create_interest_amount
            create_penalty_amount
          end
        end
      end

      private
      def create_principal_amount
        if principal_amount.to_f > 0
          amounts = find_cart.voucher_amounts.where(account_id: find_loan.receivable_account_id)
          if amounts.present?
            amounts.destroy_all
          end
          find_cart.voucher_amounts.credit.create!(
          description: "Loan Payment (Principal) - #{find_loan.account_number}",
          amount:      principal_amount.to_f,
          account:     find_loan.receivable_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end
      def create_interest_amount
        if interest_amount.to_f > 0
          amounts = find_cart.voucher_amounts.where(account_id: find_loan.interest_revenue_account_id)
          if amounts.present?
            amounts.destroy_all
          end
          find_cart.voucher_amounts.credit.create!(
          description: "Loan Payment (Interest) - #{find_loan.account_number}",
          amount:      interest_amount.to_f,
          account:     find_loan.interest_revenue_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end
      def create_penalty_amount
        if penalty_amount.to_f > 0
          amounts = find_cart.voucher_amounts.where(account_id: find_loan.penalty_revenue_account_id)
          if amounts.present?
            amounts.destroy_all
          end
          find_cart.voucher_amounts.credit.create!(
          description: "Loan Payment (Penalty) - #{find_loan.account_number}",
          amount:      interest_amount.to_f,
          account:     find_loan.penalty_revenue_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_employee
        User.find(employee_id)
      end

      def find_office
        find_employee.office
      end

      def find_loan
        find_office.loans.find(loan_id)
      end

      def find_saving
        find_office.savings.find(savings_account_id)
      end
    end
  end
end
