module SavingsAccounts
  module AdjustingEntries
    class ProgramSubscriptionPayment
      include ActiveModel::Model
      attr_accessor :savings_account_id, :cart_id, :program_subscription_id, :amount, :employee_id

      validates :savings_account_id, :cart_id, :program_subscription_id, :employee_id, :amount, presence: true
      validates :amount, numericality: true

      def create_amounts!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher_amount
          end
        end
      end

      private
      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
          amount:      amount,
          account:     find_program_subscription.account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_employee
        User.find(employee_id)
      end

      def find_office
        find_employee.office
      end

      def find_program_subscription
        find_office.program_subscriptions.find(program_subscription_id)
      end
    end
  end
end
