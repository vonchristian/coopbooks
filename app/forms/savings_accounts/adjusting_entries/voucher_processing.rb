module SavingsAccounts
  module AdjustingEntries
    class VoucherProcessing
      include ActiveModel::Model
      attr_accessor :cart_id, :savings_account_id, :date, :description, :reference_number, :employee_id, :account_number

      validates :date, :reference_number, :description, :cart_id, :savings_account_id, :employee_id, :account_number, presence: true
      def find_voucher
        find_office.vouchers.find_by!(account_number: account_number)
      end

      def create_voucher!
        if valid?
          ActiveRecord::Base.transaction do
            create_adjusting_voucher
            remove_cart_reference
          end
        end
      end

      private
      def create_adjusting_voucher
        voucher = find_office.vouchers.build(
          date:             date,
          account_number:   account_number,
          cooperative:      find_employee.cooperative,
          payee:            find_savings_account.depositor,
          reference_number: reference_number,
          description:      description,
          preparer:         find_employee)

        find_cart.voucher_amounts.each do |voucher_amount|
           Vouchers::VoucherAmount.credit.create!(
            description: voucher_amount.description,
            voucher:     voucher,
            amount:      voucher_amount.amount,
            account:     voucher_amount.account,
            recorder:    voucher_amount.recorder,
            cooperative: voucher_amount.cooperative)
        end
        Vouchers::VoucherAmount.debit.create!(
          description: 'savings withdraw',
          voucher:     voucher,
          amount:      find_cart.voucher_amounts.total,
          account:     find_savings_account.liability_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        voucher.save!
      end

      def remove_cart_reference
        find_cart.voucher_amounts.each do |voucher_amount|
          voucher_amount.cart_id = nil
          voucher_amount.save!
        end
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_employee
        User.find(employee_id)
      end
      def find_office
        find_employee.office
      end
      def find_savings_account
        find_office.savings.find(savings_account_id)
      end
    end
  end
end
