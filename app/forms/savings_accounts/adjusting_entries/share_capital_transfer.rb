module SavingsAccounts
  module AdjustingEntries
    class ShareCapitalTransfer
      include ActiveModel::Model
      attr_accessor :cart_id, :employee_id, :savings_account_id, :share_capital_id, :amount

      validates :cart_id, :employee_id, :savings_account_id, :share_capital_id, :amount, presence: true
      validates :amount, numericality: true
      def transfer_amounts!
        if valid?
          ActiveRecord::Base.transaction do
            create_amounts
          end
        end
      end

      private
      def create_amounts
        find_cart.voucher_amounts.credit.create!(
          amount: amount,
          account: find_share_capital.equity_account,
          recorder: find_employee,
          cooperative: find_employee.cooperative)
      end
      def find_cart
        Cart.find(cart_id)
      end
      def find_share_capital
        find_office.share_capitals.find(share_capital_id)
      end
      def find_office
        find_employee.office
      end
      def find_employee
        User.find(employee_id)
      end
    end
  end
end
