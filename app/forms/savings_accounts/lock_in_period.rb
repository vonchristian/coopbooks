module SavingsAccounts
  class LockInPeriod
    include ActiveModel::Model
    attr_accessor :savings_account_id, :effectivity_date, :maturity_date, :term
    def process!
      if valid?
        ActiveRecord::Base.transaction do
          create_lock_in_period
        end
      end
    end

    private
    def create_lock_in_period
      find_savings_account.lock_in_periods.create!(
        effectivity_date: effectivity_date,
        maturity_date:    maturity_date,
        account_number:   SecureRandom.uuid,
        term:             term)
    end
    def find_savings_account
      MembershipsModule::Saving.find(savings_account_id)
    end
  end
end
