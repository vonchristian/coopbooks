module LoansModule
  module LoanApplications
    class VoucherAmountProcessing
      include ActiveModel::Model
      attr_accessor :amount, :account_id, :description, :loan_application_id, :cart_id, :employee_id, :cooperative_id, :amount_type
      validates :amount, :account_id, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher_amount
          end
        end
      end

      private
      def create_voucher_amount
       find_cart.voucher_amounts.create!(
          amount_type:    amount_type,
          cooperative_id: cooperative_id,
          recorder_id:    employee_id,
          amount:         amount,
          account_id:     account_id,
          description:    description
        )
      end
      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end
      def find_cart
        Cart.find(cart_id)
      end
    end
  end
end
