module LoansModule
  module LoanApplications
    class ProgramSubscription
      include ActiveModel::Model
      attr_accessor :program_id, :amount, :loan_application_id
      validates :program_id, :amount, :loan_application_id, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_subscription
            create_voucher_amount
          end
        end
      end

      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
        description:         find_program.name,
        amount:              amount,
        account:             find_program_subscription.account,
        recorder:            find_loan_application.preparer,
        cooperative:         find_loan_application.cooperative)
      end

      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end

      def find_program_subscription
        find_loan_application.borrower.program_subscriptions.where(program: find_program).last
      end
      def find_cart
        find_loan_application.cart
      end

      def find_program
        Cooperatives::Program.find(program_id)
      end 

      def create_subscription
        Memberships::ProgramSubscriptionProcessing.new(
          subscriber: find_loan_application.borrower,
          program: find_program,
          office: find_loan_application.office,
          cooperative: find_loan_application.office.cooperative
        ).process!
      end
    end
  end
end
