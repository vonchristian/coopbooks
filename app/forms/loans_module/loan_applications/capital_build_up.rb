module LoansModule
  module LoanApplications
    class CapitalBuildUp
      include ActiveModel::Model
      attr_accessor :amount, :loan_application_id, :share_capital_id, :cart_id
      validates :amount, numericality: true, presence: true
      validates :loan_application_id, :share_capital_id, :cart_id, presence: true
      def process!
        if valid?
          ActiveRecord::Base.transaction do
            save_loan_charge
          end
        end
      end

      private
      def save_loan_charge
        find_cart.voucher_amounts.credit.create!(
        description:         "Capital Build Up",
        amount:              amount,
        account:             find_share_capital.equity_account,
        recorder:            find_loan_application.preparer,
        cooperative:         find_loan_application.cooperative)
      end

      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end
      def find_cart
        Cart.find(cart_id)
      end

      def find_share_capital
        MembershipsModule::ShareCapital.find(share_capital_id)
      end
    end
  end
end
