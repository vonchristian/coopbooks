module LoansModule
  module LoanApplications
    class SavingsAccountOpening
      include ActiveModel::Model
      attr_accessor :cart_id, :amount, :saving_product_id, :employee_id, :loan_application_id, :account_number
      validates :cart_id, :amount, :saving_product_id, :employee_id, :account_number, :loan_application_id, presence: true
      validates :amount, numericality: true
      def create_application!
        if valid?
          create_liability_account
          create_savings_account_application
          create_voucher_amount
        end
      end

      private
      def create_liability_account
        account = find_office.accounts.liabilities.create!(
        name:           "#{find_saving_product.name} - (#{find_loan_application.borrower.full_name} - #{account_number}",
        code:           SecureRandom.uuid,
        account_number: SecureRandom.uuid,
        account_category: find_liability_account_category)
      end

      def create_savings_account_application
        savings_account_application = find_office.savings_account_applications.create!(
        saving_product_id: saving_product_id,
        depositor:         find_loan_application.borrower,
        date_opened:       find_loan_application.application_date,
        account_number:    account_number,
        initial_deposit:   amount,
        cooperative:       find_employee.cooperative,
        cart_id:           cart_id,
        liability_account: find_liability_account)
      end

      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
          description: 'Savings deposit',
          cooperative: find_employee.cooperative,
          recorder:    find_employee,
          account:     find_liability_account,
          amount:      amount)
      end

      def find_liability_account
        find_office.accounts.find_by!(name: "#{find_saving_product.name} - (#{find_loan_application.borrower.full_name} - #{account_number}")
      end

      def find_employee
        User.find(employee_id)
      end
      def find_cart
        Cart.find(cart_id)
      end
      def find_office
        find_employee.office
      end
      def find_saving_product
        find_office.saving_products.find(saving_product_id)
      end

      def find_loan_application
        find_office.loan_applications.find(loan_application_id)
      end
      def find_liability_account_category
        find_office.office_saving_products.find_by!(saving_product: find_saving_product).liability_account_category
      end 
    end
  end
end
