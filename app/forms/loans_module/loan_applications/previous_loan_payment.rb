module LoansModule
  module LoanApplications
    class PreviousLoanPayment
      include ActiveModel::Model
      attr_accessor :principal_amount, :interest_amount, :penalty_amount, :loan_application_id, :cart_id, :loan_id, :employee_id
      validates :interest_amount, :penalty_amount, :principal_amount, numericality: true, presence: true
      validates :loan_application_id, :loan_id, :cart_id, :employee_id, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher_amount
          end
        end
      end

      private
      def create_voucher_amount
        create_principal_amount
        create_interest_amount
        create_penalty_amount
      end

      def create_principal_amount
        find_cart.voucher_amounts.credit.create!(
        description: "Previous Loan Payment (Principal)",
        amount:      principal_amount.to_f,
        account:     find_loan.receivable_account,
        cooperative: find_loan_application.cooperative,
        recorder:    find_loan_application.preparer)
      end

      def create_interest_amount
        if interest_amount.to_f > 0
          find_cart.voucher_amounts.credit.create!(
          description: "Previous Loan Payment (Interest)",
          amount:      interest_amount.to_f,
          account:     find_loan.interest_revenue_account,
          recorder:    find_loan_application.preparer,
          cooperative: find_loan_application.cooperative)
        end
      end

      def create_penalty_amount
        if penalty_amount.to_f > 0
          find_cart.voucher_amounts.credit.create!(
          description: "Previous Loan Payment (Penalty)",
          amount:      penalty_amount.to_f,
          account:     find_loan.penalty_revenue_account,
          recorder:    find_loan_application.preparer,
          cooperative: find_loan_application.cooperative)
        end
      end

      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_loan
        LoansModule::Loan.find(loan_id)
      end
    end
  end
end
