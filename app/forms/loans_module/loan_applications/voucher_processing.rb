module LoansModule
  module LoanApplications
    class VoucherProcessing
      include ActiveModel::Model
      attr_accessor :loan_application_id, :preparer_id, :date, :description,
      :number, :reference_number, :account_number, :voucher_account_number, :cash_account_id, :borrower_id,
      :borrower_type, :net_proceed, :cart_id

      validates :reference_number, :cash_account_id, :date, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher
          end
        end
      end

      def find_voucher
        find_office.vouchers.find_by(account_number: voucher_account_number)
      end
      private
      def create_voucher
        voucher = find_office.vouchers.new(
          account_number:   voucher_account_number,
          payee:            borrower,
          preparer:         find_employee,
          office:           find_office,
          cooperative:      find_employee.cooperative,
          description:      description,
          reference_number: reference_number,
          date:             date)

        create_voucher_amounts(voucher)

        voucher.save!

        find_loan_application.update!(voucher: voucher)
      end

      def create_voucher_amounts(voucher)
        create_loan_charges(voucher)
        create_loans_receivable(voucher)
        create_net_proceed(voucher)
      end

      def create_loans_receivable(voucher)
        voucher.voucher_amounts.debit.build(
        cooperative: find_cooperative,
        recorder:    find_employee,
        amount:      find_loan_application.loan_amount,
        description: 'Loan Amount',
        account:     find_loan_application.receivable_account)
      end

      def borrower
        borrower_type.constantize.find(borrower_id)
      end

      def create_net_proceed(voucher)
        voucher.voucher_amounts.credit.build(
        cooperative: find_cooperative,
        recorder:    find_employee,
        amount:      find_loan_application.net_proceed,
        description: 'Net Proceed',
        account_id:  cash_account_id)
      end

      def create_loan_charges(voucher)
        find_cart.voucher_amounts.each do |voucher_amount|
          voucher.voucher_amounts.build(
          amount:      voucher_amount.amount,
          recorder:    find_employee,
          cooperative: find_cooperative,
          amount_type: voucher_amount.amount_type,
          account:     voucher_amount.account,
          description: voucher_amount.description)
        end
      end

      def find_loan_application
        find_office.loan_applications.find(loan_application_id)
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_cooperative
        find_employee.cooperative
      end

      def find_employee
        User.find(preparer_id)
      end
      def find_office
        find_employee.office
      end
    end
  end
end
