module LoansModule
  module LoanApplications
    class ShareCapitalOpening
      include ActiveModel::Model
      attr_accessor :cart_id, :loan_application_id, :share_capital_product_id, :amount, :account_number, :employee_id

      validates :cart_id, :employee_id, :loan_application_id, :share_capital_product_id, :account_number, :amount, presence: true
      validates :amount, numericality: true

      def create_application!
        if valid?
          ActiveRecord::Base.transaction do
            create_equity_account
            create_share_capital_application
            create_voucher_amount
          end
        end
      end

      private
      def create_equity_account
        account = find_office.accounts.equities.create!(
        account_number: account_number,
        name:           "#{find_share_capital_product.name} - (#{find_loan_application.borrower_name} - #{account_number})",
        code:           SecureRandom.uuid,
        account_category: find_equity_account_category)
      end

      def create_share_capital_application
        share_capital_application = find_office.share_capital_applications.create!(
        share_capital_product: find_share_capital_product,
        subscriber:             find_loan_application.borrower,
        date_opened:           find_loan_application.application_date,
        account_number:        account_number,
        initial_capital:       amount,
        cooperative:           find_employee.cooperative,
        office:                find_office,
        cart:                  find_cart,
        equity_account:        credit_account)
      end
      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
        cooperative: find_employee.cooperative,
        description: 'Capital build up',
        recorder:    find_employee,
        account:     credit_account,
        amount:      amount)
      end

      def credit_account
        find_office.accounts.equities.find_by!(account_number: account_number)
      end
      def find_employee
        User.find(employee_id)
      end
      def find_office
        find_employee.office
      end

      def find_share_capital_product
        find_office.share_capital_products.find(share_capital_product_id)
      end
      
      def find_equity_account_category
        find_office.office_share_capital_products.find_by!(share_capital_product: find_share_capital_product).equity_account_category
      end

      def find_loan_application
        find_office.loan_applications.find(loan_application_id)
      end
      def find_cart
        Cart.find(cart_id)
      end
    end
  end
end
