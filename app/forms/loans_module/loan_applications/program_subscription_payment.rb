module LoansModule
  module LoanApplications
    class ProgramSubscriptionPayment
      include ActiveModel::Model
      attr_accessor :program_subscription_id, :loan_application_id, :amount

      validates :program_subscription_id, :loan_application_id, :amount,  presence: true
      validates :amount, numericality: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher_amount
          end
        end
      end

      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
        description:         find_program.name,
        amount:              amount,
        account:             find_program_subscription.account,
        recorder:            find_loan_application.preparer,
        cooperative:         find_loan_application.cooperative)
      end

      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end

      def find_program
        find_program_subscription.program
      end

      def find_cart
        find_loan_application.cart
      end

      def find_program_subscription
        MembershipsModule::ProgramSubscription.find(program_subscription_id)
      end
    end
  end
end
