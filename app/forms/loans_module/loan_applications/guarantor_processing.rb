module LoansModule
  module LoanApplications
    class GuarantorProcessing
      include ActiveModel::Model
      attr_accessor :loan_application_id, :guarantor_id, :office_id

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_guarantor
          end
        end
      end

      private
      def create_guarantor
        find_loan_application.guaranteed_loan_applications.create_or_find_by(guarantor_id: guarantor_id, guarantor_type: 'Member')
      end

      def find_loan_application
        find_office.loan_applications.find(loan_application_id)
      end

      def find_office
        Cooperatives::Office.find(office_id)
      end 
    end
  end
end
