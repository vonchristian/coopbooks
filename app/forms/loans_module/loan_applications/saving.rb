module LoansModule
  module LoanApplications
    class Saving
      include ActiveModel::Model
      attr_accessor :amount, :savings_account_id, :loan_application_id, :cart_id
      validates :amount, presence: true, numericality: true
      validates :savings_account_id, :loan_application_id, :cart_id, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher_amount
          end
        end
      end

      private
      def create_voucher_amount
        find_cart.voucher_amounts.credit.create!(
        description: "Savings Deposit",
        amount:      amount,
        recorder:    find_loan_application.preparer,
        account:     find_saving.liability_account,
        cooperative: find_loan_application.cooperative)
      end

      def find_loan_application
        LoansModule::LoanApplication.find(loan_application_id)
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_saving
        MembershipsModule::Saving.find(savings_account_id)
      end
    end
  end
end
