module LoansModule
  module Loans
    class PaymentVoucher
      include ActiveModel::Model
      attr_accessor :loan_id,
                    :principal_amount,
                    :interest_amount,
                    :penalty_amount,
                    :amortization_schedule_id,
                    :reference_number,
                    :date,
                    :description,
                    :employee_id,
                    :cash_account_id,
                    :account_number
      validates :principal_amount, :interest_amount, :penalty_amount, presence: true, numericality: { greater_than_or_equal_to: 0 }
      validates :reference_number, :date, :description, :cash_account_id, presence: true

      def create_payment_voucher!
        if valid?
          ActiveRecord::Base.transaction do
            create_voucher
          end
        end
      end

      def find_voucher
        find_office.vouchers.find_by!(account_number: account_number)
      end

      def find_loan
        find_office.loans.find(loan_id)
      end

      def schedule_id
        if amortization_schedule_id.present?
          amortization_schedule_id
        end
      end

      def find_employee
        User.find(employee_id)
      end

      def find_office
        find_employee.office
      end

      private

      def create_voucher
        voucher = find_office.vouchers.build(
          account_number:   account_number,
          cooperative:      find_employee.cooperative,
          payee:            find_loan.borrower,
          reference_number: reference_number,
          description:      description,
          preparer:         find_employee,
          date:             date
        )
        create_penalty_amount(voucher)
        create_principal_amount(voucher)
        create_interest_revenue_amount(voucher)
        create_total_cash_amount(voucher)
        voucher.save!
      end

      def create_total_cash_amount(voucher)
        voucher.voucher_amounts.debit.build(
        amount:      total_amount,
        account:     find_cash_account,
        recorder:    find_employee,
        cooperative: find_employee.cooperative)
      end

      def create_interest_revenue_amount(voucher)
        if interest_amount.to_f > 0
          voucher.voucher_amounts.credit.build(
          amount:      BigDecimal(interest_amount),
          account:     find_loan.interest_revenue_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end

      def create_penalty_amount(voucher)
        if penalty_amount.to_f > 0
          voucher.voucher_amounts.credit.build(
          amount:      BigDecimal(penalty_amount),
          account:     find_loan.penalty_revenue_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end

      def create_principal_amount(voucher)
        if principal_amount.to_f > 0
          voucher.voucher_amounts.credit.build(
          amount:      BigDecimal(principal_amount),
          account:     find_loan.receivable_account,
          recorder:    find_employee,
          cooperative: find_employee.cooperative)
        end
      end

      def find_cash_account
        find_employee.cash_accounts.find(cash_account_id)
      end

      def total_amount
        principal_amount.to_f +
        interest_amount.to_f +
        penalty_amount.to_f
      end

      def principal_amount_not_more_than_balance
        errors[:principal_amount] << "Must be less than or equal to balance." if principal_amount.to_f > find_loan.balance
      end


    end
  end
end
