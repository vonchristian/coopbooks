module LoansModule
  class LoanApplicationProcessing
    include ActiveModel::Model
    attr_accessor :borrower_id,
                  :borrower_type,
                  :term,
                  :purpose,
                  :loan_product_id,
                  :loan_amount,
                  :application_date,
                  :mode_of_payment,
                  :account_number,
                  :preparer_id,
                  :cooperative_id,
                  :office_id,
                  :cart_id,
                  :interest_rate,
                  :interest_calculation_type,
                  :interest_prededuction_rate
    validates :loan_amount,
              :interest_rate,
              :interest_prededuction_rate,
              :term, presence: true, numericality: true
    validate :maximum_term?
    validates :borrower_id,
              :borrower_type,
              :purpose,
              :loan_product_id,
              :application_date,
              :mode_of_payment,
              :account_number,
              :preparer_id,
              presence: true

    def find_loan_application
      find_office.loan_applications.find_by!(account_number: account_number)
    end

    def process!
      if valid?
        ActiveRecord::Base.transaction do
          create_receivable_account
          create_interest_revenue_account
          create_loan_application
        end
      end
    end

    private

    def create_receivable_account
      account = find_preparer.office.accounts.assets.create!(
      account_number: SecureRandom.uuid,
      name:           receivable_account_name,
      code:           SecureRandom.uuid,
      account_category: find_loan_product.receivable_account_category)
      find_office.accounts << account
    end

    def create_interest_revenue_account
      account = find_preparer.office.accounts.revenues.create!(
      account_number: SecureRandom.uuid,
      name:           interest_revenue_account_name,
      code:           SecureRandom.uuid,
      account_category: find_loan_product.interest_revenue_account_category)
      find_office.accounts << account
    end

    def create_loan_application
      loan_application =           find_office.loan_applications.create!(
        cooperative:               find_preparer.cooperative,
        purpose:                   purpose,
        borrower:                  find_borrower,
        loan_product_id:           loan_product_id,
        loan_amount:               loan_amount,
        application_date:          application_date,
        mode_of_payment:           mode_of_payment,
        preparer_id:               preparer_id,
        account_number:            account_number,
        receivable_account:        find_receivable_account,
        interest_revenue_account:  find_interest_revenue_account,
        term:                      term,
        interest_rate:             interest_rate,
        interest_calculation_type: interest_calculation_type,
        interest_prededuction_rate: interest_prededuction_rate,
        cart:                   Cart.create(employee: find_preparer))

        LoansModule::LoanApplications::Processor.new(loan_application: loan_application).process!
    end


    def find_borrower
      borrower_type.constantize.find(borrower_id)
    end

    def find_office
      find_preparer.office
    end

    def find_preparer
      User.find(preparer_id)
    end

    def find_loan_product
      find_office.loan_products.find(loan_product_id)
    end

    def find_receivable_account
      find_preparer.office.accounts.find_by!(name: receivable_account_name)
    end

    def find_interest_revenue_account
      find_preparer.office.accounts.find_by!(name: interest_revenue_account_name)
    end

    def receivable_account_name
      "Loans Receivable - #{find_loan_product.name} #{account_number}"
    end

    def interest_revenue_account_name
      "Interest Income from Loans - #{find_loan_product.name} #{account_number}"
    end

   def find_cart
     Cart.find(cart_id)
   end

    def maximum_term?
      errors[:term] << "must not exceed 120 months." if term.to_f > 120
    end
  end
end
