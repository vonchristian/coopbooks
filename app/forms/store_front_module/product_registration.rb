module StoreFrontModule
  class ProductRegistration
    include ActiveModel::Model
    attr_accessor :category_id,
                  :name,
                  :description,
                  :unit_of_measurement_code,
                  :unit_of_measurement_description,
                  :base_quantity,
                  :price,
                  :cooperative_id,
                  :store_front_id,
                  :has_conversion

    validates :name, :unit_of_measurement_code, :base_quantity, :price, presence: true
    validates :base_quantity, :price, numericality: true

    def register!
      ActiveRecord::Base.transaction do
        create_product
      end
    end

    private

    def create_product
      product = find_store_front.products.find_or_create_by(
        name:           name,
        description:    description,
        has_conversion: has_conversion)
      unit_of_measurement = product.unit_of_measurements.find_or_create_by(
        code: unit_of_measurement_code,
        description: unit_of_measurement_description,
        base_quantity: base_quantity,
        base_measurement: true
        )
      unit_of_measurement.mark_up_prices.create(price: price)
    end

    def find_cooperative
      Cooperative.find(cooperative_id)
    end

    def find_store_front
      StoreFront.find(store_front_id)
    end
  end
end
