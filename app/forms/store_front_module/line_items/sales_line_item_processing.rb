module StoreFrontModule
  module LineItems
    class SalesLineItemProcessing
     include ActiveModel::Model
      attr_accessor :unit_of_measurement_id,
                    :quantity,
                    :cart_id,
                    :product_id,
                    :unit_cost,
                    :total_cost,
                    :cart_id,
                    :barcode,
                    :employee_id,
                    :store_front_id,
                    :stock_id

      validates :quantity, numericality: { greater_than: 0.1 }
      validate :ensure_quantity_is_less_than_or_equal_to_available_quantity?

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_sales_line_item
            update_stock_availability
          end
        end
      end

      private
      def create_sales_line_item
        find_cart.sales_line_items.create!(
          stock: find_stock,
          unit_of_measurement_id: unit_of_measurement_id,
          quantity: quantity,
          unit_cost: unit_cost,
          total_cost: calculate_total_cost,
          product: find_stock.product
        )
      end
      def calculate_total_cost
        quantity.to_f * unit_cost.to_f
      end
      def find_cart
        Cart.find(cart_id)
      end

      def find_stock
        find_store_front.stocks.find(stock_id)
      end

      def find_store_front
        StoreFront.find(store_front_id)
      end

      def available_stock
        find_stock.balance + find_cart.sales_line_items.unprocessed.where(stock: find_stock).total
      end

      def update_stock_availability
        StoreFrontModule::StockAvailabilityChecker.new(stock: find_stock, cart: find_cart).update_availability!
      end


      def ensure_quantity_is_less_than_or_equal_to_available_quantity?
        errors[:quantity] << "exceeded available quantity" if quantity.to_f > available_stock
      end
    end
  end
end
