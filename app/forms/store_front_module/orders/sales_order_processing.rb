 module StoreFrontModule
  module Orders
    class SalesOrderProcessing
      include ActiveModel::Model
      attr_accessor  :customer_id,
                     :date,
                     :cash_tendered,
                     :order_change,
                     :employee_id,
                     :cart_id,
                     :discount_amount,
                     :total_cost,
                     :reference_number,
                     :cash_account_id,
                     :discount_amount

    validates :employee_id, :customer_id, :cash_tendered, :order_change, presence: true

      def process!
        if valid?
          ActiveRecord::Base.transaction do
            create_sales_order
          end
        end
      end

      private
      def create_sales_order
        order = StoreFronts::Orders::SalesOrder.new(
        customer:       find_customer,
        total_cost:     find_cart.sales_line_items.total_cost,
        discount:       discount_amount,
        date:           date,
        account_number: SecureRandom.uuid,
        store_front:    find_employee.store_front,
        employee:       find_employee)
        create_accounts(order)
        order.save!

        find_cart.sales_line_items.each do |line_item|
          line_item.cart_id = nil
          order.line_items << line_item
        end
        create_voucher(order)
        create_entry(order)
      end

      def find_customer
        Customer.find(customer_id)
      end

      def find_cart
        Cart.find(cart_id)
      end

      def find_employee
        User.find(employee_id)
      end

      def create_voucher(order)
        StoreFronts::Vouchers::SalesOrder.new(order: order).create_voucher!
      end

      def create_accounts(sales_order)
        AccountCreators::StoreFronts::Orders::SalesOrder.new(sales_order: sales_order).create_accounts!
      end
      def create_entry(order)
        ::Vouchers::EntryProcessing.new(voucher: order.voucher, employee: order.employee).process!
      end

      def total_cost_less_discount(order)
        order.total_cost - discount_amount.to_f
      end
      def find_cash_account
        find_employee.cash_accounts.find(cash_account_id)
      end
      def find_cooperative
        find_employee.cooperative
      end
    end
  end
end
