module StoreFrontModule
  module Suppliers
    class ProductSelection
      include ActiveModel::Model 
      attr_accessor :product_id, :unit_of_measurement_id
    end
  end
end
