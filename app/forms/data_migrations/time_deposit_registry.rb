module DataMigrations
  class TimeDepositRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :employee_id

    def parse!
      loan_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
      header = loan_spreadsheet.row(2)
      (3..loan_spreadsheet.last_row).each do |i|
        row = Hash[[header, loan_spreadsheet.row(i)].transpose]
        upload_data(row)
      end
    end

    private
    def upload_data(row)
      time_deposit = find_office.time_deposits.build(
        time_deposit_product: find_time_deposit_product(row),
        account_number: SecureRandom.uuid,
        depositor: find_depositor(row),
        date_deposited: row["Date Deposited"],
        depositor_name: find_depositor(row).name,
        cooperative: find_office.cooperative,
      )

          time_deposit.build_term(
          account_number:   SecureRandom.uuid,
          term:             term(row),
          effectivity_date: date_deposited(row),
          maturity_date:    maturity_date(row))

      create_accounts(time_deposit)
      time_deposit.save!
      create_entry(time_deposit, row)
    end

    def create_accounts(time_deposit)
      AccountCreators::TimeDeposit.new(time_deposit: time_deposit).create_accounts!
    end

    def create_entry(time_deposit, row)
      entry = find_office.entries.build(
        cooperative: find_employee.cooperative,
        recorder: find_employee,
        commercial_document: time_deposit,
        description: "Forwarded time deposit balance as of #{cut_off_date(row).strftime('%B %e, %Y')}",
        entry_date: cut_off_date(row)
      )
      entry.debit_amounts.build(
        account: find_temporary_time_deposit_account,
        amount:  row["Amount"].to_d)
      entry.credit_amounts.build(
        account: time_deposit.liability_account,
        amount: row["Amount"].to_d)
      entry.save!
    end

    def find_depositor(row)
      if row["Depositor Type"] == "Member"
        find_member(row)
      elsif row["Depositor Type"] == "Organization"
        find_organization(row)
      end
    end

    def find_member(row)
      find_office.member_memberships.find_by!(
        last_name:   Propercaser.new(text: row["Last Name"]).propercase,
        first_name:  Propercaser.new(text: row["First Name"]).propercase,
        middle_name: Propercaser.new(text: row["Middle Name"] || "").propercase)
    end

    def find_organization(row)
      find_office.organizations.find_by!(name: Propercaser.new(text: row["Last Name"]).propercase)
    end

    def find_time_deposit_product(row)
      find_office.time_deposit_products.find_by(name: row["Time Deposit Product"])
    end

    def cut_off_date(row)
      Date.parse(row["Cut Off Date"].to_s)
    end



    def term(row)
      if row["Term"].nil?
        (maturity_date(row).year * 12 + maturity_date(row).month) - (disbursement_date(row).year * 12 + disbursement_date(row).month)
      else
        row["Term"].to_i
      end
    end

    def loan_status(row)
      row["Status"].parameterize.gsub("-", "_")
    end

    def maturity_date(row)
      Date.parse(row["Maturity Date"].to_s)
    end
    
    def find_temporary_time_deposit_account
      find_office.accounts.find_by(name: "Temporary Time Deposit Account")
    end

    def date_deposited(row)
        Date.parse(row["Date Deposited"].to_s)
    end
    def find_office
      find_employee.office
    end
    def find_employee
      User.find(employee_id)
    end
  end


  end
