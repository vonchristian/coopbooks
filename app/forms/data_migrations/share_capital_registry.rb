module DataMigrations
  class ShareCapitalRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :employee_id

    def parse!
      ActiveRecord::Base.transaction do
        share_capital_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
        header = share_capital_spreadsheet.row(2)
        (3..share_capital_spreadsheet.last_row).each do |i|
          row = Hash[[header, share_capital_spreadsheet.row(i)].transpose]
          upload_share_capital(row)
        end
      end
    end
    def upload_share_capital(row)
      share_capital = find_office.share_capitals.build(
        subscriber:            find_subscriber(row),
        account_number:        SecureRandom.uuid,
        account_name:    find_subscriber(row).name,
        office:                find_office,
        cooperative:           find_employee.cooperative,
        share_capital_product: find_share_capital_product(row)
      )
      create_accounts(share_capital)
      share_capital.save!
      create_entry(share_capital, row)
    end

    def create_accounts(share_capital)
      AccountCreators::ShareCapital.new(share_capital: share_capital).create_accounts!
    end

    def create_entry(share_capital, row)
      entry = find_office.entries.build(
      recorder:            find_employee,
      commercial_document: share_capital,
      cooperative:         find_employee.cooperative,
      description:         "Forwarded balance of share capital as of #{cut_off_date(row).strftime("%B %e, %Y")}",
      entry_date:          cut_off_date(row))

      entry.debit_amounts.build(
      account: temporary_account(row),
      amount: row["Balance"].to_f)

      entry.credit_amounts.build(
        account: share_capital.equity_account,
        amount: row["Balance"].to_f)
      entry.save!
    end

    def find_subscriber(row)
      if row["Subscriber Type"] == "Member"
        find_member(row)
      elsif row["Subscriber Type"] == "Organization"
        find_organization(row)
      end
    end

    def find_member(row)
      find_office.member_memberships.find_by!(
        last_name:   Propercaser.new(text: row["Last Name"]).propercase,
        first_name:  Propercaser.new(text: row["First Name"]).propercase,
        middle_name: Propercaser.new(text: row["Middle Name"] || "").propercase)
    end

    def find_organization(row)
      find_office.organizations.find_by!(name: Propercaser.new(text: row["Last Name"]).propercase)
    end

    def find_employee
      User.find(employee_id)
    end

    def cut_off_date(row)
      (row["Cut Off Date"])
    end

    def find_office
      find_employee.office
    end

    def find_share_capital_product(row)
      find_office.share_capital_products.find_by!(name: row["Share Capital Product"])
    end
    def temporary_account(row)
      find_office.office_share_capital_products.where(share_capital_product: find_share_capital_product(row)).last.temporary_account
    end
  end
end
