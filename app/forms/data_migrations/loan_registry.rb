module DataMigrations
  class LoanRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :employee_id

    validates :spreadsheet, :employee_id, presence: true

    def parse!
      loan_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
      header = loan_spreadsheet.row(2)
      (3..loan_spreadsheet.last_row).each do |i|
        row = Hash[[header, loan_spreadsheet.row(i)].transpose]
        upload_loan(row)
      end
    end

    private
    def upload_loan(row)
      loan = find_office.loans.build(
        forwarded_loan:     true,
        cooperative:        find_employee.cooperative,
        borrower:           find_borrower(row),
        loan_product:       find_loan_product(row),
        loan_amount:        loan_amount(row),
        account_number:     SecureRandom.uuid,
        borrower_full_name: find_borrower(row).name,
        status:             loan_status(row))

        if row["Maturity Date"].present? && row["Disbursement Date"].present?
          loan.terms.build(
          account_number:   SecureRandom.uuid,
          term:             term(row),
          effectivity_date: disbursement_date(row),
          maturity_date:    maturity_date(row))
        end

      create_accounts(loan, row)
      loan.save!
      create_entry(loan, row)
    end

    def create_accounts(loan, row)
      AccountCreators::Loan.new(loan: loan, loan_product: find_loan_product(row)).create_accounts!
    end

    def create_entry(loan, row)
      entry = find_office.entries.build(
        cooperative: find_employee.cooperative,
        recorder: find_employee,
        commercial_document: loan,
        description: "Forwarded loan balance as of #{cut_off_date(row).strftime('%B %e, %Y')}",
        entry_date: cut_off_date(row)
      )
      entry.debit_amounts.build(
        account: loan.receivable_account,
        amount:  loan_balance(row))
      entry.credit_amounts.build(
        account: find_loan_product(row).temporary_account,
        amount: loan_balance(row))
      entry.save!
    end

    def find_borrower(row)
      if row["Borrower Type"] == "Member"
        find_member(row)
      elsif row["Borrower Type"] == "Organization"
        find_organization(row)
      end
    end

    def find_member(row)
      find_office.member_memberships.find_by!(
        last_name:   Propercaser.new(text: row["Last Name"]).propercase,
        first_name:  Propercaser.new(text: row["First Name"]).propercase,
        middle_name: Propercaser.new(text: row["Middle Name"] || "").propercase)
    end

    def find_organization(row)
      find_office.organizations.find_by!(name: Propercaser.new(text: row["Last Name"]).propercase)
    end

    def find_loan_product(row)
      find_office.loan_products.find_by(name: row["Loan Product"])
    end

    def cut_off_date(row)
      Date.parse(row["Cut Off Date"].to_s)
    end

    def loan_balance(row)
      row["Loan Balance"].to_d
    end

    def loan_amount(row)
      row["Loan Amount"].to_d
    end

    def term(row)
      if row["Term"].nil?
        (maturity_date(row).year * 12 + maturity_date(row).month) - (disbursement_date(row).year * 12 + disbursement_date(row).month)
      else
        row["Term"].to_i
      end
    end

    def loan_status(row)
      row["Status"].parameterize.gsub("-", "_")
    end

    def maturity_date(row)
      Date.parse(row["Maturity Date"].to_s)
    end

    def disbursement_date(row)
        Date.parse(row["Disbursement Date"].to_s)
    end
    def find_office
      find_employee.office
    end
    def find_employee
      User.find(employee_id)
    end
  end


  end
