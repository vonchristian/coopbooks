module DataMigrations
  class MemberRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :office_id, :employee_id

    def parse!
      savings_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
      header = savings_spreadsheet.row(2)
      (3..savings_spreadsheet.last_row).each do |i|
        row = Hash[[header, savings_spreadsheet.row(i)].transpose]
        upload_member(row)
      end
    end
    def upload_member(row)
      member = Member.find_or_create_by(
        first_name:    Propercaser.new(text: row["First Name"]).propercase,
        middle_name:   Propercaser.new(text: row["Middle Name"] || "").propercase,
        last_name:     Propercaser.new(text: row["Last Name"]).propercase,
        date_of_birth: row['Date of Birth']
      )
      member.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'default.png')), filename: 'default.png')

      create_membership(member, row)
      create_address(member, row)
      create_contacts(member, row)
    end
    def create_membership(member, row)
      membership = member.memberships.find_or_create_by!(
        cooperative:         find_office.cooperative,
        membership_category: membership_category(row),
        account_number:      SecureRandom.uuid,
        membership_date:     row["Membership Date"])

      find_office.office_memberships.find_or_create_by!(membership: membership)
    end
    def membership_category(row)
      find_office.cooperative.membership_categories.find_by!(title: row["Membership Category"])
    end

    def find_office
      find_employee.office
    end

    def find_employee
      User.find(employee_id)
    end

    def create_address(member, row)
      member.addresses.find_or_create_by(complete_address: row['Complete Address'])
    end

    def create_contacts(member, row)
      member.contacts.find_or_create_by(number: row["Contact Number"])
    end
  end
end
