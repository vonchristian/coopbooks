module DataMigrations
  class SavingRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :employee_id

    validates :spreadsheet, :employee_id, presence: true

    def parse!
      ActiveRecord::Base.transaction do
        savings_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
        header = savings_spreadsheet.row(2)
        (3..savings_spreadsheet.last_row).each do |i|
          row = Hash[[header, savings_spreadsheet.row(i)].transpose]
          upload_savings(row)
        end
      end
    end

    def upload_savings(row)
        savings_account =      find_office.savings.build(
        cooperative:           find_office.cooperative,
        account_name:          find_depositor(row).name,
        depositor:             find_depositor(row),
        saving_product:        find_saving_product(row),
        saving_group:          find_saving_group,
        account_number:        SecureRandom.uuid)
        create_accounts(savings_account)
        savings_account.save!
        create_entry(savings_account, row)
    end

    def create_accounts(savings_account)
      AccountCreators::Saving.new(saving: savings_account).create_accounts!
    end

    def create_entry(savings, row)
      find_office.entries.create!(
        cooperative:         find_office.cooperative,
        recorder:            find_employee,
        description:         "Forwarded balance of savings deposit as of #{cut_off_date(row).strftime('%B %e, %Y')}",
        entry_date:          cut_off_date(row),
        commercial_document: savings,
        debit_amounts_attributes: [
          account: debit_account(row),
          amount:  amount(row)],
        credit_amounts_attributes: [
          account: savings.liability_account,
          amount:  amount(row)])
    end

    def find_saving_product(row)
      find_office.saving_products.find_by!(name: row["Saving Product"])
    end

    def amount(row)
      row["Balance"].to_f
    end

    def find_cooperative
      find_office.cooperative
    end

    def find_depositor(row)
      if row["Depositor Type"] == "Member"
        find_member(row)
      elsif row["Depositor Type"] == "Organization"
        find_organization(row)
      end
    end

    def find_organization(row)
      find_office.organizations.find_by!(name: Propercaser.new(text: row["Last Name"]).propercase)
    end

    def find_member(row)
      find_office.member_memberships.find_by!(
        last_name:   Propercaser.new(text: row["Last Name"]).propercase,
        first_name:  Propercaser.new(text: row["First Name"]).propercase,
        middle_name: Propercaser.new(text: row["Middle Name"] || "").propercase)
    end

    def debit_account(row)
      find_temporary_account(row)
    end

    def cut_off_date(row)
      Date.parse(row["Cut Off Date"].to_s)
    end

    def account_number(row)
      row["Account Number"]
    end

    def find_office
      find_employee.office
    end
    def find_employee
      User.find(employee_id)
    end
    def find_temporary_account(row)
      find_office.office_saving_products.where(saving_product: find_saving_product(row)).last.temporary_account
    end
    def find_saving_group
      find_office.saving_groups.order(start_num: :asc).first
    end
  end
end
