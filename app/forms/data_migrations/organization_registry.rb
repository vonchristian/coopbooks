module DataMigrations
  class OrganizationRegistry
    include ActiveModel::Model
    attr_accessor :spreadsheet, :employee_id, :office_id

    def parse!
      org_spreadsheet = Roo::Spreadsheet.open(spreadsheet.path)
      header = org_spreadsheet.row(2)
      (3..org_spreadsheet.last_row).each do |i|
        row = Hash[[header, org_spreadsheet.row(i)].transpose]
        upload_organization(row)
      end
    end

    private
    def upload_organization(row)
      find_office.organizations.create!(
        name: Propercaser.new(text: row["Name"]).propercase,
        abbreviated_name: row['Abbreviated Name'])
    end
    def find_office
      Cooperatives::Office.find(office_id)
    end
  end
end
