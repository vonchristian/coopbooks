module Employees
  class Registration
    include ActiveModel::Model
    attr_accessor :first_name, :last_name, :email, :cooperative_id, :office_id, :password, :password_confirmation
    validates :first_name, :last_name, :email, :password, :password_confirmation, presence: true
    def register!
      User.create!(
        cooperative_id:        cooperative_id,
        office_id:             office_id,
        confirmed_at:          Date.current,
        email:                 email,
        first_name:            first_name,
        last_name:             last_name,
        password:              password,
        password_confirmation: password_confirmation)
    end
  end
end
