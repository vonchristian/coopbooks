class AddDisbursementDate2ToLoans < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :disbursement_date, :datetime
  end
end
