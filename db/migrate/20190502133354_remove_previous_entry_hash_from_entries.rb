class RemovePreviousEntryHashFromEntries < ActiveRecord::Migration[5.2]
  def change
    remove_column :entries, :previous_entry_hash, :string
    remove_column :entries, :encrypted_hash, :string

  end
end
