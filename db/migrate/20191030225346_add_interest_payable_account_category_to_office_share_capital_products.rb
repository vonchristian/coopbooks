class AddInterestPayableAccountCategoryToOfficeShareCapitalProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :office_share_capital_products, :interest_payable_account_category, foreign_key: { to_table: :level_one_account_categories }, type: :uuid, index: { name: 'index_int_payable_act_category_on_office_share_capital_products' }
    add_reference :office_share_capital_products, :equity_account_category,foreign_key: { to_table: :level_one_account_categories }, type: :uuid, index: { name: 'index_equity_act_category_on_office_share_capital_products' }
  end
end
