class AddHasConversionToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :has_conversion, :boolean, default: false 
  end
end
