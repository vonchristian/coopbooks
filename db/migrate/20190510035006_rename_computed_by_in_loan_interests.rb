class RenameComputedByInLoanInterests < ActiveRecord::Migration[5.2]
  def change
    rename_column :loan_interests, :computed_by_id, :employee_id
  end
end
