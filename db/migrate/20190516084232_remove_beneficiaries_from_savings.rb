class RemoveBeneficiariesFromSavings < ActiveRecord::Migration[5.2]
  def change
    remove_column :savings, :beneficiaries, :string
  end
end
