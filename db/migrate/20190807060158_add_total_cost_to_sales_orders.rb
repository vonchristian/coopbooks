class AddTotalCostToSalesOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :sales_orders, :total_cost, :decimal
    add_column :sales_orders, :discount, :decimal

  end
end
