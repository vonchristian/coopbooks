class AddVoucherToInternalUseOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :internal_use_orders, :voucher, foreign_key: true, type: :uuid 
  end
end
