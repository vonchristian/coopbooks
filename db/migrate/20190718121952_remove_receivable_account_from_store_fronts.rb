class RemoveReceivableAccountFromStoreFronts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :store_fronts, :receivable_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
