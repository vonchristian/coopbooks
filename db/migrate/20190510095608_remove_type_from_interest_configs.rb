class RemoveTypeFromInterestConfigs < ActiveRecord::Migration[5.2]
  def change
    remove_index :interest_configs, :type
    remove_column :interest_configs, :type, :string
  end
end
