class Remove2commercialDocumentFromVouchers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :vouchers, :commercial_document, polymorphic: true, type: :uuid 
  end
end
