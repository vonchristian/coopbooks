class RemoveCancelledAtFromEntries < ActiveRecord::Migration[5.2]
  def change
    remove_column :entries, :cancelled_at, :datetime
    remove_reference :entries, :cancelled_by, foreign_key: { to_table: :users }, type: :uuid 
    remove_column :entries, :cancellation_description, :string
    remove_column :entries, :archived, :boolean
  end
end
