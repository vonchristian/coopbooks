class RenameAccountOwnerNameInSavings < ActiveRecord::Migration[5.2]
  def change
    rename_column :savings, :account_owner_name, :account_name
  end
end
