class RemoveCommercialDocumentFromAmounts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :amounts, :commercial_document, polymorphic: true
  end
end
