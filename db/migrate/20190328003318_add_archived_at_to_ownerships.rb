class AddArchivedAtToOwnerships < ActiveRecord::Migration[5.2]
  def change
    add_column :ownerships, :archived_at, :datetime
  end
end
