class AddMunicipalityToSavings < ActiveRecord::Migration[5.2]
  def change
    add_reference :savings, :municipality, foreign_key: true, type: :uuid 
  end
end
