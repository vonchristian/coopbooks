class RemoveCurrentAccountIdFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loans, :current_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
