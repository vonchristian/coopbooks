class RemoveLastTransactionDateFromTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    remove_column :time_deposits, :last_transaction_date, :datetime
    remove_column :time_deposits, :beneficiaries, :string
    remove_reference :time_deposits, :membership, foreign_key: true, type: :uuid 
  end
end
