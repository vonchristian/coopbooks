class RemoveLastTransactionDateFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_column :loans, :last_transaction_date, :datetime
  end
end
