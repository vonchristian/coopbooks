class AddAccountToProgramSubscriptions < ActiveRecord::Migration[5.2]
  def change
    add_reference :program_subscriptions, :account, foreign_key: true, type: :uuid 
  end
end
