class Add2AccountToPrograms < ActiveRecord::Migration[5.2]
  def change
    add_reference :programs, :account, foreign_key: true, type: :uuid
  end
end
