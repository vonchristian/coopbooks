class AddCooperativeToProgramSubscriptions < ActiveRecord::Migration[5.2]
  def change
    add_reference :program_subscriptions, :cooperative, foreign_key: true, type: :uuid
  end
end
