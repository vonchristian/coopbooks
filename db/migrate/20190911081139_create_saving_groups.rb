class CreateSavingGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :saving_groups, id: :uuid do |t|
      t.string :title
      t.decimal :start_num
      t.decimal :end_num
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
