class AddBreakContractAccountToTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_products, :break_contract_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_break_cont_account_category_on_time_dep_products' }
    add_reference :time_deposit_products, :interest_expense_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_int_expense_account_category_on_time_dep_products' }
  end
end
