class AddLastTransactionDateToAccountCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :account_categories, :last_transaction_date, :datetime
  end
end
