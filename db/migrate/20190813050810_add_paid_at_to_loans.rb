class AddPaidAtToLoans < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :paid_at, :datetime
  end
end
