class RemoveSupplierFromPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :purchase_orders, :supplier, foreign_key: true
  end
end
