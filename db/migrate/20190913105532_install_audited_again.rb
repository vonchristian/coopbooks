class InstallAuditedAgain < ActiveRecord::Migration[6.0]
  def self.up
    create_table :audits, id: :uuid, :force => true do |t|
      t.references :auditable, polymorphic: true, type: :uuid
      t.references :associated, polymorphic: true, type: :uuid
      t.references :user, polymorphic: true, type: :uuid
      t.column :username, :string
      t.column :action, :string
      t.column :audited_changes, :text
      t.column :version, :integer, :default => 0
      t.column :comment, :string
      t.column :remote_address, :string
      t.column :request_uuid, :string
      t.column :created_at, :datetime
    end

    add_index :audits, [:auditable_type, :auditable_id, :version], :name => 'auditable_index'
    add_index :audits, [:associated_type, :associated_id], :name => 'associated_index'
    add_index :audits, [:user_id, :user_type], :name => 'user_index'
    add_index :audits, :request_uuid
    add_index :audits, :created_at
  end

  def self.down
    drop_table :audits
  end
end
