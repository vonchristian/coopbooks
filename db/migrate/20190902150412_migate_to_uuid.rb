class MigateToUuid < ActiveRecord::Migration[6.0]
  def up
    # Add UUID columns
    add_column :office_loan_products,    :uuid, :uuid, null: false, default: -> { "gen_random_uuid()" }
    add_column :office_saving_products,    :uuid, :uuid, null: false, default: -> { "gen_random_uuid()" }
    add_column :office_share_capital_products,    :uuid, :uuid, null: false, default: -> { "gen_random_uuid()" }
    add_column :office_time_deposit_products,    :uuid, :uuid, null: false, default: -> { "gen_random_uuid()" }


    remove_column :office_loan_products,          :id
    remove_column :office_saving_products,        :id
    remove_column :office_share_capital_products, :id
    remove_column :office_time_deposit_products,  :id


    rename_column :office_loan_products,    :uuid, :id
    rename_column :office_saving_products,    :uuid, :id
    rename_column :office_share_capital_products,    :uuid, :id
    rename_column :office_time_deposit_products,    :uuid, :id




    execute "ALTER TABLE office_loan_products    ADD PRIMARY KEY (id);"
    execute "ALTER TABLE office_saving_products    ADD PRIMARY KEY (id);"
    execute "ALTER TABLE office_share_capital_products    ADD PRIMARY KEY (id);"
    execute "ALTER TABLE office_time_deposit_products    ADD PRIMARY KEY (id);"



  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
