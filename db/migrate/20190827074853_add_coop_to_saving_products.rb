class AddCoopToSavingProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :saving_products, :cooperative,  foreign_key: true, type: :uuid 
  end
end
