class CreateProductCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :product_categories, id: :uuid do |t|
      t.string :title
      t.belongs_to :store_front, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
