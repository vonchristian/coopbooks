class RemoveStatusFromMemberships < ActiveRecord::Migration[5.2]
  def change
    remove_index :memberships, :status
    remove_column :memberships, :status, :integer
  end
end
