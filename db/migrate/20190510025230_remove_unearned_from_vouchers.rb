class RemoveUnearnedFromVouchers < ActiveRecord::Migration[5.2]
  def change
    remove_column :vouchers, :unearned, :boolean
  end
end
