class RenameAccountCategoriesToLevelOneAccountCategories < ActiveRecord::Migration[6.0]
  def change
    rename_table :account_categories, :level_one_account_categories

  end
end
