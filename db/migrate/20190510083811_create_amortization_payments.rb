class CreateAmortizationPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :amortization_payments, id: :uuid do |t|
      t.belongs_to :amortization_schedule, foreign_key: true, type: :uuid
      t.belongs_to :entry, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
