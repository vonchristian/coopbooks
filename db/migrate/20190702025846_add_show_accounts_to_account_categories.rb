class AddShowAccountsToAccountCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :account_categories, :show_accounts, :boolean, default: false
  end
end
