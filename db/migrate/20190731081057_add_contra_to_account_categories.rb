class AddContraToAccountCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :account_categories, :contra, :boolean, default: false
    add_column :account_categories, :type,   :string, index: true
  end
end
