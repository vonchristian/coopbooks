class RenamePaymentScheduleTypeInPrograms < ActiveRecord::Migration[5.2]
  def change
    rename_column :programs, :payment_schedule_type, :mode_of_payment
  end
end
