class CreateOfficeMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :office_memberships, id: :uuid do |t|
      t.belongs_to :office,     foreign_key: true, type: :uuid
      t.belongs_to :membership, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
