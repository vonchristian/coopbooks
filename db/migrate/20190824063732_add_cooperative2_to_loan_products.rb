class AddCooperative2ToLoanProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :loan_products, :cooperative, foreign_key: true, type: :uuid 
  end
end
