class RemoveOfficeFromAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :accounts, :office, foreign_key: true, type: :uuid
  end
end
