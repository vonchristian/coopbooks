class RemoveArchivedFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_column :loans, :archived, :boolean
    remove_reference :loans, :archived_by, foreign_key: { to_table: :users }, type: :uuid 
    remove_column :loans, :cancelled, :boolean
    remove_column :loans, :application_date, :datetime
  end
end
