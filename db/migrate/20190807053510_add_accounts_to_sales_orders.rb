class AddAccountsToSalesOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :sales_orders, :sales_account,              foreign_key: { to_table: :accounts }, type: :uuid
    add_reference :sales_orders, :sales_discount_account,     foreign_key: { to_table: :accounts }, type: :uuid
    add_reference :sales_orders, :cost_of_goods_sold_account, foreign_key: { to_table: :accounts }, type: :uuid
    add_reference :sales_orders, :receivable_account,         foreign_key: { to_table: :accounts }, type: :uuid

  end
end
