class RenameColumnAccountCategoryOnAccounts < ActiveRecord::Migration[6.0]
  def change
    rename_column :accounts, :account_category_id, :level_one_account_category_id
  end
end
