class AddOfficeToAmortizationTypes < ActiveRecord::Migration[5.2]
  def change
    add_reference :amortization_types, :office, foreign_key: true, type: :uuid
  end
end
