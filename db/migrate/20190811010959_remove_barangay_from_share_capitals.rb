class RemoveBarangayFromShareCapitals < ActiveRecord::Migration[5.2]
  def change
    remove_reference :share_capitals, :barangay,     foreign_key: true, type: :uuid
    remove_reference :share_capitals, :municipality, foreign_key: true, type: :uuid
    remove_reference :share_capitals, :organization, foreign_key: true, type: :uuid 
  end
end
