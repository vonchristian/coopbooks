class RemoveBeneficiariesFromSavingsAccountApplications < ActiveRecord::Migration[5.2]
  def change
    remove_column :savings_account_applications, :beneficiaries, :string
  end
end
