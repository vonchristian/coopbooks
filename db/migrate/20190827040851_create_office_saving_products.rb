class CreateOfficeSavingProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :office_saving_products do |t|
      t.belongs_to :saving_product, null: false, foreign_key: true, type: :uuid
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid
      t.belongs_to :liability_account_category, null: false, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_liability_acct_category_on_office_saving_products' }
      t.belongs_to :interest_expense_account_category, null: false, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_interest_expense_act_category_on_office_saving_products' }

      t.timestamps
    end
  end
end
