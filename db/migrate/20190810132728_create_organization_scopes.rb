class CreateOrganizationScopes < ActiveRecord::Migration[5.2]
  def change
    create_table :organization_scopes, id: :uuid do |t|
      t.belongs_to :organization, foreign_key: true, type: :uuid
      t.references :account,      polymorphic: true, type: :uuid

      t.timestamps
    end
  end
end
