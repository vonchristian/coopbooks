class RemoveCooperativeFromBarangays < ActiveRecord::Migration[5.2]
  def change
    remove_reference :barangays, :cooperative, foreign_key: true, type: :uuid 
  end
end
