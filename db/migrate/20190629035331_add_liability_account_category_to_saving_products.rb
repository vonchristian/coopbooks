class AddLiabilityAccountCategoryToSavingProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :saving_products, :liability_account_category, foreign_key: { to_table: :account_categories }, type: :uuid
  end
end
