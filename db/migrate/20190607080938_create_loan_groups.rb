class CreateLoanGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :loan_groups, id: :uuid do |t|
      t.belongs_to :office, foreign_key: true, type: :uuid
      t.string  :title
      t.integer :start_num
      t.integer :end_num

      t.timestamps
    end
  end
end
