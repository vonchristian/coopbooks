class RenameParentAccountSubCategoriesToLevelTwoAccountCategories < ActiveRecord::Migration[6.0]
  def change
    rename_table :parent_account_sub_categories, :level_two_account_categories
  end
end
