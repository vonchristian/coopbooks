class AddLiabilityAccountToTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposits, :liability_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
