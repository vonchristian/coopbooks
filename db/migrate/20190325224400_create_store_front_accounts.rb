class CreateStoreFrontAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :store_front_accounts, id: :uuid do |t|
      t.references :customer, polymorphic: true, type: :uuid
      t.belongs_to :store_front, foreign_key: true, type: :uuid
      t.belongs_to :cooperative, foreign_key: true, type: :uuid
      t.string :account_number, index: true, unique: true

      t.timestamps
    end
  end
end
