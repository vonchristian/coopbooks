class RemoveBarangayFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loans, :barangay,     foreign_key: true, type: :uuid
    remove_reference :loans, :municipality, foreign_key: true, type: :uuid
    remove_reference :loans, :organization, foreign_key: true, type: :uuid
    remove_reference :loans, :street,       foreign_key: true, type: :uuid 

  end
end
