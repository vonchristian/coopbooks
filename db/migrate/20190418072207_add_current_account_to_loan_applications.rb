class AddCurrentAccountToLoanApplications < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_applications, :current_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
