class Remove2AccountFromPrograms < ActiveRecord::Migration[5.2]
  def change
    remove_reference :programs, :account, foreign_key: true, type: :uuid 

  end
end
