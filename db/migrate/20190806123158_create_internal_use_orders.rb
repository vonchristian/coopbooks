class CreateInternalUseOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :internal_use_orders, id: :uuid do |t|
      t.belongs_to :store_front, foreign_key: true, type: :uuid
      t.belongs_to :employee, foreign_key: { to_table: :users }, type: :uuid
      t.belongs_to :requesting_employee, foreign_key: { to_table: :users }, type: :uuid 
      t.datetime :date

      t.timestamps
    end
  end
end
