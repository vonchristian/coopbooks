class AddVariantToLineItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :line_items, :variant, foreign_key: true, type: :uuid 
  end
end
