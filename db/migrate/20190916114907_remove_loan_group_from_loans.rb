class RemoveLoanGroupFromLoans < ActiveRecord::Migration[6.0]
  def change
    remove_reference :loans, :loan_group, null: false, foreign_key: true, type: :uuid 
  end
end
