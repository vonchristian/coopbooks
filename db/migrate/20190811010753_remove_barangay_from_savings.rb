class RemoveBarangayFromSavings < ActiveRecord::Migration[5.2]
  def change
    remove_reference :savings, :barangay,     foreign_key: true, type: :uuid
    remove_reference :savings, :municipality, foreign_key: true, type: :uuid
    remove_reference :savings, :organization, foreign_key: true, type: :uuid
  end
end
