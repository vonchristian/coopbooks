class CreateMembershipCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :membership_categories, id: :uuid do |t|
      t.belongs_to :office,      foreign_key: true, type: :uuid
      t.belongs_to :cooperative, foreign_key: true, type: :uuid
      t.string :title

      t.timestamps
    end
  end
end
