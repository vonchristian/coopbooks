class RemoveEquityAccountFromShareCapitalProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :share_capital_products, :equity_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
