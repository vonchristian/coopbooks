class RemovePreparerFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loans, :preparer, foreign_key: { to_table: :users }, type: :uuid 
  end
end
