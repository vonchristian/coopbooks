class AddTemporaryAccountToOfficeShareCapitalProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :office_share_capital_products, :temporary_account, null: false, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
