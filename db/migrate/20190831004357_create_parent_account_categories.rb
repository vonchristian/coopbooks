class CreateParentAccountCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :parent_account_categories, id: :uuid  do |t|
      t.string :title
      t.string :code
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid
      t.boolean :contra
      t.string :type

      t.timestamps
    end
    add_index :parent_account_categories, :type
  end
end
