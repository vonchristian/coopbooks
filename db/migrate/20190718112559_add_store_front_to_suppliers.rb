class AddStoreFrontToSuppliers < ActiveRecord::Migration[5.2]
  def change
    add_reference :suppliers, :store_front, foreign_key: true, type: :uuid 
  end
end
