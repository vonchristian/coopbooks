class AddTemporaryAccountCategoryToSavingProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :saving_products, :temporary_account_category, foreign_key: { to_table: :account_categories }, type: :uuid 
    add_reference :saving_products, :closing_account_category, foreign_key: { to_table: :account_categories }, type: :uuid
  end
end
