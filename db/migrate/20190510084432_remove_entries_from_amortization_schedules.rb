class RemoveEntriesFromAmortizationSchedules < ActiveRecord::Migration[5.2]
  def change
    remove_column :amortization_schedules, :entry_ids, array: true
  end
end
