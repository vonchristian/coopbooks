class RenameAccountCategoriesToLevelThreeAccountCategories < ActiveRecord::Migration[6.0]
  def change
    rename_table :parent_account_categories, :level_three_account_categories
  end
end
