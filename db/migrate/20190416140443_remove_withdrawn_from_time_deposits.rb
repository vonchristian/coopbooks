class RemoveWithdrawnFromTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    remove_column :time_deposits, :withdrawn, :boolean
  end
end
