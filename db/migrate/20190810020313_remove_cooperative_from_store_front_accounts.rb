class RemoveCooperativeFromStoreFrontAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :store_front_accounts, :cooperative, foreign_key: true, type: :uuid 
  end
end
