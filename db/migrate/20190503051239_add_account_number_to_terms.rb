class AddAccountNumberToTerms < ActiveRecord::Migration[5.2]
  def change
    add_column :terms, :account_number, :string
    add_index :terms, :account_number, unique: true
  end
end
