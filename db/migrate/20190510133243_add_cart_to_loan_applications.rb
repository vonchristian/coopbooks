class AddCartToLoanApplications < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_applications, :cart, foreign_key: true, type: :uuid 
  end
end
