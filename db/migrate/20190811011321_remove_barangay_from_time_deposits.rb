class RemoveBarangayFromTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    remove_reference :time_deposits, :barangay,     foreign_key: true, type: :uuid
    remove_reference :time_deposits, :municipality, foreign_key: true, type: :uuid
    remove_reference :time_deposits, :organization, foreign_key: true, type: :uuid 
  end
end
