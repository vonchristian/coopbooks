class RemoveAttributesFromAmortizationSchedules < ActiveRecord::Migration[5.2]
  def change
    remove_reference :amortization_schedules, :credit_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :amortization_schedules, :debit_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :amortization_schedules, :commercial_document, polymorphic: true, type: :uuid
    remove_reference :amortization_schedules, :scheduleable, polymorphic: true, type: :uuid 
    remove_index :amortization_schedules, :schedule_type
    remove_column :amortization_schedules, :schedule_type, :integer
  end
end
