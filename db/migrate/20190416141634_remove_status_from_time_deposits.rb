class RemoveStatusFromTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    remove_column :time_deposits, :status, :integer
  end
end
