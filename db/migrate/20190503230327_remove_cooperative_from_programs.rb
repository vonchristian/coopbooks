class RemoveCooperativeFromPrograms < ActiveRecord::Migration[5.2]
  def change
    remove_reference :programs, :cooperative, foreign_key: true, type: :uuid 
  end
end
