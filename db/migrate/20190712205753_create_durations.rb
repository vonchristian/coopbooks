class CreateDurations < ActiveRecord::Migration[5.2]
  def change
    create_table :durations, id: :uuid do |t|
      t.belongs_to :term, foreign_key: true, type: :uuid
      t.references :termable, polymorphic: true, type: :uuid

      t.timestamps
    end
  end
end
