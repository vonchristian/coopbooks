class CreateAssets < ActiveRecord::Migration[5.2]
  def change
    create_table :assets, id: :uuid do |t|
      t.string :name
      t.belongs_to :account, foreign_key: true, type: :uuid
      t.belongs_to :cooperative, foreign_key: true, type: :uuid
      t.belongs_to :office, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
