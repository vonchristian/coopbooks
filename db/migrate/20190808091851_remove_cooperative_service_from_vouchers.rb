class RemoveCooperativeServiceFromVouchers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :vouchers, :cooperative_service, foreign_key: true, type: :uuid 
  end
end
