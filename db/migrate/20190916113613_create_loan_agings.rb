class CreateLoanAgings < ActiveRecord::Migration[6.0]
  def change
    create_table :loan_agings, id: :uuid do |t|
      t.belongs_to :loan, null: false, foreign_key: true, type: :uuid
      t.belongs_to :loan_group, null: false, foreign_key: true, type: :uuid 
      t.datetime :date

      t.timestamps
    end
  end
end
