class RemoveStoreFrontFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :users, :store_front, foreign_key: true, type: :uuid 
  end
end
