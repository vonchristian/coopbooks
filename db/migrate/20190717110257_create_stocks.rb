class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks, id: :uuid do |t|
      t.belongs_to :product, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
