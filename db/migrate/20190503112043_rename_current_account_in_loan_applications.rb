class RenameCurrentAccountInLoanApplications < ActiveRecord::Migration[5.2]
  def change
    rename_column :loan_applications, :current_account_id, :receivable_account_id
  end
end
