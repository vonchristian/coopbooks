class Add2interestRateToLoanApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :loan_applications, :interest_rate, :decimal
  end
end
