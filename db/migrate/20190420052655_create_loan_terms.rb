class CreateLoanTerms < ActiveRecord::Migration[5.2]
  def change
    create_table :loan_terms, id: :uuid do |t|
      t.belongs_to :loan, foreign_key: true, type: :uuid
      t.belongs_to :term, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
