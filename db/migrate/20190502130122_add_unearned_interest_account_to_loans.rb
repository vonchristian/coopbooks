class AddUnearnedInterestAccountToLoans < ActiveRecord::Migration[5.2]
  def change
    add_reference :loans, :unearned_interest_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
