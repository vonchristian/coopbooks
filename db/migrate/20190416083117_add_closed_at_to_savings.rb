class AddClosedAtToSavings < ActiveRecord::Migration[5.2]
  def change
    add_column :savings, :closed_at, :datetime
  end
end
