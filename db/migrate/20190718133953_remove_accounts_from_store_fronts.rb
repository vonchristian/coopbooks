class RemoveAccountsFromStoreFronts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :store_fronts, :cost_of_goods_sold_account,    foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :payable_account,               foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :merchandise_inventory_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :sales_account,                 foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :sales_return_account,          foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :spoilage_account,              foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :sales_discount_account,        foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :purchase_return_account,       foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :internal_use_account,          foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :store_fronts, :cooperative_service,           type: :uuid
  end
end
