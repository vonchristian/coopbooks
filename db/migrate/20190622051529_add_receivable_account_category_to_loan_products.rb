class AddReceivableAccountCategoryToLoanProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_products, :receivable_account_category, foreign_key: { to_table: :account_categories }, type: :uuid 
  end
end
