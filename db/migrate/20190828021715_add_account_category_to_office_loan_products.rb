class AddAccountCategoryToOfficeLoanProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :office_loan_products, :receivable_account_category, foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :office_loan_products, :interest_revenue_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_int_revenue_acct_category_on_office_loan_products' }
    add_reference :office_loan_products, :penalty_revenue_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_penalty_revenue_acct_category_on_office_loan_products' }
    add_reference :office_loan_products, :temporary_account, foreign_key: { to_table: :accounts }, type: :uuid
    add_reference :office_loan_products, :loan_protection_plan_provider, foreign_key: true, type: :uuid
    add_reference :office_loan_products, :amortization_type,foreign_key: true, type: :uuid
  end
end
