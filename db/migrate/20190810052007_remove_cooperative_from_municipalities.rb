class RemoveCooperativeFromMunicipalities < ActiveRecord::Migration[5.2]
  def change
    remove_reference :municipalities, :cooperative, foreign_key: true, type: :uuid 
  end
end
