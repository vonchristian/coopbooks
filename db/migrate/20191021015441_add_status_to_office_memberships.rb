class AddStatusToOfficeMemberships < ActiveRecord::Migration[6.0]
  def change
    add_column :office_memberships, :status, :integer
    add_index :office_memberships, :status
  end
end
