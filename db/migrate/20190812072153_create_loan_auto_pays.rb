class CreateLoanAutoPays < ActiveRecord::Migration[5.2]
  def change
    create_table :loan_auto_pays, id: :uuid do |t|
      t.belongs_to :loan, foreign_key: true, type: :uuid
      t.belongs_to :savings_account, foreign_key: { to_table: :savings }, type: :uuid 
      t.boolean :active

      t.timestamps
    end
  end
end
