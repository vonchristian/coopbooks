class RemoveCooperativeFromSuppliers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :suppliers, :cooperative, foreign_key: true, type: :uuid
  end
end
