class RemoveOfficeFromMemberships < ActiveRecord::Migration[5.2]
  def change
    remove_reference :memberships, :office, foreign_key: true, type: :uuid 
  end
end
