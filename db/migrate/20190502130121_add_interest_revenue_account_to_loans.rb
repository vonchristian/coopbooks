class AddInterestRevenueAccountToLoans < ActiveRecord::Migration[5.2]
  def change
    add_reference :loans, :interest_revenue_account, foreign_key: { to_table: :accounts }, type: :uuid
    add_reference :loans, :penalty_revenue_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
