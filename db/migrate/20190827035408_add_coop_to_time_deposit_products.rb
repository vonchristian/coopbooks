class AddCoopToTimeDepositProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :time_deposit_products, :cooperative,  foreign_key: true, type: :uuid 
  end
end
