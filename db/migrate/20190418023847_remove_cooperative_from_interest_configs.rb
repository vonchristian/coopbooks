class RemoveCooperativeFromInterestConfigs < ActiveRecord::Migration[5.2]
  def change
    remove_reference :interest_configs, :cooperative, foreign_key: true, type: :uuid
  end
end
