class AddPayableAccountToLoanProtectionPlanProviders < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_protection_plan_providers, :payable_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
