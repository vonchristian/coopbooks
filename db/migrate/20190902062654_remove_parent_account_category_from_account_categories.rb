class RemoveParentAccountCategoryFromAccountCategories < ActiveRecord::Migration[6.0]
  def change
    remove_reference :account_categories, :parent_account_category, null: false, foreign_key: true, type: :uuid 
  end
end
