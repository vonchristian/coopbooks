class RemoveActiveFromAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_column :accounts, :active, :boolean
  end
end
