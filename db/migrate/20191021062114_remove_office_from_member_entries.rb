class RemoveOfficeFromMemberEntries < ActiveRecord::Migration[6.0]
  def change
    remove_reference :member_entries, :office, null: false, foreign_key: true, type: :uuid 
  end
end
