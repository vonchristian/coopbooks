class RemoveAccountCategoryFromPrograms < ActiveRecord::Migration[5.2]
  def change
    remove_reference :programs, :account_category, foreign_key: true, type: :uuid
    remove_reference :programs, :office, foreign_key: true, type: :uuid 
  end
end
