class AddGrandParentAccountCategoryToParentAccountCategories < ActiveRecord::Migration[6.0]
  def change
    add_reference :parent_account_categories, :grand_parent_account_category, null: false, foreign_key: true, type: :uuid, index: { name: 'index_grand_parent_act_categories_on_parent_act_categories' }
  end
end
