class RemoveLedgerAccountFromAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_column :accounts, :ledger_account, :boolean
  end
end
