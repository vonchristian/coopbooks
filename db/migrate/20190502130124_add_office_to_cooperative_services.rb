class AddOfficeToCooperativeServices < ActiveRecord::Migration[5.2]
  def change
    add_reference :cooperative_services, :office, foreign_key: true, type: :uuid
  end
end
