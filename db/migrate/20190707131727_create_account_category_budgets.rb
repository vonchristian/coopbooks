class CreateAccountCategoryBudgets < ActiveRecord::Migration[5.2]
  def change
    create_table :account_category_budgets, id: :uuid do |t|
      t.belongs_to :account_category, foreign_key: true, type: :uuid 
      t.integer :year
      t.decimal :amount

      t.timestamps
    end
  end
end
