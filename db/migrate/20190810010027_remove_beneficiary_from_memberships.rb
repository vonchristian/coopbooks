class RemoveBeneficiaryFromMemberships < ActiveRecord::Migration[5.2]
  def change
    remove_reference :memberships, :beneficiary, polymorphic: true,  type: :uuid
    remove_column :memberships, :application_date, :datetime
    remove_column :memberships, :approval_date, :datetime
    remove_column :memberships, :search_term, :string
  end
end
