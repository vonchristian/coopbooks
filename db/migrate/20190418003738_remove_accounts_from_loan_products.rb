class RemoveAccountsFromLoanProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_products, :current_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :loan_products, :past_due_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :loan_products, :restructured_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :loan_products, :litigation_account, foreign_key: { to_table: :accounts }, type: :uuid
  end
end
