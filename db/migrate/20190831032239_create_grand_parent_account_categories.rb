class CreateGrandParentAccountCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :grand_parent_account_categories, id: :uuid do |t|
      t.string :code
      t.string :title
      t.boolean :contra
      t.belongs_to :cooperative, null: false, foreign_key: true, type: :uuid
      t.string :type

      t.timestamps
    end
    add_index :grand_parent_account_categories, :type
  end
end
