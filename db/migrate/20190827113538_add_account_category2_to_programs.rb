class AddAccountCategory2ToPrograms < ActiveRecord::Migration[6.0]
  def change
    add_reference :programs, :account_category, foreign_key: true, type: :uuid 
  end
end
