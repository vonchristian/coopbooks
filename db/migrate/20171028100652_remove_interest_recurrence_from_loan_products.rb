class RemoveInterestRecurrenceFromLoanProducts < ActiveRecord::Migration[5.1]
  def change
    remove_index :loan_products, :interest_posting
    remove_column :loan_products, :interest_posting, :integer
  end
end
