class RemoveAudits < ActiveRecord::Migration[6.0]
  def change
    def up
    drop_table :audits
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
  end
end
