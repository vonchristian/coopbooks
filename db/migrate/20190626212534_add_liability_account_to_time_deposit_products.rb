class AddLiabilityAccountToTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_products, :liability_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
