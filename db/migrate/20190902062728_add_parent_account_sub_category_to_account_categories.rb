class AddParentAccountSubCategoryToAccountCategories < ActiveRecord::Migration[6.0]
  def change
    add_reference :account_categories, :parent_account_sub_category, foreign_key: true, type: :uuid 
  end
end
