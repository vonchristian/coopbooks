class AddInterestRevenueAccountCategoryToLoanProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_products, :interest_revenue_account_category, foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :loan_products, :penalty_revenue_account_category, foreign_key: { to_table: :account_categories }, type: :uuid 
  end
end
