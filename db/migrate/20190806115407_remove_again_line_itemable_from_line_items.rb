class RemoveAgainLineItemableFromLineItems < ActiveRecord::Migration[5.2]
  def change
    remove_reference :line_items, :line_itemable, polymorphic: true
    remove_reference :line_items, :order, foreign_key: true
    remove_reference :line_items, :referenced_line_item, foreign_key: { to_table: :line_items }
    remove_column :line_items, :purchase_line_item_id, :integer
    remove_column :line_items, :sales_line_item_id, :integer
    remove_column :line_items, :expiry_date, :datetime
    remove_reference :line_items, :variant, foreign_key: true


  end
end
