class AddLoanGroupToLoans < ActiveRecord::Migration[5.2]
  def change
    add_reference :loans, :loan_group, foreign_key: true, type: :uuid
  end
end
