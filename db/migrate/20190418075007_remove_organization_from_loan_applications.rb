class RemoveOrganizationFromLoanApplications < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_applications, :organization, foreign_key: true, type: :uuid 
  end
end
