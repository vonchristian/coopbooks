class CreateStockTransferOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :stock_transfer_orders, id: :uuid do |t|
      t.belongs_to :employee, foreign_key: { to_table: :users }, type: :uuid
      t.belongs_to :voucher, foreign_key: true, type: :uuid
      t.belongs_to :origin_store_front, foreign_key: { to_table: :store_fronts }, type: :uuid
      t.belongs_to :destination_store_front, foreign_key: { to_table: :store_fronts }, type: :uuid
      t.datetime :date

      t.timestamps
    end
  end
end
