class AddAccountNumberToSalesOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :sales_orders, :account_number, :string
  end
end
