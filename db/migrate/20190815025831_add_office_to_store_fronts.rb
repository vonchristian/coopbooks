class AddOfficeToStoreFronts < ActiveRecord::Migration[5.2]
  def change
    add_reference :store_fronts, :office, foreign_key: true, type: :uuid 
  end
end
