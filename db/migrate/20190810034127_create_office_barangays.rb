class CreateOfficeBarangays < ActiveRecord::Migration[5.2]
  def change
    create_table :office_barangays, id: :uuid do |t|
      t.belongs_to :office, foreign_key: true, type: :uuid
      t.belongs_to :barangay, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
