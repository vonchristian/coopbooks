class RemoveCooperativeServiceFromEntries < ActiveRecord::Migration[5.2]
  def change
    remove_reference :entries, :cooperative_service, foreign_key: true, type: :uuid 
  end
end
