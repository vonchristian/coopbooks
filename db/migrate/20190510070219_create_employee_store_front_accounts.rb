class CreateEmployeeStoreFrontAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :employee_store_front_accounts, id: :uuid do |t|
      t.belongs_to :employee,    foreign_key: { to_table: :users }, type: :uuid
      t.belongs_to :store_front, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
