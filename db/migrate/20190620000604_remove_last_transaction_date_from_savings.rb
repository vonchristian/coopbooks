class RemoveLastTransactionDateFromSavings < ActiveRecord::Migration[5.2]
  def change
    remove_column :savings, :last_transaction_date, :date
  end
end
