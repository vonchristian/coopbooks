class AddVoucherToSalesOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :sales_orders, :voucher, foreign_key: true, type: :uuid 
  end
end
