class RenameArchivingDateToArchivedAtInLoans < ActiveRecord::Migration[5.2]
  def change
    rename_column :loans, :archiving_date, :archived_at
  end
end
