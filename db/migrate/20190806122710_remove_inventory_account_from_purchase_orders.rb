class RemoveInventoryAccountFromPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :purchase_orders, :inventory_account, foreign_key: { to_table: :accounts }, type: :uuid
    remove_reference :purchase_orders, :payable_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
