class CreateOrganizationMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :organization_memberships, id: :uuid do |t|
      t.belongs_to :organization, foreign_key: true, type: :uuid
      t.references :cooperator, polymorphic: true, type: :uuid, index: { name: 'index_cooperator_on_organization_memberships' } 

      t.timestamps
    end
  end
end
