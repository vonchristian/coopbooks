class RemoveInterestRevenueAccountFromInterestConfigs < ActiveRecord::Migration[5.2]
  def change
    remove_reference :interest_configs, :interest_revenue_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
