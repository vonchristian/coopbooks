class RemoveAnnualInterestRateFromLoanApplications < ActiveRecord::Migration[5.2]
  def change
    remove_column :loan_applications, :annual_interest_rate, :decimal
  end
end
