class AddOfficeToLoanProtectionPlanProviders < ActiveRecord::Migration[5.2]
  def change
    add_reference :loan_protection_plan_providers, :office, foreign_key: true, type: :uuid
  end
end
