class AddOffice2ToTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_products, :office, foreign_key: true, type: :uuid 
  end
end
