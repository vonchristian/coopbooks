class AddTimeToEntries < ActiveRecord::Migration[5.2]
  def change
    add_column :entries, :entry_time, :time
  end
end
