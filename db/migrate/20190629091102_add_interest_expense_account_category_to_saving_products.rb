class AddInterestExpenseAccountCategoryToSavingProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :saving_products, :interest_expense_account_category, foreign_key: { to_table: :account_categories }, type: :uuid 
  end
end
