class RemoveTermableFromTerms < ActiveRecord::Migration[5.2]
  def change
    remove_reference :terms, :termable, polymorphic: true, type: :uuid 
  end
end
