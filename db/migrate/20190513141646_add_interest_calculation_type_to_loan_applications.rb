class AddInterestCalculationTypeToLoanApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :loan_applications, :interest_calculation_type, :integer
    add_index :loan_applications, :interest_calculation_type
  end
end
