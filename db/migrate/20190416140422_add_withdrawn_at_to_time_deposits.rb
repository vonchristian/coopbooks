class AddWithdrawnAtToTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    add_column :time_deposits, :withdrawn_at, :datetime
  end
end
