class AddOfficeToTimeDepositApplications < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_applications, :office, foreign_key: true, type: :uuid
  end
end
