class RemoveAccountsPayableFromLoanProtectionPlanProviders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_protection_plan_providers, :accounts_payable, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
