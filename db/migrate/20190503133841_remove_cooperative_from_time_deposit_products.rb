class RemoveCooperativeFromTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :time_deposit_products, :cooperative, foreign_key: true, type: :uuid 
  end
end
