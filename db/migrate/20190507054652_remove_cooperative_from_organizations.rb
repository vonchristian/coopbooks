class RemoveCooperativeFromOrganizations < ActiveRecord::Migration[5.2]
  def change
    remove_reference :organizations, :cooperative, foreign_key: true, type: :uuid 
  end
end
