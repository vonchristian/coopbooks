class CreateGuaranteedLoans < ActiveRecord::Migration[6.0]
  def change
    create_table :guaranteed_loans, id: :uuid do |t|
      t.references :guarantor, polymorphic: true, null: false, type: :uuid
      t.belongs_to :loan,      null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
