class CreatePurchasePrices < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_prices, id: :uuid do |t|
      t.belongs_to :variant, foreign_key: true, type: :uuid
      t.belongs_to :store_front, foreign_key: true, type: :uuid 
      t.decimal :price
      t.datetime :effectivity_date

      t.timestamps
    end
  end
end
