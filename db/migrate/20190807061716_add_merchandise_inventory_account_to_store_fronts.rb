class AddMerchandiseInventoryAccountToStoreFronts < ActiveRecord::Migration[5.2]
  def change
    add_reference :store_fronts, :merchandise_inventory_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
