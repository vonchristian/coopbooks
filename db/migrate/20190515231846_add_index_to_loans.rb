class AddIndexToLoans < ActiveRecord::Migration[5.2]
  def change
    add_index :loans, :last_transaction_date
    add_index :loans, :borrower_full_name
  end
end
