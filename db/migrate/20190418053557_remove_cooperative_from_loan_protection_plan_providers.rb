class RemoveCooperativeFromLoanProtectionPlanProviders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_protection_plan_providers, :cooperative, foreign_key: true, type: :uuid 
  end
end
