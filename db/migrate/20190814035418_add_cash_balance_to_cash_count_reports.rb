class AddCashBalanceToCashCountReports < ActiveRecord::Migration[5.2]
  def change
    add_column :cash_count_reports, :cash_balance, :decimal
  end
end
