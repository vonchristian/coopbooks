class IndexTokenOnVouchers < ActiveRecord::Migration[5.2]
  def change
    change_column :vouchers, :token, :string, unique: true

  end
end
