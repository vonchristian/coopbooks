class AddSupplierToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :purchase_orders, :supplier, polymorphic: true, type: :uuid 
  end
end
