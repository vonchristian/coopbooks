class RemoveUniqueIndexInAccounts < ActiveRecord::Migration[6.0]
  def change
    remove_index :accounts, :name
  end
end
