class RemoveTimestampsFromPgSearchDocuments < ActiveRecord::Migration[5.2]
  def change
    remove_column :pg_search_documents, :created_at, :datetime
    remove_column :pg_search_documents, :updated_at, :datetime
  end
end
