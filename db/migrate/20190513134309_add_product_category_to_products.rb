class AddProductCategoryToProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :products, :product_category, foreign_key: true, type: :uuid 
  end
end
