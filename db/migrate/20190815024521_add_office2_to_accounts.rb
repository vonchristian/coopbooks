class AddOffice2ToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_reference :accounts, :office, foreign_key: true, type: :uuid
  end
end
