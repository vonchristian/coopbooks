class RemoveCategoryTypeFromAccountCategories < ActiveRecord::Migration[5.2]
  def change
    remove_column :account_categories, :category_type, :integer
  end
end
