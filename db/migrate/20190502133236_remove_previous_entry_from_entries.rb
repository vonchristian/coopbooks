class RemovePreviousEntryFromEntries < ActiveRecord::Migration[5.2]
  def change
    remove_reference :entries, :previous_entry, foreign_key: { to_table: :entries }, type: :uuid
  end
end
