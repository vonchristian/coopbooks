class AddNoInterestToSavingProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :saving_products, :no_interest, :boolean, default: :false
  end
end
