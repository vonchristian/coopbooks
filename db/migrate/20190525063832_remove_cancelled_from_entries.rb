class RemoveCancelledFromEntries < ActiveRecord::Migration[5.2]
  def change
    remove_column :entries, :cancelled, :boolean
  end
end
