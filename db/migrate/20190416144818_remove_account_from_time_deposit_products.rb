class RemoveAccountFromTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :time_deposit_products, :account, foreign_key: true, type: :uuid
  end
end
