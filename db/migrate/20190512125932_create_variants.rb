class CreateVariants < ActiveRecord::Migration[5.2]
  def change
    create_table :variants, id: :uuid do |t|
      t.belongs_to :product, foreign_key: true, type: :uuid 
      t.boolean :is_master, default: false

      t.timestamps
    end
  end
end
