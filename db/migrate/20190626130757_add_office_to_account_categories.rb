class AddOfficeToAccountCategories < ActiveRecord::Migration[5.2]
  def change
    add_reference :account_categories, :office, foreign_key: true, type: :uuid 
  end
end
