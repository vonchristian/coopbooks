class AddExpiryDateToStocks < ActiveRecord::Migration[5.2]
  def change
    add_column :stocks, :expiry_date, :datetime
  end
end
