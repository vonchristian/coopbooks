class AddTemporaryAccountToSavingProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :saving_products, :temporary_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
