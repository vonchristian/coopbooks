class RemoveParentAccountSubCategoryIdInAccountCategories < ActiveRecord::Migration[6.0]
  def change
    remove_column :account_categories, :parent_account_sub_category_id, foreign_key: true
  end
end
