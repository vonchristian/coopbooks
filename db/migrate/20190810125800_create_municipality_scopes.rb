class CreateMunicipalityScopes < ActiveRecord::Migration[5.2]
  def change
    create_table :municipality_scopes, id: :uuid do |t|
      t.belongs_to :municipality, foreign_key: true, type: :uuid
      t.references :account, polymorphic: true, type: :uuid

      t.timestamps
    end
  end
end
