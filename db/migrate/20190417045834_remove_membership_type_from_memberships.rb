class RemoveMembershipTypeFromMemberships < ActiveRecord::Migration[5.2]
  def change
    remove_index :memberships, :membership_type
    remove_column :memberships, :membership_type, :integer
  end
end
