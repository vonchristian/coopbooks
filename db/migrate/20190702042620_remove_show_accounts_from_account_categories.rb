class RemoveShowAccountsFromAccountCategories < ActiveRecord::Migration[5.2]
  def change
    remove_column :account_categories, :show_accounts, :boolean
  end
end
