class AddVariantToSellingPrices < ActiveRecord::Migration[5.2]
  def change
    add_reference :selling_prices, :variant, foreign_key: true, type: :uuid 
  end
end
