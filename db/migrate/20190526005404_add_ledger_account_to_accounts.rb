class AddLedgerAccountToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :ledger_account, :boolean, default: false
  end
end
