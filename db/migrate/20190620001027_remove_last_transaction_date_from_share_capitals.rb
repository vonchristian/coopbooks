class RemoveLastTransactionDateFromShareCapitals < ActiveRecord::Migration[5.2]
  def change
    remove_column :share_capitals, :last_transaction_date, :date
  end
end
