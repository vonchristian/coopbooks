class AddAccountCategoriesToStoreFronts < ActiveRecord::Migration[5.2]
  def change
    add_reference :store_fronts, :inventory_category,         foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :sales_category,             foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :sales_discount_category,    foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :sales_return_category,      foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :cost_of_goods_sold_category,foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :internal_use_category,      foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :purchase_return_category,   foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :spoilage_category,          foreign_key: { to_table: :account_categories }, type: :uuid
    add_reference :store_fronts, :receivable_category,        foreign_key: { to_table: :account_categories }, type: :uuid
  end
end
