class AddOfficeLoanProductToLoanProductCharges < ActiveRecord::Migration[6.0]
  def change
    add_reference :loan_product_charges, :office_loan_product, foreign_key: true, type: :uuid
  end
end
