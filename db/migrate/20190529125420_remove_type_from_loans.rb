class RemoveTypeFromLoans < ActiveRecord::Migration[5.2]
  def change
    remove_index :loans, :type
    remove_column :loans, :type, :string
  end
end
