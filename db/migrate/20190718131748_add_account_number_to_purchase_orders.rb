class AddAccountNumberToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :account_number, :string
    add_index :purchase_orders, :account_number, unique: true
  end
end
