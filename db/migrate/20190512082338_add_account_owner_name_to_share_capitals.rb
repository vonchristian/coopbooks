class AddAccountOwnerNameToShareCapitals < ActiveRecord::Migration[5.2]
  def change
    add_column :share_capitals, :account_owner_name, :string
  end
end
