class AddOfficeToProgramSubscriptions < ActiveRecord::Migration[5.2]
  def change
    add_reference :program_subscriptions, :office, foreign_key: true, type: :uuid
  end
end
