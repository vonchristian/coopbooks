class RemovePenaltyRevenueAccountFromPenaltyConfigs < ActiveRecord::Migration[5.2]
  def change
    remove_reference :penalty_configs, :penalty_revenue_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
