class CreatePurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_orders, id: :uuid do |t|
      t.datetime   :date
      t.belongs_to :voucher,           foreign_key: true,                    type: :uuid 
      t.belongs_to :employee,          foreign_key: { to_table: :users },    type: :uuid
      t.belongs_to :store_front,       foreign_key: true,                    type: :uuid
      t.belongs_to :supplier,          foreign_key: true,                    type: :uuid
      t.belongs_to :payable_account,   foreign_key: { to_table: :accounts }, type: :uuid
      t.belongs_to :inventory_account, foreign_key: { to_table: :accounts }, type: :uuid

      t.timestamps
    end
  end
end
