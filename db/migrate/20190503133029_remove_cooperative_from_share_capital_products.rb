class RemoveCooperativeFromShareCapitalProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :share_capital_products, :cooperative, foreign_key: true, type: :uuid 
  end
end
