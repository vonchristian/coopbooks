class RemoveCancelledFromLoanApplications < ActiveRecord::Migration[5.2]
  def change
    remove_column :loan_applications, :cancelled, :boolean
  end
end
