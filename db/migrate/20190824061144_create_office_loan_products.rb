class CreateOfficeLoanProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :office_loan_products do |t|
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid
      t.belongs_to :loan_product, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
