class AddMunicipalityToShareCapitals < ActiveRecord::Migration[5.2]
  def change
    add_reference :share_capitals, :municipality, foreign_key: true, type: :uuid 
  end
end
