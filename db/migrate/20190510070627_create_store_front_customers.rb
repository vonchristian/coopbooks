class CreateStoreFrontCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :store_front_customers, id: :uuid do |t|
      t.belongs_to :store_front, foreign_key: true, type: :uuid
      t.references :customer, polymorphic: true, type: :uuid 

      t.timestamps
    end
  end
end
