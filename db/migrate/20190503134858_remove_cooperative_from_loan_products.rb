class RemoveCooperativeFromLoanProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_products, :cooperative, foreign_key: true, type: :uuid 
  end
end
