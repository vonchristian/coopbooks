class AddInterestExpenseAccountToTimeDeposits < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposits, :interest_expense_account, foreign_key: { to_table: :accounts }, type: :uuid 
  end
end
