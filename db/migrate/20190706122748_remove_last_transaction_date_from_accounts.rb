class RemoveLastTransactionDateFromAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_column :accounts, :last_transaction_date, :datetime
  end
end
