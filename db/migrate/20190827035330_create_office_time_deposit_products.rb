class CreateOfficeTimeDepositProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :office_time_deposit_products do |t|
      t.belongs_to :interest_expense_account_category, null: false, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_expense_acct_category_on_office_time_deposit_products' }
      t.belongs_to :break_contract_account_category, null: false, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_break_acct_category_on_office_time_deposit_products' }
      t.belongs_to :liability_account_category, null: false, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_liability_acct_category_on_office_time_deposit_products' }
      t.belongs_to :office, foreign_key: true, null: false, type: :uuid
      t.belongs_to :time_deposit_product, foreign_key: true, null: false, type: :uuid

      t.timestamps
    end
  end
end
