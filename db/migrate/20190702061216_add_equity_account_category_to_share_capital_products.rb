class AddEquityAccountCategoryToShareCapitalProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :share_capital_products, :equity_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_equity_account_categories_on_share_cap_products' }
    add_reference :share_capital_products, :interest_payable_account_category, foreign_key: { to_table: :account_categories }, type: :uuid, index: { name: 'index_int_payable_account_categories_on_share_cap_products' }
  end
end
