class AddClosingAccountCategoryInOfficeSavingProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :office_saving_products, :closing_account_category, foreign_key: { to_table: :account_categories }, type: :uuid 
  end
end
