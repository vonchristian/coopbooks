class AddInterestPredeductionRateToLoanApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :loan_applications, :interest_prededuction_rate, :decimal, default: 0
  end
end
