class RemoveOfficeFromTimeDepositProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :time_deposit_products, :office, foreign_key: true, type: :uuid
  end
end
