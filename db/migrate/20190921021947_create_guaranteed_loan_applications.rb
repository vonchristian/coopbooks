class CreateGuaranteedLoanApplications < ActiveRecord::Migration[6.0]
  def change
    create_table :guaranteed_loan_applications, id: :uuid do |t|
      t.belongs_to :loan_application, null: false, foreign_key: true, type: :uuid
      t.references :guarantor, polymorphic: true, null: false, type: :uuid, index: { name: 'index_guarantor_on_guaranteed_loan_applications' }

      t.timestamps
    end
  end
end
