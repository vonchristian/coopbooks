class AddCoopToShareCapitalProducts < ActiveRecord::Migration[6.0]
  def change
    add_reference :share_capital_products, :cooperative, foreign_key: true, type: :uuid 
  end
end
