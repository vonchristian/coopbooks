class RenameAccountNameNShareCapitals < ActiveRecord::Migration[5.2]
  def change
    rename_column :share_capitals, :account_owner_name, :account_name

  end
end
