class AddOfficeToOrganizations < ActiveRecord::Migration[5.2]
  def change
    add_reference :organizations, :office, foreign_key: true, type: :uuid 
  end
end
