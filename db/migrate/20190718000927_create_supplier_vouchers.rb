class CreateSupplierVouchers < ActiveRecord::Migration[5.2]
  def change
    create_table :supplier_vouchers, id: :uuid do |t|
      t.belongs_to :supplier, foreign_key: true, type: :uuid
      t.belongs_to :voucher, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
