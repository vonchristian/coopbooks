class AddStoreFrontToStocks < ActiveRecord::Migration[5.2]
  def change
    add_reference :stocks, :store_front, foreign_key: true, type: :uuid
    add_column :stocks, :barcode, :string 
  end
end
