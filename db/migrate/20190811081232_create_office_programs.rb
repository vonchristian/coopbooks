class CreateOfficePrograms < ActiveRecord::Migration[5.2]
  def change
    create_table :office_programs, id: :uuid do |t|
      t.belongs_to :program, foreign_key: true, type: :uuid
      t.belongs_to :office, foreign_key: true, type: :uuid
      t.belongs_to :account_category, foreign_key: true, type: :uuid 

      t.timestamps
    end
  end
end
