class CreateParentAccountSubCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :parent_account_sub_categories, id: :uuid do |t|
      t.string :code
      t.string :title
      t.boolean :contra
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid
      t.belongs_to :parent_account_category, null: false, foreign_key: true, type: :uuid, index: { name: 'index_parent_account_category_on_parent_account_sub_categories' }
      t.string :type

      t.timestamps
    end
    add_index :parent_account_sub_categories, :type
  end
end
