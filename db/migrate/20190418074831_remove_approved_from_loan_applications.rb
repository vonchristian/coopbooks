class RemoveApprovedFromLoanApplications < ActiveRecord::Migration[5.2]
  def change
    remove_column :loan_applications, :approved, :boolean
  end
end
