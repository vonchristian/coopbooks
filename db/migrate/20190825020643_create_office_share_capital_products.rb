class CreateOfficeShareCapitalProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :office_share_capital_products do |t|
      t.belongs_to :office, null: false, foreign_key: true, type: :uuid
      t.belongs_to :share_capital_product, null: false, foreign_key: true, type: :uuid
      t.belongs_to :equity_account_category, foreign_key: { to_table: :account_categories }, null: false, type: :uuid, index: { name: 'index_equity_account_category_on_office_share_capital_products' }
      t.belongs_to :interest_payable_account_category, foreign_key: { to_table: :account_categories }, null: false, type: :uuid, index: { name: 'index_int_payable_act_category_on_office_share_capital_products' }

      t.timestamps
    end
  end
end
