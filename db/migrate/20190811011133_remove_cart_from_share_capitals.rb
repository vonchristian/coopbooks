class RemoveCartFromShareCapitals < ActiveRecord::Migration[5.2]
  def change
    remove_reference :share_capitals, :cart, foreign_key: true, type: :uuid 
    remove_column :share_capitals, :status, :boolean
    remove_column :share_capitals, :beneficiaries, :string
  end
end
