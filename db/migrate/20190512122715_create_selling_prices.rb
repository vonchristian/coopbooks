class CreateSellingPrices < ActiveRecord::Migration[5.2]
  def change
    create_table :selling_prices, id: :uuid do |t|
      t.decimal :price
      t.datetime :effectivity_date
      t.belongs_to :store_front, foreign_key: true, type: :uuid
      
      t.timestamps
    end
  end
end
