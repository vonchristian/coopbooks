class RemoveOfficeFromLoanProducts < ActiveRecord::Migration[5.2]
  def change
    remove_reference :loan_products, :office, foreign_key: true
  end
end
