class AddCooperativeIdToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_reference :accounts, :cooperative, foreign_key: true, type: :uuid
  end
end
