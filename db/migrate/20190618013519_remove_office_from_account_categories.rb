class RemoveOfficeFromAccountCategories < ActiveRecord::Migration[5.2]
  def change
    remove_reference :account_categories, :office, foreign_key: true, type: :uuid
  end
end
