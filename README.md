
# Core Banking Platform for Cooperatives

This project integrates the services of a cooperative in a single platform so that managers can see how their cooperative is doing financially in real-time.

These services are included:

* Loans Module
* Accounting Module
* Cooperative Services Module
* Members Registry Module
* Program Subscriptions Module
* Warehouse Module
