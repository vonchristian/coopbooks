require 'rails_helper'

module Insights
  module Members
    describe ProvinceCounter do
      it '#count' do
        province    = create(:province)
        province_2  = create(:province)
        cooperative = create(:cooperative)
        member      = create(:member)
        member_2    = create(:member)
        create(:address, addressable: member, province: province)
        create(:address, addressable: member_2, province: province)
        create(:membership, cooperator: member, cooperative: cooperative)
        create(:membership, cooperator: member_2, cooperative: cooperative)


        expect(described_class.new(province: province, cooperative: cooperative).count).to eql 2
      end
    end
  end
end
