require 'rails_helper'

module SavingsModule
  describe AverageDailyBalance do
    it 'averaged_balance' do
      from_date = Date.current.beginning_of_year
      to_date   = Date.current.beginning_of_year.end_of_quarter
      saving    = create(:saving)
      cash      = create(:asset)

      entry = build(:entry, entry_date: Date.current.beginning_of_year)
      entry.debit_amounts.build(account: cash, amount: 1_000)
      entry.credit_amounts.build(account: saving.liability_account, amount: 1_000)
      entry.save!

      entry = build(:entry, entry_date: Date.current.beginning_of_year + 10.days)
      entry.debit_amounts.build(account: cash, amount: 1_000)
      entry.credit_amounts.build(account: saving.liability_account, amount: 1_000)
      entry.save!

      entry = build(:entry, entry_date: Date.current.beginning_of_year.end_of_month)
      entry.debit_amounts.build(account: saving.liability_account, amount: 1_000)
      entry.credit_amounts.build(account: cash, amount: 1_000)
      entry.save!

      entry = build(:entry, entry_date: Date.current.beginning_of_year + 20.days)
      entry.debit_amounts.build(account: cash, amount: 1_000)
      entry.credit_amounts.build(account: saving.liability_account, amount: 1_000)
      entry.save!

      entry = build(:entry, entry_date: Date.current.beginning_of_year.end_of_month)
      entry.debit_amounts.build(account: cash, amount: 1_000)
      entry.credit_amounts.build(account: saving.liability_account, amount: 1_000)
      entry.save!

      balance = described_class.new(from_date: from_date, to_date: to_date, saving: saving).averaged_balance.to_f.floor(2)

      expect(balance).to eql 2_666.66
    end

  end
end
