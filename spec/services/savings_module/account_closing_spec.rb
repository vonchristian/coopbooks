require 'rails_helper'

module SavingsModule
  describe AccountClosing do
    it '#close_account!' do
      savings_account = create(:saving, archived: false, archived_at: nil)
      described_class.new(savings_account: savings_account).close_account!

      expect(savings_account.archived).to eql true
      expect(savings_account.archived_at).to_not eql nil
    end
  end
end
