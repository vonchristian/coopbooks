require 'rails_helper'

module SavingsModule
  module BalanceAveragers
    describe Quarterly do
      it "#averaged_balance" do
        saving_product = create(:saving_product, minimum_balance: 1000)
        saving         = create(:saving, saving_product: saving_product)
        cash_account   = create(:asset)
        cooperative    = create(:cooperative)
        office         = create(:office, cooperative: cooperative)
        user           = create(:teller, office: office, cooperative: cooperative)
        deposit        = build(:entry, entry_date: Date.current.beginning_of_year.next_month.next_month,  office: office, recorder: user, cooperative: cooperative, commercial_document: saving)
        deposit.credit_amounts.build(amount: 6_000,  account: saving.liability_account)
        deposit.debit_amounts.build(amount: 6_000,  account: cash_account)
        deposit.save!

        averaged_balance = described_class.new(saving: saving, to_date: Date.current.beginning_of_year.end_of_quarter).averaged_balance
        expect(averaged_balance).to eql 2_000
      end
    end
  end
end
