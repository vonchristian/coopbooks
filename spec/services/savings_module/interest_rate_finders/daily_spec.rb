require 'rails_helper'

module SavingsModule
  module InterestRateFinders
    describe Daily do
      it "rate_divisor" do
        saving_product = create(:saving_product, interest_posting: 'daily')
        expect(saving_product.applicable_rate_finder).to eql described_class
        expect(described_class.new(saving_product: saving_product).rate_divisor).to eql 364.0
      end
    end
  end
end
