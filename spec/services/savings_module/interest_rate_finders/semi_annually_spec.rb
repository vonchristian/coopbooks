require 'rails_helper'

module SavingsModule
  module InterestRateFinders
    describe SemiAnnually do
      it "rate_divisor" do
        saving_product = create(:saving_product, interest_posting: 'semi_annually')
        expect(described_class.new(saving_product: saving_product).rate_divisor).to eql 2.0
      end
      it 'applicable_rate' do
        saving_product = create(:saving_product, interest_posting: 'semi_annually')

        expect(described_class.new(saving_product: saving_product, interest_rate: 0.02).applicable_rate).to eql 0.01
      end
    end
  end
end
