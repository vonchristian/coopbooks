require 'rails_helper'

module SavingsModule
  module InterestRateFinders
    describe Annually do
      it "rate_divisor" do
        saving_product = create(:saving_product, interest_posting: 'annually')
        expect(described_class.new(saving_product: saving_product).rate_divisor).to eql 1.0
      end
    end
  end
end
