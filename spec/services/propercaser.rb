require 'rails_helper'

describe Propercaser do
  it { expect(Propercaser.new(text: " puhot").propercase).to eql "Puhot" }
  it { expect(Propercaser.new(text: "dad-an ").propercase).to eql "Dad-an" }
  it { expect(Propercaser.new(text: "mary ann ").propercase).to eql "Mary Ann" }
  it { expect(Propercaser.new(text: "VEVERLY MARCOS").propercase).to eql "Veverly Marcos" }



end
