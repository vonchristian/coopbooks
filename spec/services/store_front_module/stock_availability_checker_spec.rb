require 'rails_helper'

module StoreFrontModule
  describe StockAvailabilityChecker, type: :model do
    it 'update_availability' do
      cooperative                  = create(:cooperative)
      office                       = create(:office, cooperative: cooperative)
      store_front                  = create(:store_front, office: office)
      sales_clerk                  = create(:sales_clerk, cooperative: cooperative, office: office)
      employee_store_front_account = create(:employee_store_front_account, employee: sales_clerk, store_front: store_front)
      product                      = create(:product, name: 'Test Product', store_front: store_front)
      uom                          = create(:unit_of_measurement, product: product, base_measurement: true, conversion_quantity: 1)
      mark_up_price                = create(:mark_up_price, unit_of_measurement: uom, price: 200)
      cart                         = create(:cart)
      stock                        = create(:stock, product: product, store_front: store_front, barcode: '11111111')
      supplier                     = create(:supplier)
      entry                        = create(:entry_with_credit_and_debit, recorder: sales_clerk, office: office, cooperative: cooperative)
      voucher                      = create(:voucher, entry: entry, payee: supplier, reference_number: 'V001')
      purchase_order               = create(:purchase_order, supplier: supplier, voucher: voucher)
      purchase_order_line_item     = create(:purchase_line_item, quantity: 10, stock: stock, order: purchase_order, unit_of_measurement: uom)

      expect(stock.available).to eql false

      described_class.new(stock: stock, cart: cart).update_availability!

      expect(stock.available).to eql true

      sales = create(:sales_line_item, quantity: 10, cart: cart, stock: stock)

      described_class.new(stock: stock, cart: cart).update_availability!
      expect(stock.available).to eql false

    end
  end
end
