require 'rails_helper'

module Members
  describe AgeCalculator do
    it '#age' do
      age = described_class.new(date_of_birth: '12/2/1990').age

      expect(age).to eql 29
    end
  end
end
