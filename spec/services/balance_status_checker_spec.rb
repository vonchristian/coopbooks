require 'rails_helper'

describe BalanceStatusChecker do
  it "set_balance_status" do
    teller         = create(:teller)
    office         = teller.office
    cooperative    = teller.cooperative
    cash_account   = create(:employee_cash_account, employee: teller, cooperative: cooperative, office: office)
    saving_product = create(:saving_product, minimum_balance: 500)
    saving         = create(:saving, saving_product: saving_product, office: office)

    described_class.new(account: saving, product: saving_product).set_balance_status
    expect(saving.has_minimum_balance).to eq false


    deposit = build(:entry, recorder: teller, office: office, cooperative: cooperative)
    deposit.credit_amounts << build(:credit_amount, amount: 5000, account: saving.liability_account)
    deposit.debit_amounts << build(:debit_amount, amount: 5_000,   account: cash_account.cash_account)
    deposit.save!

    described_class.new(account: saving, product: saving_product).set_balance_status

    expect(saving.has_minimum_balance).to eq true
  end
end
