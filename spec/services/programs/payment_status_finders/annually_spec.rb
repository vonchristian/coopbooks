require 'rails_helper'

module Programs
  module PaymentStatusFinders
    describe Annually do
      it "paid?" do
        cooperative  = create(:cooperative)
        office       = create(:office, cooperative: cooperative)
        program      = create(:program, cooperative: cooperative, mode_of_payment: 'annually', amount: 1_000)
        subscription = build(:program_subscription, program: program)
        AccountCreators::ProgramSubscription.new(subscription: subscription).create_accounts!
        subscription.save!
        cash_account = create(:asset, office: office)
        teller = create(:teller, office: office, cooperative: cooperative)


        entry = build(:entry, office: office, recorder: teller, cooperative: cooperative,  entry_date: Date.current)
        entry.debit_amounts.build(amount: 1000, account: cash_account)
        entry.credit_amounts.build(amount: 1000, account: subscription.account)
        entry.save!
        #paid?
        expect(subscription.paid?(date: Date.current)).to eql true
        expect(subscription.paid?(date: Date.current.next_year)).to eql false
        #unpaid?
        expect(subscription.unpaid?(date: Date.current)).to eql false
        expect(subscription.unpaid?(date: Date.current.next_year)).to eql true

      end
    end
  end
end
