require 'rails_helper'

module TimeDeposits
  describe InterestCalculator do
    describe 'compute' do
      it 'matured_time_deposit' do
        cooperative          = create(:cooperative)
        office               = create(:office, cooperative: cooperative)
        user                 = create(:teller, office: office, cooperative: cooperative)
        term                 = create(:term, effectivity_date: Date.current - 90.days, maturity_date: Date.yesterday)
        time_deposit_product = create(:time_deposit_product, interest_rate: 0.05, number_of_days: 90)
        cash_account         = create(:asset)
        liability_account    = create(:liability)
        time_deposit         = create(:time_deposit, term: term, liability_account: liability_account, time_deposit_product: time_deposit_product)
        entry                = build(:entry, commercial_document: time_deposit, office: office, cooperative: cooperative, recorder: user)

        entry.debit_amounts.build(amount: 100_000, account: cash_account)
        entry.credit_amounts.build(amount: 100_000, account: time_deposit.liability_account)
        entry.save!

        expect(described_class.new(time_deposit: time_deposit).compute).to eql 5_000
      end

      it 'not_matured_time_deposit' do
        cooperative          = create(:cooperative)
        office               = create(:office, cooperative: cooperative)
        user                 = create(:teller, office: office, cooperative: cooperative)
        term                 = create(:term, effectivity_date: (Date.current - 30.days), maturity_date: Date.current + 6.months)
        time_deposit_product = create(:time_deposit_product, interest_rate: 0.05, number_of_days: 90)
        cash_account         = create(:asset)
        liability_account    = create(:liability)
        time_deposit         = create(:time_deposit, time_deposit_product: time_deposit_product, date_deposited: (Date.current - 30.days), term: term, liability_account: liability_account)
        entry                = build(:entry, commercial_document: time_deposit, office: office, cooperative: cooperative, recorder: user)

        entry.debit_amounts.build(amount: 100_000, account: cash_account)
        entry.credit_amounts.build(amount: 100_000, account: time_deposit.liability_account)
        entry.save!

        expect(described_class.new(time_deposit: time_deposit).compute.floor(2)).to eql 1666.66
      end
    end
  end
end
