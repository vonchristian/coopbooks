require 'rails_helper'

module AccountingModule
  describe AccountDisplayName do
    describe "#display_name" do
      it "cash account" do
        teller       = create(:teller)
        cash         = create(:asset, name: "Cash", office: teller.office)
        cash_account = create(:employee_cash_account, cash_account: cash, employee: teller, cooperative: teller.cooperative, office: teller.office)

        expect(described_class.new(account: cash_account.cash_account).display_name).to eql 'Cash'
      end
    end
  end
end
