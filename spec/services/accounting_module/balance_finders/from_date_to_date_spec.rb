require 'rails_helper'

module AccountingModule
  module BalanceFinders
    describe FromDateToDate do
      it '.compute' do
        cooperative = create(:cooperative)
        office      = create(:office, cooperative: cooperative)
        employee    = create(:teller, office: office, cooperative: cooperative)
        asset       = create(:asset)
        revenue     = create(:revenue)
        entry       = build(:entry, cooperative: cooperative,  entry_date: Date.current, office: office, recorder: employee)
        entry_2     = build(:entry, cooperative: cooperative, entry_date: Date.yesterday, office: office, recorder: employee)

        entry.debit_amounts.build(amount: 100, account: asset)
        entry.credit_amounts.build(amount: 100, account: revenue)
        entry.save!

        entry_2.debit_amounts.build(amount: 300, account: asset)
        entry_2.credit_amounts.build(amount: 300, account: revenue)
        entry_2.save!

        expect(asset.balance(from_date: Date.current, to_date: Date.current)).to eql 100
        expect(asset.balance(from_date: Date.yesterday, to_date: Date.yesterday)).to eql 300

        expect(revenue.balance(from_date: Date.current, to_date: Date.current)).to eql 100
        expect(revenue.balance(from_date: Date.yesterday, to_date: Date.yesterday)).to eql 300
      end
    end
  end
end
