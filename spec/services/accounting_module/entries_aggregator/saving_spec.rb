require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe Saving do
      it '#debit_entries' do
        teller = create(:teller)
        office = teller.office
        cooperative = teller.cooperative
        loan   = create(:loan, office: office)
        cash   = create(:asset, office: office)
        saving = create(:saving, office: office)

        #loan disbursement
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.credit_amounts.build(account: cash, amount: 1000)
        loan_entry.save!
        #withdrawal
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.credit_amounts.build(account: cash, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office).debit_entries).to include(saving_entry)
        expect(described_class.new(entries: office.entries, office: office).debit_entries).to_not include(loan_entry)
      end

      it '#credit_entries' do
        teller = create(:teller)
        office = teller.office
        cooperative = teller.cooperative
        loan   = create(:loan, office: office)
        cash   = create(:asset, office: office)
        saving = create(:saving, office: office)

        #loan payment
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: cash, amount: 1000)
        loan_entry.credit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.save!
        #deposit
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: cash, amount: 500)
        saving_entry.credit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office).credit_entries).to include(saving_entry)
        expect(described_class.new(entries: office.entries, office: office).credit_entries).to_not include(loan_entry)
      end
    end
  end
end
