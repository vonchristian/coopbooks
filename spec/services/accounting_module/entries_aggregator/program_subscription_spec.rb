require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe ProgramSubscription do
      it '#credit_entries' do
        teller               = create(:teller)
        office               = teller.office
        cooperative          = teller.cooperative
        program_subscription = create(:program_subscription, office: office)
        cash                 = create(:asset, office: office)
        saving               = create(:saving, office: office)

        #subscription payment
        program_subscription_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        program_subscription_entry.debit_amounts.build(account: cash, amount: 1000)
        program_subscription_entry.credit_amounts.build(account: program_subscription.account, amount: 1000)
        program_subscription_entry.save!
        #withdrawal
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.credit_amounts.build(account: cash, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office, program_subscriptions: office.program_subscriptions).credit_entries).to include(program_subscription_entry)
        expect(described_class.new(entries: office.entries, office: office, program_subscriptions: office.program_subscriptions).credit_entries).to_not include(saving_entry)
      end

      it '#debit_entries' do
        teller = create(:teller)
        office = teller.office
        cooperative = teller.cooperative
        program_subscription   = create(:program_subscription, office: office)
        cash   = create(:asset, office: office)
        saving = create(:saving, office: office)

        #subscription withdrawal
        program_subscription_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        program_subscription_entry.debit_amounts.build(account: program_subscription.account, amount: 1000)
        program_subscription_entry.credit_amounts.build(account: cash, amount: 1000)
        program_subscription_entry.save!

        #deposit
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: cash, amount: 500)
        saving_entry.credit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office, program_subscriptions: office.program_subscriptions).debit_entries).to include(program_subscription_entry)
        expect(described_class.new(entries: office.entries, office: office, program_subscriptions: office.program_subscriptions).debit_entries).to_not include(saving_entry)
      end
    end
  end
end
