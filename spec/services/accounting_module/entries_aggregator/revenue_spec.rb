require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe Revenue do
      it '#credit_entries' do
        teller      = create(:teller)
        office      = teller.office
        cooperative = teller.cooperative
        loan        = create(:loan, office: office)
        cash        = create(:asset, office: office)
        revenue     = create(:revenue, office: office)

        #loan disbursement
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.credit_amounts.build(account: cash, amount: 1000)
        loan_entry.save!
        #transpo expense
        revenue_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        revenue_entry.debit_amounts.build(account: revenue, amount: 500)
        revenue_entry.credit_amounts.build(account: cash, amount: 500)
        revenue_entry.save!

        expect(described_class.new(entries: office.entries, office: office).credit_entries).to include(revenue_entry)
        expect(described_class.new(entries: office.entries, office: office).credit_entries).to_not include(loan_entry)
      end
    end
  end
end
