require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe ShareCapital do
      it '#credit_entries' do
        teller               = create(:teller)
        office               = teller.office
        cooperative          = teller.cooperative
        share_capital  = create(:share_capital, office: office)
        cash                 = create(:asset, office: office)
        saving               = create(:saving, office: office)

        #capital build up
        share_capital_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        share_capital_entry.debit_amounts.build(account: cash, amount: 1000)
        share_capital_entry.credit_amounts.build(account: share_capital.equity_account, amount: 1000)
        share_capital_entry.save!

        #withdrawal
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.credit_amounts.build(account: cash, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office).credit_entries).to include(share_capital_entry)
        expect(described_class.new(entries: office.entries, office: office).credit_entries).to_not include(saving_entry)
      end

      it '#debit_entries' do
        teller               = create(:teller)
        office               = teller.office
        cooperative          = teller.cooperative
        share_capital  = create(:share_capital, office: office)
        cash                 = create(:asset, office: office)
        saving               = create(:saving, office: office)

        #capital build up withdrawal
        share_capital_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        share_capital_entry.debit_amounts.build(account: share_capital.equity_account, amount: 1000)
        share_capital_entry.credit_amounts.build(account: cash, amount: 1000)
        share_capital_entry.save!

        #deposit
        saving_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        saving_entry.debit_amounts.build(account: cash, amount: 500)
        saving_entry.credit_amounts.build(account: saving.liability_account, amount: 500)
        saving_entry.save!

        expect(described_class.new(entries: office.entries, office: office).debit_entries).to include(share_capital_entry)
        expect(described_class.new(entries: office.entries, office: office).debit_entries).to_not include(saving_entry)
      end

    end
  end
end
