require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe BankAccount do
      it '#debit_entries' do
        teller      = create(:teller)
        office      = teller.office
        cooperative = teller.cooperative
        loan        = create(:loan, office: office)
        cash        = create(:asset, office: office)
        bank        = create(:bank_account, office: office)

        #loan disbursement
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.credit_amounts.build(account: cash, amount: 1000)
        loan_entry.save!
        #bank deposit
        bank_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        bank_entry.debit_amounts.build(account: bank.cash_account, amount: 500)
        bank_entry.credit_amounts.build(account: cash, amount: 500)
        bank_entry.save!

        expect(described_class.new(entries: office.entries, office: office).debit_entries).to include(bank_entry)
        expect(described_class.new(entries: office.entries, office: office).debit_entries).to_not include(loan_entry)
      end

      it '#credit_entries' do
        teller      = create(:teller)
        office      = teller.office
        cooperative = teller.cooperative
        loan        = create(:loan, office: office)
        cash        = create(:asset, office: office)
        bank        = create(:bank_account, office: office)

        #loan payment
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: cash, amount: 1000)
        loan_entry.credit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.save!
        #bank withdraw
        bank_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        bank_entry.debit_amounts.build(account: cash, amount: 500)
        bank_entry.credit_amounts.build(account: bank.cash_account, amount: 500)
        bank_entry.save!

        expect(described_class.new(entries: office.entries, office: office).credit_entries).to include(bank_entry)
        expect(described_class.new(entries: office.entries, office: office).credit_entries).to_not include(loan_entry)
      end
    end
  end
end
