require 'rails_helper'

module AccountingModule
  module EntriesAggregator
    describe Expense do
      it '#debit_entries' do
        teller      = create(:teller)
        office      = teller.office
        cooperative = teller.cooperative
        loan        = create(:loan, office: office)
        cash        = create(:asset, office: office)
        transport   = create(:expense, office: office)

        #loan disbursement
        loan_entry  = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        loan_entry.debit_amounts.build(account: loan.receivable_account, amount: 1000)
        loan_entry.credit_amounts.build(account: cash, amount: 1000)
        loan_entry.save!
        #transpo expense
        transport_entry = build(:entry, office: office, recorder: teller, cooperative: cooperative)
        transport_entry.debit_amounts.build(account: transport, amount: 500)
        transport_entry.credit_amounts.build(account: cash, amount: 500)
        transport_entry.save!

        expect(described_class.new(entries: office.entries, office: office).debit_entries).to include(transport_entry)
        expect(described_class.new(entries: office.entries, office: office).debit_entries).to_not include(loan_entry)
      end
    end
  end
end
