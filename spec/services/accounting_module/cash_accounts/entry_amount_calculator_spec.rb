require 'rails_helper'

module AccountingModule
  module CashAccounts
    describe EntryAmountCalculator, type: :model do

      it '#payment' do
        cooperative                = create(:cooperative)
        office                     = create(:office, cooperative: cooperative)
        teller                     = create(:teller, office: office, cooperative: cooperative)
        asset_account_category     = create(:asset_level_one_account_category, office: office)
        revenue_account_category   = create(:revenue_account_category, office: office)
        equity_account_category    = create(:equity_account_category, office: office)
        liability_account_category = create(:liability_account_category, office: office)
        cash                       = create(:asset, office: office, account_category: asset_account_category)
        membership_fee             = create(:revenue, office: office, account_category: revenue_account_category)
        mas                        = create(:equity, office: office, account_category: equity_account_category)
        saving                     = create(:liability, office: office, account_category: liability_account_category)

        entry = build(:entry, entry_date: Date.current, cooperative: cooperative, office: office, recorder: teller)
        entry.debit_amounts.build(account: cash, amount: 1000)
        entry.credit_amounts.build(account: membership_fee, amount: 100)
        entry.credit_amounts.build(account: mas, amount: 400)
        entry.credit_amounts.build(account: saving, amount: 500)
        entry.save!


        expect(described_class.new(date: Date.current, account_category: revenue_account_category, cash_account: cash).payment).to eql 100

        expect(described_class.new(date: Date.current, account_category: equity_account_category, cash_account: cash).payment).to eql 400

        expect(described_class.new(date: Date.current, account_category: liability_account_category, cash_account: cash).payment).to eql 500

      end

      it '#disbursement' do
        cooperative                    = create(:cooperative)
        office                         = create(:office, cooperative: cooperative)
        teller                         = create(:teller, office: office, cooperative: cooperative)
        cash_account_category          = create(:asset_level_one_account_category, office: office)
        loan_account_category          = create(:asset_level_one_account_category, office: office)
        savings_account_category       = create(:liability_account_category, office: office)
        mas_account_category           = create(:equity_account_category, office: office)
        share_capital_account_category = create(:equity_account_category, office: office)
        interest_account_category      = create(:revenue_account_category, office: office)
        fee_account_category           = create(:revenue_account_category, office: office)
        cash                           = create(:asset, office: office, account_category: cash_account_category)
        create(:employee_cash_account, employee: teller, cash_account: cash, office: office, cooperative: cooperative)
        loan_product = create(:loan_product, cooperative: cooperative)
        create(:office_loan_product, office: office, receivable_account_category: loan_account_category)
        loans_receivable               = create(:asset, office: office, account_category: loan_account_category)
        saving                         = create(:liability, office: office, account_category: savings_account_category)
        mas                            = create(:equity, office: office, account_category: mas_account_category)
        share_capital                  = create(:equity, office: office, account_category: share_capital_account_category)
        interest                       = create(:revenue, office: office, account_category: interest_account_category)
        service_fee                    = create(:revenue, office: office, account_category: fee_account_category)

        entry = build(:entry, office: office, entry_date: Date.current, recorder: teller, cooperative: cooperative)
        entry.debit_amounts.build(account: loans_receivable, amount: 50_000)
        entry.credit_amounts.build(account: cash, amount: 40_000)
        entry.credit_amounts.build(account: saving, amount: 3_000)
        entry.credit_amounts.build(account: mas, amount: 1_000)
        entry.credit_amounts.build(account: share_capital, amount: 1_000)
        entry.credit_amounts.build(account: interest, amount: 3_000)
        entry.credit_amounts.build(account: service_fee, amount: 2_000)
        entry.save!

        expect(described_class.new(date: Date.current, account_category: loan_account_category, cash_account: cash).disbursement).to eql 40_000
        expect(described_class.new(date: Date.current, account_category: savings_account_category, cash_account: cash).disbursement).to eql 0

        entry_2 = build(:entry, office: office, entry_date: Date.current, recorder: teller, cooperative: cooperative)
        entry_2.debit_amounts.build(account: saving, amount: 1_500)
        entry_2.credit_amounts.build(account: cash, amount: 1_500)
        entry_2.save!

        expect(described_class.new(date: Date.current, account_category: savings_account_category, cash_account: cash).disbursement).to eql 1500

        entry_3 = build(:entry, office: office, entry_date: Date.current, recorder: teller, cooperative: cooperative)
        entry_3.debit_amounts.build(account: share_capital, amount: 1_500)
        entry_3.credit_amounts.build(account: cash, amount: 1_500)
        entry_3.save!

        expect(described_class.new(date: Date.current, account_category: share_capital_account_category, cash_account: cash).disbursement).to eql 1500

      end
    end
  end
end
