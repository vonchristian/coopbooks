require 'rails_helper'

module ShareCapitals

  describe Opening do
    it 'open_account!' do
      teller                       = create(:teller)
      share_capital_product        = create(:share_capital_product, cooperative: teller.cooperative)
      office_share_capital_product = create(:office_share_capital_product, office: teller.office, share_capital_product: share_capital_product)
      share_capital_application    = create(:share_capital_application, share_capital_product: share_capital_product, office: teller.office)

      described_class.new(share_capital_application: share_capital_application, employee: teller).open_account!

      share_capital = teller.office.share_capitals.find_by(account_number: share_capital_application.account_number)

      expect(share_capital).to_not eql nil
    end
  end
end
