require 'rails_helper'

module LoansModule
  describe FirstTimeLoan do
    it 'first_time_loans' do
      member_1 = create(:member)
      member_2 = create(:member)
      loan_1   = create(:disbursed_loan, disbursement_date: Date.current, borrower: member_1)
      loan_2   = create(:disbursed_loan, disbursement_date: Date.current, borrower: member_2)
      loan_3   = create(:disbursed_loan, disbursement_date: Date.current, borrower: member_2)

      expect(described_class.new(from_date: Date.current, to_date: Date.current, loans: LoansModule::Loan.all).first_time_loans).to include(loan_1)
      expect(described_class.new(from_date: Date.current, to_date: Date.current, loans: LoansModule::Loan.all).first_time_loans).to_not include(loan_2)
    end
  end
end
