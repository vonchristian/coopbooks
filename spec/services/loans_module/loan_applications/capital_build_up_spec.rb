require 'rails_helper'

module LoansModule
  module LoanApplications
    describe CapitalBuildUp do
      it 'process!' do
        employee         = create(:loan_officer)
        cart             = create(:cart)
        loan_application = create(:loan_application, cart: cart, preparer: employee)
        share_capital    = create(:share_capital)

        described_class.new(
          cart_id:             cart.id,
          amount:              100,
          loan_application_id: loan_application.id,
          share_capital_id:    share_capital.id,
        ).process!

        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(share_capital.equity_account_id)
        expect(cart.voucher_amounts.credit.pluck(:amount_cents)).to include(10000)
        expect(cart.voucher_amounts.credit.pluck(:description)).to include('Capital Build Up')

      end
    end
  end
end
