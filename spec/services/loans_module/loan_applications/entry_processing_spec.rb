require 'rails_helper'

module LoansModule
  module LoanApplications
    describe EntryProcessing, type: :model do
      it '#process' do
        teller           = create(:teller)
        loan_application = create(:loan_application_with_voucher, office: teller.office)
        loan             = create(:loan, loan_application: loan_application, office: teller.office)

        described_class.new(employee: teller, loan_application: loan_application).process!
        expect(described_class.new(employee: teller, loan_application: loan_application).find_entry).to_not eql nil

      end
    end
  end
end
