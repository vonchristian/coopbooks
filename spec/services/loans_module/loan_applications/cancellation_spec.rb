require 'rails_helper'

module LoansModule
  module LoanApplications
    describe Cancellation do
      it '#cancel!' do
        cart                      = create(:cart)
        receivable_account        = create(:asset)
        interest_revenue_account  = create(:revenue)
        equity_account            = create(:equity)
        loan_application          = create(:loan_application, cart: cart, receivable_account: receivable_account, interest_revenue_account: interest_revenue_account)
        share_capital_application = create(:share_capital_application, equity_account: equity_account, cart: cart)

        described_class.new(loan_application: loan_application).cancel!

        expect(LoansModule::LoanApplication.find_by(id: loan_application.id)).to_not be_present
        expect(ShareCapitalApplication.find_by(id: share_capital_application.id)).to_not be_present
        expect(AccountingModule::Asset.find_by(id: receivable_account.id)).to_not be_present
        expect(AccountingModule::Revenue.find_by(id: interest_revenue_account.id)).to_not be_present
        expect(AccountingModule::Equity.find_by(id: equity_account.id)).to_not be_present
        expect(Cart.find_by(id: loan_application.cart_id)).to_not be_present
      end
    end
  end
end
