require 'rails_helper'

module LoansModule
  module AmortizationSchedules
    describe PaymentStatusUpdater do
      describe 'update_payment_status!' do
        it 'fully_paid' do
          cooperative           = create(:cooperative)
          receivable_account    = create(:asset)
          loan                  = create(:loan, receivable_account: receivable_account)
          amortization_schedule = create(:amortization_schedule, loan: loan, principal: 1000, interest: 500, total_repayment: 1_500)
          cash_account          = create(:employee_cash_account, cooperative: cooperative)
          entry                 = build(:entry)
          entry.debit_amounts.build(amount: 1500, account: cash_account.cash_account)
          entry.credit_amounts.build(amount: 1500, account: receivable_account)
          entry.save!

          amortization_schedule.entries << entry
          described_class.new(amortization_schedule: amortization_schedule).update_payment_status!

          expect(amortization_schedule.payment_status).to eql 'fully_paid'
        end

        it 'partially_paid' do
          cooperative           = create(:cooperative)
          receivable_account    = create(:asset)
          loan                  = create(:loan, receivable_account: receivable_account)
          amortization_schedule = create(:amortization_schedule, loan: loan, principal: 1000, interest: 500, total_repayment: 1_500)
          cash_account          = create(:employee_cash_account, cooperative: cooperative)
          entry                 = build(:entry)
          entry.debit_amounts.build(amount: 500, account: cash_account.cash_account)
          entry.credit_amounts.build(amount: 500, account: receivable_account)
          entry.save!

          amortization_schedule.entries << entry
          described_class.new(amortization_schedule: amortization_schedule).update_payment_status!

          expect(amortization_schedule.payment_status).to eql 'partially_paid'
        end
      end
    end
  end
end
