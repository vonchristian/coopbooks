require 'rails_helper'

  module LoansModule
    describe InterestComputation do
      it "#compute" do
        expect(described_class.new(loan_amount: 100_00, rate: 0.01, term: 12).compute).to eql 1_200.0
        expect(described_class.new(loan_amount: 100_00, rate: 0.02, term: 12).compute).to eql 2_400.0
        expect(described_class.new(loan_amount: 26_400, rate: 0.015, term: 6).compute).to eql 2_376.0
      end

      it '#applicable_term' do
        expect(described_class.new(loan_amount: 100_00, rate: 0.01, term: 12).applicable_term).to eql 12
        expect(described_class.new(loan_amount: 100_00, rate: 0.01, term: 6).applicable_term).to eql 6

        expect(described_class.new(loan_amount: 100_00, rate: 0.01, term: 14).applicable_term).to eql 12
      end
    end
  end
