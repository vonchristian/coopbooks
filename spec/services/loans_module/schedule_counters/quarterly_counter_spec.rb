require 'rails_helper'

module LoansModule
  module ScheduleCounters
    describe QuarterlyCounter do

      it 'number of months per quarter' do
        expect(LoansModule::ScheduleCounters::QuarterlyCounter::NUMBER_OF_MONTHS_PER_QUARTER).to eql 3
      end

      it '#schedule_count' do
        loan_application_quarterly_12 = create(:loan_application, mode_of_payment: 'quarterly', term: 12)
        loan_application_quarterly_24 = create(:loan_application, mode_of_payment: 'quarterly', term: 24)

        expect(described_class.new(loan_application: loan_application_quarterly_12).schedule_count).to eql 4
        expect(described_class.new(loan_application: loan_application_quarterly_24).schedule_count).to eql 8
      end
    end
  end
end
