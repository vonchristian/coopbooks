require 'rails_helper'

module LoansModule
  module Loans
    describe PaidAtUpdater do
      it 'update_paid_at!' do
        loan = create(:loan)

        #disburse
        cash  = create(:asset)
        entry = build(:entry)
        entry.debit_amounts.build(amount: 10_000, account: loan.receivable_account)
        entry.credit_amounts.build(amount: 10_000, account: cash)
        entry.save!

        expect(loan.balance).to eql 10_000

        #payment
        date = Date.current
        cash  = create(:asset)
        entry = build(:entry, entry_date: date)
        entry.debit_amounts.build(amount: 10_000, account: cash)
        entry.credit_amounts.build(amount: 10_000, account: loan.receivable_account)
        entry.save!

        expect(loan.balance).to eql 0
        described_class.new(loan: loan, date: date).update_paid_at!

        expect(loan.paid_at.to_date).to eql date.to_date
      end
    end
  end
end
