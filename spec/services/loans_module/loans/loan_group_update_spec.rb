require 'rails_helper'

module LoansModule
  module Loans
    describe LoanGroupUpdate do
      describe 'set_loan_group' do
        it 'current' do
          office             = create(:office)
          current_loan_group = create(:loan_group, start_num: 0, end_num: 0, office: office)
          current_term       = create(:term, maturity_date: Date.current.next_month)
          current_loan       = create(:loan, office: office)
          current_loan.terms << current_term

          described_class.new(loan: current_loan, date: Date.current).set_loan_group

          expect(current_loan.loan_group).to eql current_loan_group
        end
        it '1-30 days' do
          office             = create(:office)
          one_30_loan_group  = create(:loan_group, start_num: 1, end_num: 30, office: office)
          one_day_term       = create(:term, maturity_date: Date.current.yesterday)
          one_30_days_term   = create(:term, maturity_date: Date.current - 30.days)
          one_30_days_loan   = create(:loan, office: office)
          one_day_loan       = create(:loan, office: office)
          one_30_days_loan.terms << one_30_days_term
          one_day_loan.terms     << one_day_term

          described_class.new(loan: one_30_days_loan, date: Date.current).set_loan_group
          described_class.new(loan: one_day_loan, date: Date.current).set_loan_group


          expect(one_30_days_loan.loan_group).to eql one_30_loan_group
          expect(one_day_loan.loan_group).to eql one_30_loan_group
        end
      end
    end
  end
end
