require 'rails_helper'

module LoansModule
  module Loans
    describe Opening, type: :model do
      it 'process!' do
        teller           = create(:teller)
        voucher          = create(:voucher)
        loan_product     = create(:loan_product)
        loan_application = create(:loan_application, voucher: voucher)
        loan_group       = create(:loan_group, office: teller.office, start_num: 0, end_num: 0)


        described_class.new(loan_application: loan_application, loan_product: loan_product, employee: teller).create_loan!
      end
    end
  end
end
