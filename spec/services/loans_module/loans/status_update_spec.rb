require 'rails_helper'

module LoansModule
  module Loans
    describe StatusUpdate do
      describe 'set_status' do
        it '#current' do
          current_term = create(:term, maturity_date: Date.current.next_month)
          current_loan = create(:loan)
          current_loan.terms << current_term

          described_class.new(loan: current_loan).set_status
          expect(current_loan.current_loan?).to eql true
        end

        it 'past_due' do
          past_due_term = create(:term, maturity_date: Date.current.last_month)
          past_due_loan = create(:loan)
          past_due_loan.terms << past_due_term

          described_class.new(loan: past_due_loan).set_status
          expect(past_due_loan.past_due?).to eql true

        end
      end
    end
  end
end
