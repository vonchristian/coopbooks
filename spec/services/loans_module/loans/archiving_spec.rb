require 'rails_helper'

module LoansModule
  module Loans
    describe Archiving do
      it '#archive!' do
        loan = create(:loan)
        loan_2 = create(:loan)

        #disburse
        cash  = create(:asset)
        entry = build(:entry)
        entry.debit_amounts.build(amount: 10_000, account: loan.receivable_account)
        entry.credit_amounts.build(amount: 10_000, account: cash)
        entry.save!

        #payment
        date  = Date.current
        cash  = create(:asset)
        entry = build(:entry, entry_date: date)
        entry.debit_amounts.build(amount: 10_000, account: cash)
        entry.credit_amounts.build(amount: 10_000, account: loan.receivable_account)
        entry.save!

        described_class.new(loan: loan, date: date).archive!

        expect(loan.archived?).to eql true
        expect(loan_2.archived?).to eql false

      end
    end
  end
end
