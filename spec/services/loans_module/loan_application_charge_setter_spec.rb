require 'rails_helper'

module LoansModule
  describe LoanApplicationChargeSetter do
    let! (:cooperative)                            { create(:cooperative) }
    let! (:office)                                 { create(:office, cooperative: cooperative) }
    let! (:share_capital_product)                  { create(:share_capital_product, cooperative: cooperative) }
    let! (:borrower)                               { create(:member) }
    let! (:share_capital)                          { create(:share_capital, share_capital_product: share_capital_product, subscriber: borrower, office: office) }
    let! (:cart)                                   { create(:cart) }
    let! (:loan_protection_plan_provider)          { create(:loan_protection_plan_provider, business_name: "CLIMBS", rate: 1.35) }
    let! (:prededucted_straight_line_loan_product) { create(:prededucted_straight_line_loan_product, loan_protection_plan_provider: loan_protection_plan_provider, cooperative: cooperative) }
    let! (:loan_application)                       { create(:loan_application, office: office, loan_amount: 100_000, interest_rate: 0.12, interest_calculation_type: 'prededucted', term: 12, loan_product: prededucted_straight_line_loan_product, cart: cart, borrower: borrower) }
    let! (:office_loan_product)                    { create(:office_loan_product, office: office, loan_product: prededucted_straight_line_loan_product) }
    let! (:service_charge)                         { create(:loan_product_charge, name: "Service Fee", office_loan_product: office_loan_product, rate: 0.03, charge_type: 'percent_based') }
    let! (:filing_fee)                             { create(:loan_product_charge, name: "Filing Fee", office_loan_product: office_loan_product, amount: 100, charge_type: 'amount_based') }

    before(:each) do
      create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)
      described_class.new(loan_application: loan_application, cart: cart).create_charges!
    end

    it '#create_interest_on_loan_charge' do
      expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(loan_application.interest_revenue_account_id)
      expect(cart.voucher_amounts.find_by(description: "Interest on Loan").amount.amount).to eql 12_000
    end

    it "#create_charges_based_on_loan_product" do
      expect(cart.voucher_amounts.credit.pluck(:description)).to include "Service Fee"
      expect(cart.voucher_amounts.credit.pluck(:description)).to include "Filing Fee"
      expect(cart.voucher_amounts.find_by(description: "Service Fee").amount.amount).to eql 3_000
      expect(cart.voucher_amounts.find_by(description: "Filing Fee").amount.amount).to eql 100
      expect(cart.voucher_amounts.credit.pluck(:account_id)).to include service_charge.account_id
      expect(cart.voucher_amounts.credit.pluck(:account_id)).to include service_charge.account_id
    end

    it "#create_loan_protection_fund" do
      expect(cart.voucher_amounts.credit.pluck(:description)).to include "Loan Protection Fund"
      expect(cart.voucher_amounts.credit.pluck(:account_id)).to include loan_protection_plan_provider.payable_account_id
      expect(cart.voucher_amounts.find_by(description: "Loan Protection Fund").amount.amount).to eql 1_620
    end
  end
end
