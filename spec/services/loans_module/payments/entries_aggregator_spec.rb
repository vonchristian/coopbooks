require 'rails_helper'

module LoansModule
  module Payments
    describe EntriesAggregator do
      it '.entries' do
        entry        = build(:entry)
        entry_2      = create(:entry_with_credit_and_debit)
        loan         = create(:loan, office: entry.office)
        cash_account = create(:asset)
        entry.credit_amounts.build(amount: 1000,  account: loan.receivable_account)
        entry.credit_amounts.build(amount: 200,   account: loan.interest_revenue_account)
        entry.credit_amounts.build(amount: 200,   account: loan.penalty_revenue_account)
        entry.debit_amounts.build(amount: 1400,   account: cash_account)
        entry.save!

        expect(described_class.new(office: entry.office, from_date: Date.current, to_date: Date.current).entries).to include entry
        expect(described_class.new(office: entry.office, from_date: Date.current, to_date: Date.current).entries).to_not include entry_2
      end
    end
  end
end
