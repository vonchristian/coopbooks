require 'rails_helper'

module LoansModule
  describe LoanProtectionCalculator do
    it 'calculate' do
      loan_application = create(:loan_application, loan_amount: 100_000, term: 6)
      plan_provider    = create(:loan_protection_plan_provider, rate: 1.35)

      amount = described_class.new(loan_application: loan_application, plan_provider: plan_provider).calculate

      expect(amount).to eql 810
    end
  end
end 
