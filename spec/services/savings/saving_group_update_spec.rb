require 'rails_helper'

module Savings
  describe SavingGroupUpdate do
    it '.set_group' do
      teller         = create(:teller)
      office         = teller.office
      saving_group   = create(:saving_group, office: office, start_num: 0, end_num: 30)
      saving_group_2 = create(:saving_group, office: office, start_num: 31, end_num: 60)
      saving         = create(:saving, office: office)
      cash           = create(:asset, office: office)
      entry          = build(:entry, entry_date: Date.current - 5.days, recorder: teller, office: office, cooperative: teller.cooperative)
      entry.debit_amounts.build(account: cash, amount: 500)
      entry.credit_amounts.build(account: saving.liability_account, amount: 500)
      entry.save!

      described_class.new(saving: saving).set_group

      expect(saving.saving_group).to eql saving_group
      expect(saving.saving_group).to_not eql saving_group_2
    end
  end
end
