require 'rails_helper'

module AccountCreators
  module StoreFronts
    module Orders
      describe SalesOrder, type: :model do
        it '#create_accounts!' do
          sales_order = build(:sales_order, sales_account_id: nil, sales_discount_account_id: nil, cost_of_goods_sold_account_id: nil, receivable_account_id: nil)

          described_class.new(sales_order: sales_order).create_accounts!

          expect(sales_order.sales_account).to_not eql nil
          expect(sales_order.sales_discount_account).to_not eql nil
          expect(sales_order.cost_of_goods_sold_account).to_not eql nil
          expect(sales_order.receivable_account).to_not eql nil
        end
      end
    end
  end
end
