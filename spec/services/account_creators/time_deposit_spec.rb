require 'rails_helper'

module AccountCreators
  describe TimeDeposit do
    it 'create_account!' do
      office = create(:office)
      time_deposit_product = create(:time_deposit_product)
      office_time_deposit_product = create(:office_time_deposit_product, office: office, time_deposit_product: time_deposit_product)
      time_deposit = build(:time_deposit, time_deposit_product: time_deposit_product, office: office, liability_account: nil, interest_expense_account: nil)
      described_class.new(time_deposit: time_deposit).create_accounts!
      time_deposit.save!

      liability_account        = AccountingModule::Liability.find_by!(name: "Time Deposits - (#{time_deposit.depositor_name} - #{time_deposit.account_number}")
      interest_expense_account = AccountingModule::Expense.find_by!(name: "Interest Expense on Time Deposits - (#{time_deposit.depositor_name} - #{time_deposit.account_number}")
      expect(time_deposit.liability_account).to eql liability_account
      expect(time_deposit.interest_expense_account).to eql interest_expense_account
    end
  end
end
