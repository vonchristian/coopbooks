require 'rails_helper'

module AccountCreators
  describe ProgramSubscription do
    it 'create_account' do
      cooperative  = create(:cooperative)
      office       = create(:office, cooperative: cooperative)
      program      = create(:program, cooperative: cooperative)
      office_program = create(:office_program, office: office, program: program)
      subscription = build(:program_subscription, program: program, office: office, account_id: nil)
      described_class.new(subscription: subscription).create_accounts!
      subscription.save!
      account = AccountingModule::Account.find_by(name: "#{subscription.name} (#{subscription.subscriber_name} - #{subscription.account_number}")
      expect(subscription.account).to eql account

      expect(office_program.account_category.accounts).to include(account)
    end
  end
end
