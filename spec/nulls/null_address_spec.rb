require 'rails_helper'

describe NullAddress do
  it 'complete_address' do
    expect(described_class.new.complete_address).to eql 'No address entered'
  end

  it 'details' do
    expect(described_class.new.details).to eql 'No address entered'
  end

  it 'address' do
    expect(described_class.new.address).to eql 'No address entered'
  end

  it 'barangay' do
    expect(described_class.new.barangay).to eql 'No barangay entered'
  end

  it 'street' do
    expect(described_class.new.street).to eql 'No street entered'
  end

  it 'municipality' do
    expect(described_class.new.municipality).to eql 'No municipality entered'
  end

  it 'province' do
    expect(described_class.new.province).to eql 'No province entered'
  end

  it 'barangay_name' do
    expect(described_class.new.barangay_name).to eql 'No barangay entered'
  end

  it 'street_name' do
    expect(described_class.new.street_name).to eql 'No street entered'
  end

  it 'municipality_name' do
    expect(described_class.new.municipality_name).to eql 'No municipality entered'
  end

  it 'province_name' do
    expect(described_class.new.province_name).to eql 'No province entered'
  end

end
