require 'rails_helper'

describe Term do

  describe 'validations' do
    it { is_expected.to validate_presence_of :term }
    it { is_expected.to validate_numericality_of :term }
    it { is_expected.to validate_presence_of :account_number }
    it { is_expected.to validate_uniqueness_of :account_number }
    it { is_expected.to validate_presence_of :effectivity_date }
    it { is_expected.to validate_presence_of :maturity_date }
  end

  it "#days_elapsed" do
    term = create(:term, effectivity_date: Date.current)

    travel_to Date.current + 10.days
    expect(term.days_elapsed).to eql 10
  end

  it ".past_due" do
    past_due_term = create(:term, term: 1, effectivity_date: Date.today, maturity_date: Date.today + 1.month)
    current_term  = create(:term, term: 12, effectivity_date: Date.today, maturity_date: Date.today + 12.month)
    travel_to Date.today + 32.days

    expect(described_class.past_due).to include(past_due_term)
    expect(described_class.past_due).to_not include(current_term)

  end

  it ".current" do
    old_term     = create(:term, effectivity_date: Date.today.last_month)
    current_term = create(:term, effectivity_date: Date.today)

    expect(described_class.current).to eql current_term
  end

  it "#matured?" do
    term = create(:term, term: 1, effectivity_date: Date.today, maturity_date: Date.today + 1.month)

    expect(term.matured?).to be false

    travel_to Date.today + 31.days

    expect(term.matured?).to be true
  end

  it '#number_of_days_past_due' do
    term          = create(:term, maturity_date: Date.current.next_month)
    past_due_term = create(:term, maturity_date: Date.current - 30.days)

    expect(term.number_of_days_past_due).to eql 0
    expect(past_due_term.number_of_days_past_due).to eql 30
  end
end
