require 'rails_helper'

module Addresses
  describe Province, type: :model do
    describe 'associations' do
      it { is_expected.to have_many :municipalities }
      it { is_expected.to have_many :barangays }
      it { is_expected.to have_many :addresses }
      it { is_expected.to have_many :members }
      it { is_expected.to have_many :organizations }

    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of :name }
    end
  end
end
