require 'rails_helper'

module Addresses
  RSpec.describe Barangay, type: :model do
    describe 'associations' do
      it { is_expected.to belong_to :municipality }
      it { is_expected.to have_many :streets }
      it { is_expected.to have_many :addresses }
      it { is_expected.to have_many :members }
      it { is_expected.to have_many :organizations }
      it { is_expected.to have_many :barangay_scopes }
      it { is_expected.to have_many :loans }
      it { is_expected.to have_many :savings }
      it { is_expected.to have_many :share_capitals }
      it { is_expected.to have_many :time_deposits }
      it { is_expected.to have_many :program_subscriptions }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:municipality_id) }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:municipality).with_prefix }
    end

    it 'name_and_municipality' do
      municipality = create(:municipality, name: "Test Municipality")
      barangay     = create(:barangay, name: 'Test Barangay', municipality: municipality)

      expect(barangay.name_and_municipality).to eql 'Test Barangay, Test Municipality'
    end
  end
end
