require 'rails_helper'

module Offices
  describe OfficeProgram do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :program }
      it { is_expected.to belong_to :account_category }
    end
    describe 'validations' do
      it 'validate_uniqueness_of(:program_id).scoped_to(:office_id)' do
        program          = create(:program)
        office           = create(:office)
        office_program   = create(:office_program, office: office, program: program)
        office_program_2 = build(:office_program, office: office, program: program)
        office_program_2.save

        expect(office_program_2.errors[:program_id]).to eql(['has already been taken'])
      end

      it 'validate_uniqueness_of account_category_id' do
        program          = create(:program)
        office           = create(:office)
        account_category = create(:asset_level_one_account_category)
        office_program   = create(:office_program, office: office, program: program, account_category: account_category)
        office_program_2 = build(:office_program, office: office, program: program, account_category: account_category)
        office_program_2.save

        expect(office_program_2.errors[:account_category_id]).to eql(['has already been taken'])

      end
    end
  end
end
