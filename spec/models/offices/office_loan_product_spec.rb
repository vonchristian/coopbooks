require 'rails_helper'

module Offices
  describe OfficeLoanProduct do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :loan_product }
      it { is_expected.to belong_to :receivable_account_category }
      it { is_expected.to belong_to :interest_revenue_account_category }
      it { is_expected.to belong_to :penalty_revenue_account_category }
      it { is_expected.to belong_to :temporary_account }
      it { is_expected.to belong_to(:loan_protection_plan_provider).optional }
      it { is_expected.to belong_to :amortization_type }
      it { is_expected.to have_many :loan_product_charges }
    end
    
    describe 'validations' do
      it 'unique loan_product per office' do
        office = create(:office)
        loan_product = create(:loan_product)
        office_loan_product = create(:office_loan_product, office: office, loan_product: loan_product)
        office_loan_product_2 = build(:office_loan_product, office: office, loan_product: loan_product)
        office_loan_product_2.save

        expect(office_loan_product_2.errors[:loan_product_id]).to eql(['has already been taken'])
      end
      it " unique account categories", pending: true do
        raise 'Not yet implemented'
      end
    end
  end
end
