require 'rails_helper'

module Offices
  describe SavingGroup do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to have_many :savings }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :title }
      it { is_expected.to validate_presence_of :start_num }
      it { is_expected.to validate_presence_of :end_num }
      it { is_expected.to validate_numericality_of(:start_num) }
      it { is_expected.to validate_numericality_of(:end_num) }
    end

    it '#num_range' do
      saving_group = build(:saving_group, start_num: 1, end_num: 30)

      expect(saving_group.num_range).to eql 1..30
    end
  end
end
