require 'rails_helper'

module Offices
  describe OfficeTimeDepositProduct  do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :time_deposit_product }
    end

    describe 'validations' do
      it 'unique time deposit product per office' do
        office = create(:office)
        time_deposit_product = create(:time_deposit_product)
        create(:office_time_deposit_product, office: office, time_deposit_product: time_deposit_product)
        office_time_deposit_product = build(:office_time_deposit_product, office: office, time_deposit_product: time_deposit_product)
        office_time_deposit_product.save

        expect(office_time_deposit_product.errors[:time_deposit_product_id]).to eql(['has already been taken'])
      end
    end
  end
end 
