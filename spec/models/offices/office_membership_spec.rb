require 'rails_helper'

module Offices
  describe OfficeMembership do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :membership }
    end
  end 
end
