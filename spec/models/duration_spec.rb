require 'rails_helper'

describe Duration, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to :term }
    it { is_expected.to belong_to :termable }
  end
end
