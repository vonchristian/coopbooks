require 'rails_helper'

describe SavingsAccountApplication do
  describe 'associations' do
    it { is_expected.to belong_to(:cart).optional }
    it { is_expected.to belong_to :cooperative }
    it { is_expected.to belong_to :office }
    it { is_expected.to belong_to :saving_product }
    it { is_expected.to belong_to :depositor }
    it { is_expected.to belong_to :liability_account }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :account_number }
    it { is_expected.to validate_uniqueness_of :account_number }
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:name).to(:depositor).with_prefix }
  end

end
