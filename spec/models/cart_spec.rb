require 'rails_helper'

describe Cart do
  describe 'associations' do
    it { is_expected.to belong_to :employee }
    it { is_expected.to have_many :cash_counts }
    it { is_expected.to have_many :loan_applications }
    it { is_expected.to have_many :voucher_amounts }
    it { is_expected.to have_many :share_capital_applications }
    it { is_expected.to have_many :savings_account_applications }
  end
end
