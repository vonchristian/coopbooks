require 'rails_helper'

module MembershipsModule
  describe ProgramSubscription do
    context "associations" do
    	it { is_expected.to belong_to :program }
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :cooperative }
    	it { is_expected.to belong_to :subscriber }
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to belong_to :account }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :account_number }
      it { is_expected.to validate_uniqueness_of :account_number }

      it 'unique program_id scoped_to subscriber_id' do
        program = create(:program)
        member  = create(:member)
        program_subscription   = create(:program_subscription, program: program, subscriber: member)
        program_subscription_2 = build(:program_subscription, program: program, subscriber: member)
        program_subscription_2.save
        expect(program_subscription_2.errors[:program_id]).to eql(['has already been taken'])
      end
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:program) }
      it { is_expected.to delegate_method(:amount).to(:program) }
      it { is_expected.to delegate_method(:description).to(:program) }
      it { is_expected.to delegate_method(:payment_status_finder).to(:program) }
      it { is_expected.to delegate_method(:name).to(:subscriber).with_prefix }
      it { is_expected.to delegate_method(:balance).to(:account) }
      it { is_expected.to delegate_method(:debits_balance).to(:account) }
      it { is_expected.to delegate_method(:credits_balance).to(:account) }
      it { is_expected.to delegate_method(:entries).to(:account) }
    end

    it '.accounts' do
      account        = create(:asset)
      subscription   = create(:program_subscription)
      subscription_2 = create(:program_subscription)

      expect(described_class.accounts).to include(subscription.account)
      expect(described_class.accounts).to include(subscription_2.account)
      expect(described_class.accounts).to_not include(account)
    end

  end
end
