 require 'rails_helper'

module MembershipsModule
  describe TimeDeposit do
    context 'associations' do
      it { is_expected.to belong_to :cooperative }
    	it { is_expected.to belong_to :depositor }
    	it { is_expected.to belong_to :time_deposit_product }
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :liability_account }
      it { is_expected.to belong_to :interest_expense_account }
      it { is_expected.to belong_to :term }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:break_contract_fee).to(:time_deposit_product).with_prefix }
      it { is_expected.to delegate_method(:name).to(:time_deposit_product).with_prefix }
      it { is_expected.to delegate_method(:interest_rate).to(:time_deposit_product).with_prefix }
      it { is_expected.to delegate_method(:full_name).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:name).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:first_and_last_name).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:maturity_date).to(:term).with_prefix }
      it { is_expected.to delegate_method(:deposit_date).to(:term).with_prefix }
      it { is_expected.to delegate_method(:matured?).to(:term).with_prefix }
      it { is_expected.to delegate_method(:name).to(:office).with_prefix }
    end

    it '.matured' do
      matured_term         = create(:term, maturity_date: Date.yesterday)
      current_term         = create(:term, maturity_date: Date.current.next_month)
      matured_time_deposit = create(:time_deposit, term: matured_term)
      current_time_deposit = create(:time_deposit, term: current_term)
      expect(described_class.matured).to include(matured_time_deposit)
      expect(described_class.matured).to_not include(current_time_deposit)
    end

    it '#withdrawn?' do
      time_deposit           = create(:time_deposit, withdrawn_at: nil)
      withdrawn_time_deposit = create(:time_deposit, withdrawn_at: Date.current)

      expect(time_deposit.withdrawn?).to eql false
      expect(withdrawn_time_deposit.withdrawn?).to eql true
    end
  end
end
