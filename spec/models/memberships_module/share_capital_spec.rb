require 'rails_helper'

module MembershipsModule
  describe ShareCapital do
    context 'associations' do
      it { is_expected.to belong_to :cooperative }
    	it { is_expected.to belong_to :subscriber }
    	it { is_expected.to belong_to :share_capital_product }
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :equity_account }
      it { is_expected.to belong_to :interest_on_capital_account }
    	it { is_expected.to have_many :entries }
    end

    context 'delegations' do
      it { is_expected.to delegate_method(:name).to(:subscriber).with_prefix }
      it { is_expected.to delegate_method(:name).to(:office).with_prefix }
    	it { is_expected.to delegate_method(:name).to(:share_capital_product).with_prefix }
      it { is_expected.to delegate_method(:balance).to(:equity_account) }
      it { is_expected.to delegate_method(:default_product?).to(:share_capital_product).with_prefix }
      it { is_expected.to delegate_method(:cost_per_share).to(:share_capital_product).with_prefix }
      it { is_expected.to delegate_method(:minimum_balance).to(:share_capital_product).with_prefix }

    end

    it '.equity_accounts' do
      asset = create(:asset)
      share_capital   = create(:share_capital)
      share_capital_2 = create(:share_capital)

      expect(described_class.equity_accounts).to include(share_capital.equity_account)
      expect(described_class.equity_accounts).to include(share_capital_2.equity_account)
      expect(described_class.equity_accounts).to_not include(asset)
    end

    it '.interest_on_capital_accounts' do
      asset = create(:asset)
      share_capital = create(:share_capital)

      expect(described_class.interest_on_capital_accounts).to include(share_capital.interest_on_capital_account)
      expect(described_class.interest_on_capital_accounts).to_not include(asset)
    end

  end
end
