require 'rails_helper'

module MembershipsModule
  describe Saving do
    context "associations" do
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to belong_to :liability_account }
      it { is_expected.to belong_to :interest_expense_account }
    	it { is_expected.to belong_to :depositor }
      it { is_expected.to belong_to :office }
    	it { is_expected.to belong_to :saving_product }
      it { is_expected.to belong_to :saving_group }
      it { is_expected.to have_many :ownerships }
      it { is_expected.to have_many :member_depositors }
      it { is_expected.to have_many :entries }
      it { is_expected.to have_many :durations }
      it { is_expected.to have_many :lock_in_periods }
      it { is_expected.to have_many :entry_transactions }
      it { is_expected.to have_many :entries }

    end
    context 'delegations' do
    	it { is_expected.to delegate_method(:name).to(:saving_product).with_prefix }
      it { is_expected.to delegate_method(:applicable_rate).to(:saving_product).with_prefix }
      it { is_expected.to delegate_method(:closing_account).to(:saving_product).with_prefix }
      it { is_expected.to delegate_method(:name).to(:office).with_prefix }
      it { is_expected.to delegate_method(:name).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:current_address_complete_address).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:current_contact_number).to(:depositor).with_prefix }
      it { is_expected.to delegate_method(:current_occupation).to(:depositor).with_prefix }
    	it { is_expected.to delegate_method(:interest_rate).to(:saving_product).with_prefix }

    end
    it ".liability_accounts" do
      asset  = create(:asset)
      saving = create(:saving)

      expect(described_class.liability_accounts).to include(saving.liability_account)
      expect(described_class.liability_accounts).to_not include(asset)
    end

    it ".interest_expense_accounts" do
      asset = create(:asset)
      saving = create(:saving)

      expect(described_class.interest_expense_accounts).to include(saving.interest_expense_account)
      expect(described_class.interest_expense_accounts).to_not include(asset)
    end


    it '#closed?' do
      active = create(:saving, closed_at: nil)
      closed = create(:saving, closed_at: Date.current)

      expect(active.closed?).to be false
      expect(closed.closed?).to be true
    end
  end
end
