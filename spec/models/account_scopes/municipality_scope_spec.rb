require 'rails_helper'

module AccountScopes
  describe MunicipalityScope, type: :model do
    describe 'associations' do
      it { is_expected.to belong_to :municipality }
      it { is_expected.to belong_to :account }
    end
    describe 'validations' do
      it 'validate_uniqueness_of(:account_id).scoped_to(:municipality_id)' do
        municipality = create(:municipality)
        saving       = create(:saving)
        create(:municipality_scope, municipality: municipality, account: saving)
        account_2    = build(:municipality_scope, municipality: municipality, account: saving)
        account_2.save
        expect(account_2.errors[:account_id]).to eql(['has already been taken'])

      end
    end
  end
end
