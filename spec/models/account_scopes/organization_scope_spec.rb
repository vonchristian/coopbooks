require 'rails_helper'

module AccountScopes
  describe OrganizationScope do
    describe 'associations' do
      it { is_expected.to belong_to :organization }
      it { is_expected.to belong_to :account }
    end
    describe 'validations' do
      it 'validate unique account_id scoped to organization_id' do
        organization = create(:organization)
        saving       = create(:saving)
        create(:organization_scope, organization: organization, account: saving)
        org_scope = build(:organization_scope, organization: organization, account: saving)
        org_scope.save

        expect(org_scope.errors[:account_id]).to eql(['has already been taken'])
      end
    end
  end
end
