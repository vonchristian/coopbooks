require 'rails_helper'

module AccountScopes
  describe BarangayScope do
    describe 'associations' do
      it { is_expected.to belong_to :barangay }
      it { is_expected.to belong_to :account }
    end

    describe 'validations' do
      it 'validate_uniqueness_of(:account_id).scoped_to(:barangay_id)' do
        barangay = create(:barangay)
        saving       = create(:saving)
        create(:barangay_scope, barangay: barangay, account: saving)
        account_2    = build(:barangay_scope, barangay: barangay, account: saving)
        account_2.save
        expect(account_2.errors[:account_id]).to eql(['has already been taken'])

      end
    end
  end
end
