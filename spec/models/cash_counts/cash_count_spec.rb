require 'rails_helper'

module CashCounts
  describe CashCount do
    describe 'associations' do
      it { is_expected.to belong_to :bill }
      it { is_expected.to belong_to :cash_count_report }
    end
    describe 'validations' do
    end
    describe 'delegations' do
      it { is_expected.to delegate_method(:bill_amount).to(:bill) }
    end

    it '.total' do
      bill         = create(:bill, bill_amount: 100)
      cash_count_1 = create(:cash_count, bill: bill, quantity: 1)
      cash_count_2 = create(:cash_count, bill: bill, quantity: 2)

      expect(described_class.total).to eql 300
    end


    it "#subtotal" do
      bill         = create(:bill, bill_amount: 100)
      cash_count_1 = create(:cash_count, bill: bill, quantity: 1)
      cash_count_2 = create(:cash_count, bill: bill, quantity: 2)

      expect(cash_count_1.subtotal).to eql 100
      expect(cash_count_2.subtotal).to eql 200

    end
  end
end
