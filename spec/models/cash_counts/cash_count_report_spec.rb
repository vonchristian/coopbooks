require 'rails_helper'

module CashCounts
  describe CashCountReport do
    describe 'associations' do
      it { is_expected.to belong_to :employee }
      it { is_expected.to have_many :cash_counts }
      it { is_expected.to have_many :bills }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :date }
    end
  end
end 
