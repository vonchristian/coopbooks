require 'rails_helper'

module CashCounts
  describe Bill do
    describe 'validations' do
      it { is_expected.to validate_presence_of :bill_amount }
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of :name }
      it { is_expected.to validate_numericality_of :bill_amount }
    end
  end
end
