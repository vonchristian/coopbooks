require 'rails_helper'

describe BankAccount do
  describe 'associations' do
    it { is_expected.to belong_to :office }
    it { is_expected.to belong_to :cooperative }
    it { is_expected.to belong_to :cash_account }
    it { is_expected.to belong_to :interest_revenue_account }
  
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :bank_name }
    it { is_expected.to validate_presence_of :bank_address }
    it { is_expected.to validate_presence_of :account_number }
    it{ is_expected.to validate_uniqueness_of(:bank_name).scoped_to(:office_id).case_insensitive }
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:entries).to(:cash_account) }
    it { is_expected.to delegate_method(:balance).to(:cash_account) }
    it { is_expected.to delegate_method(:debits_balance).to(:cash_account) }
    it { is_expected.to delegate_method(:credits_balance).to(:cash_account) }
    it { is_expected.to delegate_method(:balance).to(:interest_revenue_account).with_prefix }


  end

  it "#name" do
    bank = build(:bank_account, bank_name: "LBP")
    expect(bank.name).to eql "LBP"
  end
end
