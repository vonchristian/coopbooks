require 'rails_helper'

describe StoreFront do
  describe 'associations' do
    it { is_expected.to belong_to :inventory_category }
    it { is_expected.to belong_to :sales_category }
    it { is_expected.to belong_to :sales_discount_category }
    it { is_expected.to belong_to :sales_return_category }
    it { is_expected.to belong_to :cost_of_goods_sold_category }
    it { is_expected.to belong_to :internal_use_category }
    it { is_expected.to belong_to :purchase_return_category }
    it { is_expected.to belong_to :spoilage_category }
    it { is_expected.to belong_to :receivable_category }
    it { is_expected.to have_many :orders }
    it { is_expected.to have_many :line_items }
    it { is_expected.to have_many :store_front_accounts }
    it { is_expected.to have_many :customers }
    it { is_expected.to have_many :suppliers }
    it { is_expected.to have_many :employee_store_front_accounts }
    it { is_expected.to have_many :employees }
    it { is_expected.to have_many :ledger_accounts }
    it { is_expected.to have_many :accounts }
  end
end
