require 'rails_helper'

describe Tag do
  describe 'associations' do
    it { is_expected.to have_many :taggings }
    it { is_expected.to have_many :taggables }
  end
end
