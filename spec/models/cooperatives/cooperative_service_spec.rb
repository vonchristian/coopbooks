require 'rails_helper'

module Cooperatives
  describe CooperativeService do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to have_many :ledger_accounts }
      it { is_expected.to have_many :accounts }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :title }
      it { is_expected.to validate_uniqueness_of :title }
    end
  end
end
