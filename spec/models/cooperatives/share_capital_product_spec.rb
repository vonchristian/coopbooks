require 'rails_helper'

module Cooperatives
	describe ShareCapitalProduct do
	  context 'associations' do
      it { is_expected.to belong_to :cooperative } 
	  	it { is_expected.to have_many :subscribers }
	  end
    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:cooperative_id).case_insensitive }
      it { is_expected.to validate_presence_of :cost_per_share }
      it { is_expected.to validate_presence_of :minimum_share }
      it { is_expected.to validate_numericality_of :cost_per_share }
      it { is_expected.to validate_numericality_of :minimum_share }

    end
	end
end
