require 'rails_helper'

module Cooperatives
  module AreaOfOperations
    describe OfficeBarangay do
      describe 'associations' do
        it { is_expected.to belong_to :office }
        it { is_expected.to belong_to :barangay }
      end
    end
  end
end
