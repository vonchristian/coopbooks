require 'rails_helper'

module Cooperatives
  module AreaOfOperations
    describe OfficeMunicipality, type: :model do
      describe 'associations' do
        it { is_expected.to belong_to :office }
        it { is_expected.to belong_to :municipality }
      end
    end
  end
end
