require 'rails_helper'

module Cooperatives
  module AreaOfOperations
    describe OfficeProvince, type: :model do
      describe 'associations' do
        it { is_expected.to belong_to :office }
        it { is_expected.to belong_to :province }
      end
      describe 'validations' do
        it 'validate_uniqueness_of province scoped to office' do
          office = create(:office)
          province = create(:province)
          office.office_provinces.create!(province: province)
          office_province = office.office_provinces.build(province: province)
          office_province.save
          expect(office_province.errors[:province_id]).to eql(["has already been taken"])
        end
      end
    end
  end
end
