require 'rails_helper'

module Cooperatives
  describe Office do
    describe 'associations' do
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to have_many :loans }
      it { is_expected.to have_many :amortization_schedules }
      it { is_expected.to have_many :savings }
      it { is_expected.to have_many :time_deposits }
      it { is_expected.to have_many :share_capitals }
      it { is_expected.to have_many :entries }
      it { is_expected.to have_many :bank_accounts }
      it { is_expected.to have_many :loan_applications }
      it { is_expected.to have_many :vouchers }
      it { is_expected.to have_many :accounts }
      it { is_expected.to have_many :office_saving_products }
      it { is_expected.to have_many :saving_products }
      it { is_expected.to have_many :office_share_capital_products }
      it { is_expected.to have_many :share_capital_products }
      it { is_expected.to have_many :office_time_deposit_products }
      it { is_expected.to have_many :time_deposit_products }
      it { is_expected.to have_many :office_loan_products }
      it { is_expected.to have_many :loan_products }
      it { is_expected.to have_many :office_programs }
      it { is_expected.to have_many :programs }
      it { is_expected.to have_many :accounts }
      it { is_expected.to have_many :memberships }
      it { is_expected.to have_many :member_memberships }
      it { is_expected.to have_many :membership_categories }
      it { is_expected.to have_many :interest_configs }
      it { is_expected.to have_many :amortization_types }
      it { is_expected.to have_many :loan_protection_plan_providers }
      it { is_expected.to have_many :cooperative_services }
      it { is_expected.to have_many :savings_account_applications }
      it { is_expected.to have_many :share_capital_applications }
      it { is_expected.to have_many :time_deposit_applications }
      it { is_expected.to have_many :program_subscriptions }
      it { is_expected.to have_many :organizations }
      it { is_expected.to have_many :bank_accounts }
      it { is_expected.to have_many :loan_groups }
      it { is_expected.to have_many :office_provinces }
      it { is_expected.to have_many :office_municipalities }
      it { is_expected.to have_many :office_barangays }
      it { is_expected.to have_many :provinces }
      it { is_expected.to have_many :municipalities }
      it { is_expected.to have_many :barangays }
      it { is_expected.to have_many :store_fronts }
      it { is_expected.to have_many :employee_cash_accounts }
      it { is_expected.to have_many :cash_accounts }
      it { is_expected.to have_many :saving_groups }
      it { is_expected.to have_many :level_one_account_categories }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_presence_of :contact_number }
      it { is_expected.to validate_presence_of :address }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:cooperative_id) }
    end

    it ".types" do
      expect(described_class.types).to eql ["Cooperatives::Offices::MainOffice", "Cooperatives::Offices::SatelliteOffice", "Cooperatives::Offices::BranchOffice"]
    end

    it "#normalized_type" do
      office = build(:office, type: "Cooperatives::Offices::MainOffice")

      expect(office.normalized_type).to eql "MainOffice"
    end
  end
end
