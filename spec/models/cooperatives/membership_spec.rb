require 'rails_helper'

module Cooperatives
  describe Membership do
    describe 'associations' do
      it { is_expected.to belong_to :cooperator }
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to belong_to :membership_category }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :cooperator_id }
      it { is_expected.to validate_presence_of(:cooperative_id) }
      it { is_expected.to validate_uniqueness_of :account_number }
      it { is_expected.to validate_presence_of :account_number }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:cooperative).with_prefix }
      it { is_expected.to delegate_method(:title).to(:membership_category).with_prefix }
    end
    it '.approved_at' do
      membership = create(:membership, membership_date: Date.current)
      old_membership = create(:membership, membership_date: Date.current.last_month)

      expect(described_class.approved_at(from_date: Date.current, to_date: Date.current)).to include(membership)
      expect(described_class.approved_at(from_date: Date.current.last_month, to_date: Date.current.last_month)).to include(old_membership)

    end
  end
end
