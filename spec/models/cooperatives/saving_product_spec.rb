require 'rails_helper'

module Cooperatives
  describe SavingProduct do
    describe "associations" do
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to have_many :savings }
    end

    describe "validations" do
    	it { is_expected.to validate_presence_of :interest_posting }
    	it { is_expected.to validate_presence_of :interest_rate }
    	it do
    		is_expected.to validate_numericality_of(:interest_rate).is_greater_than_or_equal_to(0.01)
        is_expected.to validate_numericality_of(:minimum_balance)
    	end
    	it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:office_id) }
    end

    describe 'enums' do
      it { is_expected.to define_enum_for(:interest_posting).with_values([:daily, :weekly, :monthly, :quarterly, :semi_annually, :annually]) }
    end

    it '.no_interests' do
      with_interest    = create(:saving_product, no_interest: false)
      without_interest = create(:saving_product, no_interest: true)

      expect(described_class.no_interests).to include(without_interest)
      expect(described_class.no_interests).to_not include(with_interest)
    end


    it "#balance_averager" do
      daily         = create(:saving_product, interest_posting: 'daily')

      annually      = create(:saving_product, interest_posting: 'annually')

      expect(daily.balance_averager).to eql SavingsModule::BalanceAveragers::Daily
      semi_annually = create(:saving_product, interest_posting: 'semi_annually')
      expect(annually.balance_averager).to eql SavingsModule::BalanceAveragers::Annually
      expect(semi_annually.balance_averager).to eql SavingsModule::BalanceAveragers::SemiAnnually
    end

    it ".total_savings" do
      saving_product     = create(:saving_product)
      subscriber         = create(:saving, saving_product: saving_product)
      another_subscriber = create(:saving, saving_product: saving_product)
      expect(described_class.total_savings).to eq 2
    end
  end
end
