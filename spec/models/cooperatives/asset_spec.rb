require 'rails_helper'

module Cooperatives
  describe Asset do
    describe 'associations' do
      it { is_expected.to belong_to :asset_account }
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to :cooperative }
    end
  end
end
