require 'rails_helper'

module Cooperatives
  describe MembershipCategory do
    describe 'associations' do
      it { is_expected.to belong_to :cooperative }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :title }
      it { is_expected.to validate_uniqueness_of(:title).scoped_to(:cooperative_id) }
    end
  end
end
