require 'rails_helper'

describe ShareCapitalApplication do
  describe 'associations' do
    it { is_expected.to belong_to(:cart).optional }
    it { is_expected.to belong_to :subscriber }
    it { is_expected.to belong_to :share_capital_product }
    it { is_expected.to belong_to :cooperative }
    it { is_expected.to belong_to :office }
    it { is_expected.to belong_to :equity_account }
  end
end
