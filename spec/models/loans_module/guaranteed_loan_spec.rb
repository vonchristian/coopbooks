require 'rails_helper'

module LoansModule
  describe GuaranteedLoan do
    describe 'associations' do
      it { is_expected.to belong_to :loan }
      it { is_expected.to belong_to :guarantor }
    end
  end
end 
