require 'rails_helper'

module LoansModule
  module Loans
    describe LoanDiscount do
      describe 'associations' do
        it { is_expected.to belong_to :loan }
        it { is_expected.to belong_to :employee }
      end

      describe 'enums' do
        it { is_expected.to define_enum_for(:discount_type).with_values([:interest, :penalty])}
      end

      it '.total' do
        discount_1 = create(:loan_discount, amount: 500)
        discount_2 = create(:loan_discount, amount: 500)

        expect(described_class.total).to eql 1_000
      end
    end
  end
end
