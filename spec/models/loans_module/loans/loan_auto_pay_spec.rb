require 'rails_helper'

module LoansModule
  module Loans
    describe LoanAutoPay, type: :model do
      it 'associations' do
        borrower      = create(:member)
        loan          = create(:loan, borrower: borrower)
        saving        = create(:saving, depositor: borrower)
        loan_auto_pay = create(:loan_auto_pay, loan: loan, savings_account: saving)
        expect(loan_auto_pay).to belong_to(:loan)
        expect(loan_auto_pay).to belong_to(:savings_account)
      end
      describe 'validations' do
        it 'unique savings_id per loan' do
          borrower      = create(:member)
          loan          = create(:loan, borrower: borrower)
          saving        = create(:saving, depositor: borrower)
          create(:loan_auto_pay, loan: loan, savings_account: saving)
          loan_auto_pay = build(:loan_auto_pay, loan: loan, savings_account: saving)
          loan_auto_pay.save

          expect(loan_auto_pay.errors[:savings_account_id]).to eql ['has already been taken']
        end
      end

      it '.active' do
        active_auto_pay   = create(:loan_auto_pay, active: true)
        inactive_auto_pay = create(:loan_auto_pay, active: false)

        expect(described_class.active).to include(active_auto_pay)
        expect(described_class.active).to_not include(inactive_auto_pay)
      end
    end
  end
end
