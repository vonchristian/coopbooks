require 'rails_helper'

module LoansModule
  module Loans
    describe LoanAccount do
      describe 'associations' do
        it { is_expected.to belong_to :loan }
        it { is_expected.to belong_to :account }
      end

      describe 'validations' do
        it 'unique account scoped to loan' do
          loan  = create(:loan)
          asset = create(:asset)
          create(:loan_account, loan: loan, account: asset)
          loan_account = build(:loan_account, loan: loan, account: asset)
          loan_account.save

          expect(loan_account.errors[:account_id]).to eql ['has already been taken']
        end
      end

      it '.accounts' do
        loan_account = create(:loan_account)
        asset        = create(:asset)

        expect(described_class.accounts).to include(loan_account.account)
        expect(described_class.accounts).to_not include(asset)
      end
    end
  end
end
