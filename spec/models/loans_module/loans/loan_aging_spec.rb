require 'rails_helper'

module LoansModule
  module Loans
    describe LoanAging do
      describe 'associations' do
        it { is_expected.to belong_to :loan }
        it { is_expected.to belong_to :loan_group }
      end

      describe 'validations' do
        it 'unique loan_group scoped to loan' do
          loan_group = create(:loan_group)
          loan       = create(:loan)
          loan.loan_groups << loan_group
          loan_aging = build(:loan_aging, loan: loan, loan_group: loan_group)
          loan_aging.save

          expect(loan_aging.errors[:loan_group_id]).to eql(['has already been taken'])
        end
      end

      it '.current' do
        old    = create(:loan_aging, date: Date.current.last_year)
        recent = create(:loan_aging, date: Date.current)

        expect(described_class.current).to eql(recent)
        expect(described_class.current).to_not eql(old)
      end
    end
  end
end
