require 'rails_helper'

module LoansModule
  describe LoanApplication do
    describe 'associations' do
      it { is_expected.to belong_to :loan_product }
      it { is_expected.to belong_to :cart }
      it { is_expected.to belong_to :receivable_account }
      it { is_expected.to belong_to :interest_revenue_account }
      it { is_expected.to belong_to :borrower }
      it { is_expected.to belong_to :preparer }
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to belong_to :office }
      it { is_expected.to belong_to(:voucher).optional }
      it { is_expected.to have_one :loan }
      it { is_expected.to have_many :amortization_schedules }
      it { is_expected.to have_many :guaranteed_loan_applications }
      it { is_expected.to have_many :member_guarantors }
    end
    describe 'validations' do
     it {is_expected.to monetize(:loan_amount) }
     it { is_expected.to validate_presence_of :mode_of_payment }
     it { is_expected.to validate_presence_of :application_date }
     it { is_expected.to validate_presence_of :term }
     it { is_expected.to validate_numericality_of :term }
     it { is_expected.to validate_presence_of :account_number }
     it { is_expected.to validate_presence_of :interest_rate }
     it { is_expected.to validate_numericality_of :interest_rate }
     it { is_expected.to validate_presence_of :interest_calculation_type }
     it { is_expected.to validate_presence_of :interest_prededuction_rate }
     it { is_expected.to validate_numericality_of :interest_prededuction_rate }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:borrower).with_prefix }
      it { is_expected.to delegate_method(:interest_revenue_account).to(:loan_product).with_prefix }
      it { is_expected.to delegate_method(:current_interest_config).to(:loan_product) }
      it { is_expected.to delegate_method(:amortization_type).to(:loan_product) }

      it { is_expected.to delegate_method(:avatar).to(:borrower).with_prefix }
      it { is_expected.to delegate_method(:straight_balance?).to(:current_interest_config).with_prefix }
      it { is_expected.to delegate_method(:annually?).to(:current_interest_config).with_prefix }
    end

    describe 'enums' do
      it { is_expected.to define_enum_for(:mode_of_payment).with_values([ :daily, :weekly, :monthly, :semi_monthly, :quarterly, :semi_annually, :lumpsum]) }
      it { is_expected.to define_enum_for(:interest_calculation_type).with_values([:prededucted, :add_on]) }
    end

  end
end
