require 'rails_helper'
module LoansModule
  describe Loan do
    context 'associations' do
      it { is_expected.to have_one :loan_auto_pay }
      it { is_expected.to belong_to(:voucher).optional }
      it { is_expected.to belong_to :receivable_account }
      it { is_expected.to belong_to :interest_revenue_account }
      it { is_expected.to belong_to(:unearned_interest_account).optional }
      it { is_expected.to belong_to :penalty_revenue_account }
      it { is_expected.to belong_to(:loan_application).optional }
      it { is_expected.to belong_to :cooperative }
      it { is_expected.to belong_to :office }
    	it { is_expected.to belong_to :borrower }
    	it { is_expected.to belong_to :loan_product }
      it { is_expected.to have_many :amortization_schedules }
      it { is_expected.to have_many :loan_terms }
      it { is_expected.to have_many :terms }
      it { is_expected.to have_many :notices }
      it { is_expected.to have_many :loan_interests }
      it { is_expected.to have_many :loan_penalties }
      it { is_expected.to have_many :loan_discounts }
      it { is_expected.to have_many :notes }
      it { is_expected.to have_many :loan_co_makers }
      it { is_expected.to have_many :member_co_makers }
      it { is_expected.to have_many :loan_accounts }
      it { is_expected.to have_many :accounts }
      it { is_expected.to have_many :entries }
      it { is_expected.to have_many :loan_agings }
      it { is_expected.to have_many :loan_groups }
      it { is_expected.to have_many :guaranteed_loans }
      it { is_expected.to have_many :member_guarantors }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :borrower_full_name }
    end

    context 'delegations' do
    	it { is_expected.to delegate_method(:name).to(:borrower).with_prefix }
      it { is_expected.to delegate_method(:age).to(:borrower).with_prefix }
      it { is_expected.to delegate_method(:contact_number).to(:borrower).with_prefix }
      it { is_expected.to delegate_method(:current_address).to(:borrower).with_prefix }
    	it { is_expected.to delegate_method(:name).to(:loan_product).with_prefix }
      it { is_expected.to delegate_method(:maximum_loanable_amount).to(:loan_product) }
      it { is_expected.to delegate_method(:interest_rate).to(:loan_product).with_prefix }
      it { is_expected.to delegate_method(:avatar).to(:borrower) }
      it { is_expected.to delegate_method(:name).to(:barangay).with_prefix }
      it { is_expected.to delegate_method(:name).to(:office).with_prefix }
      it { is_expected.to delegate_method(:balance).to(:receivable_account) }
      it { is_expected.to delegate_method(:payment_processor).to(:loan_product) }
    end

    describe 'enums' do
      it { is_expected.to define_enum_for(:status).with_values([:current_loan, :past_due]) }
    end

    it '.paid_loans' do
      paid_loan   = create(:loan ,paid_at: Date.current)
      unpaid_loan = create(:loan ,paid_at: nil)

      expect(described_class.paid_loans).to include(paid_loan)
      expect(described_class.paid_loans).to_not include(unpaid_loan)
    end

    it '.receivable_accounts' do
      loan   = create(:loan)
      loan_2 = create(:loan)

      expect(described_class.receivable_accounts).to include(loan.receivable_account)
      expect(described_class.receivable_accounts).to include(loan_2.receivable_account)
    end

    it '.interest_revenue_accounts' do
      loan   = create(:loan)
      loan_2 = create(:loan)

      expect(described_class.interest_revenue_accounts).to include(loan.interest_revenue_account)
      expect(described_class.interest_revenue_accounts).to include(loan_2.interest_revenue_account)
    end

    it '.penalty_revenue_accounts' do
      loan   = create(:loan)
      loan_2 = create(:loan)

      expect(described_class.penalty_revenue_accounts).to include(loan.penalty_revenue_account)
      expect(described_class.penalty_revenue_accounts).to include(loan_2.penalty_revenue_account)
    end

    it ".active" do
      loan          = create(:loan)
      archived_loan = create(:loan, archived_at: Date.current)

      expect(described_class.active).to include(loan)
      expect(described_class.active).to_not include(archived_loan)
    end

    it '.loan_accounts' do
      loan   = create(:loan)
      loan_account = create(:loan_account, loan: loan)

      expect(described_class.loan_accounts).to include(loan_account)
    end

    it '.updated_at(args={})' do
      updated_loan = create(:loan, disbursement_date: Date.current.last_month)
      loan         = create(:loan)
      updated_loan.accounts << updated_loan.receivable_account
      cash         = create(:asset)

      entry = build(:entry, entry_date: Date.current)
      entry.debit_amounts.build(account: cash, amount: 1000)
      entry.credit_amounts.build(account: updated_loan.receivable_account, amount: 1000)
      entry.save!

      expect(described_class.updated_at(from_date: Date.current, to_date: Date.current)).to include(updated_loan)
      expect(described_class.updated_at(from_date: Date.current, to_date: Date.current)).to_not include(loan)

    end

    it '.auto_pay_enabled' do
      borrower      = create(:member)
      loan          = create(:loan, borrower: borrower)
      saving        = create(:saving, depositor: borrower)
      loan_auto_pay = create(:loan_auto_pay, loan: loan, active: true, savings_account: saving)
      loan_2        = create(:loan)
      expect(described_class.auto_pay_enabled).to include(loan)
      expect(described_class.auto_pay_enabled).to_not include(loan_2)

    end



    it ".past_due_loans" do
      past_due_loan = create(:disbursed_loan, disbursement_date: Date.today - 2.months)
      past_due_term = create(:term, maturity_date: Date.today.last_month)
      loan          = create(:loan)
      past_due_loan.terms << past_due_term

      expect(described_class.past_due_loans).to include(past_due_loan)
      expect(described_class.past_due_loans).to_not include(loan)
    end

    it ".past_due_loans_on(args)" do
      next_month_loan = create(:disbursed_loan)
      next_year_loan  = create(:disbursed_loan)
      next_year_term  = create(:term, maturity_date: Date.today.next_year)
      next_month_term = create(:term, maturity_date: Date.today.next_month)
      next_month_loan.terms << next_month_term
      next_year_loan.terms << next_year_term

      expect(described_class.past_due_loans_on(from_date: Date.today.next_month.beginning_of_month, to_date: Date.today.next_month.end_of_month)).to include(next_month_loan)
      expect(described_class.past_due_loans_on(from_date: Date.today.next_month.beginning_of_month, to_date: Date.today.next_month.end_of_month)).to_not include(next_year_loan)
      expect(described_class.past_due_loans_on(from_date: Date.current.next_year.beginning_of_year, to_date: Date.current.next_year.end_of_year)).to include(next_year_loan)
      expect(described_class.past_due_loans_on(from_date: Date.current.next_year.beginning_of_year, to_date: Date.current.next_year.end_of_year)).to_not include(next_month_loan)
    end

    it ".not_archived" do
      loan = create(:loan, archived_at: nil)
      archived_loan = create(:loan, archived_at: Date.current)

      expect(described_class.not_archived).to include(loan)
      expect(described_class.not_archived).to_not include(archived_loan)
    end

    it ".archived" do
      loan = create(:loan, archived_at: nil)
      archived_loan = create(:loan, archived_at: Date.current)

      expect(described_class.archived).to_not include(loan)
      expect(described_class.archived).to include(archived_loan)
    end

    it '.disbursed' do
      disbursed_loan   = create(:loan, disbursement_date: Date.current)
      undisbursed_loan = create(:loan, disbursement_date: nil)

      expect(described_class.disbursed).to include(disbursed_loan)
      expect(described_class.disbursed).to_not include(undisbursed_loan)
    end

    it '#terms_elapsed' do
    end
    it '#balance_for_loan_group(loan_group)' do
      office        = create(:office)
      loan_group    = create(:loan_group, office: office, start_num: 0, end_num: 0)
      current_term  = create(:term, maturity_date: Date.current.next_month)
      past_due_term = create(:term, maturity_date: Date.current - 30.days)
      current_loan  = create(:loan, office: office)
      past_due_loan = create(:loan, office: office)

      past_due_loan.terms << past_due_term
      current_loan.terms  << current_term
      LoansModule::Loans::LoanGroupUpdate.new(loan: current_loan, date: Date.current).set_loan_group
      LoansModule::Loans::LoanGroupUpdate.new(loan: past_due_loan, date: Date.current).set_loan_group
    end
  end
end
