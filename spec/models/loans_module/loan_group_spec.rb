require 'rails_helper'

module LoansModule
  describe LoanGroup do
    describe 'associations' do
      it { is_expected.to belong_to :office }
      it { is_expected.to have_many :loan_agings }
      it { is_expected.to have_many :loans }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :title }
      it { is_expected.to validate_presence_of :start_num }
      it { is_expected.to validate_presence_of :end_num }
      it { is_expected.to validate_numericality_of :start_num }
      it { is_expected.to validate_numericality_of :end_num }

      it 'validate uniqueness of title scoped to office' do
        office       = create(:office)
        loan_group   = create(:loan_group, title: 'Loan Group 1', office: office)
        loan_group_2 = build(:loan_group, title: 'Loan Group 1', office: office)
        loan_group_2.save
        expect(loan_group_2.errors[:title]).to eql ["has already been taken"]
      end
    end

    it '#num_range' do
      loan_group = build(:loan_group, start_num: 1, end_num: 30)

      expect(loan_group.num_range).to eql 1..30
    end
  end
end
