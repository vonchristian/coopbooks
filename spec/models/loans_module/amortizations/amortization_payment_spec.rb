require 'rails_helper'

module LoansModule
  module Amortizations
    describe AmortizationPayment do
      describe 'associations' do
        it { is_expected.to belong_to :amortization_schedule }
        it { is_expected.to belong_to :entry }
      end 
    end
  end
end
