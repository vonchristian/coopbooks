require 'rails_helper'

module LoansModule
  describe GuaranteedLoanApplication  do
    describe 'associations' do
      it { is_expected.to belong_to :loan_application }
      it { is_expected.to belong_to :guarantor }
    end
  end
end
