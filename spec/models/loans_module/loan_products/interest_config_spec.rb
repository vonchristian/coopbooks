require 'rails_helper'

module LoansModule
  module LoanProducts
    describe InterestConfig do
      it { is_expected.to define_enum_for(:calculation_type).with_values([:add_on, :prededucted])}

      describe 'associations' do
        it { is_expected.to belong_to :loan_product }
      end

      describe "validations" do
        it { is_expected.to validate_presence_of :rate }
        it { is_expected.to validate_numericality_of :rate }
      end

      it ".current" do
        interest_config         = create(:interest_config, created_at: Date.today.yesterday)
        current_interest_config = create(:interest_config, created_at: Date.today)

        expect(described_class.current).to eql current_interest_config
        expect(described_class.current).to_not eql interest_config
      end

      it "#compute_interest" do
        interest_config   = create(:interest_config, rate: 0.12)
        interest_config_2 = create(:interest_config, rate: 0.15)

        expect(interest_config.compute_interest(amount: 100_000, term: 12)).to eql 12_000.00
        expect(interest_config_2.compute_interest(amount: 100_000, term: 12)).to eql 15_000.00
      end

      it "#monthly_interest_rate" do
        interest_config = build_stubbed(:interest_config, rate: 0.12)

        expect(interest_config.monthly_interest_rate).to eql 0.01
      end
    end
  end
end
