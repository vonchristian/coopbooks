require 'rails_helper'

module LoansModule
  describe LoanTerm do
    describe 'associations' do
      it { is_expected.to belong_to :loan }
      it { is_expected.to belong_to :term }
    end
  end
end
