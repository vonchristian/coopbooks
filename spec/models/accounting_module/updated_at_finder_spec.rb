require 'rails_helper'

module AccountingModule
  describe UpdatedAtFinder do
    it 'updated_at(args)' do
      asset             = create(:asset)
      cash              = create(:asset)
      revenue           = create(:revenue)
      account_category1 = create(:asset_level_one_account_category)
      account_category2 = create(:revenue_level_one_account_category)
      account_category1.accounts << cash
      account_category1.accounts << revenue

      entry1 = build(:entry, entry_date: Date.current)
      entry1.debit_amounts.build(account: cash, amount: 500)
      entry1.credit_amounts.build(account: revenue, amount: 500)
      entry1.save!

      expect(AccountingModule::LevelOneAccountCategory.updated_at(from_date: Date.current, to_date: Date.current)).to include(account_category1)
      expect(AccountingModule::LevelOneAccountCategory.updated_at(from_date: Date.current, to_date: Date.current)).to_not include(account_category2)
      expect(AccountingModule::Account.updated_at(from_date: Date.current, to_date: Date.current)).to include(cash)
      expect(AccountingModule::Account.updated_at(from_date: Date.current, to_date: Date.current)).to include(revenue)
      expect(AccountingModule::Account.updated_at(from_date: Date.current, to_date: Date.current)).to_not include(asset)


    end
  end
end
