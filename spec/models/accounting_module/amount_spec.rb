require 'rails_helper'
require "money-rails/test_helpers"

module AccountingModule
  describe Amount do

    subject { build(:amount) }
    it { is_expected.to_not be_valid }

    describe 'associations' do
      it { is_expected.to belong_to :entry }
      it { is_expected.to belong_to :account }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of     :type }
      it { is_expected.to validate_presence_of     :account }
      it { is_expected.to validate_presence_of     :entry }
      it { is_expected.to validate_presence_of     :account }
      it {is_expected.to monetize(:amount) }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:account).with_prefix }
      it { is_expected.to delegate_method(:display_name).to(:account).with_prefix }
      it { is_expected.to delegate_method(:recorder).to(:entry) }
      it { is_expected.to delegate_method(:reference_number).to(:entry) }
      it { is_expected.to delegate_method(:description).to(:entry) }
      it { is_expected.to delegate_method(:entry_date).to(:entry) }
      it { is_expected.to delegate_method(:name).to(:recorder).with_prefix }
    end

    context 'scopes' do
      it '.debits' do
        cash_on_hand  = create(:asset)
        revenue       = create(:revenue)
        entry         = build(:entry)
        debit_amount  = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
        credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
        entry.save!

        expect(described_class.debits).to include(debit_amount)
        expect(described_class.debits).to_not include(credit_amount)
      end

      it '.credits' do
        cash_on_hand  = create(:asset)
        revenue       = create(:revenue)
        entry         = build(:entry)
        debit_amount  = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
        credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
        entry.save!

        expect(described_class.credits).to include(credit_amount)
        expect(described_class.credits).to_not include(debit_amount)
      end

      it ".for_recorder(args={})" do

        cash_on_hand  = create(:asset)
        revenue       = create(:revenue)
        entry         = build(:entry)
        debit_amount  = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
        credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
        entry.save!

        expect(described_class.for_recorder(recorder_id: entry.recorder_id)).to include(debit_amount)
        expect(described_class.for_recorder(recorder_id: entry.recorder_id)).to include(credit_amount)
      end


      it ".total_cash_amount" do
        cash_account = create(:employee_cash_account)
        revenue      = create(:revenue)
        entry        = build(:entry)
        entry.debit_amounts.build(account: cash_account.cash_account, amount: 1_000)
        entry.credit_amounts.build(amount: 1_000, account: revenue )
        entry.save!

        expect(described_class.total_cash_amount).to eql 1_000
      end
    end


    it "#entered_on(args={})" do
      cash_on_hand         = create(:asset)
      revenue              = create(:revenue)
      entry                = build(:entry, entry_date: Date.today )
      recent_debit_amount  = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
      recent_credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      old_entry         = build(:entry, entry_date: Date.today.yesterday )
      old_debit_amount  = old_entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
      old_credit_amount = old_entry.credit_amounts.build(amount: 1_000, account: revenue )
      old_entry.save!

      expect(described_class.entered_on(from_date: Date.today, to_date: Date.today)).to include(recent_debit_amount)
      expect(described_class.entered_on(from_date: Date.today, to_date: Date.today)).to include(recent_credit_amount)
      expect(described_class.entered_on(from_date: Date.today, to_date: Date.today)).to_not include(old_debit_amount)
      expect(described_class.entered_on(from_date: Date.today, to_date: Date.today)).to_not include(old_credit_amount)

      expect(described_class.entered_on(from_date: Date.today.yesterday, to_date: Date.today.yesterday)).to include(old_credit_amount)
      expect(described_class.entered_on(from_date: Date.today.yesterday, to_date: Date.today.yesterday)).to include(old_debit_amount)
      expect(described_class.entered_on(from_date: Date.today.yesterday, to_date: Date.today.yesterday)).to_not include(recent_debit_amount)
      expect(described_class.entered_on(from_date: Date.today.yesterday, to_date: Date.today.yesterday)).to_not include(recent_credit_amount)
    end

    it ".for_account(args={})" do
      cash_on_hand         = create(:asset)
      revenue              = create(:revenue)
      entry                = build(:entry)
      recent_debit_amount  = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
      recent_credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      expect(described_class.for_account(account_id: cash_on_hand.id)).to include(recent_debit_amount)
      expect(described_class.for_account(account_id: cash_on_hand.id)).to_not include(recent_credit_amount)

      expect(described_class.for_account(account_id: revenue.id)).to include(recent_credit_amount)
      expect(described_class.for_account(account_id: revenue.id)).to_not include(recent_debit_amount)
    end


    it ".excluding_account(args={})" do
      cash_on_hand = create(:asset)
      revenue = create(:revenue)
      entry = build(:entry)
      recent_debit_amount = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
      recent_credit_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      expect(described_class.excluding_account(account_id: cash_on_hand.id)).to include(recent_credit_amount)
      expect(described_class.excluding_account(account_id: cash_on_hand.id)).to_not include(recent_debit_amount)

      expect(described_class.excluding_account(account_id: revenue.id)).to include(recent_debit_amount)
      expect(described_class.excluding_account(account_id: revenue.id)).to_not include(recent_credit_amount)
    end

    it ".with_cash_accounts" do
      revenue             = create(:revenue)
      cash_account        = create(:employee_cash_account)
      entry               = build(:entry)
      cash_on_hand_amount = entry.debit_amounts.build(account: cash_account.cash_account, amount: 1_000)
      revenue_amount      = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      expect(described_class.with_cash_accounts).to include(cash_on_hand_amount)
      expect(described_class.with_cash_accounts).to_not include(revenue_amount)

      expect(described_class.without_cash_accounts).to include(revenue_amount)
      expect(described_class.without_cash_accounts).to_not include(cash_on_hand_amount)
    end

    it ".without_cash_accounts" do
      cash_account = create(:employee_cash_account)
      revenue      = create(:revenue)
      entry        = build(:entry)
      cash_on_hand_amount = entry.debit_amounts.build(account: cash_account.cash_account, amount: 1_000)
      revenue_amount = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      expect(described_class.without_cash_accounts).to include(revenue_amount)
      expect(described_class.without_cash_accounts).to_not include(cash_on_hand_amount)
    end



    it "#debit?" do
      debit_amount = build(:debit_amount)
      credit_amount = build(:credit_amount)

      expect(debit_amount.debit?).to be true
      expect(credit_amount.debit?).to be false
    end

    it "#credit?" do
      debit_amount = build(:debit_amount)
      credit_amount = build(:credit_amount)

      expect(debit_amount.credit?).to be false
      expect(credit_amount.credit?).to be true
    end

    it ".total" do
      cash_on_hand        = create(:asset)
      revenue             = create(:revenue)
      entry               = build(:entry)
      cash_on_hand_amount = entry.debit_amounts.build(account: cash_on_hand, amount: 1_000)
      revenue_amount      = entry.credit_amounts.build(amount: 1_000, account: revenue )
      entry.save!

      expect(entry.amounts.total).to eql 2_000
      expect(entry.debit_amounts.total).to eql 1_000
      expect(entry.credit_amounts.total).to eql 1_000
    end
  end
end
