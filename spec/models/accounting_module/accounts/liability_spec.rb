require 'spec_helper'

module AccountingModule
  module Accounts
    describe Liability do
      it_behaves_like 'a AccountingModule::Account subtype', kind: :liability, normal_balance: :credit
    end
  end 
end
