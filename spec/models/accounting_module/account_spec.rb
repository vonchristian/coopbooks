require 'rails_helper'

module AccountingModule
  describe Account, type: :model do
    describe 'associations' do
      it { is_expected.to belong_to :level_one_account_category }
      it { is_expected.to belong_to :office }
      it { is_expected.to have_many :amounts }
      it { is_expected.to have_many :credit_amounts }
      it { is_expected.to have_many :debit_amounts }
      it { is_expected.to have_many :entries }
      it { is_expected.to have_many :debit_entries }
      it { is_expected.to have_many :credit_entries }
      it { is_expected.to have_many :account_budgets }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :type }
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_presence_of :code }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:office_id) }
      it { is_expected.to validate_uniqueness_of(:code).scoped_to(:office_id) }

    end
    describe 'scopes' do
      let(:asset)     { create(:asset) }
      let(:liability) { create(:liability) }
      let(:equity)    { create(:equity) }
      let(:revenue)   { create(:revenue) }
      let(:expense)   { create(:expense) }

      it ".assets" do
        expect(described_class.assets).to include asset
        expect(described_class.assets).to_not include liability
        expect(described_class.assets).to_not include equity
        expect(described_class.assets).to_not include revenue
        expect(described_class.assets).to_not include expense
      end

      it ".liabilities" do
        expect(described_class.liabilities).to include liability
        expect(described_class.liabilities).to_not include asset
        expect(described_class.liabilities).to_not include equity
        expect(described_class.liabilities).to_not include revenue
        expect(described_class.liabilities).to_not include expense
      end

      it ".equities" do
        expect(described_class.equities).to include equity
        expect(described_class.equities).to_not include asset
        expect(described_class.equities).to_not include liability
        expect(described_class.equities).to_not include revenue
        expect(described_class.equities).to_not include expense
      end

      it ".revenues" do
        expect(described_class.revenues).to include revenue
        expect(described_class.revenues).to_not include asset
        expect(described_class.revenues).to_not include liability
        expect(described_class.revenues).to_not include equity
        expect(described_class.revenues).to_not include expense
      end

      it ".expense" do
        expect(described_class.expenses).to include expense
        expect(described_class.expenses).to_not include asset
        expect(described_class.expenses).to_not include liability
        expect(described_class.expenses).to_not include equity
        expect(described_class.expenses).to_not include revenue
      end
    end

    it ".updated_at(args)" do
      old_account      = create(:asset)
      updated_account  = create(:asset)
      cash_account     = create(:asset)
      entry            = build(:entry, entry_date: Date.current)
      entry.debit_amounts.build(account: updated_account)
      entry.credit_amounts.build(account: cash_account)
      entry.save!

      expect(described_class.updated_at(from_date: Date.current, to_date: Date.current)).to include(updated_account)
      expect(described_class.updated_at(from_date: Date.current, to_date: Date.current)).to_not include(old_account)

    end

    it ".updated_by(employee)" do
      liability   = create(:liability)
      asset       = create(:asset)
      revenue     = create(:revenue)
      entry       = build(:entry)
      entry.credit_amounts << create(:credit_amount, account: asset)
      entry.debit_amounts  << create(:debit_amount, account: revenue)
      entry.save!

      expect(described_class.updated_by(entry.recorder)).to include(asset)
      expect(described_class.updated_by(entry.recorder)).to include(revenue)
      expect(described_class.updated_by(entry.recorder)).to_not include(liability)
    end

    it ".types" do
      expect(described_class.types).to eql  ["AccountingModule::Accounts::Asset",
       "AccountingModule::Accounts::Equity",
       "AccountingModule::Accounts::Liability",
       "AccountingModule::Accounts::Expense",
       "AccountingModule::Accounts::Revenue"]
     end

    it "#account_name" do
      asset = build(:asset, name: "Cash on Hand")
      expect(asset.account_name).to eql "Cash on Hand"
    end
    context 'balance' do
      let(:account) { build(:account) }
      subject { account }

      it { is_expected.to_not be_valid }  # must construct a child type instead

      describe "when using a child type" do
        let(:account) { create(:account, type: "AccountingModule::Asset") }
        it { is_expected.to be_valid }
      end

      it "calling the instance method #balance should raise a NoMethodError" do
        expect { subject.balance }.to raise_error NoMethodError, "undefined method 'balance'"
      end
    end





    describe ".trial_balance" do
      subject { described_class.trial_balance }
      it { is_expected.to be_kind_of BigDecimal }

      context "when given no entries" do
        it { is_expected.to eql 0 }
      end

      context "when given correct entries" do
        before {
          office = create(:office)
          # credit accounts
          liability      = create(:liability, office: office)
          equity         = create(:equity, office: office)
          revenue        = create(:revenue, office: office)
          contra_asset   = create(:asset, :contra => true, office: office)
          contra_expense = create(:expense, :contra => true, office: office)
          # credit amounts
          ca1 = build(:credit_amount, :account => liability, :amount => 100000)
          ca2 = build(:credit_amount, :account => equity, :amount => 1000)
          ca3 = build(:credit_amount, :account => revenue, :amount => 40404)
          ca4 = build(:credit_amount, :account => contra_asset, :amount => 2)
          ca5 = build(:credit_amount, :account => contra_expense, :amount => 333)

          # debit accounts
          asset            = create(:asset, office: office)
          expense          = create(:expense, office: office)
          contra_liability = create(:liability, :contra => true, office: office)
          contra_equity    = create(:equity, :contra => true, office: office)
          contra_revenue   = create(:revenue, :contra => true, office: office)
          # debit amounts
          da1 = build(:debit_amount, :account => asset, :amount => 100000)
          da2 = build(:debit_amount, :account => expense, :amount => 1000)
          da3 = build(:debit_amount, :account => contra_liability, :amount => 40404)
          da4 = build(:debit_amount, :account => contra_equity, :amount => 2)
          da5 = build(:debit_amount, :account => contra_revenue, :amount => 333)

          create(:entry, :credit_amounts => [ca1], :debit_amounts => [da1])
          create(:entry, :credit_amounts => [ca2], :debit_amounts => [da2])
          create(:entry, :credit_amounts => [ca3], :debit_amounts => [da3])
          create(:entry, :credit_amounts => [ca4], :debit_amounts => [da4])
          create(:entry, :credit_amounts => [ca5], :debit_amounts => [da5])
        }

        it { is_expected.to eql 0 }
      end

      it '#normalized_type' do
        asset = create(:asset)
        expect(asset.normalized_type).to eql "Asset"
      end
    end
  end
end
