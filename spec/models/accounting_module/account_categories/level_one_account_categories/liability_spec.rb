require 'rails_helper'

module AccountingModule
  module AccountCategories
    module LevelOneAccountCategories
      describe Liability do
        it 'normal_credit_balance' do
          expect(described_class.normal_credit_balance).to eql false
        end
      end
    end
  end
end
