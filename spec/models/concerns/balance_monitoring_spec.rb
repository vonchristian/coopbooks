require 'rails_helper'

describe BalanceMonitoring do
  let(:min_saving)   { create(:saving, has_minimum_balance: true) { include BalanceMonitoring } }
  let(:below_saving) { create(:saving, has_minimum_balance: false) { include BalanceMonitoring } }

  it 'below_minimum_balance' do
    expect(MembershipsModule::Saving.below_minimum_balance).to include(below_saving)
    expect(MembershipsModule::Saving.below_minimum_balance).to_not include(min_saving)
  end

  it 'has_minimum_balance' do
    expect(MembershipsModule::Saving.has_minimum_balances).to include(min_saving)
    expect(MembershipsModule::Saving.has_minimum_balances).to_not include(below_saving)
  end
end
