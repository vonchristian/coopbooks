require 'rails_helper'

describe TimeRange do
  it '#range' do
    range = (described_class.new(from_time: Time.now.beginning_of_day, to_time: Time.now.end_of_day).range)
    expect(range).to eql Time.now.beginning_of_day..Time.now.end_of_day
  end
  describe '#start_time' do
    it 'returns from_time if is_a?(Time)' do
      expect(described_class.new(from_time: Time.now.beginning_of_day, to_time: Time.now.end_of_day).start_time).to eql(Time.now.beginning_of_day)
    end
    it 'parses from_time if is a string' do
      start_time = (described_class.new(from_time: Time.now.to_s, to_time: Time.now.to_s).start_time)
      expect(start_time).to eql Time.zone.parse(Time.now.to_s)
    end
  end
  describe '#end_time' do
    it 'returns to_time if is_a?(Time)' do
      expect(described_class.new(from_time: Time.now, to_time: Time.now.end_of_day).end_time).to eql(Time.now.end_of_day)
    end
      it 'parses to_time if is a string' do
        to_time = (described_class.new(from_time: Time.now, to_time: Time.now.end_of_day.to_s).end_time)
        expect(to_time).to eql Time.zone.parse(Time.now.end_of_day.to_s)
    end
  end
end
