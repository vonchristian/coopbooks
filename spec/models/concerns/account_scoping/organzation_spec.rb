require 'rails_helper'

module AccountScoping
  describe Organization do
    let(:saving)             { create(:saving) { include AccountScoping::Organization } }
    let(:organization_scope) { create(:organization_scope, account: saving) }

    describe 'associations' do
      it { expect(saving).to have_one :organization_scope }
    end

    describe 'delegations' do
      it { expect(saving).to delegate_method(:organization).to(:organization_scope).allow_nil }
      it { expect(saving).to delegate_method(:name).to(:organization).with_prefix.allow_nil }
    end
  end
end
