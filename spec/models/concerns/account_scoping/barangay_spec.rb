require 'rails_helper'

module AccountScoping
  describe Barangay do
    let(:saving)           { create(:saving) { include AccountScoping::Barangay } }
    let(:barangay_scope) { create(:barangay_scope, account: saving) }

    describe 'associations' do
      it { expect(saving).to have_one :barangay_scope }
    end

    describe 'delegations' do
      it { expect(saving).to delegate_method(:barangay).to(:barangay_scope).allow_nil }
      it { expect(saving).to delegate_method(:name).to(:barangay).with_prefix.allow_nil }
    end
  end
end
