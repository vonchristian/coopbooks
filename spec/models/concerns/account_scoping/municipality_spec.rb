require 'rails_helper'

module AccountScoping
  describe Municipality do
    let(:saving)           { create(:saving) { include AccountScoping::Municipality } }
    let(:municipality_scope) { create(:municipality_scope, account: saving) }

    describe 'associations' do
      it { expect(saving).to have_one :municipality_scope }
    end

    describe 'delegations' do
      it { expect(saving).to delegate_method(:municipality).to(:municipality_scope).allow_nil }
      it { expect(saving).to delegate_method(:name).to(:municipality).with_prefix.allow_nil }
    end
  end
end
