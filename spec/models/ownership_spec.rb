require 'rails_helper'

describe Ownership, type: :model do
  describe 'associations' do
  	it { is_expected.to belong_to :ownable }
  	it { is_expected.to belong_to :owner }
  end

  it "#archived?" do
    archived_ownership = create(:ownership, archived_at: Date.current)
    active_ownership   = create(:ownership, archived_at: nil)

    expect(archived_ownership.archived?).to eql true
    expect(active_ownership.archived?).to eql false 

  end
end
