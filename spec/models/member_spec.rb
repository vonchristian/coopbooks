require 'rails_helper'

describe Member, type: :model do
  describe "associations" do
    it { is_expected.to have_many :memberships }
  	it { is_expected.to have_many :loans }
  	it { is_expected.to have_many :savings }
  	it { is_expected.to have_many :share_capitals }
  	it { is_expected.to have_many :time_deposits }
  	it { is_expected.to have_many :program_subscriptions }
  	it { is_expected.to have_many :subscribed_programs }
    it { is_expected.to have_many :sales }
    it { is_expected.to have_many :organization_memberships }
    it { is_expected.to have_many :organizations }
    it { is_expected.to have_many :contacts }
    it { is_expected.to have_many :beneficiaries }
    it { is_expected.to have_many :loan_applications }
    it { is_expected.to have_many :share_capital_applications }
    it { is_expected.to have_many :savings_account_applications }
    it { is_expected.to have_many :time_deposit_applications }
    it { is_expected.to have_many :identifications }
    it { is_expected.to have_many :store_front_accounts }
    it { is_expected.to have_many :entry_transactions }
    it { is_expected.to have_many :entries }
  end

  describe 'validations' do
  end
  it "#full_name" do
  	member = build(:member, first_name: "Von", middle_name: "Pinosan", last_name: "Halip")

  	expect(member.full_name).to eql("Halip, Von Pinosan")
  end

  it "#name" do
    member = build(:member, first_name: "Von", middle_name: "Pinosan", last_name: "Halip")

    expect(member.name).to eql("Halip, Von Pinosan")
  end

  it "#first_and_name" do
  	member = create(:member, first_name: "Von", middle_name: "Pinosan", last_name: "Halip")

  	expect(member.first_and_last_name).to eql("Von Halip")
  end
end
