require 'rails_helper'
require "money-rails/test_helpers"

module Vouchers
  describe VoucherAmount do
    subject { build(:voucher_amount) }
    it { is_expected.to_not be_valid }

    describe 'associations' do
      it { is_expected.to belong_to :recorder }
      it { is_expected.to belong_to :account }
      it { is_expected.to belong_to(:voucher).optional }
      it { is_expected.to belong_to(:cart).optional }
    end
    describe 'validations' do
      it { is_expected.to validate_presence_of :account_id }
      it { is_expected.to validate_presence_of :amount_type }
      it { is_expected.to monetize(:amount) }

    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:account).with_prefix }
    end

    it { is_expected.to define_enum_for(:amount_type).with_values([:debit, :credit]) }

    it ".total" do
      voucher_amount_1 = create(:voucher_amount, amount: 100)
      voucher_amount_2 = create(:voucher_amount, amount: 100)

      expect(described_class.total).to eql 200
    end

    it ".with_no_vouchers" do
      voucher = create(:voucher)
      voucher_amount_1 = create(:voucher_amount, amount: 100)
      voucher_amount_2 = create(:voucher_amount, amount: 100, voucher: voucher)

      expect(described_class.with_no_vouchers).to include(voucher_amount_1)
      expect(described_class.with_no_vouchers).to_not include(voucher_amount_2)

    end

    it '.total' do
      voucher_amount   = create(:voucher_amount, amount: 100)
      voucher_amount_2 = create(:voucher_amount, amount: 100)

      expect(described_class.total).to eql 200
    end

    it '.valid?' do
      debit  = create(:voucher_amount, amount_type: 'debit', amount: 100)
      credit = create(:voucher_amount, amount_type: 'credit', amount: 100)

      expect(described_class.valid?).to eql true
    end

    describe 'scopes' do
      it '.with_no_vouchers' do
        voucher         = create(:voucher)
        with_no_voucher = create(:voucher_amount, voucher: nil)
        with_voucher    = create(:voucher_amount, voucher: voucher)

        expect(described_class.with_no_vouchers).to include with_no_voucher
        expect(described_class.with_no_vouchers).to_not include with_voucher
      end

      it '.with_cash_accounts' do
        cooperative           = create(:cooperative)
        office                = create(:office, cooperative: cooperative)
        employee              = create(:teller, cooperative: cooperative, office: office)
        revenue               = create(:revenue, office: office)
        liability             = create(:liability, office: office)
        employee_cash_account = create(:employee_cash_account, office: office, cooperative: cooperative, employee: employee)
        cash_amount           = create(:voucher_amount, account: employee_cash_account.cash_account)
        non_cash_amount       = create(:voucher_amount, account: revenue)

        expect(described_class.with_cash_accounts).to include(cash_amount)
        expect(described_class.with_cash_accounts).to_not include(non_cash_amount)
      end

      it '.for_account(args={})' do
        revenue_account = create(:revenue)
        expense_account = create(:expense)
        revenue_amount  = create(:voucher_amount, account: revenue_account)
        expense_amount  = create(:voucher_amount, account: expense_account)

        expect(described_class.for_account(account: revenue_account)).to include(revenue_amount)
        expect(described_class.for_account(account: revenue_account)).to_not include(expense_amount)
      end

      it '.excluding_account(args={})' do
        revenue_account = create(:revenue)
        expense_account = create(:expense)
        revenue_amount  = create(:voucher_amount, account: revenue_account)
        expense_amount  = create(:voucher_amount, account: expense_account)

        expect(described_class.excluding_account(account: revenue_account)).to include(expense_amount)
        expect(described_class.excluding_account(account: revenue_account)).to_not include(revenue_amount)
      end
    end
  end
end
