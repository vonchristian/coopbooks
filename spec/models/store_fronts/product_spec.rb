require 'rails_helper'

module StoreFronts
  describe Product do
    describe 'associations' do
      it { is_expected.to have_one :master_variant }
      it { is_expected.to belong_to :store_front }
      it { is_expected.to belong_to(:product_category).optional }
      it { is_expected.to have_many :variants }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:store_front_id) }
    end
  end
end
