require 'rails_helper'

module StoreFronts
  describe SellingPrice, type: :model do
    describe 'associations' do
      it { is_expected.to belong_to :variant }
      it { is_expected.to belong_to :store_front }
    end
  end
end
