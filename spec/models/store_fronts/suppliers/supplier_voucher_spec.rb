require 'rails_helper'

module StoreFronts
  module Suppliers
    describe SupplierVoucher do
      describe 'associations' do
        it { is_expected.to belong_to :supplier }
        it { is_expected.to belong_to :voucher }
      end 
    end
  end
end
