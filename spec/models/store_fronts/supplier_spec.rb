require 'rails_helper'

module StoreFronts
  describe Supplier do
    describe 'associations' do
      it { is_expected.to belong_to :store_front }
      it { is_expected.to have_many :supplier_vouchers }
      it { is_expected.to have_many :vouchers }
      it { is_expected.to have_many :purchase_orders }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :first_name }
      it { is_expected.to validate_presence_of :last_name }
      it { is_expected.to validate_presence_of :contact_number }
      it { is_expected.to validate_presence_of :address }
      it { is_expected.to validate_presence_of :business_name }
    end
  end
end
