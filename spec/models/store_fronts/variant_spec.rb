require 'rails_helper'

module StoreFronts
  describe Variant, type: :model do
    describe 'associations' do
      it { is_expected.to belong_to :product }
      it { is_expected.to have_many :purchase_prices }
      it { is_expected.to have_many :selling_prices }
    end
  end
end
