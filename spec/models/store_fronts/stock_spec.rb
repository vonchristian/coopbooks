require 'rails_helper'

module StoreFronts
  describe Stock do
    describe 'associations' do
      it { is_expected.to belong_to :product }
      it { is_expected.to belong_to :store_front }
      it { is_expected.to have_one  :purchase }
      it { is_expected.to have_many :sales }
      it { is_expected.to have_many :stock_transfers }
      it { is_expected.to have_many :internal_uses }
      it { is_expected.to have_many :spoilages }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:unit_of_measurement).to(:purchase) }
      it { is_expected.to delegate_method(:unit_cost).to(:purchase) }
      it { is_expected.to delegate_method(:total_cost).to(:purchase) }
      it { is_expected.to delegate_method(:quantity).to(:purchase) }
      it { is_expected.to delegate_method(:name).to(:product) }
      it { is_expected.to delegate_method(:has_conversion?).to(:product) }
      it { is_expected.to delegate_method(:code).to(:unit_of_measurement).with_prefix }
      it { is_expected.to delegate_method(:price).to(:unit_of_measurement) }
    end

    it '#balance' do
      stock    = create(:stock)
      entry    = create(:entry_with_credit_and_debit)
      voucher  = create(:voucher, entry: entry)
      order    = create(:purchase_order, voucher: voucher)
      purchase = create(:purchase_line_item, quantity: 100, stock: stock)
      order.line_items << purchase
      expect(stock.balance).to eql 100

      #sales
      sale_entry   = create(:entry_with_credit_and_debit)
      sale_voucher = create(:voucher, entry: sale_entry)
      sales_order  = create(:sales_order, voucher: sale_voucher)
      sale         = create(:sales_line_item, stock: stock, quantity: 10)
      sales_order.line_items << sale
      expect(stock.balance).to eql 90

      #stock_transfers
      transfer_entry   = create(:entry_with_credit_and_debit)
      transfer_voucher = create(:voucher, entry: transfer_entry)
      transfer_order   = create(:stock_transfer_order, voucher: transfer_voucher)
      transfer         = create(:stock_transfer_line_item, quantity: 10, stock: stock, order: transfer_order)
      expect(stock.balance).to eql 80

      #internal_uses
      internal_entry   = create(:entry_with_credit_and_debit)
      internal_voucher = create(:voucher, entry: internal_entry)
      internal_order   = create(:internal_use_order, voucher: internal_voucher)
      internal_use     = create(:internal_use_line_item, quantity: 10, stock: stock, order: internal_order)

      expect(stock.balance).to eql 70
      #
      # #spoilage
      spoilage_entry   = create(:entry_with_credit_and_debit)
      spoilage_voucher = create(:voucher, entry: spoilage_entry)
      spoilage_order   = create(:internal_use_order, voucher: spoilage_voucher)
      spoilage         = create(:spoilage_line_item, quantity: 10, stock: stock, order: spoilage_order)

      expect(stock.balance).to eql 60
      #
      # #purchase_returns
      # purchase_return_entry   = create(:entry_with_credit_and_debit)
      # purchase_return_voucher = create(:voucher, entry: purchase_return_entry)
      # purchase_return_order   = create(:purchase_return_order, voucher: purchase_return_voucher)
      # purchase_return         = create(:purchase_return_line_item, quantity: 10, stock: stock, order: purchase_return_order)
      #
      # expect(stock.balance).to eql 50
      #
      # #sales_returns
      # sales_return_entry   = create(:entry_with_credit_and_debit)
      # sales_return_voucher = create(:voucher, entry: sales_return_entry)
      # sales_return_order   = create(:sales_return_order, voucher: sales_return_voucher)
      # sales_return         = create(:sales_return_line_item, quantity: 10, stock: stock, order: sales_return_order)
      #
      # expect(stock.balance).to eql 60

    end
    it '.available' do
      available_stock     = create(:stock, available: true)
      not_available_stock = create(:stock, available: false)

      expect(described_class.available).to include(available_stock)
      expect(described_class.available).to_not include(not_available_stock)
    end

    it '#available_stock_for_cart' do
      cooperative              = create(:cooperative)
      office                   = create(:office, cooperative: cooperative)
      sales_clerk              = create(:sales_clerk, cooperative: cooperative, office: office)
      store_front              = create(:store_front, office: office)
      product                  = create(:product, name: 'Test Product', store_front: store_front)
      uom                      = create(:unit_of_measurement, product: product, base_measurement: true, conversion_quantity: 1)
      store_front              = create(:store_front)
      cart                     = create(:cart)
      stock                    = create(:stock, product: product, store_front: store_front, barcode: '11111111')
      supplier                 = create(:supplier)
      entry                    = create(:entry_with_credit_and_debit, recorder: sales_clerk, office: office, cooperative: cooperative)
      voucher                  = create(:voucher, entry: entry, payee: supplier, reference_number: 'V001')
      purchase_order           = create(:purchase_order, supplier: supplier, voucher: voucher)
      purchase_order_line_item = create(:purchase_line_item, quantity: 10, stock: stock, order: purchase_order)
      sales_line_item          = create(:sales_line_item, quantity: 1, stock: stock, cart: cart)
      expect(stock.available_stock_for_cart(cart)).to eql 9
    end
  end
end
