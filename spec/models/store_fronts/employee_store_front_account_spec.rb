require 'rails_helper'

module StoreFronts
  module Accounts
    describe EmployeeStoreFrontAccount do
      describe 'associations' do
        it { is_expected.to belong_to :employee }
        it { is_expected.to belong_to :store_front }
      end
    end
  end
end 
