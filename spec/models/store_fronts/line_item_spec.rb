require 'rails_helper'

module StoreFronts
  describe LineItem do
    describe 'associations' do
      it { is_expected.to belong_to :unit_of_measurement }
      it { is_expected.to belong_to(:cart).optional }
      it { is_expected.to belong_to(:stock).optional }
      it { is_expected.to belong_to(:order).optional }
      it { is_expected.to have_many :barcodes }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :quantity }
      it { is_expected.to validate_presence_of :unit_cost }
      it { is_expected.to validate_presence_of :total_cost }
      it { is_expected.to validate_numericality_of :quantity }
      it { is_expected.to validate_numericality_of :unit_cost }
      it { is_expected.to validate_numericality_of :total_cost }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:code).to(:unit_of_measurement).with_prefix }
      it { is_expected.to delegate_method(:conversion_multiplier).to(:unit_of_measurement) }
    end
    it '.total' do
      order     = create(:purchase_order)
      processed = create(:purchase_line_item, quantity: 10, order: order)

      expect(described_class.total).to eql 10
    end
  end
end
