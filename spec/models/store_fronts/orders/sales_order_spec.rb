require 'rails_helper'

module StoreFronts
  module Orders
    describe SalesOrder do
      describe 'associations' do
        it { is_expected.to belong_to :customer }
        it { is_expected.to belong_to :store_front }
        it { is_expected.to belong_to :sales_account }
        it { is_expected.to belong_to :sales_discount_account }
        it { is_expected.to belong_to :cost_of_goods_sold_account }
        it { is_expected.to belong_to(:receivable_account).optional }
        it { is_expected.to belong_to :employee }
        it { is_expected.to have_many :line_items }
      end

      describe 'validations' do
        it { is_expected.to validate_presence_of :date }
      end
    end
  end
end
