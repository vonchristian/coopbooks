require 'rails_helper'

module StoreFronts
  module Orders
    describe PurchaseOrder do
      describe 'associations' do
        it { is_expected.to belong_to :employee }
        it { is_expected.to belong_to :store_front }
        it { is_expected.to belong_to :supplier }
        it { is_expected.to belong_to :voucher }
        it { is_expected.to have_many :line_items }
      end
      describe 'validations' do
        it { is_expected.to validate_presence_of :date }
      end
    end
  end
end
