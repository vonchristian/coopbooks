require 'rails_helper'

module StoreFronts
  module Orders
    describe StockTransferOrder do
      describe 'associations' do
        it { is_expected.to belong_to :employee }
        it { is_expected.to belong_to :voucher }
        it { is_expected.to belong_to :origin_store_front }
        it { is_expected.to belong_to :destination_store_front }
      end
    end
  end 
end
