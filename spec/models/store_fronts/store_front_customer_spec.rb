require 'rails_helper'

module StoreFronts
  describe StoreFrontCustomer do
    describe 'associations' do
      it { is_expected.to belong_to :customer }
      it { is_expected.to belong_to :store_front }
    end
  end 
end
