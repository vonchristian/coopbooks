require 'rails_helper'

module Organizations
  describe OrganizationMembership do
    describe 'associations' do
      it { is_expected.to belong_to :organization }
      it { is_expected.to belong_to :cooperator }
    end
  end
end
