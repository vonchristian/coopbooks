require 'rails_helper'

module IdentificationsModule
  describe Identification do
    describe 'associations' do
      it { is_expected.to belong_to :identity_provider }
      it { is_expected.to belong_to :identifiable }
    end

    describe 'validations' do
      it { is_expected.to validate_presence_of :issuance_date }
      it { is_expected.to validate_presence_of :expiry_date }
      it { is_expected.to validate_presence_of :number }
    end

    describe 'delegations' do
      it { is_expected.to delegate_method(:name).to(:identity_provider).with_prefix }
    end
  end
end
