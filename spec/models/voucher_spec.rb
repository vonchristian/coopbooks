require 'rails_helper'

RSpec.describe Voucher  do
  describe 'associations' do
    it { is_expected.to belong_to :cooperative }
    it { is_expected.to belong_to :office }
    it { is_expected.to belong_to(:entry).optional }
    it { is_expected.to belong_to :payee }
    it { is_expected.to belong_to :preparer }
    it { is_expected.to belong_to(:disburser).optional }
    it { is_expected.to have_many :voucher_amounts }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :account_number }
    it { is_expected.to validate_uniqueness_of :account_number }
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:name).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:abbreviated_name).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:address).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:contact_number).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:address).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:contact_number).to(:cooperative).with_prefix }
    it { is_expected.to delegate_method(:full_name).to(:preparer).with_prefix }
    it { is_expected.to delegate_method(:full_name).to(:disburser).with_prefix }
    it { is_expected.to delegate_method(:current_occupation).to(:preparer).with_prefix }
    it { is_expected.to delegate_method(:current_occupation).to(:disburser).with_prefix }
    it { is_expected.to delegate_method(:name).to(:payee).with_prefix }
    it { is_expected.to delegate_method(:avatar).to(:payee) }
  end

  it '.disbursement_vouchers' do
    teller       = create(:teller)
    revenue      = create(:revenue, office: teller.office)
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    cash_amount  = create(:voucher_amount, amount_type: 'debit', account: cash_account.cash_account, recorder: teller, cooperative: teller.cooperative, amount: 1_000)
    revenue      = create(:voucher_amount, amount_type: 'credit', account: revenue, cooperative: teller.cooperative, amount: 1_000)
    voucher = create(:voucher)
    voucher.voucher_amounts << cash_amount
    voucher.voucher_amounts << revenue
    voucher.save!

    expect(described_class.disbursement_vouchers).to include(voucher)
  end
  it '.loan_disbursement_vouchers' do
    disbursement_voucher = create(:voucher)
    loan = create(:loan_application, voucher: disbursement_voucher)

    expect(described_class.loan_disbursement_vouchers).to include(disbursement_voucher)
  end

  it ".payees" do
  end


  it "#disbursed?" do
    voucher = create(:voucher)
    voucher_2 = create(:voucher)
    entry = create(:entry_with_credit_and_debit)
    voucher.update_attributes(entry: entry)

    expect(voucher.disbursed?).to eql true
    expect(voucher_2.disbursed?).to eql false
  end

  it '.disbursed' do
    disbursed_voucher = create(:voucher)
    undisbursed_voucher = create(:voucher)
    entry = create(:entry_with_credit_and_debit, entry_date: Date.today)
    disbursed_voucher.entry = entry
    disbursed_voucher.save

    expect(described_class.disbursed).to include(disbursed_voucher)
    expect(described_class.disbursed).to_not include(undisbursed_voucher)

  end
  it '.disbursed_on(args={})' do
    teller = create(:teller)
    cooperative = teller.cooperative
    office = teller.office
    disbursed_voucher = create(:voucher)
    another_disbursed_voucher = create(:voucher)
    undisbursed_voucher = create(:voucher)

    entry = create(:entry_with_credit_and_debit, recorder: teller, office: office, cooperative: cooperative, entry_date: Date.today)
    another_entry = create(:entry_with_credit_and_debit, entry_date: Date.today + 1.day, cooperative: cooperative, recorder: teller, office: office)

    disbursed_voucher.entry = entry
    another_disbursed_voucher.entry = another_entry
    disbursed_voucher.save
    another_disbursed_voucher.save

    expect(described_class.disbursed_on(from_date: Date.today, to_date: Date.today)).to include(disbursed_voucher)
    expect(described_class.disbursed_on(from_date: Date.today, to_date: Date.today)).to_not include(undisbursed_voucher)
    expect(described_class.disbursed_on(from_date: Date.today, to_date: Date.today)).to_not include(another_disbursed_voucher)

    expect(described_class.disbursed_on(from_date: Date.today + 1.day, to_date: Date.today + 1.day)).to include(another_disbursed_voucher)
    expect(described_class.disbursed_on(from_date: Date.today + 1.day, to_date: Date.today + 1.day)).to_not include(disbursed_voucher)
    expect(described_class.disbursed_on(from_date: Date.today + 1.day, to_date: Date.today + 1.day)).to_not include(undisbursed_voucher)

  end

  it 'cancelled?' do
    voucher = create(:voucher)
    cancelled_voucher = create(:voucher, cancelled_at: Date.current)

    expect(voucher.cancelled?).to be false
    expect(cancelled_voucher.cancelled?).to be true
  end


  describe 'callbacks' do
    it '.set_date' do
      voucher = create(:voucher)

      expect(voucher.date).to be_present
      expect(voucher.date.to_date).to eql Date.today
    end
  end
end
