require 'rails_helper'

RSpec.describe Organization do
  describe 'associations' do
    it { is_expected.to belong_to :office }
    it { is_expected.to have_many :organization_memberships }
    it { is_expected.to have_many :member_memberships }
    it { is_expected.to have_many :employee_memberships }
    it { is_expected.to have_many :loans }
    it { is_expected.to have_many :savings }
    it { is_expected.to have_many :share_capitals }
    it { is_expected.to have_many :member_savings }
    it { is_expected.to have_many :time_deposits }

    it { is_expected.to have_many :entry_transactions }
    it { is_expected.to have_many :entries }
  end
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:office_id) }
  end
end
