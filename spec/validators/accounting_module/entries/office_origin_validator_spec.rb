require 'rails_helper'

module AccountingModule
  module Entries
    describe OfficeOriginValidator do
      it 'valid?' do
        cooperative = create(:cooperative)
        office      = create(:office, cooperative: cooperative)
        employee    = create(:teller, office: office, cooperative: cooperative)
        entry       = build(:entry_with_credit_and_debit, recorder: employee, office: office, cooperative: cooperative)
        office_2    = create(:office)
        expect(described_class.new(entry: entry, office: office).valid?).to eql true
        expect(described_class.new(entry: entry, office: office_2).valid?).to eql false

      end
    end
  end
end
