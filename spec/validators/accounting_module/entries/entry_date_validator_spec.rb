require 'rails_helper'

module AccountingModule
  module Entries
    describe EntryDateValidator do
      it 'valid?' do
        entry = build(:entry_with_credit_and_debit, created_at: Date.current, entry_date: nil)

        expect(described_class.new(entry: entry, date: Date.current).valid?).to eql true
        expect(described_class.new(entry: entry, date: Date.current.beginning_of_day).valid?).to eql true
        expect(described_class.new(entry: entry, date: Date.current.end_of_day).valid?).to eql true
        expect(described_class.new(entry: entry, date: Date.current.last_year).valid?).to eql false
        expect(described_class.new(entry: entry, date: Date.current.next_day).valid?).to eql false
      end
    end
  end
end
