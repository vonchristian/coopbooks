require 'rails_helper'

module AccountingModule
  module Entries
    describe AccountOriginValidator do
      describe 'valid?' do
        it 'with the same origin' do
          invalid_asset = create(:asset)
          cooperative   = create(:cooperative)
          office        = create(:office, cooperative: cooperative)
          cash          = create(:asset, office: office)
          asset         = create(:asset, office: office)

          entry         = build(:entry_with_credit_and_debit,  cooperative: cooperative, office: office)
          entry.debit_amounts.build(amount: 1000, account: asset)
          entry.credit_amounts.build(amount: 1000, account: cash)

          expect(described_class.new(entry: entry, account: cash).valid?).to eql true
          expect(described_class.new(entry: entry, account: asset).valid?).to eql true
          expect(described_class.new(entry: entry, account: invalid_asset).valid?).to eql false
        end
      end
    end
  end
end
