require 'rails_helper'

module AccountingModule
  module Entries
    describe RecorderOriginValidator do

      it 'valid?' do
        cooperative = create(:cooperative)
        office      = create(:office, cooperative: cooperative)
        teller      = create(:teller, office: office, cooperative: cooperative)
        entry       = build(:entry_with_credit_and_debit, office: office, cooperative: cooperative, recorder: nil)
        teller_2    = create(:teller)
        expect(described_class.new(entry: entry, recorder: teller).valid?).to eql true
        expect(described_class.new(entry: entry, recorder: teller_2).valid?).to eql false

      end
    end
  end
end
