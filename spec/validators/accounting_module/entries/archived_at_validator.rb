require 'rails_helper'

module AccountingModule
  module Entries
    describe ArchivedAtValidator do
      it 'valid?!' do
        entry = build(:entry_with_credit_and_debit, archived_at: nil, created_at: Date.current)

        expect(described_class.new(entry: entry, archived_at: Date.current).valid?).to eql true
        expect(described_class.new(entry: entry, archived_at: Date.current.end_of_day).valid?).to eql true
        expect(described_class.new(entry: entry, archived_at: Date.current.beginning_of_day).valid?).to eql true

        expect(described_class.new(entry: entry, archived_at: Date.current.next_month).valid?).to eql false
        expect(described_class.new(entry: entry, archived_at: Date.current.end_of_day + 1.second).valid?).to eql false
        expect(described_class.new(entry: entry, archived_at: Date.current.beginning_of_day - 1.second).valid?).to eql false
      end
    end
  end
end
