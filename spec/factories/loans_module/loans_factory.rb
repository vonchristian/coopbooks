FactoryBot.define do
  factory :loan, class: LoansModule::Loan do
    association :receivable_account, factory: :asset
    association :interest_revenue_account, factory: :revenue
    association :penalty_revenue_account, factory: :revenue
    association :loan_application
    association :cooperative
    association :office
    association :loan_product
    association :borrower, factory: :member
    association :voucher
    borrower_full_name    { Faker::Name.name }
    disbursement_date     { Date.current }
    status                { 'current_loan' }



    factory :loan_with_balance, class: LoansModule::Loan do
      after(:build) do |loan|
        receivable_account = create(:asset)
        cash_account       = create(:asset)
        entry              = build(:entry)
        entry.debit_amounts.build(amount: 10_000, account: receivable_account)
        entry.credit_amounts.build(amount: 10_000, account: cash_account)
        entry.save!

        loan.receivable_account = receivable_account
      end
    end

    factory :disbursed_loan do |loan|
      after(:build) do |loan|
        entry            = create(:entry_with_credit_and_debit, entry_date: loan.disbursement_date)
        voucher          = create(:voucher, entry: entry)
        loan_application = create(:loan_application, voucher: voucher)
        loan.loan_application = loan_application
      end
    end
  end
end
