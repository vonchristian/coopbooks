FactoryBot.define do
  factory :loan_account, class: LoansModule::Loans::LoanAccount do
    association :loan
    association :account, factory: :asset
  end
end
