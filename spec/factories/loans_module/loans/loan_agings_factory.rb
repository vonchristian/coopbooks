FactoryBot.define do
  factory :loan_aging, class: LoansModule::Loans::LoanAging do
    association :loan
    association :loan_group
    date        { Date.current }
  end
end
