FactoryBot.define do
  factory :loan_auto_pay, class: LoansModule::Loans::LoanAutoPay do
    association :loan
    association :savings_account, factory: :saving
    active { true }
  end
end
