FactoryBot.define do
  factory :loan_interest, class: LoansModule::Loans::LoanInterest do
    association :loan
    association :employee, factory: :loan_officer
    description { Faker::Company.bs }
    date        { Date.current }
  end
end
