FactoryBot.define do
  factory :loan_discount, class: LoansModule::Loans::LoanDiscount do
    association :loan
    association :employee, factory: :user
    amount { 100 }
    discount_type { 1 }
  end
end
