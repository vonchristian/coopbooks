FactoryBot.define do
  factory :loan_term do
    association :loan
    association :term
  end
end
