FactoryBot.define do
  factory :loan_group, class: LoansModule::LoanGroup do
    association :office
    title     { Faker::Company.name }
    start_num { 1 }
    end_num   { 1 }
  end
end
