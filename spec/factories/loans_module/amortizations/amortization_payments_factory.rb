FactoryBot.define do
  factory :amortization_payment do
    association :amortization_schedule
    association :entry
  end
end
