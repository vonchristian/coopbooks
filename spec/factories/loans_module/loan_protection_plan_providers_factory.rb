FactoryBot.define do
  factory :loan_protection_plan_provider, class: LoansModule::LoanProtectionPlanProvider do
    business_name { Faker::Company.name }
    association :office
    association :payable_account, factory: :liability
    rate { 0.35 }
  end
end
