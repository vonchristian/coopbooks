FactoryBot.define do
  factory :interest_config, class: LoansModule::LoanProducts::InterestConfig do
    association :loan_product
    rate { 0.12 }

    factory :add_on_interest_config, class: LoansModule::LoanProducts::InterestConfig do
      calculation_type { 'add_on' }
    end

    factory :prededucted_interest_config, class: LoansModule::LoanProducts::InterestConfig do
      calculation_type { 'prededucted' }
    end
  end
end
