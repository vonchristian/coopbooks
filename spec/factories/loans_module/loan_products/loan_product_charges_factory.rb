FactoryBot.define do
  factory :loan_product_charge, class: LoansModule::LoanProducts::LoanProductCharge do
    association :office_loan_product
    association :account, factory: :revenue
    name { Faker::Company.name }
    amount { 100 }
    rate   { 0.5 }
  end
end
