FactoryBot.define do
  factory :interest_prededuction, class: LoansModule::LoanProducts::InterestPrededuction do
    rate { 0.02 }
    amount { 1000 }
    number_of_payments { 3 }
    association :loan_product
  end
end
