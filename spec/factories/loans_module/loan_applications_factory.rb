FactoryBot.define do
  factory :loan_application, class: LoansModule::LoanApplication do
    association :cooperative
    association :office
    association :cart
    association :receivable_account, factory: :asset
    association :interest_revenue_account, factory: :revenue
    association :borrower,        factory: :member
    association :preparer,        factory: :loan_officer
    association :loan_product
    mode_of_payment  { 'lumpsum' }
    application_date { Date.current }
    term             { 12 }
    account_number   { SecureRandom.uuid }
    interest_rate    { 0.02 }
    interest_calculation_type { 1 }
    interest_prededuction_rate { 1 }
    factory :loan_application_with_voucher, class: LoansModule::LoanApplication do
      after(:build) do |loan_application|
        loan_officer             = create(:loan_officer)
        receivable_account       = create(:asset)
        interest_revenue_account = create(:revenue)
        cash_account             = create(:employee_cash_account, cooperative: loan_officer.cooperative)
        voucher                  = create(:voucher)
        voucher.voucher_amounts.debit.build(amount: 1000, account: receivable_account, recorder: loan_officer, cooperative: loan_officer.cooperative)
        voucher.voucher_amounts.credit.build(amount: 100, account: interest_revenue_account, recorder: loan_officer, cooperative: loan_officer.cooperative)
        voucher.voucher_amounts.credit.build(amount: 100, account: cash_account.cash_account, recorder: loan_officer, cooperative: loan_officer.cooperative)
        voucher.save!
        loan_application.voucher                  = voucher
        loan_application.receivable_account       = receivable_account
        loan_application.interest_revenue_account = interest_revenue_account
        loan_application.preparer                 = loan_officer
      end
    end
  end
end
