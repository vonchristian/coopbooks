FactoryBot.define do
  factory :amortization_schedule, class: LoansModule::AmortizationSchedule do
    after(:build) do |amortization_schedule|
      cooperative = create(:cooperative)
      amortization_schedule.cooperative = cooperative
    end
    association :loan
    association :office
    date { Date.current }
    principal { 100 }
    interest { 50 }
    total_repayment { 150 }
  end
end
