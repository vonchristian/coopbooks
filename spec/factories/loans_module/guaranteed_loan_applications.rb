FactoryBot.define do
  factory :guaranteed_loan_application, class: LoansModule::GuaranteedLoanApplication do
    loan_application { nil }
    guarantor { nil }
  end
end
