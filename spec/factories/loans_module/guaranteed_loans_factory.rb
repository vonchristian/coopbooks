FactoryBot.define do
  factory :guaranteed_loan, class: LoansModule::GuaranteedLoan do
    guarantor { nil }
    loan { nil }
  end
end
