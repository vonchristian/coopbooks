FactoryBot.define do
  factory :address do
    complete_address { Faker::Address.full_address }
    association :addressable, factory: :member
  end
end
