FactoryBot.define do
  factory :employee_cash_account, class: Employees::EmployeeCashAccount do
    after(:build) do |account|
      cooperative = create(:cooperative)
      office      = create(:office, cooperative: cooperative)
      employee    = create(:teller, office: office, cooperative: cooperative)
      cash_account = create(:cash_account, office: office)
      account.office       ||= office
      account.employee     ||= employee
      account.cash_account ||= cash_account
      account.cooperative ||= cooperative
    end
  end
end
 
