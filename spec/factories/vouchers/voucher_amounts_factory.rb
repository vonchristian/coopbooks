FactoryBot.define do
  factory :voucher_amount, class: "Vouchers::VoucherAmount" do
    amount      { Faker::Number.number(digits: 5) }
    association :account, factory: :asset
    association :recorder, factory: :teller
    association :cooperative

    factory :voucher_debit_amount, class: Vouchers::VoucherAmount do
      amount_type { 'debit' }
    end
  end
end
