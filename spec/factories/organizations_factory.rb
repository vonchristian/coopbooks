FactoryBot.define do
  factory :organization do
    name             { Faker::Company.name }
    abbreviated_name { Faker::Company.name }

    association :office
  end
end
