FactoryBot.define do
  factory :store_front do
    association :office
    association :merchandise_inventory_account, factory: :asset
    name    { Faker::Company.name }
    address { Faker::Address.full_address }
    association :receivable_category,         factory: :asset_level_one_account_category
    association :cost_of_goods_sold_category, factory: :expense_level_one_account_category
    association :inventory_category,          factory: :asset_level_one_account_category
    association :sales_category,              factory: :revenue_level_one_account_category
    association :spoilage_category,           factory: :expense_level_one_account_category
    association :sales_discount_category,     factory: :revenue_level_one_account_category
    association :internal_use_category,       factory: :expense_level_one_account_category
    association :purchase_return_category,    factory: :asset_level_one_account_category
    association :sales_return_category,       factory: :revenue_level_one_account_category
  end
end
