FactoryBot.define do
  factory :share_capital_application do
    association :subscriber, factory: :member
    association :share_capital_product
    association :cooperative
    association :office
    association :equity_account, factory: :equity
  end
end
