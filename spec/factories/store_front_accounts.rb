FactoryBot.define do
  factory :store_front_account do
    customer { nil }
    store_front { nil }
  end
end
