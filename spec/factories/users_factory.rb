FactoryBot.define do
  factory :user, aliases: [:employee] do
    after(:build) do |user|
      cooperative = create(:cooperative)
      office      = create(:office, cooperative: cooperative)
      user.cooperative ||= cooperative
      user.office      ||= office
    end
    first_name            { Faker::Name.first_name }
    middle_name           { Faker::Name.last_name }
    last_name             { Faker::Name.last_name }
    email                 { Faker::Internet.email }
    password              { 'secret_password' }
    password_confirmation { 'secret_password' }
    confirmed_at          { Date.current }

    role { 0 }

    factory :teller, class: User do
      role { 'teller' }
    end

    factory :loan_officer do
      role { 'loan_officer' }
    end
    factory :branch_manager do
      role { 'branch_manager' }
    end

    factory :general_manager do
      role { 'general_manager' }
    end

    factory :bookkeeper do
      role { 'bookkeeper' }
    end
    factory :accountant do
      role { 'accountant' }
    end
    
    factory :sales_clerk do
      role { 'sales_clerk' }
    end
  end
end
