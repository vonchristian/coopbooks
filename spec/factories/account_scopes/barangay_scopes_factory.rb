FactoryBot.define do
  factory :barangay_scope, class: AccountScopes::BarangayScope do
    association :account, factory: :loan
    association :barangay
  end
end
