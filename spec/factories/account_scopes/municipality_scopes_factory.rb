FactoryBot.define do
  factory :municipality_scope, class: AccountScopes::MunicipalityScope do
    association :municipality
    association :account, factory: :saving
  end
end
