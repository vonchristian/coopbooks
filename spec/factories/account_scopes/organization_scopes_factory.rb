FactoryBot.define do
  factory :organization_scope, class: AccountScopes::OrganizationScope do
    association :organization
    association :account, factory: :saving
  end
end
