FactoryBot.define do
  factory :organization_membership, class: Organizations::OrganizationMembership do
    association :organization
    association :cooperator, factory: :member
  end
end
