FactoryBot.define do
  factory :ownership do
    association :owner, factory: :member
    association :ownable, factory: :saving
    archived_at { nil }
  end
end
