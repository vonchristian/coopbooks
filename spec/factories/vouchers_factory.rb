FactoryBot.define do
  factory :voucher do
    account_number { SecureRandom.uuid }
    association :cooperative
    association :office
    association :payee, factory: :member
    association :preparer, factory: :user
    description  { 'voucher description' }
  end
end
