FactoryBot.define do
  factory :cooperative_service, class: Cooperatives::CooperativeService do
    title { Faker::Company.bs }
    association :office
  end
end
