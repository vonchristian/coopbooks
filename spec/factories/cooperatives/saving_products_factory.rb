FactoryBot.define do
  factory :saving_product, class: "Cooperatives::SavingProduct" do
    interest_rate    { 0.02 }
    interest_posting { 'quarterly' }
    minimum_balance  { 1_000 }
    sequence(:name)  { |n| "#{n}"}
    association      :cooperative
  end
end
