FactoryBot.define do
  factory :program, class: Cooperatives::Program do
    name                  { Faker::Name.name }
    description           { Faker::Company.bs }
    amount                { 500 }
    association           :cooperative
    mode_of_payment { 'annually' }
  end
end
