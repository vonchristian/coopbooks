FactoryBot.define do
  factory :office, class: Cooperatives::Office do
    association :cooperative
    type           { "Cooperatives::Offices::MainOffice" }
    name           { Faker::Company.name }
    address        { Faker::Address.full_address }
    contact_number { SecureRandom.uuid }

    factory :main_office, class: Cooperatives::Offices::MainOffice do
    end
    factory :satellite_office, class: Cooperatives::Offices::SatelliteOffice do
    end
    factory :branch_office, class: Cooperatives::Offices::BranchOffice do
    end
  end
end
