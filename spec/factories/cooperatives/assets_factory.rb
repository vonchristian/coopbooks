FactoryBot.define do
  factory :coop_asset, class: Cooperatives::Asset do
    name { "MyString" }
    account { nil }
  end
end
