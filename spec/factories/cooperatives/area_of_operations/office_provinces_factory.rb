FactoryBot.define do
  factory :office_province, class: Cooperatives::AreaOfOperations::OfficeProvince do
    association :province
    association :office
  end
end
