FactoryBot.define do
  factory :office_barangay, class: Cooperatives::AreaOfOperations::OfficeBarangay do
    association :office
    association :barangay
  end
end
