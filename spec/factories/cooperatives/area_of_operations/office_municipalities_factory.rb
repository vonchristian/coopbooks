FactoryBot.define do
  factory :office_municipality, class: Cooperatives::AreaOfOperations::OfficeMunicipality do
    association :office
    association :municipality
  end
end
