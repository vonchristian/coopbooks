FactoryBot.define do
  factory :regular_membership_category, class: Cooperatives::MembershipCategory do
    title { 'Regular Member' }
    association :cooperative
  end
end
