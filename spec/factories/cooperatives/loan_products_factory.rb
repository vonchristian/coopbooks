FactoryBot.define do
  factory :loan_product, class: Cooperatives::LoanProduct do
    association :cooperative
    association :temporary_account,                 factory: :asset
    association :receivable_account_category,       factory: :asset_level_one_account_category
    association :interest_revenue_account_category, factory: :revenue_level_one_account_category
    association :penalty_revenue_account_category,  factory: :revenue_level_one_account_category
    association :amortization_type

    maximum_loanable_amount { 100_000_000 }
    name                    { Faker::Company.bs }
    description             { Faker::Company.bs }

    factory :prededucted_straight_line_loan_product, class: Cooperatives::LoanProduct do
      after(:build) do  |loan_product|
        amortization_type = create(:amortization_type, calculation_type: 'straight_line')
        create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted', rate: 0.12)
        create(:interest_prededuction, loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
        loan_product.amortization_type = amortization_type
      end
    end
  end
end
