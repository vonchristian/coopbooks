FactoryBot.define do
  factory :share_capital_product, class: Cooperatives::ShareCapitalProduct do
    association    :cooperative
    name           { Faker::Company.name }
    cost_per_share { 500 }
    minimum_share { 50 }

  end
end
