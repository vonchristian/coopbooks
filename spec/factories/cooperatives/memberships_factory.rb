FactoryBot.define do
  factory :membership, class: Cooperatives::Membership do
    association :cooperative
    association :cooperator, factory: :member
    association :membership_category, factory: :regular_membership_category
    membership_date  { Date.current }
    account_number   { SecureRandom.uuid }
    factory :regular_membership, class: Cooperatives::Membership do
      association :membership_category, factory: :regular_membership_category
    end
  end
end
