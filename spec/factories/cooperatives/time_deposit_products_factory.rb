FactoryBot.define do
  factory :time_deposit_product, class: Cooperatives::TimeDepositProduct do
    association :cooperative
    name                { Faker::Company.name }
    minimum_deposit     { 1 }
    maximum_deposit     { 1_000_000 }
    interest_rate       { 0.05 }
    break_contract_fee  { 0 }
    break_contract_rate { 500 }
    number_of_days      { 90 }
  end
end
