FactoryBot.define do
  factory :office_share_capital_product, class: Offices::OfficeShareCapitalProduct do
    association :office
    association :share_capital_product
    association :temporary_account, factory: :liability
    association :equity_account_category, factory: :equity_level_one_account_category
    association :interest_payable_account_category, factory: :revenue_level_one_account_category
  end
end
