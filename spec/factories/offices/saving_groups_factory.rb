FactoryBot.define do
  factory :saving_group, class: Offices::SavingGroup do
    title       { Faker::Company.bs }
    start_num   { 0 }
    end_num     { 30 }
    association :office
  end
end
