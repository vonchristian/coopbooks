FactoryBot.define do
  factory :office_membership, class: Offices::OfficeMembership do
    office { nil }
    membership { nil }
  end
end
