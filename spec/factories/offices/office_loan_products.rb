FactoryBot.define do
  factory :office_loan_product, class: Offices::OfficeLoanProduct do
    association :office
    association :loan_product
    association :receivable_account_category, factory: :asset_level_one_account_category
    association :interest_revenue_account_category, factory: :revenue_level_one_account_category
    association :penalty_revenue_account_category, factory: :revenue_level_one_account_category
    association :loan_protection_plan_provider
    association :amortization_type
    association :temporary_account, factory: :asset
  end
end
