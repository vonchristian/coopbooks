FactoryBot.define do
  factory :office_program, class: Offices::OfficeProgram do
    association :program
    association :office
    association :level_one_account_category, factory: :liability_level_one_account_category
  end
end
