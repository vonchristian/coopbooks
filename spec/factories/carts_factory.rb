FactoryBot.define do
  factory :cart do
    association :employee, factory: :teller
  end
end
