FactoryBot.define do
  factory :identification, class: "IdentificationsModule::Identification" do
    identifiable { nil }
    number { "MyString" }
    issuance_date { Date.current }
    expiry_date   { Date.current.next_year }
  end

end
