FactoryBot.define do
  factory :identity_provider, class: "IdentificationsModule::IdentityProvider" do
    name           { "MyString" }
    account_number { SecureRandom.uuid }
  end
end
