FactoryBot.define do
  factory :time_deposit, class: MembershipsModule::TimeDeposit do
    date_deposited { Date.current }
    association :depositor, factory: :member
    association :liability_account, factory: :liability
    association :interest_expense_account, factory: :expense
    association :office
    association :cooperative
    association :time_deposit_product
    association :term
    account_number { SecureRandom.uuid }
  end
end
