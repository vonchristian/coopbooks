FactoryBot.define do
  factory :saving, class: MembershipsModule::Saving do
    association :cooperative
    association :office
    association :depositor, factory: :member
    association :saving_product
    association :liability_account, factory: :liability
    association :interest_expense_account, factory: :expense
    association :saving_group
    account_number        { SecureRandom.uuid }
    account_name          { Faker::Company.bs }

    factory :saving_with_balance, class: MembershipsModule::Saving do
      after(:build) do |saving|
        teller            = create(:teller)
        cash_account      = create(:employee_cash_account, employee: teller, office: teller.office)
        liability_account = create(:liability, office: teller.office)
        entry             = build(:entry, recorder: teller, office: teller.office, cooperative: teller.cooperative)
        entry.debit_amounts.build(account: cash_account.cash_account, amount: 10_000 )
        entry.credit_amounts.build(account: liability_account, amount: 10_000 )
        entry.save!
        saving.liability_account = liability_account
      end
    end
  end
end
