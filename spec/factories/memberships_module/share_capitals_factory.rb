FactoryBot.define do
  factory :share_capital, class: MembershipsModule::ShareCapital do
    account_name { Faker::Company.bs }
    association :subscriber,                  factory: :member
    association :equity_account,              factory: :equity
    association :interest_on_capital_account, factory: :equity
    association :share_capital_product
    association :office
    association :cooperative


    account_number { SecureRandom.uuid }
  end
end
