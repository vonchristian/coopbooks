FactoryBot.define do
  factory :purchase_price do
    variant { nil }
    store_front { nil }
    price { "9.99" }
    effectivity_date { "2019-05-13 21:46:13" }
  end
end
