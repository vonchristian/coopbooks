FactoryBot.define do
  factory :term do
    term             { 1 }
    account_number   { SecureRandom.uuid }
    effectivity_date { Date.current - 6.months }
    maturity_date    { Date.current }
  end
end
