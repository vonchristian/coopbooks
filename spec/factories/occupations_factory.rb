FactoryBot.define do
  factory :occupation do
    title { Faker::Company.bs }
  end
end
