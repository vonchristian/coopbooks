FactoryBot.define do
  factory :cooperative do
    name                { "#{Faker::Name.name} #{SecureRandom.uuid}" }
    address             { Faker::Address.full_address }
    abbreviated_name    { SecureRandom.uuid }
    registration_number { SecureRandom.uuid }

  end
end
