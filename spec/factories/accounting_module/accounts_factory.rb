FactoryBot.define do
  factory :account, class: AccountingModule::Account do
    association :office
    association :level_one_account_category, factory: :asset_level_one_account_category
    account_number  { SecureRandom.uuid }
    sequence(:name) { Faker::Company.bs }
    code            { Faker::Number.number(digits: 10) }

    factory :asset, class: AccountingModule::Accounts::Asset do
    end

    factory :asset_contra, class: AccountingModule::Accounts::Asset do
      contra { true }
    end


    factory :liability, class: AccountingModule::Accounts::Liability do
    end

    factory :equity, class: AccountingModule::Accounts::Equity do
    end

    factory :expense, class: AccountingModule::Accounts::Expense do
    end

    factory :revenue, class: AccountingModule::Accounts::Revenue do
    end
    factory :cash_account, class: AccountingModule::Accounts::Asset do
        name { "Cash on Hand  #{SecureRandom.uuid}"}
    end
  end
end
