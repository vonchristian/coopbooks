FactoryBot.define do
  factory :account_budget, class: AccountingModule::AccountBudget do
    year { Date.current.year }
    proposed_amount { 100_000 }
    association :cooperative
    association :account, factory: :asset
  end
end
