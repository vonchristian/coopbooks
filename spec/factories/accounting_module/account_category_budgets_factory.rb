FactoryBot.define do
  factory :account_category_budget, class: AccountingModule::AccountCategoryBudget do
    account_category { nil }
    year { 1 }
    amount { "9.99" }
  end
end
