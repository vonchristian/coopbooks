FactoryBot.define do
  factory :contact do
    number { Faker::Number.number(digits: 10) }
    association :contactable, factory: :member
  end
end
