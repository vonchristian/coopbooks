FactoryBot.define do
  factory :unit_of_measurement, class: StoreFronts::UnitOfMeasurement do
    association :product
    description   { Faker::Name.first_name }
    code          { Faker::Name.first_name }
    base_quantity { 1 }
  end
end
