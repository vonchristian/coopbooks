FactoryBot.define do
  factory :stock, class: 'StoreFronts::Stock' do
    association :product
    association :store_front
  end
end
