FactoryBot.define do
  factory :supplier, class: 'StoreFronts::Supplier' do
    association :store_front
    business_name  { Faker::Company.name }
    first_name     { Faker::Name.first_name }
    last_name      { Faker::Name.last_name }
    address        { Faker::Address.full_address }
    contact_number { Faker::Number.number(digits: 10) }
  end
end
