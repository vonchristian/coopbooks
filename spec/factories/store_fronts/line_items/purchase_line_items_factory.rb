FactoryBot.define do
  factory :purchase_line_item, class: 'StoreFronts::LineItems::PurchaseLineItem' do
    quantity    { 1 }
    unit_cost   { 1 }
    total_cost  { 1 }
    association :stock
    association :unit_of_measurement
    association :product
    
  end
end
