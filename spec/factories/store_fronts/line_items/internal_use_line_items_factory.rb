FactoryBot.define do
  factory :internal_use_line_item, class: 'StoreFronts::LineItems::InternalUseLineItem' do
    quantity    { 1 }
    unit_cost   { 1 }
    total_cost  { 1 }
    association :stock
    association :unit_of_measurement
    association :product 
  end
end
