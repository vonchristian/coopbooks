FactoryBot.define do
  factory :spoilage_line_item, class: 'StoreFronts::LineItems::SpoilageLineItem' do
    quantity    { 1 }
    unit_cost   { 1 }
    total_cost  { 1 }
    association :stock
    association :unit_of_measurement
    association :product

  end
end
