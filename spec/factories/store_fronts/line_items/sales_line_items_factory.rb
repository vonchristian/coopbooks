FactoryBot.define do
  factory :sales_line_item, class: 'StoreFronts::LineItems::SalesLineItem' do
    quantity    { 1 }
    unit_cost   { 1 }
    total_cost  { 1 }
    association :stock
    association :unit_of_measurement
    association :product
    
  end
end
