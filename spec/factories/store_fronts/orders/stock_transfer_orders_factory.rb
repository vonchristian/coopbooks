FactoryBot.define do
  factory :stock_transfer_order, class: 'StoreFronts::Orders::StockTransferOrder' do
    association :employee
    association :voucher
    association :origin_store_front, factory: :store_front
    association :destination_store_front, factory: :store_front
    date { Date.current }
  end
end
