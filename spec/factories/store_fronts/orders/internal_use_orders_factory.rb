FactoryBot.define do
  factory :internal_use_order, class: StoreFronts::Orders::InternalUseOrder do
    association :store_front
    association :employee
    association :requesting_employee, factory: :employee
    date { Date.current }
  end
end
