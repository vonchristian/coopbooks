FactoryBot.define do
  factory :sales_order, class: 'StoreFronts::Orders::SalesOrder' do
    association :customer, factory: :member
    association :store_front
    association :employee
    association :sales_account, factory: :revenue
    association :sales_discount_account, factory: :revenue
    association :cost_of_goods_sold_account, factory: :expense
    association :receivable_account, factory: :asset
    date { Date.current }
  end
end
