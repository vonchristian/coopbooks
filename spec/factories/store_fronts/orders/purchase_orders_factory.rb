FactoryBot.define do
  factory :purchase_order, class: StoreFronts::Orders::PurchaseOrder do
    association :store_front
    association :employee
    association :supplier
    association :voucher

    date { Date.current }
  end
end
