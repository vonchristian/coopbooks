FactoryBot.define do
  factory :store_front_customer, class: StoreFronts::StoreFrontCustomer do
    association :customer, factory: :member
    association :store_front
  end
end
