FactoryBot.define do
  factory :product_category do
    title { "MyString" }
    store_front { nil }
  end
end
