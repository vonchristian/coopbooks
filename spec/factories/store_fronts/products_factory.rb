FactoryBot.define do
  factory :product, class: 'StoreFronts::Product' do
    name { Faker::Company.bs }
    association :store_front
  end
end
