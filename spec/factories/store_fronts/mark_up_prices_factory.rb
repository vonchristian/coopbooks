FactoryBot.define do
  factory :mark_up_price, class: 'StoreFronts::MarkUpPrice' do
    association :unit_of_measurement
    price { Faker::Number(digits: 5) }
  end
end 
