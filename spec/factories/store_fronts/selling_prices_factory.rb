FactoryBot.define do
  factory :selling_price, class: StoreFronts::SellingPrice do
    price { "9.99" }
    effectivity_date { "2019-05-12 20:27:15" }
    association :store_front
    association :product
  end
end
