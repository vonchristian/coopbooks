FactoryBot.define do
  factory :supplier_voucher, class: StoreFronts::Suppliers::SupplierVoucher do
    association :supplier
    association :voucher
  end
end
