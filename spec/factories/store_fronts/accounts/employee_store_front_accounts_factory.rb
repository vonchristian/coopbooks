FactoryBot.define do
  factory :employee_store_front_account, class: StoreFronts::Accounts::EmployeeStoreFrontAccount do
    association :employee
    association :store_front
  end
end
