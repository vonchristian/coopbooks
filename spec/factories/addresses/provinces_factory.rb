FactoryBot.define do
  factory :province, class: Addresses::Province do
    name { Faker::Company.name }
  end
end 
