FactoryBot.define do
  factory :municipality, class: Addresses::Municipality do
    name        { Faker::Company.name }
    association :province
  end
end
