FactoryBot.define do
  factory :barangay, class: Addresses::Barangay do
    name        { Faker::Company.name }
    association :municipality
  end
end
