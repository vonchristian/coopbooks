FactoryBot.define do
  factory :savings_account_application do
    association :liability_account, factory: :liability
    association :cooperative
    association :office
    association :depositor, factory: :member
    association :saving_product
    account_number { SecureRandom.uuid }


  end
end
