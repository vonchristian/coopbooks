FactoryBot.define do
  factory :cash_count, class: 'CashCounts::CashCount' do
    association :bill
    association :cash_count_report
    quantity { "9.99" }
  end
end
