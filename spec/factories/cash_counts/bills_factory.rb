FactoryBot.define do
  factory :bill, class: 'CashCounts::Bill' do
    bill_amount { "9.99" }
    name { "MyString" }
  end
end
