FactoryBot.define do
  factory :program_subscription, class: MembershipsModule::ProgramSubscription do
    association :program
    association :subscriber, factory: :member
    association :office
    association :cooperative
    association :account, factory: :liability
    account_number { SecureRandom.uuid }
  end
end
