FactoryBot.define do
  factory :membership_category, class: Cooperatives::MembershipCategory do
    association :office
    title { 'Regular Member' }
  end
end
