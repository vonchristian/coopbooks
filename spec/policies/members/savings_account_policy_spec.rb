require 'rails_helper'

module Members
  describe SavingsAccountPolicy do
    subject { Members::SavingsAccountPolicy.new(user, record) }
    let(:record) { create(:saving) }

    context 'teller' do
      let(:user) { create(:user, role: 'teller') }

      it { is_expected.to permit_action(:new)}
      it { is_expected.to permit_action(:create)}
      it { is_expected.to_not permit_action(:destroy)}
    end
    context 'branch_manager' do
      let(:user) { create(:user, role: 'branch_manager') }

      it { is_expected.to_not permit_action(:new)}
      it { is_expected.to_not permit_action(:create)}
      it { is_expected.to_not permit_action(:destroy)}
    end
    context 'accountant' do
      let(:user) { create(:user, role: 'accountant') }

      it { is_expected.to_not permit_action(:new)}
      it { is_expected.to_not permit_action(:create)}
      it { is_expected.to_not permit_action(:destroy)}
    end
  end
end
