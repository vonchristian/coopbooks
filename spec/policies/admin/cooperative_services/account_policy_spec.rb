require 'rails_helper'

module Admin
  module CooperativeServices
    describe AccountPolicy do
      subject { Admin::CooperativeServices::AccountPolicy.new(user, record) }
      let(:record) { create(:asset) }
      context 'general_manager' do
        let(:user) { create(:user, role: 'general_manager') }

        it { is_expected.to permit_action(:new) }
        it { is_expected.to permit_action(:create) }

      end

      context 'loan officer' do
        let(:user) { create(:user, role: 'loan_officer') }

        it { is_expected.to_not permit_action(:new) }
        it { is_expected.to_not permit_action(:create) }

      end

       context 'teller' do
        let(:user) { create(:user, role: 'teller') }

        it { is_expected.to_not permit_action(:new) }
        it { is_expected.to_not permit_action(:create) }

      end
    end
  end
end
