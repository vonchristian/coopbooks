require 'rails_helper'

describe ShareCapitalApplicationProcessing, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :share_capital_product_id }
    it { is_expected.to validate_presence_of :subscriber_id }
    it { is_expected.to validate_presence_of :subscriber_type }
    it { is_expected.to validate_presence_of :date_opened }
    it { is_expected.to validate_presence_of :amount }
    it { is_expected.to validate_numericality_of :amount }
    it { is_expected.to validate_presence_of :reference_number }
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :employee_id }
    it { is_expected.to validate_presence_of :account_number }
    it { is_expected.to validate_presence_of :cash_account_id }
    it { is_expected.to validate_presence_of :voucher_account_number }
  end
end
