require 'rails_helper'

module AccountingModule
  describe AccountCategoryRegistration, type: :model do
    describe 'validations' do
      let(:office) { create(:office) }
      let(:asset_account_category) { create(:asset_level_one_account_category, office: office) }
      it { expect(asset_account_category).to validate_presence_of :title }
      it { expect(asset_account_category).to validate_presence_of :code }
      it { expect(asset_account_category).to validate_presence_of :type }
    end

    it '#register!' do
      office = create(:office)
      parent_account_sub_category = create(:asset_parent_account_sub_category, office: office)

      described_class.new(
        office_id: office.id,
        title: 'Cash in Bank',
        type: 'Asset',
        parent_account_sub_category_id: parent_account_sub_category.id,
        code: '13213'
      ).register!

      account_category = office.account_categories.find_by(title: 'Cash in Bank')

      expect(account_category).to_not eql nil
    end
  end
end
