require 'rails_helper'

module AccountingModule
  describe AccountRegistration, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :name }
      it { is_expected.to validate_presence_of :code }
      it { is_expected.to validate_presence_of :type }
      it { is_expected.to validate_presence_of :account_category_id }

    end

    it '#register!' do
      office           = create(:office)
      account_category = create(:asset_level_one_account_category, office: office)
      described_class.new(
        office_id:           office.id,
        name:                'Cash on Hand',
        code:                '312313',
        type:                'Asset',
        contra:              true,
        account_category_id: account_category.id).register!

      account = office.accounts.find_by(name: "Cash on Hand")

      expect(account).to_not eql nil
      expect(office.accounts).to include(account)
    end
  end
end
