require 'rails_helper'

module AccountingModule
  module Entries
    describe SavingsInterestExpenseEntry, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :date }
        it { is_expected.to validate_presence_of :description }
        it { is_expected.to validate_presence_of :reference_number }
        it { is_expected.to validate_presence_of :account_number }
        it { is_expected.to validate_presence_of :employee_id }
      end
    end
  end
end                                                                                    
