require 'rails_helper'

module AccountingModule
  describe GrandParentAccountCategoryRegistration, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :title }
      it { is_expected.to validate_presence_of :code }
      it { is_expected.to validate_presence_of :cooperative_id }
      it { is_expected.to validate_presence_of :type }
    end

    it 'register!' do
      cooperative = create(:cooperative)
      described_class.new(
        cooperative_id: cooperative.id,
        title: "Cash",
        code: '101',
        type: 'Asset',
        contra: false,
      ).register!

      category = cooperative.grand_parent_account_categories.find_by!(title: 'Cash')
      expect(category).to_not eql nil
    end
  end
end
