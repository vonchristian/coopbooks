require 'rails_helper'

describe MembershipApplication, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :first_name }
    it { is_expected.to validate_presence_of :middle_name }
    it { is_expected.to validate_presence_of :last_name }
    it { is_expected.to validate_presence_of :date_of_birth }
    it { is_expected.to validate_presence_of :sex }
    it { is_expected.to validate_presence_of :civil_status }
    it { is_expected.to validate_presence_of :cooperative_id }
    it { is_expected.to validate_presence_of :office_id }
    it { is_expected.to validate_presence_of :contact_number }
    it { is_expected.to validate_presence_of :complete_address }
    it { is_expected.to validate_presence_of :barangay_id }
    it { is_expected.to validate_presence_of :membership_date }
    it { is_expected.to validate_presence_of :membership_category_id }
    it { is_expected.to validate_presence_of :account_number }
  end
end
