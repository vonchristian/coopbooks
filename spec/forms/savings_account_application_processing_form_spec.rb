require 'rails_helper'

describe SavingsAccountApplicationProcessing, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :reference_number }
    it { is_expected.to validate_presence_of :amount }
    it { is_expected.to validate_presence_of :date }
    it { is_expected.to validate_presence_of :saving_product_id }
    it { is_expected.to validate_presence_of :cash_account_id }
    it { is_expected.to validate_presence_of :account_number }
    it { is_expected.to validate_presence_of :depositor_id }
    it { is_expected.to validate_presence_of :depositor_type }
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :employee_id }
    it { is_expected.to validate_presence_of :voucher_account_number }

  end
end
