require 'rails_helper'

module DataMigrations
  describe SavingRegistry, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :spreadsheet }
      it { is_expected.to validate_presence_of :employee_id }
    end
  end
end
