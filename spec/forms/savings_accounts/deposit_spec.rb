require 'rails_helper'

module Savings
  describe Deposit, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :saving_id }
      it { is_expected.to validate_presence_of :employee_id }
      it { is_expected.to validate_presence_of :amount }
      it { is_expected.to validate_numericality_of :amount }
      it { is_expected.to validate_presence_of :or_number }
      it { is_expected.to validate_presence_of :description }
      it { is_expected.to validate_presence_of :date }
      it { is_expected.to validate_presence_of :cash_account_id }
      it { is_expected.to validate_presence_of :account_number }

    end

    it '#create_voucher!' do
      teller       = create(:teller)
      cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
      saving       = create(:saving, office: teller.office, cooperative: teller.cooperative)
      account_number = "286da588-6611-4c85-a5d2-aed86c8e9c2b"
      described_class.new(
        date:            Date.current,
        description:     'savings deposit',
        amount:          100,
        saving_id:       saving.id,
        cash_account_id: cash_account.cash_account_id,
        employee_id:     teller.id,
        or_number:       '31321',
        account_number:  account_number).create_voucher!

      voucher = described_class.new(date: Date.current, description: 'savings deposit', amount: 100, saving_id: saving.id, cash_account_id: cash_account.cash_account_id, employee_id: teller.id, or_number: '31321', account_number: account_number).find_voucher

      expect(voucher).to_not eql nil

      #create_voucher_amounts
      expect(voucher.voucher_amounts.debit.pluck(:account_id)).to include(cash_account.cash_account_id)
      expect(voucher.voucher_amounts.credit.pluck(:account_id)).to include(saving.liability_account_id)

      #remove_cart_reference
      expect(voucher.voucher_amounts.pluck(:cart_id).compact).to be_empty
    end
  end
end
