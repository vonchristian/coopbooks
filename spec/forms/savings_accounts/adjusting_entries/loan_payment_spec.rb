require 'rails_helper'

module SavingsAccounts
  module AdjustingEntries
    describe LoanPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :principal_amount }
        it { is_expected.to validate_presence_of :interest_amount }
        it { is_expected.to validate_presence_of :penalty_amount }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :savings_account_id }
        it { is_expected.to validate_presence_of :loan_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_numericality_of :principal_amount }
        it { is_expected.to validate_numericality_of :interest_amount }
        it { is_expected.to validate_numericality_of :penalty_amount }
      end

      it '#save_payment' do
        accountant      = create(:accountant)
        cart            = create(:cart, employee: accountant)
        savings_account = create(:saving, office: accountant.office)
        loan            = create(:loan, borrower: savings_account.depositor, office: accountant.office)

        described_class.new(
        principal_amount:   1000,
        interest_amount:    100,
        penalty_amount:     100,
        cart_id:            cart.id,
        savings_account_id: savings_account.id,
        loan_id:            loan.id,
        employee_id:        accountant.id).
        save_payment!

        expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.receivable_account_id)
        expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.interest_revenue_account_id)
        expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.penalty_revenue_account_id)

      end
    end
  end
end
