require 'rails_helper'

module SavingsAccounts
  module AdjustingEntries
    describe ProgramSubscriptionPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :savings_account_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :program_subscription_id }
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
      end
      it '#create_amounts!' do
        accountant           = create(:accountant)
        savings_account      = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
        program              = create(:program, amount: 100, cooperative: accountant.cooperative)
        program_subscription = create(:program_subscription, subscriber: savings_account.depositor, program: program, office: accountant.office)
        cart                 = create(:cart, employee: accountant)
        described_class.new(
          employee_id: accountant.id,
          cart_id: cart.id,
          savings_account_id: savings_account.id,
          program_subscription_id: program_subscription.id,
          amount: 100).create_amounts!

        expect(cart.voucher_amounts.pluck(:account_id)).to include(program_subscription.account_id)
      end
    end
  end
end
