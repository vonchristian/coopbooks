require 'rails_helper'

module SavingsAccounts
  module AdjustingEntries
    describe VoucherProcessing, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :date }
        it { is_expected.to validate_presence_of :reference_number }
        it { is_expected.to validate_presence_of :description }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :savings_account_id }
        it { is_expected.to validate_presence_of :account_number }
      end
    end
  end
end
