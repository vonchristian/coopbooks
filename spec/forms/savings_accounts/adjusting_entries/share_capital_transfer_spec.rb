require 'rails_helper'

module SavingsAccounts
  module AdjustingEntries
    describe ShareCapitalTransfer, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :savings_account_id }
        it { is_expected.to validate_presence_of :share_capital_id }
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
      end

      it '#transfer_amounts!' do
        accountant      = create(:accountant)
        savings_account = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
        share_capital   = create(:share_capital, office: accountant.office, subscriber: savings_account.depositor)
        cart            = create(:cart, employee: accountant)

        described_class.new(
          cart_id: cart.id,
          employee_id: accountant.id,
          savings_account_id: savings_account.id,
          share_capital_id: share_capital.id,
          amount: 100).transfer_amounts!

        expect(cart.voucher_amounts.pluck(:account_id)).to include(share_capital.equity_account_id)
      end
    end
  end
end
