require 'rails_helper'

module Savings
  describe Withdraw, type: :model do
    it "#create_voucher!" do
      teller         = create(:teller)
      cash_account   = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
      saving         = create(:saving, office: teller.office, cooperative: teller.cooperative)
      account_number = "b9fa0482-48cd-4d74-a2ab-ee8f31b776fd"

      described_class.new(
        date:            Date.current,
        or_number:       '31231',
        saving_id:       saving.id,
        employee_id:     teller.id,
        account_number:  account_number,
        cash_account_id: cash_account.cash_account_id).create_voucher!

      voucher =  described_class.new(
        date:            Date.current,
        or_number:       '31231',
        saving_id:       saving.id,
        employee_id:     teller.id,
        account_number:  account_number,
        cash_account_id: cash_account.cash_account_id).find_voucher

      expect(voucher).to_not eql nil
      expect(voucher.voucher_amounts.pluck(:account_id)).to include(cash_account.cash_account_id)
      expect(voucher.voucher_amounts.pluck(:account_id)).to include(saving.liability_account_id)

    end
  end
end
