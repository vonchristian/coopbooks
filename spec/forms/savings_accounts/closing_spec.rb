require 'rails_helper'

module Savings
  describe Closing, type: :model do
    it "create_voucher!" do
      teller         = create(:teller)
      revenue        = create(:revenue, office: teller.office)
      cash_account   = create(:employee_cash_account, employee: teller, office: teller.office)
      saving         = create(:saving_with_balance, office: teller.office)
      account_number = "57f333f4-c019-4764-9550-10b186545e20"

      entry        = build(:entry, recorder: teller, office: teller.office, cooperative: teller.cooperative)
      entry.debit_amounts.build(account: cash_account.cash_account, amount: 100_000)
      entry.credit_amounts.build(account: revenue, amount: 100_000)
      entry.save!


      described_class.new(
        date:               Date.current,
        account_number:     account_number,
        amount:             saving.balance,
        employee_id:        teller.id,
        savings_account_id: saving.id,
        reference_number:   account_number,
        cash_account_id:    cash_account.cash_account_id,
        ).create_voucher!

      voucher = described_class.new(
        date:               Date.current,
        account_number:     account_number,
        amount:             saving.balance,
        employee_id:        teller.id,
        savings_account_id: saving.id,
        reference_number:   account_number,
        cash_account_id:    cash_account.cash_account_id,
        ).find_voucher

      expect(voucher).to be_valid
      expect(voucher.voucher_amounts.debit.pluck(:account_id)).to include(saving.liability_account_id)
      expect(voucher.voucher_amounts.credit.pluck(:account_id)).to include(cash_account.cash_account_id)

    end
  end
end
