require 'rails_helper'

module Savings
  describe InterestExpense, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :date }
      it { is_expected.to validate_presence_of :reference_number }
      it { is_expected.to validate_presence_of :description }
      it { is_expected.to validate_presence_of :amount }
      it { is_expected.to validate_presence_of :employee_id }
      it { is_expected.to validate_presence_of :account_number }
      it { is_expected.to validate_presence_of :savings_account_id }
      it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    end

    it '#process!' do
      account_number  = '918b5cf9-09d2-4982-8d45-f0fb5b1a81ef'
      bookkeeper      = create(:bookkeeper)
      office          = bookkeeper.office
      savings_account = create(:saving, office: office)

      described_class.new(
        account_number:     account_number,
        date:               Date.current,
        reference_number:   '232',
        description:        'interest expense',
        amount:             11,
        employee_id:        bookkeeper.id,
        savings_account_id: savings_account.id
      ).process!

      voucher = office.vouchers.find_by(account_number: account_number)

      expect(office.vouchers).to include(voucher)
      expect(voucher.voucher_amounts.accounts).to include(savings_account.liability_account)
      expect(voucher.voucher_amounts.accounts).to include(savings_account.interest_expense_account)
      expect(voucher.voucher_amounts.credit.total).to eql 11.0
      expect(voucher.voucher_amounts.debit.total).to eql 11.0

    end
  end
end
