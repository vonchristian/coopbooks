require 'rails_helper'

describe CashCountProcessing, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :bill_1000_count }
  end 
end
