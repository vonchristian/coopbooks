require 'rails_helper'

module Members
  describe Address, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :member_id }
      it { is_expected.to validate_presence_of :barangay_id }
      it { is_expected.to validate_presence_of :municipality_id }
      it { is_expected.to validate_presence_of :province_id }
      it { is_expected.to validate_presence_of :complete_address }
    end

    it '#register' do
      member       = create(:member)
      province     = create(:province)
      municipality = create(:municipality, province: province)
      barangay     = create(:barangay, municipality: municipality)

      expect(member.addresses.size).to eql 0

      described_class.new(
        member_id:        member.id,
        province_id:      province.id,
        municipality_id:  municipality.id,
        barangay_id:      barangay.id,
        complete_address: 'Test Address'
      ).register!

      expect(member.addresses).to_not eql nil
    end
  end
end
