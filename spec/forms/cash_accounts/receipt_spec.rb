require 'rails_helper'

module CashAccounts
  describe Receipt, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :amount }
      it { is_expected.to validate_presence_of :account_id }
      it { is_expected.to validate_presence_of :cash_account_id }

      it { is_expected.to validate_presence_of :cart_id }
      it { is_expected.to validate_presence_of :employee_id }

      it { is_expected.to validate_numericality_of :amount }
    end

    it '#create_amounts!' do
      cash_account = create(:employee_cash_account)
      cart         = create(:cart)
      revenue      = create(:revenue)

      described_class.new(
        amount:      100,
        cart_id:     cart.id,
        employee_id: cash_account.employee_id,
        account_id:  revenue.id,
        cash_account_id: cash_account.cash_account_id).
        create_amounts!

      expect(cart.voucher_amounts.debit.accounts).to include(cash_account.cash_account)
      expect(cart.voucher_amounts.credit.accounts).to include(revenue)

    end
  end
end
