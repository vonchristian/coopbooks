require 'rails_helper'

module BankAccounts
  describe Withdraw, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :bank_account_id }
      it { is_expected.to validate_presence_of :reference_number }
      it { is_expected.to validate_presence_of :employee_id }
      it { is_expected.to validate_presence_of :amount }
      it { is_expected.to validate_presence_of :description }
      it { is_expected.to validate_presence_of :account_number }
      it { is_expected.to validate_presence_of :date }
      it { is_expected.to validate_presence_of :cash_account_id }
    end
    it '#create_voucher!' do
      teller       = create(:teller)
      cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
      bank_account = create(:bank_account, office: teller.office, cooperative: teller.cooperative)
      deposit      = build(:entry, office: teller.office, cooperative: teller.cooperative, recorder: teller)
      deposit.debit_amounts.build(amount: 50_000, account: bank_account.cash_account)
      deposit.credit_amounts.build(amount: 50_000, account: cash_account.cash_account)
      deposit.save!

      described_class.new(
        employee_id:      teller.id,
        bank_account_id:  bank_account.id,
        reference_number: '31312',
        amount:           10_000,
        account_number:   "1ccfe218-055b-4adb-bd74-fe5b9eb83322",
        description:      'bank withdraw',
        date:             Date.current,
        cash_account_id:  cash_account.cash_account_id
      ).create_voucher!

      voucher = described_class.new(
        employee_id:      teller.id,
        bank_account_id:  bank_account.id,
        reference_number: '31312',
        amount:           10_000,
        account_number:   "1ccfe218-055b-4adb-bd74-fe5b9eb83322",
        description:      'bank withdraw',
        date:             Date.current,
        cash_account_id:  cash_account.cash_account_id
      ).find_voucher

      expect(voucher).to be_present
      expect(voucher.voucher_amounts.credit.pluck(:account_id)).to include bank_account.cash_account_id
      expect(voucher.voucher_amounts.debit.pluck(:account_id)).to include cash_account.cash_account_id

      expect(voucher.voucher_amounts.debit.total).to eql 10_000
      expect(voucher.voucher_amounts.credit.total).to eql 10_000
    end
  end
end
