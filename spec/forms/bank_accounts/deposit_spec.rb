require 'rails_helper'

module BankAccounts
  describe Deposit, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :amount }
      it { is_expected.to validate_presence_of :reference_number }
      it { is_expected.to validate_presence_of :bank_account_id }
      it { is_expected.to validate_presence_of :employee_id }
      it { is_expected.to validate_presence_of :description }
      it { is_expected.to validate_presence_of :reference_number }
      it { is_expected.to validate_presence_of :account_number }
      it { is_expected.to validate_presence_of :date }
      it { is_expected.to validate_presence_of :cash_account_id }
      it { is_expected.to validate_numericality_of :amount }
    end

    it 'create_deposit_voucher' do
      teller       = create(:teller)
      cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
      bank_account = create(:bank_account, cooperative: teller.cooperative, office: teller.office)

      described_class.new(
        amount:           1000,
        bank_account_id:  bank_account.id,
        cash_account_id:  cash_account.cash_account_id,
        employee_id:      teller.id,
        description:      'bank deposit',
        reference_number: '001',
        account_number:   "96c88544-1361-410b-875c-516f9ed20ae7",
        date:             Date.current
      ).create_voucher!

      voucher = described_class.new(
        amount:           1000,
        bank_account_id:  bank_account.id,
        cash_account_id:  cash_account.cash_account_id,
        employee_id:      teller.id,
        description:      'bank deposit',
        reference_number: '001',
        account_number:   "96c88544-1361-410b-875c-516f9ed20ae7",
        date:             Date.current
      ).find_voucher

      expect(voucher).to be_present
      expect(voucher.voucher_amounts.debit.pluck(:account_id)).to include(bank_account.cash_account_id)
      expect(voucher.voucher_amounts.credit.pluck(:account_id)).to include(cash_account.cash_account_id)
      expect(voucher.voucher_amounts.debit.total).to eql 1_000
      expect(voucher.voucher_amounts.credit.total).to eql 1_000
    end
  end
end
