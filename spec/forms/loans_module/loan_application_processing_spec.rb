require 'rails_helper'

module LoansModule
  describe LoanApplicationProcessing, type: :model do
    describe 'validations' do
      it { is_expected.to validate_presence_of :term }
      it { is_expected.to validate_presence_of :interest_rate }
      it { is_expected.to validate_presence_of :interest_prededuction_rate }

      it { is_expected.to validate_presence_of :loan_amount }
      it { is_expected.to validate_presence_of :loan_product_id }
      it { is_expected.to validate_presence_of :mode_of_payment }
      it { is_expected.to validate_presence_of :borrower_id }
      it { is_expected.to validate_presence_of :borrower_type }
      it { is_expected.to validate_presence_of :purpose }
      it { is_expected.to validate_presence_of :application_date }
      it { is_expected.to validate_presence_of :account_number }
      it { is_expected.to validate_presence_of :preparer_id }
      it { is_expected.to validate_numericality_of :term }
      it { is_expected.to validate_numericality_of :interest_rate }
      it { is_expected.to validate_numericality_of :interest_prededuction_rate }
      it { is_expected.to validate_numericality_of :loan_amount }
    end

    it '#process' do
      loan_officer = create(:loan_officer)
      loan_product = create(:loan_product, cooperative: loan_officer.cooperative)
      interest_config = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
      interest_prededuction = create(:interest_prededuction, calculation_type: 'percent_based', loan_product: loan_product, rate: 1, amount: 0, number_of_payments: 0)
      create(:office_loan_product, office: loan_officer.office, loan_product: loan_product)
      cart = create(:cart, employee: loan_officer)
      member = create(:member)
      described_class.new(
        term:                      12,
        loan_amount:               100_000,
        interest_rate:             0.12,
        borrower_id:               member.id,
        borrower_type:             member.class.to_s,
        loan_product_id:           loan_product.id,
        purpose:                   'loan',
        application_date:          Date.current,
        mode_of_payment:           'monthly',
        account_number:            "0773d92d-71f7-4452-9505-8a597051ad41",
        preparer_id:               loan_officer.id,
        cart_id:                   cart.id,
        interest_calculation_type: 'prededucted',
        interest_prededuction_rate: 1
      ).process!

      loan_application = described_class.new(
        term:                      12,
        loan_amount:               100_000,
        interest_rate:             0.12,
        borrower_id:               member.id,
        borrower_type:             member.class.to_s,
        loan_product_id:           loan_product.id,
        purpose:                   'loan',
        application_date:          Date.current,
        mode_of_payment:           'monthly',
        account_number:            "0773d92d-71f7-4452-9505-8a597051ad41",
        preparer_id:               loan_officer.id,
        cart_id:                   cart.id,
        interest_calculation_type: 'prededucted',
        interest_prededuction_rate: 1

      ).find_loan_application

      expect(loan_application).to be_present
    end
  end
end
