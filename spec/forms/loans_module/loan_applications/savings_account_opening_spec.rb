require 'rails_helper'

module LoansModule
  module LoanApplications
    describe SavingsAccountOpening, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :saving_product_id }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :account_number }
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
      end

      it 'create_application!' do
        loan_officer     = create(:loan_officer)
        office           = loan_officer.office
        loan_application = create(:loan_application, office: office)
        saving_product   = create(:saving_product)
        cart             = create(:cart, employee: loan_officer)
        account_number   = "435ceed3-75b4-4037-be70-87d005530e92"
        create(:office_saving_product, saving_product: saving_product, office: office)
        described_class.new(
          cart_id:             cart.id,
          employee_id:         loan_officer.id,
          loan_application_id: loan_application.id,
          saving_product_id:   saving_product.id,
          account_number:      account_number,
          amount: 1000).create_application!

        liability_account = office.accounts.find_by!(name: "#{saving_product.name} - (#{loan_application.borrower.full_name} - #{account_number}")
        expect(office.savings_account_applications.find_by!(account_number: account_number)).to be_present
        expect(cart.voucher_amounts.pluck(:account_id)).to include(liability_account.id)
      end
    end
  end
end
