require 'rails_helper'

module LoansModule
  module LoanApplications
    describe Saving, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
        it { is_expected.to validate_presence_of :savings_account_id }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :cart_id }
      end

      it '#process!' do
        cart             = create(:cart)
        saving           = create(:saving)
        loan_application = create(:loan_application, cart: cart)
        described_class.new(amount: 100, savings_account_id: saving.id, loan_application_id: loan_application.id, cart_id: cart.id).process!

        expect(cart.voucher_amounts.pluck(:account_id)).to include(saving.liability_account_id)

      end
    end
  end
end
