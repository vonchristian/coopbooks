require 'rails_helper'

module LoansModule
  module LoanApplications
    describe ProgramSubscriptionPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :program_subscription_id }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
      end

      it '#process!' do
        member               = create(:member)
        loan_application     = create(:loan_application, borrower: member)
        program              = create(:program)
        program_subscription = create(:program_subscription, program: program, subscriber: member)

        described_class.new(amount: 500, loan_application_id: loan_application.id, program_subscription_id: program_subscription.id).process!

        expect(loan_application.cart.voucher_amounts.credit.pluck(:account_id)).to include(program_subscription.account_id)
      end
    end
  end
end

