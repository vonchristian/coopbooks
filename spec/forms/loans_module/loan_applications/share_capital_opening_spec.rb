require 'rails_helper'

module LoansModule
  module LoanApplications
    describe ShareCapitalOpening, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :share_capital_product_id }
        it { is_expected.to validate_presence_of :account_number }
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
      end
     it 'create_application!' do
        loan_officer          = create(:loan_officer)
        office                = loan_officer.office
        loan_application      = create(:loan_application, office: office)
        share_capital_product = create(:share_capital_product, cooperative: loan_officer.cooperative)
        cart                  = create(:cart, employee: loan_officer)
        account_number        = "435ceed3-75b4-4037-be70-87d005530e92"
        office_share_capital_product = create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)

        described_class.new(
          cart_id:                  cart.id,
          employee_id:              loan_officer.id,
          loan_application_id:      loan_application.id,
          share_capital_product_id: share_capital_product.id,
          account_number:           account_number,
          amount: 1000).create_application!

        equity_account = office.accounts.find_by!(account_number: account_number)
        expect(office.share_capital_applications.find_by!(account_number: account_number)).to be_present
        expect(cart.voucher_amounts.pluck(:account_id)).to include(equity_account.id)
      end
    end
  end
end
