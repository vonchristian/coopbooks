require 'rails_helper'

module LoansModule
  module LoanApplications
    describe Disbursement, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :disbursement_date }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :employee_id }
      end
      it "#disburse!" do
        teller                    = create(:teller)
        loan_group                = create(:loan_group, office: teller.office, start_num: 0, end_num: 0)
        cash_account              = create(:employee_cash_account, employee: teller, office: teller.office)
        cart                      = create(:cart)
        voucher                   = create(:voucher, office: teller.office, cooperative: teller.cooperative)
        loan_application          = create(:loan_application, cart: cart, voucher: voucher, interest_calculation_type: 'prededucted', office: teller.office)
        share_capital_product     = create(:share_capital_product, cooperative: teller.cooperative)
        create(:office_share_capital_product, office: teller.office, share_capital_product: share_capital_product)
        share_capital_application = create(:share_capital_application, share_capital_product: share_capital_product, office: teller.office, cart: cart)
        voucher.voucher_amounts.credit.create!(account: cash_account.cash_account, amount: 8_000, recorder: teller, cooperative: teller.cooperative)
        voucher.voucher_amounts.debit.create!(account: loan_application.receivable_account, amount: 10_000, recorder: teller, cooperative: teller.cooperative)
        voucher.voucher_amounts.debit.create!(account: share_capital_application.equity_account, amount: 2_000, recorder: teller, cooperative: teller.cooperative)
        described_class.new(
          disbursement_date: Date.current,
          employee_id: teller.id,
          loan_application_id: loan_application.id).disburse!

        expect(teller.office.loans.find_by(account_number: loan_application.account_number)).to be_present
        expect(teller.office.share_capitals.find_by(account_number: share_capital_application.account_number)).to be_present
      end
    end
  end
end
