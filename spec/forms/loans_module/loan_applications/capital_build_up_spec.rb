require 'rails_helper'

module LoansModule
  module LoanApplications
    describe CapitalBuildUp, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :share_capital_id }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_numericality_of :amount }
      end

      it '#process' do
        cart             = create(:cart)
        loan_application = create(:loan_application, cart: cart)
        share_capital    = create(:share_capital)
        described_class.new(amount: 100, loan_application_id: loan_application.id, share_capital_id: share_capital.id, cart_id: cart.id).process!

        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include share_capital.equity_account_id
      end
    end
  end
end
