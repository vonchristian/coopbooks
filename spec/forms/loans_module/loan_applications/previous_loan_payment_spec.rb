require 'rails_helper'

module LoansModule
  module LoanApplications
    describe PreviousLoanPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :principal_amount }
        it { is_expected.to validate_presence_of :interest_amount }
        it { is_expected.to validate_presence_of :penalty_amount }
        it { is_expected.to validate_presence_of :loan_application_id }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :loan_id }

        it { is_expected.to validate_presence_of :employee_id }
      end

      describe '#process' do
        let(:loan_officer)     { create(:loan_officer) }
        let(:cart)             { create(:cart, employee: loan_officer) }
        let(:loan_application) { create(:loan_application, cart: cart, preparer: loan_officer) }
        let(:loan)             { create(:loan, borrower: loan_application.borrower) }

        it 'with principal' do
          described_class.new(loan_id: loan.id, cart_id: cart.id, loan_application_id: loan_application.id, employee_id: loan_officer.id, principal_amount: 1_000, interest_amount: 0, penalty_amount: 0).process!

          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.receivable_account_id)
        end

        it 'with principal and interest' do
          described_class.new(loan_id: loan.id, cart_id: cart.id, loan_application_id: loan_application.id, employee_id: loan_officer.id, principal_amount: 1_000, interest_amount: 500, penalty_amount: 0).process!

          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.receivable_account_id)
          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.interest_revenue_account_id)
        end

        it 'with principal, interest and penalty' do
          described_class.new(loan_id: loan.id, cart_id: cart.id, loan_application_id: loan_application.id, employee_id: loan_officer.id, principal_amount: 1_000, interest_amount: 500, penalty_amount: 500).process!

          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.receivable_account_id)
          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.interest_revenue_account_id)
          expect(cart.voucher_amounts.pluck(:account_id)).to include(loan.penalty_revenue_account_id)
        end
      end
    end
  end
end
