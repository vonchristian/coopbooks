require 'rails_helper'
module LoansModule
  module Loans
    describe PaymentVoucher, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :principal_amount }
        it { is_expected.to validate_presence_of :interest_amount }
        it { is_expected.to validate_presence_of :penalty_amount }
        it { is_expected.to validate_presence_of :date }
        it { is_expected.to validate_presence_of :reference_number }
        it { is_expected.to validate_presence_of :description }
        it { is_expected.to validate_presence_of :cash_account_id }
        it { is_expected.to validate_numericality_of(:principal_amount) }
        it { is_expected.to validate_numericality_of(:interest_amount) }
        it { is_expected.to validate_numericality_of(:penalty_amount) }
        it 'principal amount is less than or equal to loan balance' do
        end

        it '#create_payment_voucher!' do
          teller       = create(:teller)
          cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
          loan         = create(:loan, office: teller.office)

          described_class.new(
            loan_id: loan.id,
            principal_amount: 1000,
            interest_amount:  1000,
            penalty_amount:   1000,
            reference_number: '312312',
            date:             Date.current,
            description:      'loan payment',
            employee_id:      teller.id,
            cash_account_id:  cash_account.cash_account_id,
            account_number:   '11111111').create_payment_voucher!

          expect(teller.office.vouchers.find_by!(account_number: '11111111')).to be_present
          end
      end
    end
  end
end
