require 'rails_helper'

module ShareCapitals
  module AdjustingEntries
    describe VoucherProcessing, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :date }
        it { is_expected.to validate_presence_of :reference_number }
        it { is_expected.to validate_presence_of :description }
        it { is_expected.to validate_presence_of :account_number }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :share_capital_id }

      end

      it '#create_voucher' do
        accountant = create(:accountant)
        office = accountant.office
        share_capital = create(:share_capital, office: office)
        cart = create(:cart, employee: accountant)
        described_class.new(
          employee_id:      accountant.id,
          date:             Date.current,
          reference_number: 'dasdasd',
          description:      'voucher description',
          cart_id:          cart.id,
          share_capital_id: share_capital.id,
          account_number:   "024511c1-3d1d-409f-9c96-2186c273a418").create_voucher!

        expect(office.vouchers.find_by(account_number: "024511c1-3d1d-409f-9c96-2186c273a418")).to be_present
      end
    end
  end
end
