require 'rails_helper'

module ShareCapitals
  module AdjustingEntries
    describe LoanPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :loan_id }
        it { is_expected.to validate_presence_of :share_capital_id }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :employee_id }
        it { is_expected.to validate_presence_of :principal_amount }
        it { is_expected.to validate_presence_of :interest_amount }
        it { is_expected.to validate_presence_of :penalty_amount }
        it { is_expected.to validate_numericality_of :principal_amount }
        it { is_expected.to validate_numericality_of :interest_amount }
        it { is_expected.to validate_numericality_of :penalty_amount }
      end

      it '#create_voucher_amounts!' do
        accountant    = create(:accountant)
        share_capital = create(:share_capital, office: accountant.office)
        loan          = create(:loan, borrower: share_capital.subscriber, office: accountant.office)
        cart          = create(:cart, employee: accountant)

        described_class.new(
          employee_id:      accountant.id,
          cart_id:          cart.id,
          share_capital_id: share_capital.id,
          loan_id:          loan.id,
          principal_amount: 1000,
          interest_amount:  100,
          penalty_amount:   100).create_amounts!

        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(loan.receivable_account_id)
        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(loan.interest_revenue_account_id)
        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(loan.penalty_revenue_account_id)
      end
    end
  end
end


