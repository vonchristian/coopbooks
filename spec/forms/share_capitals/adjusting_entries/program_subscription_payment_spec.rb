require 'rails_helper'

module ShareCapitals
  module AdjustingEntries
    describe ProgramSubscriptionPayment, type: :model do
      describe 'validations' do
        it { is_expected.to validate_presence_of :amount }
        it { is_expected.to validate_numericality_of :amount }
        it { is_expected.to validate_presence_of :cart_id }
        it { is_expected.to validate_presence_of :share_capital_id }
        it { is_expected.to validate_presence_of :program_subscription_id }
      end

      it '#create_amounts' do
        accountant    = create(:accountant)
        share_capital = create(:share_capital, office: accountant.office)
        cart          = create(:cart, employee: accountant)
        program       = create(:program, amount: 100, cooperative: accountant.cooperative)
        program_subscription = create(:program_subscription, subscriber: share_capital.subscriber, program: program)

        described_class.new(
          cart_id: cart.id,
          employee_id: accountant.id,
          share_capital_id: share_capital.id,
          program_subscription_id: program_subscription.id,
          amount: 100).create_amounts!

        expect(cart.voucher_amounts.credit.pluck(:account_id)).to include(program_subscription.account_id)
      end
    end
  end
end
