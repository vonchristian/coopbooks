require 'rails_helper'
include ChosenSelect

describe "New membership application" do
  before(:each) do
    cooperative         = create(:cooperative)
    occupation          = create(:occupation, title: 'Farming')
    barangay            = create(:barangay, name: "Test barangay")
    office              = create(:office, cooperative: cooperative)
    membership_category = create(:regular_membership_category, title: "Regular Member", cooperative: cooperative)
    user                = create(:user, role: 'teller', cooperative: cooperative, office: office)
    login_as(user, scope: :user)
    visit members_url
    click_link "New Member"
  end

  it "wtih valid attributes", js: true do
    fill_in "First name",  with: "Von"
    fill_in "Middle name", with: "P"
    fill_in "Last name",   with: "Halip"
    choose "Male"
    choose "Married"
    fill_in "Date of birth",  with: "02/12/1990"
    fill_in "Contact number", with: "48234239482934"
    fill_in "Email",          with: 'ddd@example.com'
    fill_in "Complete address", with: "dasjd aksdaj skda"
    page.execute_script "window.scrollBy(0,10000)"

    select_from_chosen "Test barangay", from: 'Barangay'
    select_from_chosen 'Regular Member', from: 'Membership category'
    page.execute_script "window.scrollBy(0,10000)"

    fill_in 'Membership date', with: Date.current

    select_from_chosen 'Farming', from: 'Occupation'

    click_button "Save Member"

    expect(page).to have_content("saved successfully")
  end
end
