require 'rails_helper'

describe 'Index Organization', type: :system do
  before(:each) do
    office = create(:office)
    user = create(:user, office: office)
    login_as(user, :scope => :user)
    organization = create(:organization, name: "Women's Organization", office: office)
    organization = create(:organization, name: "Barangay Org", office: office)

    organization_2 = create(:organization, name: "Test Organization")
    visit organizations_url
  end

  it "lists organizations" do
    expect(page).to have_content("Women's Organization")
    expect(page).to have_content("Barangay Org")

    expect(page).to_not have_content("Test Organization")
  end
  it 'returns search' do
    fill_in 'organizationSearchForm', with: "Women's Organization"
    click_button 'Search Organization'

    expect(page).to have_content("Women's Organization")
    expect(page).to_not have_content("Barangay Org")
  end
end
