require 'rails_helper'

describe 'New organization' do
  before(:each) do
    user = create(:branch_manager)
    organization = create(:organization, name: 'Testing Women Organization', abbreviated_name: 'TWO', office: user.office)
    login_as(user, scope: :user)
    visit organizations_path
    click_link 'New Organization'
  end

  it 'with valid attributes' do
    fill_in 'Name',             with: 'Test Org'
    fill_in 'Abbreviated name', with: 'TO'

    click_button 'Create Organization'

    expect(page).to have_content("created successfully")
  end

  it 'with invalid attributes' do
    click_button 'Create Organization'

    expect(page).to have_content("can't be blank")
  end
  it 'unique name' do
    fill_in 'Name',             with: 'Testing Women Organization'
    fill_in 'Abbreviated name', with: 'TWO'

    click_button 'Create Organization'

    expect(page).to have_content("already been taken")
  end 

end
