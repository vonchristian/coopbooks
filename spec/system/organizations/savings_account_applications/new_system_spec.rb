require 'rails_helper'
include ChosenSelect
describe 'New savings account application' do
  before(:each) do
    teller          = create(:teller)
    cash_account    = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    saving_product  = create(:saving_product, name: "Regular Savings", cooperative: teller.cooperative)
    organization    = create(:organization, office: teller.office)
    create(:office_saving_product, office: teller.office, saving_product: saving_product)
    create(:saving_group, office: teller.office)
    login_as(teller, scope: :user)
    visit organization_savings_accounts_path(organization)
    click_link "New Savings Account"

  end

  it "with valid attributes", js: true do

    fill_in "Date",             with: Date.current
    select_from_chosen "Regular Savings", from: 'Savings Account Product'
    fill_in "Initial Deposit",  with: 5_000
    fill_in "Description",      with: "Opening of savings account"
    fill_in "Reference number", with: "909090"
    select "Cash on Hand"

    click_button "Proceed"

    expect(page).to have_content "created successfully"

    page.execute_script "window.scrollBy(0,10000)"
    click_link "Confirm Transaction"

    expect(page).to have_content "opened successfully"
  end

  it "with invalid attributes", js: true do
    click_button "Proceed"

    expect(page).to have_content("can't be blank")
  end
end
