require 'rails_helper'

describe 'New organization member' do
  it 'with valid attributes' do
    manager           = create(:general_manager)
    cooperative       = manager.cooperative
    office            = manager.office
    organization      = create(:organization, office: office)
    member            = create(:member)
    membership        = create(:regular_membership, cooperative: cooperative)
    office_membership = create(:office_membership, membership: membership, office: office)

    login_as(manager, scope: :user)
    visit organizations_path
    click_link organization.name
    click_link "#{organization.id}-members"

    click_link 'Add Member'

    click_button 'Add Member'

    expect(page).to have_content('added successfully')
  end
end
