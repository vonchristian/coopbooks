require 'rails_helper'

describe 'Time deposit interest expense posting' do
  before(:each) do
    teller       = create(:teller)
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative )
    time_deposit = create(:time_deposit, office: teller.office, cooperative: teller.cooperative)
    entry        = build(:entry, commercial_document: time_deposit, office: teller.office, cooperative: teller.cooperative, recorder: teller)
    entry.debit_amounts.build(amount: 100_000, account: cash_account.cash_account)
    entry.credit_amounts.build(amount: 100_000, account: time_deposit.liability_account)
    entry.save!

    login_as(teller, scope: :user)
    visit time_deposit_url(time_deposit)
    click_link "#{time_deposit.id}-settings"
    click_link 'New Interest Expense'
  end
  it 'with valid attributes' do
    fill_in 'Date', with: Date.current
    fill_in 'Reference number', with: '909090'
    fill_in 'Description', with: 'Interest expense'
    fill_in 'Amount', with: 1000
    click_button 'Proceed'
    expect(page).to have_content('created successfully')

    click_link 'Confirm Transaction'

    expect(page).to have_content('posted successfully')
  end

  it 'with invalid attributes' do
    click_button 'Proceed'

    expect(page).to have_content("can't be blank")
  end
end
