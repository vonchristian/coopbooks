require 'rails_helper'

describe 'Time deposit withdrawal' do
  before(:each) do
    teller       = create(:teller)
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative )
    time_deposit = create(:time_deposit, office: teller.office, cooperative: teller.cooperative)
    entry        = build(:entry, commercial_document: time_deposit, office: teller.office, cooperative: teller.cooperative, recorder: teller)
    entry.debit_amounts.build(amount: 100_000, account: cash_account.cash_account)
    entry.credit_amounts.build(amount: 100_000, account: time_deposit.liability_account)
    entry.save!

    login_as(teller, scope: :user)
    visit time_deposit_url(time_deposit)
    click_link 'Withdraw'
  end
  it 'with valid attributes' do
    fill_in 'Date', with: Date.current
    fill_in 'Reference Number / OR Number', with: '909090'
    click_button 'Proceed'
    expect(page).to have_content('created successfully')

    click_link 'Confirm Transaction'

    expect(page).to have_content('withdrawn successfully')
  end

  it 'with invalid attributes' do
    click_button 'Proceed'

    expect(page).to have_content("can't be blank")
  end
end
