require 'rails_helper'

describe 'Edit member membership' do
  before(:each) do
    user       = create(:user, role: 'teller')
    member     = create(:member)
    membership = create(:regular_membership, cooperator: member, cooperative: user.cooperative)
    create(:office_membership, office: user.office, membership: membership)
    login_as(user, scope: :user)

    visit member_url(member)
    click_link 'Settings'
  end

  it 'with valid attributes' do


  end
end
