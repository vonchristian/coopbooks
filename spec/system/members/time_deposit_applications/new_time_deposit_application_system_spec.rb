require 'rails_helper'
include ChosenSelect
describe 'New time deposit application' do
  before(:each) do
    teller = create(:teller)
    office = teller.office
    asset  = create(:asset, name: "Cash on Hand", office: teller.office)
    cash_account = create(:employee_cash_account, cash_account: asset, employee: teller, office: teller.office, cooperative: teller.cooperative)
    time_deposit_product  = create(:time_deposit_product, name: "Time Deposit (100,000)", cooperative: teller.cooperative)
    member                = create(:member)
    membership            = create(:regular_membership, cooperator: member, cooperative: teller.cooperative)
    office_membership     = create(:office_membership, membership: membership, office: teller.office)
    create(:office_time_deposit_product, office: office, time_deposit_product: time_deposit_product)
    login_as(teller, scope: :user)
    visit member_time_deposits_url(member)
    find(:css, 'i.fa.fa-plus-circle').click
  end

  it "with valid attributes", js: true do
    select "Cash on Hand"
    fill_in "Date",             with: Date.current.strftime("%B %e, %Y")
    fill_in "Reference number", with: "53345"
    fill_in "Description",      with: "Time deposit"
    fill_in "Amount",           with: 100_000
    select_from_chosen "Time Deposit (100,000)", from: 'Time deposit product'
    fill_in "Term",             with: 6
    fill_in "Beneficiaries",    with: "Beneficiary"

    click_button "Proceed"
    expect(page).to have_content "created successfully"

    click_link "Confirm"

    expect(page).to have_content "saved successfully"

  end

  it "with invalid attributes", js: true do
    click_button "Proceed"

    expect(page).to have_content "can't be blank"
  end
end
