require 'rails_helper'

describe 'Index of member time deposits' do
  it "logged in" do
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    member            = create(:member)
    membership        = create(:regular_membership, cooperator: member, cooperative: cooperative)
    office_membership = create(:office_membership, membership: membership, office: office)

    user = create(:user, role: 'teller', office: office, cooperative: cooperative)
    login_as(user, scope: :user )
    visit member_time_deposits_url(member)
    expect(page).to have_content("Time Deposits")
  end
end
