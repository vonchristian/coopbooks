require 'rails_helper'

describe 'EDIT Identification' do
  before(:each) do
    provider          = create(:identity_provider, abbreviated_name: "BIR")
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    member            = create(:member)
    identification    = create(:identification, identifiable: member, identity_provider: provider)
    membership        = create(:regular_membership, cooperative: cooperative, cooperator: member)
    office_membership = create(:office_membership, office: office, membership: membership)
    user              = create(:user, role: 'teller', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit members_path
    click_link member.first_and_last_name
    click_link 'Edit Identification'
  end

  it 'with valid attributes' do
    fill_in 'Issuance date', with: '01/12/2018'
    fill_in 'Expiry date',   with: '01/12/2020'
    fill_in 'ID Number',     with: '3131312'

    click_button 'Update Identification'

    expect(page).to have_content('updated successfully')
  end

  it 'with invalid attributes' do
    fill_in 'Issuance date', with: ''

    click_button 'Update Identification'

    expect(page).to have_content("can't be blank")
  end
end
