require 'rails_helper'

describe 'New Identification' do
  before(:each) do
    provider    = create(:identity_provider, abbreviated_name: "BIR")
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    member      = create(:member)
    membership  = create(:regular_membership, cooperative: cooperative, cooperator: member)
    office_membership = create(:office_membership, office: office, membership: membership)
    user = create(:user, role: 'teller', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit members_path
    click_link member.first_and_last_name
    click_link 'New Identification'
  end

  it 'with valid attributes' do
    select 'BIR'
    fill_in 'Issuance date', with: Date.current.strftime("%B %e, %Y")
    fill_in 'Expiry date',   with: Date.current.next_year.strftime("%B %e, %Y")
    fill_in 'ID Number',     with: '3131312'

    click_button 'Save Identification'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do

    click_button 'Save Identification'

    expect(page).to have_content("can't be blank")
  end
end
