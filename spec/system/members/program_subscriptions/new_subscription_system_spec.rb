require 'rails_helper'

describe 'New program subscription' do
  it 'with valid attributes' do
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    program           = create(:program, cooperative: cooperative, name: 'Mutual Aid System', mode_of_payment: 'annually')
    office_program    = create(:office_program, program: program, office: office)
    member            = create(:member)
    membership        = create(:regular_membership, cooperator: member, cooperative: cooperative)
    office_membership = create(:office_membership, membership: membership, office: office)
    user              = create(:teller, office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit member_path(member)
    click_link 'Subscriptions'
    click_link 'Subscribe'



    expect(page).to have_content('Subscribed')
    expect(member.programs).to include(program)
  end
end
