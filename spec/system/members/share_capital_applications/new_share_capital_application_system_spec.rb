
require 'rails_helper'

describe 'New share capital account' do
  before(:each) do
    teller                = create(:teller)
    office                = teller.office
    cash_account          = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    share_capital_product = create(:share_capital_product, name: "Paid Up Share Capital", cooperative: teller.cooperative)
    member                = create(:member)
    membership            = create(:regular_membership, cooperator: member, cooperative: teller.cooperative)
    office_membership     = create(:office_membership, membership: membership, office: teller.office)
    office_share_capital_product = create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)
    login_as(teller, scope: :user)
    visit member_share_capitals_url(member)
    click_link "New Share Capital Account"
  end

  it "with valid attributes" do
    fill_in "Date",             with: Date.current
    select "Paid Up Share Capital"
    fill_in "Amount",           with: 5_000
    fill_in "Reference number", with: "909090"
    select "Cash on Hand"

    click_button "Proceed"

    expect(page).to have_content "created successfully"


    click_link "Confirm Transaction"

    expect(page).to have_content "opened successfully"
  end

  it 'with invalid attributes' do
    click_button 'Proceed'

    expect(page).to have_content("can't be blank")
  end
end
