require 'rails_helper'
include ChosenSelect
describe 'New savings account application' do
  before(:each) do
    teller = create(:teller)
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    saving_product        = create(:saving_product, name: "Regular Savings", cooperative: teller.cooperative)
    member                = create(:member)
    membership            = create(:regular_membership, cooperator: member, cooperative: teller.cooperative)
    office_membership     = create(:office_membership, office: teller.office, membership: membership)
    create(:saving_group, office: teller.office)
    create(:office_saving_product, office: teller.office, saving_product: saving_product)
    login_as(teller, scope: :user)
    visit member_savings_accounts_url(member)
    find(:css, 'i.fa.fa-plus-circle').click

  end

  it "with valid attributes", js: true do
    fill_in "Date",             with: Date.current
    select_from_chosen "Regular Savings", from: 'Savings Account Product'
    fill_in "Initial Deposit",  with: 5_000
    fill_in "Description",      with: "Opening of savings account"
    fill_in "Reference number", with: "909090"
    select "Cash on Hand"

    click_button "Proceed"

    expect(page).to have_content "created successfully"


    click_link "Confirm Transaction"

    expect(page).to have_content "opened successfully"
  end

  it "with invalid attributes", js: true do
    click_button "Proceed"

    expect(page).to have_content("can't be blank")
  end
end
