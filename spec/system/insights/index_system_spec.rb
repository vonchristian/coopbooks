require 'rails_helper'

describe 'Insights index page' do
  it 'is valid' do
    user = create(:teller)
    login_as(user, scope: :user)
    visit insights_path

    expect(page).to have_content('Insights')
  end
end
