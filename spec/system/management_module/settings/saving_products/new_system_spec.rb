require 'rails_helper'

describe 'New saving product' do
  before(:each) do
    cooperative = create(:cooperative)

    manager = create(:general_manager, cooperative: cooperative)
    login_as(manager, scope: :user)
    visit management_module_settings_path
    click_link 'Saving Products'
    click_link 'New Saving Product'
  end

  it 'with valid attributes' do
    fill_in 'Name', with: 'Regular Savings'
    fill_in 'Interest rate', with: 0.02
    fill_in 'Minimum balance', with: 1_000
    choose 'Quarterly'

    click_button 'Create Saving Product'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Create Saving Product'

    expect(page).to have_content("can't be blank")
  end
end
