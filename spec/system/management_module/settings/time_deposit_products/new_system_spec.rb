require 'rails_helper'

describe 'New time deposit product ' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    manager = create(:general_manager, cooperative: cooperative, office: office)
    login_as(manager, scope: :user)
    visit management_module_settings_path
    click_link 'Time Deposit Products'
    click_link 'New Time Deposit Product'
  end

  it 'with valid attributes' do
    fill_in 'Name', with: 'Test Time Deposit Product'
    fill_in 'Minimum Deposit Amount', with: 1
    fill_in 'Maximum Deposit Amount', with: 100_000
    fill_in 'Interest rate', with: 0.04
    fill_in 'Number of days', with: 100
    fill_in 'Break contract fee', with: 100
    fill_in 'Break contract rate', with: 0

    click_button 'Create Time Deposit Product'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Create Time Deposit Product'

    expect(page).to have_content("can't be blank")
  end
end
