require 'rails_helper'

describe 'New office' do
  before(:each) do
    manager = create(:general_manager)
    login_as(manager, scope: :user)
    visit management_module_settings_path
    click_link 'office-settings'
    click_link 'New Office'
  end
  it 'with valid attributes' do
    fill_in 'Name', with: 'Test office'
    select 'Cooperatives::Offices::MainOffice'
    fill_in 'Contact number', with: '313123'
    fill_in 'Address', with: 'Lagawe, Ifugao'

    click_button 'Save Office'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Office'

    expect(page).to have_content("can't be blank")
  end 
end
