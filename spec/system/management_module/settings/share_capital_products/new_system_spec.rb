require 'rails_helper'
describe 'New share capital product' do
  before(:each) do
    cooperative              = create(:cooperative)
    office                   = create(:main_office, name: "Lamut", cooperative: cooperative)
    user                     = create(:user, role: 'general_manager', office: office, cooperative: cooperative)

    login_as(user, :scope => :user)
    visit management_module_settings_path
    click_link 'Share Capital Products'
    click_link 'New Share Capital Product'
  end

  it ' with valid attributes' do
    fill_in "Name",                 with: "Paid up"
    fill_in 'Cost per share',       with: 500
    fill_in 'Minimum No. of share', with: 10
    fill_in 'Minimum Paid-Up Balance', with: 5_000

    click_button "Create Share Capital Product"

    expect(page).to have_content "created successfully"
  end

  it 'with invalid attributes' do
    click_button 'Create Share Capital Product'

    expect(page).to have_content("can't be blank")
  end
end
