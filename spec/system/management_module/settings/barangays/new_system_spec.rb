require 'rails_helper'
include ChosenSelect
describe 'New barangay' do
  before(:each) do
    manager      = create(:general_manager)
    municipality = create(:municipality, name: 'Test Municipality')
    login_as(manager, scope: :user)
    visit management_module_settings_barangays_path
    click_link 'New Barangay'
  end

  it 'valid', js: true do
    fill_in 'Name', with: 'Test'
    select_from_chosen 'Test Municipality', from: 'Municipality'
    click_button 'Save Barangay'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes', js: true do
    click_button 'Save Barangay'

    expect(page).to have_content("can't be blank")
  end
end
