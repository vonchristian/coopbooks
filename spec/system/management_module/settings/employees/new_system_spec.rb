require 'rails_helper'
include ChosenSelect

describe 'New employee' do
  before(:each) do
    manager = create(:general_manager)
    login_as(manager, scope: :user)
    visit management_module_settings_path
    click_link 'employee-settings'
    click_link 'New Employee'
  end
  it 'with valid attributes' do
  end 
end
