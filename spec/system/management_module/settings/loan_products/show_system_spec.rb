require 'rails_helper'

describe 'loan products show' do
  it 'valid' do
    user              = create(:branch_manager)
    loan_product      = create(:loan_product, name: 'Regular Loan', cooperative: user.cooperative)
    login_as(user, scope: :user)
    visit management_module_settings_path

    click_link 'Loan Products'
    click_link 'Regular Loan'
    expect(page).to have_content('Regular Loan')
  end
end
