require 'rails_helper'

describe 'Edit loan product' do
  before(:each) do
    cooperative                       = create(:cooperative)
    office                            = create(:office, cooperative: cooperative)
    loan_product                      = create(:loan_product, name: 'Regular Loan', cooperative: cooperative)
    user                              = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    amortization_type                 = create(:amortization_type, name: 'Straight Line', office: office)

    provider                          = create(:loan_protection_plan_provider, business_name: 'CLIMBS', office: office)
    loan_product.amortization_type                 = amortization_type
    loan_product.save!
    login_as(user, scope: :user)
    visit management_module_settings_path
    click_link 'Loan Products'
    click_link 'Regular Loan'
    click_link 'Edit Loan Product'
  end

  it 'valid' do
    fill_in 'Name', with: 'Regular Loan 2'
 


    click_button 'Update Loan Product'


    expect(page).to have_content('updated successfully')
  end
end
