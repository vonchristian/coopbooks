require 'rails_helper'
include ChosenSelect
describe 'New loan product' do
  before(:each) do
    cooperative                       = create(:cooperative)
    office                            = create(:office, cooperative: cooperative)
    user                              = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    amortization_type                 = create(:amortization_type, name: 'Straight Line', office: office)
    receivable_account_category       = create(:asset_level_one_account_category, office: office, title: 'Loans Receivables')
    interest_revenue_account_category = create(:revenue_account_category, office: office, title: 'Interest Income')
    penalty_revenue_account_category  = create(:revenue_account_category, office: office, title: 'Penalty Income')
    provider                          = create(:loan_protection_plan_provider, business_name: 'CLIMBS', office: office)

    login_as(user, scope: :user)
    visit management_module_settings_loan_products_path
    click_link 'New Loan Product'
  end

  it 'with valid attributes', js: true do
    fill_in 'Name',                           with: 'Regular Loan'
    fill_in 'Description',                    with: 'regular loan'
    fill_in 'Maximum loanable amount',        with: 100_000
    fill_in 'Grace period',                   with: 7
    select_from_chosen  'CLIMBS', from: 'Loan protection plan provider'
    select_from_chosen  'Loans Receivables', from: 'Receivable Account'
    select_from_chosen  'Interest Income', from: 'Interest Revenue Account'
    select_from_chosen  'Penalty Income',  from: 'Penalty Revenue Account'

    fill_in 'Annual Interest Rate',           with: 0.12
    choose  'Prededucted'
    choose  'Percent Based'
    choose  'On First Year'
    fill_in 'Prededucted rate',               with: 1
    fill_in 'Prededucted amount',             with: 0
    fill_in 'Prededucted number of payments', with: 0
    fill_in 'Penalty rate',                   with: 0.02
    choose  'Straight Line'

    click_button 'Create Loan Product'
  end

  it 'with invalid attributes', js: true do
    click_button 'Create Loan Product'

    expect(page).to have_content("can't be blank")
  end
end
