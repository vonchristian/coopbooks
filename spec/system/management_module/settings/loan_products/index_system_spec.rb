require 'rails_helper'

describe 'Index loan products' do
  it 'valid' do
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    user              = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    amortization_type = create(:amortization_type, name: 'Straight Line', office: office)
    provider          = create(:loan_protection_plan_provider, business_name: 'CLIMBS', office: office)
    loan_product      = create(:loan_product, cooperative: cooperative, name: 'Regular Loan')
    loan_product_2    = create(:loan_product, name: 'Another Loan')

    login_as(user, scope: :user)
    visit management_module_settings_path
    click_link 'Loan Products'
    expect(page).to have_content('Regular Loan')
    expect(page).to_not have_content('Another Loan')

  end
end
