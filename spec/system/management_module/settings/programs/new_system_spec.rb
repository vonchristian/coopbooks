require 'rails_helper'

describe 'New program' do
  before(:each) do
    cooperative = create(:cooperative)
    manager     = create(:general_manager, cooperative: cooperative)
    login_as(manager, scope: :user)
    visit management_module_settings_path
    click_link 'program-settings'
    click_link 'New Program'
  end

  it 'with valid attributes' do
    fill_in 'Name', with: 'Membership Fee'
    fill_in 'Description', with: 'Test description'
    choose 'Annually'
    fill_in 'Amount', with: 100

    click_button 'Save Program'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Program'

    expect(page).to have_content("can't be blank")
  end
end
