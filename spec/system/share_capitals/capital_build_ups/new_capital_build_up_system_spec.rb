require 'rails_helper'

describe 'New share capital capital build up' do
  before(:each) do
    teller           = create(:teller)
    cash_account     = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    share_capital    = create(:share_capital, cooperative: teller.cooperative, office: teller.office)
    login_as(teller, scope: :user)

    visit share_capital_url(share_capital)
    click_link "Add Capital"
  end

  it 'with valid attributes' do
    fill_in 'Date',              with: Date.current
    fill_in "Amount",            with: 100_000
    fill_in 'Description',       with: "Capital Build Up"
    fill_in  "Reference Number", with: '909045'
    click_button 'Proceed'
    expect(page).to have_content('saved successfully')

    click_on "Confirm Transaction"
    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button "Proceed"

    expect(page).to have_content("can't be blank")
  end
end
