require 'rails_helper'

describe 'Share capital adjusting entries' do
  before(:each) do
    accountant    = create(:accountant)
    share_capital = create(:share_capital, office: accountant.office, cooperative: accountant.cooperative)
    loan = create(:loan, borrower: share_capital.subscriber, office: accountant.office)
    login_as(accountant, scope: :user)
    visit share_capital_path(share_capital)
    click_link "#{share_capital.id}-settings"
    
    click_link "Adjusting Entries"
    click_link 'Add Payment', match: :first
  end
  it 'valid' do
    fill_in 'Principal amount', with: 1_000
    fill_in 'Interest amount', with: 100
    fill_in 'Penalty amount', with: 100

    click_button 'Add Payment'
    expect(page).to have_content('added successfully')
  end
end
