require 'rails_helper'

describe 'Share capital adjusting entries' do
  it 'valid' do
    accountant    = create(:accountant)
    share_capital = create(:share_capital, office: accountant.office, cooperative: accountant.cooperative)
    login_as(accountant, scope: :user)
    visit share_capital_path(share_capital)

    click_link "#{share_capital.id}-settings"
    click_link "Adjusting Entries"
  end
end
