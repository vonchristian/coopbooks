require 'rails_helper'

describe 'Share capital program subscription payment' do
  before(:each) do
    accountant           = create(:accountant)
    office               = accountant.office
    share_capital        = create(:share_capital, office: office, cooperative: accountant.cooperative)
    program              = create(:program, amount: 100)
    office_program       = create(:office_program, program: program, office: office)
    program_subscription = create(:program_subscription, subscriber: share_capital.subscriber, program: program, office: office)
    login_as(accountant, scope: :user)
    visit share_capital_path(share_capital)
    click_link "#{share_capital.id}-settings"
    click_link "Adjusting Entries"
    click_link 'Add Payment', match: :first
  end

  it 'valid' do
    fill_in 'Amount', with: 100

    click_button 'Add Payment'

    expect(page).to have_content('added successfully')
  end
end
