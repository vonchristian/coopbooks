require 'rails_helper'

describe 'New withdraw' do
  before(:each) do
    teller         = create(:teller)
    office         = teller.office
    cooperative    = teller.cooperative
    cash_account   = create(:employee_cash_account, office: office, cooperative: cooperative, employee: teller)
    cash_account_2 = create(:asset, name: 'Cash on Hand 2', office: office)
    bank_account   = create(:bank_account, office: office)
    deposit        = build(:entry, office: office, cooperative: cooperative, recorder: teller)
    deposit.debit_amounts.build(amount: 100_000, account: bank_account.cash_account)
    deposit.credit_amounts.build(amount: 100_000, account: cash_account_2)
    deposit.save!
    teller.employee_cash_accounts.create(cash_account: cash_account_2, office: cash_account.office, cooperative: cash_account.cooperative)
    login_as(teller, scope: :user)

    visit bank_account_path(bank_account)
    click_link 'Withdraw'
  end

  it 'with valid attributes' do
    fill_in 'Date', with: Date.current
    fill_in 'Amount', with: 10_000
    fill_in 'Reference number', with: '324423'
    fill_in 'Description', with: '432434'
    select 'Cash on Hand 2'
    click_button 'Proceed'


    click_link 'Confirm Transaction'

  end
end
