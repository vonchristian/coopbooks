require 'rails_helper'

describe 'New Deposit' do
  before(:each) do
    teller = create(:teller)
    office = teller.office
    cooperative = teller.cooperative
    cash_account = create(:employee_cash_account, employee: teller, office: office, cooperative: cooperative)
    cash_account_2 = create(:asset, name: 'Cash on Hand 2', office: office)
    bank_account = create(:bank_account, office: office)
    teller.employee_cash_accounts.create(cash_account: cash_account_2, office: office, cooperative: cooperative)
    login_as(teller, scope: :user)

    visit bank_account_path(bank_account)
    click_link 'Deposit'
  end

  it 'with valid attributes' do
    fill_in 'Date', with: Date.current
    fill_in 'Amount', with: 100_000
    fill_in 'Reference number', with: '324423'
    fill_in 'Description', with: '432434'
    select 'Cash on Hand 2'
    click_button 'Proceed'


    click_link 'Confirm Transaction'

  end
end
