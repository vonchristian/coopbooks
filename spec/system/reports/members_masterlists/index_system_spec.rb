require 'rails_helper'

describe 'Member masterlist index' do
  before(:each) do
    manager = create(:general_manager)
    login_as(manager, scope: :user)
    visit reports_members_masterlists_path
  end
  it 'valid' do
    expect(page).to have_content("Members Masterlist")
  end
end
