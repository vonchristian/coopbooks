require 'rails_helper'

describe 'Members masterlist report by gender' do
  before(:each) do
    cooperative         = create(:cooperative)
    office              = create(:office, cooperative: cooperative)
    male                = create(:member, sex: 'male')
    female              = create(:member, sex: 'female')
    membership_1        = create(:regular_membership, cooperative: cooperative, cooperator: male)
    membership_2        = create(:regular_membership, cooperative: cooperative, cooperator: female)
    office_membership_1 = create(:office_membership, office: office, membership: membership_1)
    office_membership_2 = create(:office_membership, office: office, membership: membership_2)
    user                = create(:user, role: 'teller', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit reports_members_masterlists_path
    click_link 'By Gender'
  end

  it 'when logged in' do
    expect(page).to have_content 'Male'
    expect(page).to have_content 'Female'
  end
end
