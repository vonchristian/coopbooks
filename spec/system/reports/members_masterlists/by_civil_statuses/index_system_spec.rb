require 'rails_helper'

describe 'Members masterlist report by civil_status' do
  before(:each) do
    cooperative         = create(:cooperative)
    office              = create(:office, cooperative: cooperative)
    member_1           = create(:member, civil_status: 'single')
    member_2            = create(:member, civil_status: 'married')
    membership_1        = create(:regular_membership, cooperative: cooperative, cooperator: member_1)
    membership_2        = create(:regular_membership, cooperative: cooperative, cooperator: member_2)
    office_membership_1 = create(:office_membership, office: office, membership: membership_1)
    office_membership_2 = create(:office_membership, office: office, membership: membership_2)
    user                = create(:user, role: 'teller', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit reports_members_masterlists_path
    click_link 'By Civil Status'
  end

  it 'when logged in' do
    expect(page).to have_content 'Single'
    expect(page).to have_content 'Married'
    expect(page).to have_content 'Widow'
    expect(page).to have_content 'Divorced'


  end
end
