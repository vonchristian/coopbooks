require 'rails_helper'

describe 'Program unpaid subscriptions index' do
  before(:each) do
    cooperative  = create(:cooperative)
    office       = create(:office, cooperative: cooperative)
    teller       = create(:teller, office: office, cooperative: cooperative)
    program      = create(:program, name: 'Mutual Aid System', cooperative: cooperative, amount: 500, mode_of_payment: 'annually')
    program_2    = create(:program, name: 'Membership Fees', cooperative: cooperative, mode_of_payment: 'one_time_payment')
    mas          = create(:office_program, office: office, program: program)
    mem          = create(:office_program, office: office, program: program_2)
    member       = create(:member, last_name: 'Cruz', first_name: 'Juan', middle_name: 'De La')
    member_2     = create(:member, last_name: 'Santos', first_name: 'Juan', middle_name: 'De La')
    membership   = create(:regular_membership, cooperative: cooperative, cooperator: member)
    membership_2 = create(:regular_membership, cooperative: cooperative, cooperator: member_2)
    create(:office_membership, office: office, membership: membership)
    create(:office_membership, office: office, membership: membership_2)

    subscription = build(:program_subscription, subscriber: member, program: program, office: office)
    AccountCreators::ProgramSubscription.new(subscription: subscription).create_accounts!
    subscription.save!

    subscription_2 = build(:program_subscription, subscriber: member_2, program: program, office: office)
    AccountCreators::ProgramSubscription.new(subscription: subscription_2).create_accounts!
    subscription_2.save!

    asset        = create(:asset, office: office)
    create(:employee_cash_account, employee: teller, office: office, cooperative: cooperative, cash_account: asset)

    #subscription payment
    entry = build(:entry, recorder: teller, office: office, entry_date: Date.current, cooperative: cooperative)
    entry.debit_amounts.build(account: asset, amount: 500)
    entry.credit_amounts.build(account: subscription.account, amount: 500)
    entry.save!

    login_as(teller, scope: :user)
    visit programs_path
    click_link 'Mutual Aid System'
    click_link "#{program.id}-reports"
    click_link 'Unpaid Subscriptions'
  end

  it 'valid' do
    expect(page).to have_content('Santos, Juan De La')
    expect(page).to_not have_content('Cruz, Juan De La')

  end
end
