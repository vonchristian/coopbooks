require 'rails_helper'

describe 'Program report index' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    teller      = create(:teller, office: office, cooperative: cooperative)
    program     = create(:program, name: 'Mutual Aid System', cooperative: cooperative)
    program_2   = create(:program, name: 'Membership Fees', cooperative: cooperative)
    mas         = create(:office_program, office: office, program: program)
    mem         = create(:office_program, office: office, program: program_2)
    login_as(teller, scope: :user)
    visit programs_path
    click_link 'Mutual Aid System'
    click_link "#{program.id}-reports"
  end

  it 'valid' do
    expect(page).to have_content('Mutual Aid System Reports')
  end
end
