require 'rails_helper'
include ChosenSelect
describe 'Account category edit page' do
  before(:each) do
    cooperative      = create(:cooperative)
    office           = create(:office, cooperative: cooperative)
    parent_sub       = create(:asset_parent_account_sub_category, office: office, title: 'Cash on Hand')
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    accountant       = create(:accountant, cooperative: cooperative, office: office)

    login_as(accountant, scope: :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
    click_link "#{account_category.id}-settings"
    click_link 'Edit'
  end

  it 'with valid attributes', js: true do
    fill_in 'Title', with: 'Cash Test'
    fill_in 'Code', with: '123'
    select_from_chosen 'Cash on Hand', from: 'Parent account sub category'

    click_button 'Update Account Category'

    expect(page).to have_content('updated successfully')
  end
end
