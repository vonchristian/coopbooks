require 'rails_helper'

describe 'New account category' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    parent      = create(:asset_parent_account_category, title: 'Cash and Cash Equivalents', office: office)
    parent_sub  = create(:asset_parent_account_sub_category, title: 'Cash on Hand', office: office, parent_account_category: parent)
    account_cat = create(:asset_level_one_account_category, parent_account_sub_category: parent_sub,  office: office, title: 'Test Title', code: '101')
    manager     = create(:branch_manager, cooperative: cooperative, office: office)
    login_as(manager, scope: :user)
    visit office_parent_account_categories_path(office)
    click_link 'Cash and Cash Equivalents'
    click_link 'Sub Categories'
    click_link 'Cash on Hand'
    click_link 'New Account Category'
  end

  it 'with valid attributes', js: true do
    fill_in 'Title', with: 'Cash on Hand Lamut'
    fill_in 'Code',  with: '1011'
    choose 'Asset'

    click_button 'Save Account Category'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes', js: true do
    click_button 'Save Account Category'

    expect(page).to have_content("can't be blank")
  end

  it 'validates unique code' do
    fill_in 'Title', with: 'Cash on Hand Lamut'
    fill_in 'Code', with: '101'
    choose 'Asset'

    click_button 'Save Account Category'

    expect(page).to have_content('has already been taken')
  end

  it 'validates unique title' do
    fill_in 'Title', with: 'Test Title'
    fill_in 'Code', with: '101'
    choose 'Asset'

    click_button 'Save Account Category'

    expect(page).to have_content('has already been taken')
  end
end
