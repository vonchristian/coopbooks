require 'rails_helper'
include ChosenSelect

describe 'New parent account category' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    grand_asset = create(:asset_grand_parent_account_category, title: 'Current Assets', cooperative: cooperative)
    manager     = create(:branch_manager, cooperative: cooperative, office: office)
    login_as(manager, scope: :user)
    visit office_parent_account_categories_path(office)
    click_link 'New Parent Account Category'
  end

  it 'with valid attributes',js: true do
    select_from_chosen 'Current Assets', from: 'Grand parent account category'
    fill_in 'Title', with: 'Cash and Cash Equivalents'
    fill_in 'Code', with: '1011'
    choose 'Asset'

    click_button 'Save Parent Account Category'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes', js: true do
    click_button 'Save Parent Account Category'

    expect(page).to have_content("can't be blank")
  end
end
