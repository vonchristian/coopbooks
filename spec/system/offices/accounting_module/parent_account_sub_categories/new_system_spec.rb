require 'rails_helper'

describe 'New parent account sub category' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    parent_asset = create(:asset_parent_account_category, title: 'Cash and Cash Equivalents', office: office)
    manager     = create(:branch_manager, cooperative: cooperative, office: office)
    login_as(manager, scope: :user)
    visit office_parent_account_categories_path(office)
    click_link 'Cash and Cash Equivalents'
    click_link 'Sub Categories'
    click_link 'New Parent Account Sub Category'
  end

  it 'with valid attributes' do
    fill_in 'Title', with: 'Cash and Cash Equivalents'
    fill_in 'Code',  with: '1011'
    choose 'Asset'

    click_button 'Save Parent Account Sub Category'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Parent Account Sub Category'

    expect(page).to have_content("can't be blank")
  end
end
