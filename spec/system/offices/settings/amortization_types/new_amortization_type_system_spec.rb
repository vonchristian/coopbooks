require 'rails_helper'

describe 'New amortization type' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    user        = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit office_path(office)
    click_link "#{office.id}-settings"
    click_link 'New Amortization Type'
  end

  it 'with valid attributes' do
    fill_in 'Name', with: 'Straight Line Amortization'
    choose 'Straight Line'
    choose 'Equal Principal'

    click_button 'Create Amortization Type'

    expect(page).to have_content('created successfully')
  end

  it 'with invalid attributes' do
    click_button 'Create Amortization Type'

    expect(page).to have_content("can't be blank")
  end
end
