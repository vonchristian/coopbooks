require 'rails_helper'
include ChosenSelect
describe 'New office savings product' do
  before(:each) do
    cooperative = create(:cooperative)
    office                     = create(:office, cooperative: cooperative)
    saving_product             = create(:saving_product, name: 'Regular Savings', cooperative: cooperative)
    liability_account_category = create(:liability_account_category, title: 'Savings Deposits', office: office)
    expense                    = create(:expense_account_category, title: 'Interest expense on Deposits', office: office)
    closing_account_category   = create(:revenue_account_category, title: 'Closing Fees', office: office)
    temporary_account          = create(:liability, name: 'Temporary Savings Accounts', office: office)
    manager                    = create(:general_manager, office: office, cooperative: cooperative)
    login_as(manager, scope: :user)
    visit office_settings_path(office)
    click_link 'New Saving Product'
  end
  it 'valid', js: true do
    select_from_chosen 'Regular Savings', from: 'Saving product'
    select_from_chosen 'Savings Deposits', from: 'Liability account category'
    select_from_chosen 'Interest expense on Deposits', from: 'Interest expense account category'
    select_from_chosen 'Temporary Savings Accounts', from: 'Temporary account'
    select_from_chosen 'Closing Fees', from: 'Closing account category'

    click_button 'Create Saving Product'

  end
end
