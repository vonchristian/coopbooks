require 'rails_helper'
include ChosenSelect
describe 'New office loan product' do
  before(:each) do
    loan_officer = create(:loan_officer)
    cooperative  = loan_officer.cooperative
    office       = loan_officer.office

    loan_product                      = create(:loan_product, name: 'Regular Loan', cooperative: cooperative)
    receivable_account_category       = create(:asset_level_one_account_category, title: 'Loans Receivables - Regular Loan', office: office)
    interest_revenue_account_category = create(:revenue_account_category, title: 'Interest Income from Loans - Regular Loan', office: office)
    penalty_revenue_account_category  = create(:revenue_account_category, title: 'Loan Penalties - Regular Loan', office: office)
    loan_protection_plan_provider     = create(:loan_protection_plan_provider, business_name: 'Loan Protection Provider', office: office)
    temporary_account                 = create(:asset, name: 'Temporary Regular Loan', office: office)
    amortization_type                 = create(:amortization_type, name: 'Straight Line', office: office)
    login_as(loan_officer, scope: :user)
    visit office_loan_products_path(office)

    click_link 'New Loan Product'
  end

  it 'with valid attributes', js: true do
    select_from_chosen 'Regular Loan', from: 'Loan product'
    select_from_chosen 'Loans Receivables - Regular Loan', from: 'Receivable account category'
    select_from_chosen 'Interest Income from Loans - Regular Loan', from: 'Interest revenue account category'
    select_from_chosen 'Loan Penalties - Regular Loan', from: 'Penalty revenue account category'
    select_from_chosen 'Loan Protection Provider', from: 'Loan protection plan provider'
    select_from_chosen 'Temporary Regular Loan', from: 'Temporary account'
    select_from_chosen 'Straight Line', from: 'Amortization type'

    click_button 'Save Loan Product'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes', js: true do
    click_button 'Save Loan Product'

    expect(page).to have_content("can't be blank")
  end
end
