require 'rails_helper'
include ChosenSelect
describe 'New loan product charge' do
  before(:each) do
    user              = create(:branch_manager)
    loan_product      = create(:loan_product, name: 'Regular Loan', cooperative: user.cooperative)
    revenue           = create(:revenue, name: 'Service Fees', office: user.office)
    create(:office_loan_product, office: user.office, loan_product: loan_product)
    login_as(user, scope: :user)
    visit office_settings_path(user.office)
    click_link 'Loan Products'
    click_link 'Regular Loan'
    click_link 'New Charge'
  end

  it 'with valid attributes' do
    fill_in 'Name', with: 'Service Fee'
    choose 'Percent Based'
    fill_in 'Rate', with: 0.03
    select 'Service Fees'
    click_button 'Create Charge'

    expect(page).to have_content('created successfully')
  end

  it 'with invalid attributes' do
    click_button 'Create Charge'

    expect(page).to have_content("can't be blank")
  end
end
