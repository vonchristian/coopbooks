require 'rails_helper'

describe 'Settings index page' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:main_office, name: "Lamut", cooperative: cooperative)
    user        = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    login_as(user, :scope => :user)
    visit office_url(office)
  end
  it 'valid' do
    expect(page).to have_content("Lamut")
    expect(page).to have_content("Settings")

  end
end
