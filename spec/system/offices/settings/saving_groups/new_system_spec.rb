require 'rails_helper'

describe 'New saving group' do
  before(:each) do
    manager = create(:branch_manager)
    login_as(manager, scope: :user)
    visit office_settings_path(manager.office)
    click_link 'Saving Groups'
    click_link 'New Saving Group'
  end

  it 'valid' do
    fill_in 'Title',     with: '1-30 Days'
    fill_in 'Start num', with: 1
    fill_in 'End num',   with: 30

    click_button 'Create Saving Group'

    expect(page).to have_content('saved successfully')
  end

  it 'invalid' do
    click_button 'Create Saving Group'

    expect(page).to have_content("can't be blank")
  end
end
