require 'rails_helper'

describe 'New loan protection plan provider' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, cooperative: cooperative)
    account     = create(:liability, name: 'Accounts Payable - CLIMBS', office: office)
    user        = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit office_path(office)
    click_link "#{office.id}-settings"
    click_link 'New Loan Protection Plan Provider'
  end

  it 'with valid attributes' do
    fill_in 'Business name', with: 'CLIMBS'
    fill_in 'Rate', with: 0.35
    select 'Accounts Payable - CLIMBS'

    click_button 'Create Loan Protection Plan Provider'

    expect(page).to have_content('created successfully')
  end

  it 'with invalid attributes' do
    click_button 'Create Loan Protection Plan Provider'

    expect(page).to have_content("can't be blank")
  end
end
