require 'rails_helper'

include ChosenSelect

describe 'New office share capital product' do
  before(:each) do
    cooperative                       = create(:cooperative)
    office                            = create(:office, cooperative: cooperative)
    share_capital_product             = create(:share_capital_product, name: 'Paid Up Share Capital', cooperative: cooperative)
    equity_account_category           = create(:equity_level_one_account_category, title: 'Paid Up Share Capital', office: office)
    interest_payable_account_category = create(:equity_level_one_account_category, title: 'Interest Payable', office: office)
    temporary_account                 = create(:equity, name: 'Temporary Share Capital Account', office: office)
    manager                           = create(:general_manager, office: office, cooperative: cooperative)
    login_as(manager, scope: :user)
    visit office_settings_path(office)
    click_link 'Share Capital Products'
    click_link 'New Share Capital Product'
  end

  it 'with valid attributes', js: true do
    select_from_chosen 'Paid Up Share Capital', from: 'Share capital product'
    select_from_chosen 'Paid Up Share Capital', from: 'Equity Account Category', match: :first
    select_from_chosen 'Interest Payable', from: 'Interest payable account category'
    select_from_chosen 'Temporary Share Capital', from: 'Temporary account'

    click_button 'Save Share Capital Product'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes', js: true do
    click_button 'Save Share Capital Product'

    expect(page).to have_content("can't be blank")
  end
end
