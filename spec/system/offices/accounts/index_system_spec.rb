require 'rails_helper'

describe 'Accounts index' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:main_office, name: "Lamut", cooperative: cooperative)
    user        = create(:user, role: 'general_manager', office: office, cooperative: cooperative)
    login_as(user, :scope => :user)
    visit office_url(office)
  end

  it 'when signed in' do
    click_link 'Accounting'
  end
end
