require 'rails_helper'

describe 'New member registry' do
  it 'with valid attributes' do
    cooperative         = create(:cooperative)
    office              = create(:office, cooperative: cooperative)
    membership_category = create(:regular_membership_category, title: 'Regular Member', cooperative: cooperative)
    user                = create(:branch_manager, office: office, cooperative: cooperative)
    login_as(user, scope: :user)
    visit office_settings_path(office)

    click_link "#{office.id}-settings"
    click_link 'Migrate Members'
    click_link 'Upload'
    attach_file("Upload File", Rails.root + "spec/support/registries/member_registry.xlsx")
    click_button 'Upload'

    expect(office.memberships.count).to eql 1

  end
end
