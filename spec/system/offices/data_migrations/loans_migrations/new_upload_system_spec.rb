require 'rails_helper'

describe 'New loan registry' do
  it 'with valid attributes' do
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    loan_group        = create(:loan_group, office: office, start_num: 0, end_num: 0)
    user              = create(:branch_manager, office: office, cooperative: cooperative)
    loan_product      = create(:loan_product, name: 'Regular Loan', cooperative: cooperative)
    loan_product_2    = create(:loan_product, name: 'Emergency Loan', cooperative: cooperative)
    organization      = create(:organization, name: 'Womens Organization', office: office)
    member            = create(:member, first_name: 'Test', middle_name: 'Test', last_name: 'Test')
    membership        = create(:regular_membership, cooperative: cooperative, cooperator: member)
    office_membership = create(:office_membership, office: office, membership: membership)
    create(:office_loan_product, loan_product: loan_product, office: office)
    create(:office_loan_product, loan_product: loan_product_2, office: office)
    login_as(user, scope: :user)
    visit office_path(office)
    click_link "#{office.id}-settings"
    click_link 'Migrate Loans'
    click_link 'New Upload'
    attach_file("Upload File", Rails.root + "spec/support/registries/loan_registry.xlsx")
    click_button 'Upload'

    expect(office.loans.count).to eql 2
  end
end
