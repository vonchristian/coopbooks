require 'rails_helper'

describe 'New share capital registry' do
  it 'with valid attributes' do
    cooperative           = create(:cooperative)
    office                = create(:office, cooperative: cooperative)
    share_capital_product = create(:share_capital_product, name: 'Paid Up Share Capital - Common', cooperative: cooperative)
    organization          = create(:organization, name: 'Womens Organization', office: office)
    member                = create(:member, first_name: 'Test', middle_name: 'Test', last_name: 'Test')
    membership            = create(:regular_membership, cooperative: cooperative, cooperator: member)
    office_membership     = create(:office_membership, office: office, membership: membership)
    user                  = create(:branch_manager, office: office, cooperative: cooperative)
    create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)
    login_as(user, scope: :user)
    visit office_settings_path(office)

    click_link "#{office.id}-settings"
    click_link 'Migrate Share Capitals'
    click_link 'Upload'
    attach_file("Upload File", Rails.root + "spec/support/registries/share_capital_registry.xlsx")
    click_button 'Upload'

    expect(office.share_capitals.count).to eq 2
    share_capital = office.share_capitals.find_by!(account_name: 'Womens Organization')

    expect(share_capital.balance).to eql 5_000

    share_capital = office.share_capitals.find_by!(account_name: 'Test, Test Test')

    expect(share_capital.balance).to eql 4_500
  end
end
