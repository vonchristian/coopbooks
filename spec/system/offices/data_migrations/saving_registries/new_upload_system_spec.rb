require 'rails_helper'

describe 'New saving registry' do
  it 'with valid attributes' do
    cooperative       = create(:cooperative)
    office            = create(:office, cooperative: cooperative)
    saving_group      = create(:saving_group, office: office, start_num: 0, end_num: 30)
    saving_product    = create(:saving_product, name: 'Regular Savings', cooperative: cooperative)
    organization      = create(:organization, name: 'Womens Organization', office: office)
    member            = create(:member, first_name: 'Test', middle_name: 'Test', last_name: 'Test')
    membership        = create(:regular_membership, cooperative: cooperative, cooperator: member)
    office_membership = create(:office_membership, office: office, membership: membership)
    user              = create(:branch_manager, office: office, cooperative: cooperative)
    create(:office_saving_product, office: office, saving_product: saving_product)
    login_as(user, scope: :user)
    visit office_settings_path(office)

    click_link "#{office.id}-settings"
    click_link 'Migrate Savings'
    click_link 'Upload'
    attach_file("Upload File", Rails.root + "spec/support/registries/saving_registry.xlsx")
    click_button 'Upload'

    expect(office.savings.count).to eq 2
    saving = office.savings.find_by!(account_name: 'Womens Organization')

    expect(saving.balance).to eql 5_000

    saving_2 = office.savings.find_by(account_name: 'Test, Test Test')

    expect(saving_2.balance).to eql 4_500
  end
end
