require 'rails_helper'

feature 'Index of asset inventories' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:office, name: "Lamut", cooperative: cooperative)
    user        = create(:bookkeeper, office: office, cooperative: cooperative)
    login_as(user, :scope => :user)
    visit office_path(office)
    click_link 'Asset Inventories'
  end

  scenario 'valid' do
  end
end
