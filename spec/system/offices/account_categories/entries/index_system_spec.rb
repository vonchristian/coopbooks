require 'rails_helper'

describe 'Account category entries index page' do
  before(:each) do
    cooperative      = create(:cooperative)
    office           = create(:office, cooperative: cooperative)
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    asset            = create(:asset, name: 'Cash in Bank', account_category: account_category, office: office)
    asset_2          = create(:asset, name: 'Cash in Bank 2', account_category: account_category, office: office)
    accountant       = create(:accountant, cooperative: cooperative, office: office)
    entry            = build(:entry, cooperative: cooperative, description: 'Cash deposit', recorder: accountant, office: office, cooperative: cooperative)
    entry.debit_amounts.build(amount: 10_000, account: asset)
    entry.credit_amounts.build(amount: 10_000, account: asset_2)
    entry.save!

    login_as(accountant, scope: :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
    click_link "#{account_category.id}-entries"
  end
  it 'valid' do
    expect(page).to have_content('Cash deposit')
  end
end
