require 'rails_helper'

describe 'Account category settings index page' do
  before(:each) do
    cooperative      = create(:cooperative)
    office           = create(:office, cooperative: cooperative)
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    accountant       = create(:accountant, cooperative: cooperative, office: office)
    login_as(accountant, scope: :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
    click_link "#{account_category.id}-settings"
  end
  it 'valid' do
    expect(page).to have_content('Cash Settings')
  end
end
