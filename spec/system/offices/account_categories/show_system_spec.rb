require 'rails_helper'

describe 'Account category show  page' do
  before(:each) do
    cooperative      = create(:cooperative)
    office           = create(:office, cooperative: cooperative)
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    account_category_2 = create(:asset_level_one_account_category, title: 'Cash on Hand 123')

    accountant       = create(:accountant, cooperative: cooperative, office: office)

    login_as(accountant, scope: :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
  end

  it 'with valid attributes' do
    expect(page).to have_content('Cash')
    expect(page).to_not have_content('Cash on Hand 123')
    save_and_open_page
  end
end
