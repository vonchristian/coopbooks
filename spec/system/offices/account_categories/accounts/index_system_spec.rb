require 'rails_helper'

describe 'Account category accounts index page' do
  before(:each) do
    cooperative      = create(:cooperative)
    office           = create(:office, cooperative: cooperative)
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    asset            = create(:asset, name: 'Cash in Bank', account_category: account_category, office: office)
    asset_2          = create(:asset, name: 'Cash in Bank 2', account_category: account_category, office: office)
    test_asset       = create(:asset, name: 'Test asset')
    accountant       = create(:accountant, cooperative: cooperative, office: office)
    login_as(accountant, scope: :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
    click_link "#{account_category.id}-accounts"
  end
  it 'valid' do
    expect(page).to have_content('Cash in Bank')
    expect(page).to have_content('Cash in Bank 2')
    expect(page).to_not have_content('Test asset')
  end
end
