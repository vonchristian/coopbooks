require 'rails_helper'

describe 'New account' do
  before(:each) do
    cooperative = create(:cooperative)
    office      = create(:main_office, name: "Lamut", cooperative: cooperative)
    user        = create(:user, role: 'bookkeeper', office: office, cooperative: cooperative)
    account_category = create(:asset_level_one_account_category, title: 'Cash', office: office)
    login_as(user, :scope => :user)
    visit office_account_categories_path(office)
    click_link 'Cash'
    click_link 'Accounts'
    click_link 'New Account'
  end

  it 'with valid attributes' do
    fill_in "Code", with: "42342423"
    fill_in "Name", with: "Cash in Bank (PNB-442342-43242)"
    choose "Asset"
    check 'Contra'


    click_button "Create Account"

    expect(page).to have_content("saved successfully")
  end

  it 'with invalid attributes' do
    click_button "Create Account"

    expect(page).to have_content("can't be blank")
  end
end
