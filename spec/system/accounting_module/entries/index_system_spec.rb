require 'rails_helper'

describe 'Entries index' do
  it ' valid' do
    bookkeeper = create(:bookkeeper)
    login_as(bookkeeper, scope: :user)
    entry = create(:entry_with_credit_and_debit, description: 'test entry', recorder: bookkeeper, office: bookkeeper.office, cooperative: bookkeeper.cooperative)
    visit accounting_module_entries_path

    expect(page).to have_content('Entries')
    expect(page).to have_content('test entry')
  end
end

