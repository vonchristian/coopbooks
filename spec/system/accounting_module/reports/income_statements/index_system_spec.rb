require 'rails_helper'

describe 'Income statement index' do
  before(:each) do
    accountant = create(:accountant)
    login_as(accountant, scope: :user)
    visit accounting_module_reports_path
    click_link 'Income Statement Report'
  end

  it 'valid' do
    expect(page).to have_content('Income Statement')
  end
end
