require 'rails_helper'

describe 'New grand parent account category' do
  before(:each) do
    accountant = create(:accountant)
    login_as(accountant, scope: :user)
    visit accounting_module_grand_parent_account_categories_path
    click_link 'New Category'
  end

  it 'valid' do
    fill_in 'Title', with: 'Current Assets'
    fill_in 'Code', with: '101'
    choose 'Asset'
    check 'Contra'
    click_button 'Save Category'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Category'

    expect(page).to have_content "can't be blank"
  end 
end
