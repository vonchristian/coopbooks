require 'rails_helper'

describe 'Enable loan autopay' do
  before(:each) do
    cooperative  = create(:cooperative)
    office       = create(:office, cooperative: cooperative)
    borrower     = create(:member)
    loan         = create(:loan, borrower: borrower, office: office)
    saving       = create(:saving, depositor: borrower, office: office)
    loan_officer = create(:loan_officer, cooperative: cooperative, office: office)
    login_as(loan_officer, scope: :user)
    visit loan_path(loan)
    click_link "#{loan.id}-settings"
    click_link 'Enable Auto Pay'
  end

  it 'with valid attributes' do
    click_button 'Select'

    expect(page).to have_content('enabled successfully')
  end
end
