require 'rails_helper'

describe 'Loans settings page' do
  it 'valid' do
    user            = create(:loan_officer)
    loan_product    = create(:loan_product, cooperative: user.cooperative)
    create(:office_loan_product, loan_product: loan_product, office: user.office)
    interest_config = create(:interest_config, loan_product: loan_product)
    loan            = create(:loan, loan_product: loan_product, office: user.office, cooperative: user.cooperative)
    term            = create(:term)
    loan.terms << term
    login_as(user, scope: :user)
    visit loan_path(loan)

    click_link "#{loan.id}-settings"

    expect(page).to have_content('Loan Settings')
  end
end
