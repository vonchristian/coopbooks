require 'rails_helper'

describe 'New loan interest' do
  before(:each) do
    loan_officer = create(:loan_officer)
    loan         = create(:loan, office: loan_officer.office)
    login_as(loan_officer, scope: :user)
    visit loans_path
    click_link loan.borrower_name
    click_link "#{loan.id}-interests"
    click_link 'New Interest'
  end

  it 'with valid attributes' do
    fill_in 'Date',        with: Date.current
    fill_in 'Amount',      with: 500
    fill_in 'Description', with: 'loan interest'

    click_button 'Save Interest'

    expect(page).to have_content('posted successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Interest'

    expect(page).to have_content("can't be blank")
  end
end
