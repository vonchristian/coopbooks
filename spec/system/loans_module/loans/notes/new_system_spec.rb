require 'rails_helper'

describe 'New loan note' do
  before(:each) do
    cooperative = create(:cooperative)
    office = create(:office, cooperative: cooperative)
    loan = create(:loan, office: office)
    loan_officer = create(:loan_officer, office: office, cooperative: cooperative)
    login_as(loan_officer, scope: :user)
    visit loan_path(loan)
    click_link 'Notes'
    click_link 'Add Note'
  end

  it 'with valid attributes' do
    fill_in 'Title', with: 'Update'
    fill_in 'Content', with: 'Test description'
    fill_in 'Date', with: Date.current.strftime('%B %e, %Y')

    click_button 'Save Note'

    expect(page).to have_content('saved successfully')
  end
end
