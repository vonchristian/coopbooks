require 'rails_helper'

describe 'New loan payment' do
  before(:each) do
    teller = create(:teller)
    loan_group            = create(:loan_group, office: teller.office, start_num: 0, end_num: 0)
    cash_account          = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    loan_product          = create(:loan_product, cooperative: teller.cooperative)
    interest_config       = create(:interest_config, rate: 0.12, calculation_type: 'prededucted', loan_product: loan_product)
    member                = create(:member)
    receivable_account    = create(:asset, office: teller.office)
    loan_application      = create(:loan_application, term: 12, loan_product: loan_product, borrower: member, receivable_account: receivable_account, office: teller.office, cooperative: cash_account.cooperative, mode_of_payment: 'lumpsum', application_date: Date.current, account_number: SecureRandom.uuid)
    voucher               = create(:voucher, description: 'loan disbursement', office: teller.office, cooperative: teller.cooperative, preparer: loan_application.preparer, payee: member)
    debit_voucher_amount  = create(:voucher_amount, recorder: teller, amount_type: 'debit', amount: 10_000, account: loan_application.receivable_account, voucher: voucher, cooperative: teller.cooperative)
    credit_voucher_amount = create(:voucher_amount, recorder: teller, amount_type: 'credit', amount: 10_000, account: cash_account.cash_account, voucher: voucher, cooperative: teller.cooperative)
    loan_application.update(voucher: voucher)
    create(:office_loan_product, office: teller.office, loan_product: loan_product)
    login_as(teller, scope: :user)
    visit loans_module_loan_application_url(loan_application)
    click_link 'Disburse Voucher'
    fill_in 'Disbursement date', with: '30/3/2019'
    click_button 'Disburse Loan Application'

    visit loan_path(loan_application.loan)
    click_link 'New Payment'
  end

  it 'with valid attributes' do

    fill_in 'Date', with: Date.current
    fill_in 'Description', with: 'Loan payment'
    fill_in 'Principal amount', with: 500
    fill_in 'Interest amount', with: 50
    fill_in 'Penalty amount', with: 100
    fill_in 'Reference number', with: '42342'
    click_button 'Proceed'
    expect(page).to have_content('created successfully')

    click_link 'Confirm'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Proceed'


    expect(page).to have_content("can't be blank")
  end

end
