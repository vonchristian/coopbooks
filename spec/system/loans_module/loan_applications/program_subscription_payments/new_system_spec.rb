require 'rails_helper'

describe 'New loan application previous loan payment' do
  it 'valid' do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    cooperative           = loan_officer.cooperative
    loan_product          = create(:loan_product, cooperative: cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    loan_application      = create(:loan_application, loan_amount: 100_000, office: office, preparer: loan_officer)
    share_capital         = create(:share_capital, subscriber: loan_application.borrower, office: office)
    previous_loan         = create(:loan_with_balance, borrower: loan_application.borrower, office: office)
    program               = create(:program, name: 'MAS', cooperative: cooperative, amount: 500)
    office_program        = create(:office_program, office: office, program: program)
    program_subscription = create(:program_subscription, program: program, subscriber: loan_application.borrower)
    create(:office_loan_product, loan_product: loan_product, office: office)

    login_as(loan_officer, scope: :user)

    visit new_loans_module_loan_application_voucher_path(loan_application)

    click_link "add-payment-#{program_subscription.id}"
    fill_in 'Amount', with: 500
    click_button 'Add Payment'

    expect(loan_application.cart.voucher_amounts.pluck(:account_id)).to include(program_subscription.account_id)

  end
end
