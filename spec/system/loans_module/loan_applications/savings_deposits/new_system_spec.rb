require 'rails_helper'

describe 'loan application saving' do
  before(:each)do
    cooperative           = create(:cooperative)
    office                = create(:office, cooperative: cooperative)
    loan_product          = create(:loan_product, cooperative: cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    loan_officer          = create(:loan_officer, office: office, cooperative: cooperative)
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    saving                = create(:saving, depositor: loan_application.borrower, office: office)
    create(:office_loan_product, loan_product: loan_product, office: office)

    login_as(loan_officer, scope: :user)

    visit new_loans_module_loan_application_voucher_path(loan_application)

    click_link 'Add Saving'
  end

  it 'valid' do
    fill_in 'Amount', with: 100

    click_button 'Add Saving'
    expect(page).to have_content('added successfully')
  end

  it 'with invalid attributes' do
    click_button 'Add Saving'

    expect(page).to have_content("can't be blank")
  end
end
