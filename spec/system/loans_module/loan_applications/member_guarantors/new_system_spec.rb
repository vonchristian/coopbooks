require 'rails_helper'

describe 'new loan application member guarantor' do
  it 'valid' do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    loan_product          = create(:loan_product, cooperative: loan_officer.cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    member                = create(:member)
    membership            = create(:regular_membership, cooperative: loan_officer.cooperative, cooperator: member)
    office_membership = create(:office_membership, office: office, membership: membership)

    create(:office_loan_product, loan_product: loan_product, office: office)

    login_as(loan_officer, scope: :user)

    visit new_loans_module_loan_application_voucher_path(loan_application)
    click_link "#{loan_application.id}-new-member-guarantor"
    click_button 'Select'
    expect(loan_application.guaranteed_loan_applications.pluck(:guarantor_id)).to include(member.id)
  end
end
