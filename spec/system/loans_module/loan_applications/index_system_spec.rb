require 'rails_helper'

describe 'Loan applications index' do
  it 'valid' do
   loan_officer = create(:loan_officer)
   office = loan_officer.office
   cooperative = loan_officer.cooperative
   loan_application = create(:loan_application, preparer: loan_officer, office: office, cooperative: cooperative)

    login_as(loan_officer, scope: :user)

    visit loans_module_loan_applications_path

    expect(page).to have_content('Loan Applications')
    expect(page).to have_content(loan_application.borrower_name)
  end
end
