require 'rails_helper'

describe 'New loan application previous loan payment' do
  it 'valid' do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    loan_product          = create(:loan_product, cooperative: loan_officer.cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    loan_officer          = create(:loan_officer, office: office)
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    share_capital         = create(:share_capital, subscriber: loan_application.borrower, office: office)
    previous_loan         = create(:loan_with_balance, borrower: loan_application.borrower, office: office)

    login_as(loan_officer, scope: :user)

    visit new_loans_module_loan_application_voucher_path(loan_application)
    click_link 'Add Payment'
    fill_in 'Principal', with: 10_000
    fill_in 'Interest',  with: 3_000
    fill_in 'Penalty',   with: 1_000
    click_button 'Add Payment'

    expect(loan_application.cart.voucher_amounts.pluck(:account_id)).to include(previous_loan.receivable_account_id)
    expect(loan_application.cart.voucher_amounts.pluck(:account_id)).to include(previous_loan.interest_revenue_account_id)
    expect(loan_application.cart.voucher_amounts.pluck(:account_id)).to include(previous_loan.penalty_revenue_account_id)


  end
end
