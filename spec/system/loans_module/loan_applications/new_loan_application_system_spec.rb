require 'rails_helper'

feature 'Create Loan application' do
  before(:each) do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    cooperative           = loan_officer.cooperative
    service_fee_account   = create(:revenue, office: office)
    form_fee_account      = create(:revenue, office: office)
    share_capital_product = create(:share_capital_product, cooperative: cooperative)
    borrower              = create(:member, first_name: "Juan", last_name: "Cruz", middle_name: "Dela")
    share_capital         = create(:share_capital, share_capital_product: share_capital_product, subscriber: borrower)
    amortization_type     = create(:amortization_type, calculation_type: 'straight_line', office: office, repayment_calculation_type: 'equal_principal')
    loan_product          = create(:loan_product, name: "Salary Loan", cooperative: cooperative, amortization_type: amortization_type)
    office_loan_product   = create(:office_loan_product, loan_product: loan_product, office: office)
    service_fee           = create(:loan_product_charge, rate: 0.03, name: "Service Fee", account: service_fee_account, office_loan_product: office_loan_product, charge_type: 'percent_based')
    form_fee              = create(:loan_product_charge, amount: 150, name: "Form Fee", account: form_fee_account, office_loan_product: office_loan_product, charge_type: 'amount_based')
    cash_account          = create(:asset, name: 'Cash on Hand', office: office)
    teller                = create(:user, role: 'teller', cooperative: cooperative, office: office)
    office.employee_cash_accounts.create!(cooperative: cooperative, cash_account: cash_account, employee: teller)
    office_share_capital_product = create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted', rate: 0.12)
    interest_prededuction = create(:interest_prededuction, loan_product: loan_product, calculation_type: 'percent_based', amount: 0, number_of_payments: 0, rate: 0.75)
    membership            = create(:regular_membership, cooperator: borrower, cooperative: cooperative, account_number: SecureRandom.uuid)
    office_membership     = create(:office_membership, office: office, membership: membership)
    login_as(loan_officer, :scope => :user)
    visit member_loans_url(borrower)
    click_link "New Loan"
    click_link 'Select', match: :first
    click_link 'Select', match: :first
  end

  scenario 'with valid attributes' do

    fill_in "Loan amount",                with: 100_000
    fill_in 'Annual Interest Rate (APR)', with: 0.03
    fill_in 'Prededucted Interest Rate',  with: 1.0
    fill_in "Loan Term (in months)",      with: 24
    select 'Monthly'
    fill_in "Purpose",                    with: 'loan'

    click_button "Proceed"

    expect(page).to have_content 'saved successfully.'
    select 'Cash on Hand'
    fill_in 'Reference number', with: '43424324'
    click_button 'Create Voucher'

    expect(page).to have_content('saved successfully')
  end

  scenario 'with invalid attributes' do
    click_button "Proceed"


    expect(page).to have_content("can't be blank")
  end
end
