require 'rails_helper'

describe 'Loan application disbursement' do
  before(:each) do
    teller                = create(:teller)
    office                = teller.office
    cooperative           = teller.cooperative
    loan_group            = create(:loan_group, office: office, start_num: 0, end_num: 0)
    loan_product          = create(:loan_product, cooperative: cooperative)
    interest_config       = create(:interest_config, rate: 0.12, calculation_type: 'prededucted', loan_product: loan_product)
    member                = create(:member)
    cash_account          = create(:asset, office: office)
    receivable_account    = create(:asset, office: office)
    employee_cash_account = create(:employee_cash_account, office: office, cash_account: cash_account, cooperative: cooperative, employee: teller)
    create(:office_loan_product, loan_product: loan_product, office: office)
    cart                  = create(:cart)
    loan_application      = create(:loan_application, cart: cart, term: 12, loan_product: loan_product, borrower: member, receivable_account: receivable_account, office: office, cooperative: cooperative, mode_of_payment: 'lumpsum', application_date: Date.current, account_number: SecureRandom.uuid)

    voucher               = create(:voucher, description: 'loan disbursement', office: office, cooperative: cooperative, preparer: loan_application.preparer, payee: member)
    debit_voucher_amount  = create(:voucher_amount, recorder: teller, amount_type: 'debit', amount: 10_000, account: receivable_account, voucher: voucher, cooperative: cooperative)
    credit_voucher_amount = create(:voucher_amount, recorder: teller, amount_type: 'credit', amount: 10_000, account: cash_account, voucher: voucher, cooperative: cooperative)
    loan_application.update(voucher: voucher)
    login_as(teller, scope: :user)
    visit loans_module_loan_application_voucher_url(loan_application_id: loan_application.id, id: voucher.id)
    click_link 'Disburse Loan'
  end

  it 'with valid attributes' do
    fill_in 'Disbursement date', with: Date.current

    click_button 'Disburse Loan'

    expect(page).to have_content('disbursed successfully')

  end
end
