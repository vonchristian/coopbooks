require 'rails_helper'

describe 'loan application savings account opening' do
  before(:each) do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    cooperative           = loan_officer.cooperative
    loan_product          = create(:loan_product, cooperative: cooperative)
    create(:office_loan_product, loan_product: loan_product, office: office)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    saving_product        = create(:saving_product, name: "Regular Savings", cooperative: cooperative)
    create(:office_saving_product, saving_product: saving_product, office: office)
  
    login_as(loan_officer, scope: :user)
    visit new_loans_module_loan_application_voucher_path(loan_application)
    click_link    "Open Savings Account"
  end

  it 'valid' do
    select 'Regular Savings'
    fill_in      'Amount', with: 100

    click_button 'Open Account'

    expect(page).to have_content('added successfully')
  end

  it 'invalid attributes' do
    click_button 'Open Account'

    expect(page).to have_content("can't be blank")
  end
end
