require 'rails_helper'

describe 'loan application share capital opening' do
  before(:each) do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    cooperative           = loan_officer.cooperative
    loan_product          = create(:loan_product, cooperative: cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    share_capital_product = create(:share_capital_product, name: "Paid up Share Capital", cooperative: cooperative)
    office_share_capital_product = create(:office_share_capital_product, office: office, share_capital_product: share_capital_product)
    create(:office_loan_product, loan_product: loan_product, office: office)


    login_as(loan_officer, scope: :user)
    visit new_loans_module_loan_application_voucher_path(loan_application)
    click_link    "Open Share Capital Account"
  end

  it 'with valid attributes' do
    select 'Paid up Share Capital'
    fill_in      'Amount', with: 100

    click_button 'Open Account'

    expect(page).to have_content('added successfully')
  end

  it 'with invalid attributes' do
    click_button 'Open Account'

    expect(page).to have_content("can't be blank")
  end
end
