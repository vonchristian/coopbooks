require 'rails_helper'

describe 'New loan application additional deductions' do
  it 'valid' do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    loan_product          = create(:loan_product, cooperative: loan_officer.cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    share_capital         = create(:share_capital, subscriber: loan_application.borrower, office: office)
    notarial              = create(:liability, name: 'Notarial Fees', office: office)

    create(:office_loan_product, loan_product: loan_product, office: office)
    login_as(loan_officer, scope: :user)

    visit new_loans_module_loan_application_voucher_path(loan_application)
    save_and_open_page
    click_link 'Add Deductions'
    fill_in 'Amount', with: 100
    fill_in 'Description', with: 'Notarial Fee'
    select 'Notarial Fees'
    click_button 'Add Deduction'

    expect(cart.voucher_amounts.pluck(:account_id)).to include(notarial.id)
    expect(cart.voucher_amounts.pluck(:description)).to include('Notarial Fee')

  end
end
