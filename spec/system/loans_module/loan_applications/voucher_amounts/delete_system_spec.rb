require 'rails_helper'

describe 'Edit loan application voucher amount' do
  it 'valid' do
    loan_officer          = create(:loan_officer)
    office                = loan_officer.office
    cooperative           = loan_officer.cooperative
    loan_product          = create(:loan_product, cooperative: cooperative)
    interest_prededuction = create(:interest_prededuction, prededuction_scope: 'on_first_year', loan_product: loan_product, calculation_type: 'percent_based', rate: 1)
    interest_config       = create(:interest_config, loan_product: loan_product, calculation_type: 'prededucted')
    cart                  = create(:cart, employee: loan_officer)
    loan_application      = create(:loan_application, loan_amount: 100_000, cart: cart, office: office, preparer: loan_officer)
    share_capital         = create(:share_capital, subscriber: loan_application.borrower, office: office)
    notarial              = create(:liability, name: 'Notarial Fees', office: office)
    create(:office_loan_product, loan_product: loan_product, office: office)

    cart.voucher_amounts.credit.create!(description: 'Notarial Fee', amount: 100, account: notarial, cooperative: loan_officer.cooperative, recorder: loan_officer, voucher_id: nil)

    login_as(loan_officer, scope: :user)
    visit new_loans_module_loan_application_voucher_path(loan_application)
    click_link 'Cancel Notarial Fee'

    expect(cart.voucher_amounts.find_by(description: 'Notarial Fee')).to_not be_present
  end
end
