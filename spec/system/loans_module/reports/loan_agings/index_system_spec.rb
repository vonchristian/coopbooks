require 'rails_helper'

describe 'Loan agings index' do
  before(:each) do
    loan_officer = create(:loan_officer)
    office       = loan_officer.office
    loan_group   = create(:loan_group, office: office, title: 'Loan Group Test')
    login_as(loan_officer, scope: :user)

    visit loans_module_reports_loan_agings_path
  end
  it 'valid' do
    expect(page).to have_content('Loan Group Test')
  end
end
