require 'rails_helper'

describe 'New loan group' do
  before(:each) do
    loan_officer = create(:loan_officer)
    login_as(loan_officer, scope: :user)
    visit loans_module_settings_path
    click_link 'New Loan Group'
  end

  it 'valid' do
    fill_in 'Title',     with: '1-31 Days'
    fill_in 'Start num', with: 1
    fill_in 'End num',   with: 30

    click_button 'Create Loan Group'

    expect(page).to have_content('created successfully')
  end

  it 'invalid' do
    click_button 'Create Loan Group'

    expect(page).to have_content("can't be blank")
  end
end
