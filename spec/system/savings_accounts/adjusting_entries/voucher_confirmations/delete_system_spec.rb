require 'rails_helper'

describe 'New savings account adjusting entries' do
  it 'is valid' do
    accountant      = create(:accountant)
    teller          = create(:teller, office: accountant.office, cooperative: accountant.cooperative)
    savings_account = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
    loan            = create(:loan, borrower: savings_account.depositor, office: accountant.office)
    voucher         = create(:voucher, office: accountant.office)
    voucher.voucher_amounts.debit.create!(amount: 100, account: savings_account.liability_account, recorder: accountant, cooperative: accountant.cooperative)
    voucher.voucher_amounts.credit.create!(amount: 100, account: loan.receivable_account, recorder: accountant, cooperative: accountant.cooperative)
    login_as(accountant, scope: :user)

    visit savings_account_adjusting_entry_voucher_confirmation_path(id: voucher.id, savings_account_id: savings_account.id)

    click_link 'Cancel'
    expect(accountant.office.vouchers.find_by(id: voucher.id)).to_not be_present
  end
end
