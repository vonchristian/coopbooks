# require 'rails_helper'
#
# describe 'New savings account adjusting entry voucher confirmation' do
#   it 'is valid' do
#     accountant      = create(:accountant)
#
#     savings_account = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
#     loan            = create(:loan, borrower: savings_account.depositor, office: accountant.office, cooperative: accountant.cooperative)
#     voucher         = create(:voucher, preparer: accountant, office: accountant.office, cooperative: accountant.cooperative)
#     voucher.voucher_amounts.debit.create!(amount: 100, account: savings_account.liability_account, recorder: accountant, cooperative: accountant.cooperative)
#     voucher.voucher_amounts.credit.create!(amount: 100, account: loan.receivable_account, recorder: accountant, cooperative: accountant.cooperative)
#
#     login_as(accountant, scope: :user)
#
#     visit savings_account_adjusting_entry_voucher_confirmation_path(id: voucher.id, savings_account_id: savings_account.id)
#     click_button 'Confirm Transaction'
#   end
# end
