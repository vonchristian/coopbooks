require 'rails_helper'

describe 'New savings account adjusting entries' do
  it 'is valid' do
    accountant      = create(:accountant)
    teller          = create(:teller, office: accountant.office, cooperative: accountant.cooperative)
    savings_account = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
    loan            = create(:loan, borrower: savings_account.depositor, office: accountant.office)
    cart = create(:cart, employee: accountant)
    login_as(accountant, scope: :user, cart: cart)
    visit savings_account_url(savings_account)
    click_link "#{savings_account.id}-settings"
    click_link 'Adjusting Entries'

    click_link 'Add Payment'
    fill_in 'Principal amount', with: 500
    fill_in 'Interest amount',  with: 100
    fill_in 'Penalty amount',   with: 100
    click_button 'Add Payment'

    click_link 'Remove Amount', match: :first
    expect(page).to have_content('removed successfully')
  end
end
