require 'rails_helper'

describe 'New savings account share capital transfer' do
  before(:each) do
    accountant      = create(:accountant)
    savings_account = create(:saving, office: accountant.office, cooperative: accountant.cooperative)
    share_capital   = create(:share_capital, office: accountant.office, subscriber: savings_account.depositor)
    login_as(accountant, scope: :user)
    visit savings_account_url(savings_account)
    click_link "#{savings_account.id}-settings"

    click_link 'Adjusting Entries'
    click_link "transfer-amount-#{share_capital.id}"
  end

  it 'with valid attributes' do
    fill_in 'Amount', with: 500

    click_button 'Transfer Amount'

    expect(page).to have_content 'added successfully'
  end

  it 'with invalid attributes' do
    click_button 'Transfer Amount'

    expect(page).to have_content("can't be blank")
  end
end
