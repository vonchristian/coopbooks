require 'rails_helper'

describe 'New savings account adjusting entries' do
  before(:each) do
    accountant      = create(:accountant)
    office          = accountant.office
    savings_account = create(:saving, office: office, cooperative: accountant.cooperative)
    program         = create(:program, amount: 100)
    office_program  = create(:office_program, office: office, program: program)
    program_subscription = create(:program_subscription, subscriber: savings_account.depositor, program: program, office: office)
    login_as(accountant, scope: :user)
    visit savings_account_url(savings_account)
    click_link "#{savings_account.id}-settings"

    click_link 'Adjusting Entries'
    click_link "add-payment-#{program_subscription.id}"
  end

  it 'with valid attributes' do
    fill_in 'Amount', with: 500

    click_button 'Add Payment'

    expect(page).to have_content('added successfully')
  end

  it 'with invalid attributes' do
    click_button 'Add Payment'

    expect(page).to have_content("can't be blank")
  end

end
