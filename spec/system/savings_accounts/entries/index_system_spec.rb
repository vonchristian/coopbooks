require 'rails_helper'

describe 'Savings entries index' do
  before(:each) do
    teller       = create(:teller)
    cash_account = create(:employee_cash_account, employee: teller, cooperative: teller.cooperative, office: teller.office)
    saving       = create(:saving, office: teller.office, cooperative: teller.cooperative)
    entry_1      = build(:entry, reference_number: 'REF100', recorder: teller, office: teller.office, cooperative: teller.cooperative )
    entry_1.debit_amounts.build(account: cash_account.cash_account, amount: 10_000)
    entry_1.credit_amounts.build(account: saving.liability_account, amount: 10_000)
    entry_1.save!

    entry_2 = build(:entry, reference_number: 'REF200', recorder: teller, office: teller.office, cooperative: teller.cooperative )
    entry_2.debit_amounts.build(account: cash_account.cash_account, amount: 10_000)
    entry_2.credit_amounts.build(account: saving.liability_account, amount: 10_000)
    entry_2.save!

    login_as(teller, scope: :user)
    visit savings_account_path(saving)
  end

  it 'valid' do
    expect(page).to have_content('Transactions')
  end

  it 'displays the correct entry when searched' do
    fill_in 'saving-entries-search-form', with: 'REF100'
    click_button 'Search Transaction'

    expect(page).to have_content('REF100')
    expect(page).to_not have_content('REF200')
  end
end
