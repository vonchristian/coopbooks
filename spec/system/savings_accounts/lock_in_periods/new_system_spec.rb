require 'rails_helper'

describe 'New savings account lock-in period' do
  before(:each) do
    employee        = create(:branch_manager)
    savings_account = create(:saving, office: employee.office, cooperative: employee.cooperative)
    login_as(employee, scope: :user)
    visit savings_account_url(savings_account)
    click_link "#{savings_account.id}-settings"
    click_link 'New Lock-in Period'
  end
  it 'with valid attributes' do
    fill_in 'Effectivity date', with: Date.current
    fill_in 'Maturity date', with: Date.current.next_year
    fill_in 'Term', with: 364

    click_button 'Save Lock-in Period'

    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
  end
end
