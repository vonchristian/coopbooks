require 'rails_helper'

feature 'New savings account closing' do
  before(:each) do
    teller           = create(:teller)
    cash_account     = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    saving_product   = create(:saving_product, cooperative: teller.cooperative)
    savings_account  = create(:saving, saving_product: saving_product, cooperative: teller.cooperative, office: teller.office)
    deposit          = build(:entry, cooperative: teller.cooperative, commercial_document: savings_account, recorder: teller, office: teller.office)
    deposit.debit_amounts.build(amount: 50_000, account: cash_account.cash_account)
    deposit.credit_amounts.build(amount: 50_000, account: savings_account.liability_account)
    deposit.save!

    login_as(teller, scope: :user)
    visit savings_account_url(savings_account)
    click_link "#{savings_account.id}-settings"

    click_link 'Close Account'
  end
  scenario 'with valid attributes' do
    fill_in 'Date',             with: Date.current
    fill_in 'Reference number', with: '43244'
    click_button "Close Account"
    expect(page).to have_content 'created successfully'
    click_link 'Confirm Transaction'

    expect(page).to have_content 'closed successfully'

  end
  scenario 'with invalid attributes' do
    click_button 'Close Account'

    expect(page).to have_content("can't be blank")
  end
end
