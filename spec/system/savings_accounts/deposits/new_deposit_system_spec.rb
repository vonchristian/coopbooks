require 'rails_helper'

feature 'New savings account deposit' do
  before(:each) do
    teller           = create(:teller)
    cash_account     = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    saving_product   = create(:saving_product, cooperative: teller.cooperative)
    savings_account  = create(:saving, saving_product: saving_product, cooperative: teller.cooperative, office: teller.office)

    login_as(teller, scope: :user)
    visit savings_account_url(savings_account)
    click_link "Deposit"
  end
  scenario 'with valid attributes' do
    fill_in "Amount",           with: 100_000
    fill_in 'Date',             with: Date.current
    fill_in 'Reference Number', with: '909045'

    click_button "Proceed"
    expect(page).to have_content('created successfully')

    click_link 'Confirm'
    expect(page).to have_content('confirmed successfully')
  end

  scenario 'with invalid attributes' do
    click_button "Proceed"

    expect(page).to have_content("can't be blank")
  end
end
