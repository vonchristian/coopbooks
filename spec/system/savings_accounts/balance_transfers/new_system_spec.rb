require 'rails_helper'

describe 'New savings accounts balance transfer' do
  before(:each) do
    teller      = create(:teller)
    office      = teller.office
    origin      = create(:saving, office: office)
    destination = create(:saving, office: office)
    cash        = create(:asset, office: office)
    #deposit
    entry       = build(:entry, recorder: teller, office: teller.office, cooperative: teller.cooperative)
    entry.debit_amounts.build(account: cash, amount: 1_000)
    entry.credit_amounts.build(account: origin.liability_account, amount: 1_000)
    entry.save!
    login_as(teller, scope: :user)
    visit savings_account_path(origin)
    click_link "#{origin.id}-settings"
    click_link 'Balance Transfer'
    click_link 'Select', match: :first
  end

  it 'with valid attributes' do
    fill_in 'Amount', with: 500
    fill_in 'Date', with: Date.current.strftime('%B %e, %Y')
    fill_in 'Reference number', with: '42432'
    click_button 'Proceed'

    expect(page).to have_content('created successfully.')
    click_link 'Confirm Transaction'

    expect(page).to have_content('confirmed successfully')
  end
end
