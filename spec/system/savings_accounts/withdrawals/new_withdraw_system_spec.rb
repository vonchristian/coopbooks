require 'rails_helper'

feature 'New savings account withdraw' do
  before(:each) do
    teller           = create(:teller)
    cash_account     = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    saving_product   = create(:saving_product, cooperative: teller.cooperative)
    savings_account  = create(:saving, saving_product: saving_product, cooperative: teller.cooperative, office: teller.office)
    deposit          = build(:entry, cooperative: teller.cooperative, commercial_document: savings_account, recorder: teller, office: teller.office)
    deposit.debit_amounts.build(amount: 50_000, account: cash_account.cash_account)
    deposit.credit_amounts.build(amount: 50_000, account: savings_account.liability_account)
    deposit.save!

    login_as(teller, scope: :user)
    visit savings_account_url(savings_account)
    click_link "Withdraw"
  end

  scenario 'with valid attributes' do
    fill_in "Amount",           with: 10_000
    fill_in 'Date',             with: Date.today
    fill_in 'Reference Number', with: '909045'
    click_button "Proceed"
    expect(page).to have_content('created successfully')
    click_link 'Confirm'
    expect(page).to have_content('confirmed successfully')
  end

  # scenario 'exceeded available balance' do
  #   fill_in "Amount",           with: 100_000
  #   fill_in 'Date',             with: Date.today
  #   fill_in 'Reference Number', with: '909045'
  #   click_button "Proceed"
  #   expect(page).to have_content('Exceeded available balance')
  # end

  scenario 'with invalid attributes' do
    click_button "Proceed"

    expect(page).to have_content("can't be blank")
  end
end
