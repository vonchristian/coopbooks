require 'rails_helper'

describe 'New savings account interest expense' do
  before(:each) do
    bookkeeper = create(:bookkeeper)
    office     = bookkeeper.office
    saving     = create(:saving, office: office)
    login_as(bookkeeper, scope: :user)
    visit savings_account_path(saving)
    click_link "#{saving.id}-settings"
    click_link 'New Interest Expense'
  end

  it 'with valid attributes' do
    fill_in 'Date',             with: Date.current
    fill_in 'Reference number', with: '123'
    fill_in 'Description',      with: 'interest expense'
    fill_in 'Interest amount',  with: 11

    click_button 'Proceed'

    expect(page).to have_content('created successfully')

    click_link 'Confirm Transaction'

    expect(page).to have_content('confirmed successfully')
  end

  it 'with invalid attributes' do
    click_button 'Proceed'

    expect(page).to have_content("can't be blank")
  end 
end
