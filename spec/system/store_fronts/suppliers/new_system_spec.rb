require 'rails_helper'

describe 'new supplier' do
  before(:each) do
    office     = create(:office)
    store_front      = create(:store_front, office: office)
    sales_clerk      = create(:sales_clerk, office: office)
    store_front.employees << sales_clerk
    login_as(sales_clerk, scope: :user)
    visit store_front_suppliers_path(store_front)
    click_link 'New Supplier'
  end
  it 'with valid attributes' do
    fill_in 'Business name', with: 'Test Supplier'
    fill_in 'First name',    with: 'Juan'
    fill_in 'Last name',     with: 'Cruz'
    fill_in 'Address',       with: 'Lagawe'
    fill_in 'Contact number', with: '000'
    click_button 'Save Supplier'

    expect(page).to have_content('Test Supplier')
    expect(page).to have_content('saved successfully')
  end

  it 'with invalid attributes' do
    click_button 'Save Supplier'

    expect(page).to have_content("can't be blank")
  end
end
