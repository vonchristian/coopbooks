require 'rails_helper'

describe 'Supplier index page' do
  before(:each) do
    office  = create(:office)
    store_front  = create(:store_front, office: office)
    supplier     = create(:supplier, business_name: 'Test Supplier', store_front: store_front)
    sales_clerk  = create(:sales_clerk, office: office)
    login_as(sales_clerk, scope: :user)
    visit store_front_suppliers_path(store_front)
  end
  it 'valid' do
    expect(page).to have_content('Test Supplier')
  end
end
