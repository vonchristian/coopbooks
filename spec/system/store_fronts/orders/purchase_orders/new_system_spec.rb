require 'rails_helper'

describe 'Supplier new purchase' do
  before(:each) do
    office           = create(:office)
    store_front           = create(:store_front, office: office)
    product               = create(:product, name: 'Test Product', store_front: store_front)
    uom                   = create(:unit_of_measurement, product: product, description: '50g can')
    supplier              = create(:supplier, business_name: 'Test Supplier', store_front: store_front)
    sales_clerk           = create(:sales_clerk, office: office)
    entry                 = create(:entry_with_credit_and_debit)
    voucher               = create(:voucher, reference_number: '101', entry: entry)
    supplier.vouchers     << voucher
    store_front.employees << sales_clerk
    login_as(sales_clerk, scope: :user)
    visit store_front_suppliers_path(store_front)
    click_link 'Test Supplier'
    click_link 'New Purchase'
    click_link 'Select'

  end
  it 'valid' do
    select '50g can'
    fill_in 'Quantity',   with: 1
    fill_in 'Unit cost',  with: 10
    fill_in 'Total cost', with: 10
    fill_in 'Barcode',    with: '32132193'

    click_button 'Add to Cart'

    fill_in 'Date', with: Date.current.strftime('%B %e, %Y')
    select '101'

    click_button 'Save Purchase'

    expect(page).to have_content('saved successfully')
  end
end
