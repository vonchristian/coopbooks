require 'rails_helper'
include ChosenSelect
describe 'New cash sales order' do
  let!(:member)  { create(:member) }

  before(:each) do
    cooperative              = create(:cooperative)
    office                   = create(:office, cooperative: cooperative)
    store_front              = create(:store_front, office: office)
    sales_clerk              = create(:sales_clerk, cooperative: cooperative, office: office)
    employee_store_front_account = create(:employee_store_front_account, employee: sales_clerk, store_front: store_front)
    product                  = create(:product, name: 'Test Product', store_front: store_front)
    uom                      = create(:unit_of_measurement, product: product, base_measurement: true, conversion_quantity: 1)
    mark_up_price            = create(:mark_up_price, unit_of_measurement: uom, price: 200)
    cart                     = create(:cart)
    stock                    = create(:stock, product: product, store_front: store_front, barcode: '11111111', available: true)
    supplier                 = create(:supplier)
    entry                    = create(:entry_with_credit_and_debit, recorder: sales_clerk, office: office, cooperative: cooperative)
    voucher                  = create(:voucher, entry: entry, payee: supplier, reference_number: 'V001')
    purchase_order           = create(:purchase_order, supplier: supplier, voucher: voucher)
    purchase_order_line_item = create(:purchase_line_item, quantity: 10, stock: stock, order: purchase_order, unit_of_measurement: uom)
    cash                     = create(:asset)
    member                   = create(:member, first_name: 'Juan', last_name: 'Cruz', middle_name: 'Dela')
    sales_clerk.employee_cash_accounts.create!(cash_account: cash, office: office, cooperative: cooperative, default_account: true)
    login_as(sales_clerk, scope: :user)
    visit new_store_front_module_sales_line_item_path
  end
  it 'with valid attributes', js: true do
    fill_in 'stock-search-form', with: '11111111'
    click_button 'stock-search-btn'

    fill_in 'line_item_quantity',   with: 1
    fill_in 'line_item_selling_price',  with: 100

    click_button "Add Item"

    expect(page).to have_content('Added to cart')


    fill_in 'sale-order-date', with: Date.current.strftime('%B %e, %Y')
    select_from_chosen 'Cruz, Juan Dela', from: 'Customer'
    fill_in 'Cash tendered',    with: 100
    fill_in 'Discount',         with: 0
    fill_in 'Reference number', with: '120DFG'
    page.execute_script "window.scrollBy(0,10000)"
    click_button 'Complete Order'


    expect(page).to have_content('processed successfully')

  end
  it 'with searched product', js: true do
    fill_in 'stock-search-form', with: 'Test Product'
    click_button 'stock-search-btn'


    fill_in 'line_item_quantity',   with: 1
    fill_in 'line_item_selling_price',  with: 100

    click_button "Add Item"

    expect(page).to have_content('Added to cart')

    fill_in 'sale-order-date', with: Date.current.strftime('%B %e, %Y')
    select_from_chosen 'Cruz, Juan Dela', from: 'Customer'
    fill_in 'Cash tendered',    with: 100
    fill_in 'Discount',         with: 0
    fill_in 'Reference number', with: '120DFG'
page.execute_script "window.scrollBy(0,10000)"

    click_button 'Complete Order'


    expect(page).to have_content('processed successfully')

  end
end
