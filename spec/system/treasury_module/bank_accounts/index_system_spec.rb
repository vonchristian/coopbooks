require 'rails_helper'

describe 'Bank accounts index' do
  before(:each) do
    teller       = create(:teller)
    bank_account = create(:bank_account, office: teller.office, bank_name: 'Land Bank')
    login_as(teller, scope: :user)
    visit treasury_module_bank_accounts_path
  end

  it 'valid' do
    expect(page).to have_content('Land Bank')
  end
end
