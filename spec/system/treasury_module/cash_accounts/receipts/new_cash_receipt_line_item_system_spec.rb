require 'rails_helper'

feature 'New cash receipt' do
  before(:each) do
    teller = create(:teller)
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    revenue      = create(:revenue, name: "Sales", office: teller.office)

    member       = create(:member, last_name: 'Cruz', first_name: 'Juan', middle_name: 'Mark')
    membership   = create(:regular_membership, cooperative: teller.cooperative, cooperator: member)
    cart = create(:cart, employee: teller)
    login_as(teller, scope: :user)
    visit treasury_module_cash_account_url(cash_account.cash_account)
    click_link "#{cash_account.cash_account_id}-receive"

  end
  scenario 'with valid attributes' do
    expect(page).to_not have_content("Sales Main Account")
    select  "Sales"
    fill_in "Amount", with: 100_000
    click_button "Add"

    expect(page).to have_content('Added successfully')

    fill_in 'Date',             with: Date.current
    fill_in 'Description',      with: 'Sales'
    fill_in 'Reference number', with: '424242'
    select 'Cruz, Juan'

    click_button 'Proceed'
    click_link 'Confirm Transaction'

    expect(page).to have_content('saved successfully')


  end
end
