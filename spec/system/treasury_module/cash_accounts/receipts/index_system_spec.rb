require 'rails_helper'

describe 'Cash account receipts index' do
  before(:each) do
    teller       = create(:teller)
    revenue      = create(:revenue, office: teller.office)
    cash         = create(:asset, office: teller.office, name: 'Cash on Hand')
    cash_account = create(:employee_cash_account, employee: teller, office: teller.office, cooperative: teller.cooperative)
    entry        = build(:entry, reference_number: 'REF1', recorder: teller, office: teller.office, cooperative: teller.cooperative)
    entry.debit_amounts.build(amount: 400, account: cash_account.cash_account)
    entry.credit_amounts.build(amount: 400, account: revenue )
    entry.save!

    entry_2        = build(:entry, reference_number: 'REF2', recorder: teller, office: teller.office, cooperative: teller.cooperative)
    entry_2.debit_amounts.build(amount:  400, account: cash_account.cash_account)
    entry_2.credit_amounts.build(amount: 400, account: revenue )
    entry_2.save!
    login_as(teller, scope: :user)
    visit treasury_module_cash_account_path(cash_account.cash_account)
    click_link "#{cash_account.cash_account_id}-cash-receipts"
  end
  it 'valid' do
    expect(page).to have_content('REF1')
    expect(page).to have_content('REF2')
  end

  it 'when search is present' do

    fill_in 'cash-account-receipts-search-form', with: 'REF1'
    click_button 'Search Receipts'

    expect(page).to have_content('REF1')
    expect(page).to_not have_content('REF2')
  end
end
