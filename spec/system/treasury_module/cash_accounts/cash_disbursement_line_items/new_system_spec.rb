require 'rails_helper'

feature 'New cash disbursement' do
  before(:each) do
    teller       = create(:teller)
    revenue      = create(:revenue, office: teller.office)
    cash         = create(:asset, office: teller.office)
    cash_account = create(:employee_cash_account, employee: teller, cash_account: cash, office: teller.office, cooperative: teller.cooperative)
    entry        = build(:entry, office: teller.office, cooperative: teller.cooperative, recorder: teller)
    entry.debit_amounts.build(amount: 50_000, account: cash_account.cash_account)
    entry.credit_amounts.build(amount: 50_000, account: revenue)
    entry.save!
    expense      = create(:revenue, name: "Transportation", office: teller.office)
    expense_3    = create(:revenue, name: "Meals", office: teller.office)

    member       = create(:member, last_name: 'Cruz', first_name: 'Juan', middle_name: 'Mark')
    membership   = create(:regular_membership, cooperative: teller.cooperative, cooperator: member)


    cart = create(:cart, employee: teller)
    login_as(teller, scope: :user)
    visit treasury_module_cash_account_url(cash_account.cash_account)
    click_link "#{cash_account.cash_account_id}-disburse"
  end
  scenario 'with valid attributes' do
    expect(page).to_not have_content("Transportation Main Account")
    select  "Transportation"
    fill_in "Amount", with: 1000
    click_button "Add"

    expect(page).to have_content('Added successfully')
    select  "Meals"
    fill_in "Amount", with: 500
    click_button "Add"


    fill_in 'Date',             with: Date.current
    fill_in 'Description',      with: 'Sales'
    fill_in 'Reference number', with: '424242'
    select 'Cruz, Juan'

    click_button 'Proceed'
    click_link 'Confirm Transaction'

    expect(page).to have_content('saved successfully')


  end
end
