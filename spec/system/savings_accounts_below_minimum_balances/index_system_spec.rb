require 'rails_helper'

describe 'Savings accounts below minimum balances index page' do
  it 'valid' do
    teller   = create(:teller)
    office   = teller.office
    saving   = create(:saving, office: office, has_minimum_balance: true)
    saving_2 = create(:saving, office: office, has_minimum_balance: false)
    login_as(teller, scope: :user)
    visit savings_accounts_below_minimum_balances_path

    expect(page).to have_content(saving_2.depositor_name)
    expect(page).to_not have_content(saving.depositor_name)
  end
end
